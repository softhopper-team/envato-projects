<?php
/**
 * Template Name: About Me
 */
?>
<?php 
    get_header();
    global $glimmer;
?>
<!-- Content
================================================== -->
<div id="content" class="site-content about-me">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-title">
                <h2 class="section-title"><?php the_title(); ?></h2>
                    <div class="ex-small-border"></div>    
                </div> <!-- /.header-title --> 
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->

        <div class="row">
            <div class="col-md-4">
                <!-- main section -->
                <div id="main">
                    
                    <article class="post">

                        <h2 class="author-name">
                        <?php 
                            if ( isset ( $glimmer['author_name'] ) ) echo $glimmer['author_name'];
                        ?>
                        </h2> <!-- /.author-name -->

                        <div class="entry-content">
                            <?php 
                                if ( isset ( $glimmer['author_description'] ) ) echo $glimmer['author_description'];
                            ?>                                
                        </div> <!-- /.entry-content --> 
                        <footer class="entry-footer">
                               <div class="author-sign">
                                    <img src="<?php if ( isset ( $glimmer['author_sign']['url'] ) ) echo $glimmer['author_sign']['url']; ?>" alt="Sign" >
                                    <h3>
                                    <?php 
                                        if ( isset ( $glimmer['author_name'] ) ) echo $glimmer['author_name'];
                                    ?>
                                    </h3>
                               </div>
                               <div class="follow-link">
                                     <span><?php _e('Follow me :', 'glimmer' ); ?></span>
                                    <?php
                                        // show social link 
                                        if( isset ( $glimmer['author_social_link'] )  && ! empty ( $glimmer['author_social_link']) ) {
                                            $social_link = $glimmer['author_social_link'];                      
                                            foreach ( $social_link as $key => $value ) {
                                            if ( $value['title'] ) {
                                        ?>
                                        <a href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                                        <?php
                                                }
                                            }
                                        }
                                    ?>       
                               </div> <!-- /.follow-link -->
                            </footer> <!-- /.entry-footer -->
                    </article> <!-- /.post -->
                </div> <!-- /#main -->
            </div> <!-- /.col-md-8 -->

            <div class="col-md-8">
                <figure class="author-image">
                    <img class="img-responsive" src="<?php if ( isset ( $glimmer['author_img']['url'] ) ) echo $glimmer['author_img']['url']; ?>" alt="Author Img" >
                </figure> <!-- /.author-image -->                                    
            </div> <!-- .col-md-4 -->
        </div> <!-- /.row -->

        <div id="author-skill" class="clearfix">
            <h3><?php _e('Skills', 'glimmer' ); ?></h3>
            <div class="small-border"></div>
            <div class="row">
                <?php
                    // show author skill
                    if( isset ( $glimmer['author_skills'] )  && ! empty ( $glimmer['author_skills']) ) {
                        $social_link = $glimmer['author_skills'];                      
                        foreach ( $social_link as $key => $value ) {
                        if ( $value['title'] ) {
                    ?>
                    <div class="col-sm-4 pd-bottom">
                        <div class="skill-area">
                            <div class="percent-area">
                                <div class="skillbar" data-percent="<?php echo $value['url']; ?>">
                                    <h5 class="skillbar-title"><?php echo $value['title'];?></h5>
                                    <div class="skillbar-bg">
                                        <div class="skillbar-bar"></div>    
                                    </div>
                                    <div class="skill-bar-percent"></div>
                                </div> <!-- /.skillbar --> 
                            </div> <!-- /.percent-area -->
                        </div> <!-- /.skill-area --> 
                    </div> <!-- /.col-sm-6 -->
                    <?php
                            } // end if
                        } //end foreach
                    } // end if author_skills
                ?>
            </div> <!-- /.row --> 
        </div> <!-- /#author-skill -->
    </div> <!-- /.container -->    
</div><!-- #content -->
<?php get_footer(); ?>