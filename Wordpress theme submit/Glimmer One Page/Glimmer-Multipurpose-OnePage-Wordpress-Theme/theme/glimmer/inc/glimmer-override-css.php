<?php
// adding some css code
function glimmer_override_css() {
global $glimmer;
?>
    
<?php 
    if( get_current_template() == "landing-page.php" ) {
        if ( is_admin_bar_showing() ) {
        ?>
        <style type="text/css">
             @media (min-width:783px) {
                .navbar-default.stuck {
                    top: 32px;
                }
            }

            @media (min-width:601px) and (max-width:782px) {
                .navbar-default.stuck { 
                    top: 46px;
                }
                
            }
            @media (max-width:600px) {
                html {
                    margin-top: 0 !important;
                }      
            }
        </style>
        <?php
        }
    }
    else {
        if ( is_admin_bar_showing() ) {
        ?>
        <style type="text/css">
             @media (min-width:783px) {
                .navbar-default {
                  top: 32px;
                }
                .navbar-default.stuck {
                    top: 32px;
                }
                .blog-header {
                  margin-top: 84px;
                }
                .blog-header.stuck {
                  margin-top: 12px;
                }    
            }

            @media (min-width:601px) and (max-width:782px) {
                .navbar-default.stuck { 
                    top: 46px;
                }
                .navbar-default {
                    top: 46px;
                }
                .blog-header.stuck {
                    margin-top: 14px;
                }
            }
            @media (max-width:600px) {
                .navbar-default.stuck { 
                    top: 0;
                }
                .navbar-default {
                    top: 46px;
                }
                .blog-header {
                    margin-top: -10px !important;
                }
                .blog-header.stuck {
                    margin-top: -50px !important;
                }           
            }
        </style>
        <?php
    }
}  
?>
<?php 
    if ($glimmer['section_header_display'] == false) {
    ?>
    <style type="text/css">
        .sticky-wrapper { 
            height: 56px !important;
        }
    </style>
    <?php
    }
?>
<style type="text/css"> 
.blog-header {
    background: url(<?php header_image(); ?>) center center fixed;             
}
.navbar-brand {
    background: url(<?php echo $glimmer['section_header_nav_logo']['url']; ?>) no-repeat 50% 50%;
}  
.team {
    background: <?php if (isset($glimmer['section_team_bg_color'])) echo $glimmer['section_team_bg_color']; ?> url(<?php if (isset($glimmer['section_team_bg']['url'])) echo $glimmer['section_team_bg']['url']; ?>) center center fixed;   
}
.fun-fact {
	background: <?php if (isset($glimmer['section_funfact_bg_color'])) echo $glimmer['section_funfact_bg_color']; ?> url(<?php if (isset($glimmer['section_funfact_bg']['url'])) echo $glimmer['section_funfact_bg']['url']; ?>) center center fixed; 
}
.testimonial {
	background: <?php if (isset($glimmer['section_testimonial_bg_color'])) echo $glimmer['section_testimonial_bg_color']; ?> url(<?php if (isset($glimmer['section_testimonial_bg']['url'])) echo $glimmer['section_testimonial_bg']['url']; ?>) center center fixed; 
}
.our-clients {
	background: <?php if (isset($glimmer['section_client_bg_color'])) echo $glimmer['section_client_bg_color']; ?> url(<?php if (isset($glimmer['section_client_bg']['url'])) echo $glimmer['section_client_bg']['url']; ?>) center center fixed; 
}

</style> 						
<?php
}
add_action('wp_head', 'glimmer_override_css',200);

