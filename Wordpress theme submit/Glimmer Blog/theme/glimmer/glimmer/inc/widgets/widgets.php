<?php
/**
 * Load and register widgets
 *
 * @package Glimmer
 */

require_once THEME_DIR . '/inc/widgets/popular-posts.php';
require_once THEME_DIR . '/inc/widgets/latest-posts.php';
require_once THEME_DIR . '/inc/widgets/about-me.php';
require_once THEME_DIR . '/inc/widgets/follow-me.php';
require_once THEME_DIR . '/inc/widgets/advertisement.php';
require_once THEME_DIR . '/inc/widgets/aboutus-contact.php';

/**
 * Register widgets
 *
 */

add_action('widgets_init','glimmer_register_widgets');
function glimmer_register_widgets() {
	register_widget('Glimmer_Popular_Posts');
	register_widget('Glimmer_Latest_Posts');
	register_widget('Glimmer_About_Me');
	register_widget('Glimmer_Follow_Me');
	register_widget('Glimmer_Advertisement');
	register_widget('Glimmer_Aboutus_Contact');
}