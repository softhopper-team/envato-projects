<?php
/**
 * Candid functions and definitions
 *
 * @package Candid
 */

/**
 * Define theme's constant
 */
if ( ! defined( 'THEME_VERSION' ) ) {
	define( 'THEME_VERSION', '1.0' );
}
if ( ! defined( 'THEME_DIR' ) ) {
	define( 'THEME_DIR', get_template_directory() );
}
if ( ! defined( 'THEME_URL' ) ) {
	define( 'THEME_URL', get_template_directory_uri() );
}

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 1170; /* pixels */
}

/**
 * Inlcude the Redux theme options framework
 */

//Inlcude the Redux theme options framework extension
if ( !class_exists( 'candid_redux_register_custom_extension_loader' ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-extensions-loader/loader.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-extensions-loader/loader.php' );
}

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-framework-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-framework-config.php' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'candid_setup' ) ) :

function candid_setup() {
	global $candid;
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Candid, use a find and replace
	 * to change 'candid' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'candid', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main-menu' => esc_html__( 'Top Menu', 'candid' ),
		'footer-menu' => esc_html__( 'Footer Menu', 'candid' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) 
	);
	/* Define image size */
	add_image_size( 'fearured-img', 555, 350, true );
	add_image_size( 'single-full', 1170, 550, true );
	add_image_size( 'popular-post', 90, 80, true );
	add_image_size( 'latest-post', 90, 80, true );
	add_image_size( 'related-posts', 370, 200, true );
	
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'gallery', 'audio', 'video', 'quote', 'link', 'chat', 'status'
		) 
	);

	//background with box and wide layout
	$bg_color = '';
	if (!isset($candid['layout'])) {
		$candid['layout'] = 1;
	}
    ( $candid['layout'] == 1 ) ? $bg_color = 'f2f2f2' : $bg_color = 'fffff';
        
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'candid_custom_background_args', array(
		'default-color' => $bg_color,
		'default-image' => '',
	) ) );

	// Create custom category when theme setup
	if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
        require_once (ABSPATH.'/wp-admin/includes/taxonomy.php'); 
        if ( ! get_cat_ID( 'Featured' ) ) {
            wp_create_category( 'Featured' );
        }
    }
}
endif; // candid_setup
add_action( 'after_setup_theme', 'candid_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function candid_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'candid_content_width', 640 );
}
add_action( 'after_setup_theme', 'candid_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function candid_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'candid' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h2 class="widget-title">',
		'after_title'   => '</h2></div>',
	) );
	// Register footer sidebar
	register_sidebars( 4, array(
		'name'          => __( 'Footer %d', 'candid' ),
		'id'            => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h2 class="widget-title">',
		'after_title'   => '</h2></div>',
	) );
}
add_action( 'widgets_init', 'candid_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function candid_scripts() {
	global $wp_scripts, $candid;
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style('google-roboto-font', "$protocol://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic,900,900italic,100,100italic");
	wp_enqueue_style('font-awesome', THEME_URL . "/css/font-awesome.min.css");
	wp_enqueue_style('candid-icon', THEME_URL . "/css/candid-icon.css");
	wp_enqueue_style('bootstrap', THEME_URL . "/css/bootstrap.min.css");
	wp_enqueue_style('owl-carousel', THEME_URL . "/css/owl.carousel.css");
	wp_enqueue_style('jquery-fancybox', THEME_URL . "/css/jquery.fancybox.css");
	wp_enqueue_style('owl-theme', THEME_URL . "/css/owl.theme.css");
	wp_enqueue_style( 'candid-style', get_stylesheet_uri() );

	// Color Schemer
	switch( $candid['candid_color_scheme'] ) {
        case 1: //80b92e
            $candid_color_scheme = "#80b92e";
            break;
        case 2: //f26d7e
            $candid_color_scheme = "#f26d7e";
            break;
        case 3: //044f67
            $candid_color_scheme = "#044f67";
            break;
        case 4: //006442
            $candid_color_scheme = "#006442";
            break;
        case 5: //1691bd
            $candid_color_scheme = "#1691bd";
            break;
        case 6: //067b82
            $candid_color_scheme = "#067b82";
            break;
        case 7: //34495e
            $candid_color_scheme = "#34495e";
            break;
        case 8: //d2527f
            $candid_color_scheme = "#d2527f";
            break;
        case 9: //turquoise
            $candid_color_scheme = $candid['candid_custom_color'];
            break;
        default:
            $candid_color_scheme = "#373e48";
            break;
    }
	// Support rtl language
	if (  $candid['rtl_support'] == true ) {
        wp_enqueue_style('rtl', THEME_URL . "/rtl.css");
    }

	wp_enqueue_script('respond-js', THEME_URL . '/js/respond.min.js', array(), '1.4.2', false);
	// Load the Internet Explorer 9 specific js.
	$wp_scripts->add_data( 'respond-js', 'conditional', 'lt IE 9' );	
	wp_enqueue_script('modernizr', THEME_URL . '/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', false);
	wp_enqueue_script('bootstrap-js', THEME_URL . '/js/bootstrap.min.js', array("jquery"), '3.3.4', true);
	wp_enqueue_script('fitvids-js', THEME_URL . '/js/jquery.fitvids.js', array("jquery"), false, true);
	wp_enqueue_script('smoothscroll-js', THEME_URL . '/js/SmoothScroll.js', array("jquery"), false, true);
	wp_enqueue_script('owl-carousel-js', THEME_URL . '/js/owl.carousel.min.js', array("jquery"), false, true);
	wp_enqueue_script('jquery-fancybox-js', THEME_URL . '/js/jquery.fancybox.js', array("jquery"), false, true);
	//preloader js
	if (  $candid['preloader'] == true ) {
		wp_enqueue_script('queryloader2-js', THEME_URL . '/js/queryloader2.min.js', array("jquery"), false, true);
	}
	wp_enqueue_script('appear-js', THEME_URL . '/js/jquery.appear.js', array("jquery"), false, true);
	wp_enqueue_script('googleapis-js', "$protocol://maps.googleapis.com/maps/api/js?sensor=true", array("jquery"), false, true);
	wp_enqueue_script('gmaps-js', THEME_URL . '/js/gmaps.js', array("jquery"), false, true);
	wp_enqueue_script('plugins', THEME_URL . '/js/plugins.js', array("jquery"), false, true);
	wp_enqueue_script('candid-js', THEME_URL . '/js/candid.js', array("jquery"), false, true);
	wp_enqueue_script( 'candid-navigation', THEME_URL . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'candid-skip-link-focus-fix', THEME_URL . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( isset( $candid['contact_lat'] ) && isset( $candid['contact_lon'] ) ) {
		( $candid['sidebar_layout'] == 1 ) ? $owl_item = 7 : $owl_item = 5;
        wp_localize_script("candid-js", "candid", array (
	        	"lat" => $candid['contact_lat'],
	        	"lon" => $candid['contact_lon'],
	        	"map_mouse_wheel" => $candid['map_mouse_wheel'],
	        	"map_zoom_control" => $candid['map_zoom_control'],
	        	"map_point_img" => $candid['contact_map_point_img'],
	        	"preloader_bar_color" => $candid_color_scheme,
	        	"owl_item" => $owl_item,
	        	"theme_uri" => THEME_URL 
        	)
        );
    }

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'candid_scripts' );

/**
 * Enqueue scripts and styles for WordPress admin panel.
 */
function candid_admin_panel_scripts() {
	// enqueue style
	wp_enqueue_style('thickbox');
	wp_enqueue_style('wp-color-picker');
	wp_enqueue_style('admin-font-awesome', THEME_URL . "/css/font-awesome.min.css");
	wp_enqueue_style('admin-custom', THEME_URL . "/css/backend/custom.css");
	wp_enqueue_style('redux-custom', THEME_URL . "/css/backend/redux-custom.css");	

	// enqueue scripts
	wp_enqueue_script('media-upload');
  	wp_enqueue_script('thickbox');
    wp_enqueue_script('wp-color-picker');
	wp_enqueue_script('candid-backend-js', THEME_URL . '/js/backend/admin.js', array("jquery","media-upload","thickbox","wp-color-picker"), false, true);
}
add_action( 'admin_enqueue_scripts', 'candid_admin_panel_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require THEME_DIR . '/inc/functions/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require THEME_DIR . '/inc/functions/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require THEME_DIR . '/inc/functions/extras.php';

/**
 * Customizer additions.
 */
require THEME_DIR . '/inc/functions/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require THEME_DIR . '/inc/functions/jetpack.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require THEME_DIR . '/inc/libs/tgm-plugin-activation/tgm-admin-config.php';

/**
 * Configure CMB2 Meta Box
 */
require THEME_DIR . '/inc/libs/cmb2/cmb2-config.php';

/**
 * Load custom widget
 */
require THEME_DIR . '/inc/widgets/widgets.php';

/**
 * Register Custom Navigation Walker
 */
require THEME_DIR . '/inc/functions/candid-nav-walker.php';

/**
 * Wordpress comment seciton override 
 */
require THEME_DIR . '/inc/functions/wp-comment-section-override.php';

/**
 * Query function to get post
 */
require THEME_DIR . '/inc/functions/query-function-for-post.php';

/**
 * Popular Post functions
 */
require THEME_DIR . '/inc/functions/popular-post.php';

/**
 * Include header, Hooks for template header
 */
require THEME_DIR . '/inc/frontend/header.php';

/**
 * Include header, Hooks for template header
 */
require THEME_DIR . '/inc/frontend/footer.php';

// change read more link of wordpress excerpt
add_action( 'the_content_more_link', 'candid_add_morelink_class', 10, 2 );
function candid_add_morelink_class( $link, $text )
{
    return str_replace(
        'more-link',
        'more-link button',
        $link
    );
}

// remove parentheses from category list and add span class to count
add_filter('wp_list_categories','candid_categories_postcount_filter');
function candid_categories_postcount_filter ($args) {
	$args = str_replace('(', '<span class="count"> ', $args);
	$args = str_replace(')', ' </span>', $args);
   return $args;
}

// remove parentheses from archive list and add span class to count
add_filter('get_archives_link', 'candid_archive_count_no_brackets');
	function candid_archive_count_no_brackets($links) {
	$links = str_replace('</a>&nbsp;(', '</a> <span class="count">', $links);
	$links = str_replace(')', ' </span>', $links);
	return $links;
}

/* remove redux menu under the tools */
add_action( 'admin_menu', 'candid_remove_redux_menu', 12 );
function candid_remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}

// candid rewrite excerpt function
function candid_wp_new_excerpt($text)
{
	if ($text == '')
	{
		$text = get_the_content('');
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$text = strip_tags($text);
		$text = nl2br($text);
		$excerpt_length = apply_filters('excerpt_length', 55);
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			array_push($words, '...');
			$text = implode(' ', $words);
		}
	}
	return $text;
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'candid_wp_new_excerpt');

