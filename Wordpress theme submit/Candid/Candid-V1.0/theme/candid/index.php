<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Candid
 */
?>
<?php get_header(); ?>
<?php get_template_part( 'template-parts/content', 'featured' ); ?>    
<!-- Content
================================================== -->
<?php
    /* Show box or wide layout with user condition*/
    $layout = '';
    if ( $candid['layout'] == 1 ) {
        $layout = 'box';
    } 
?>
<main id="content" class="<?php echo $layout; ?>">
    <div class="container">
        <div class="row">
            <?php
                /* Show sidebar with user condition*/
                $columns_grid = 8;
                if ( $candid['sidebar_layout'] == 2 ) {
                    get_sidebar();
                } elseif ( $candid['sidebar_layout'] == 1 ) {
                    $columns_grid = 12;
                }
            ?>
            <div class="col-md-<?php echo $columns_grid; ?>">
                <!-- main section -->
                <div id="main">
                    <?php 
                    $cat_slug = get_category_by_slug('featured'); 
                    $cat_id = $cat_slug->term_id;
                    $query = new WP_Query ( 
                    array ( 
                            'category__not_in' => $cat_id,
                        )
                    );

                    if ( $query->have_posts() ) : ?>

                    <?php /* Start the Loop */ ?>
                    <?php while ( $query->have_posts() ) : $query->the_post(); ?>

                        <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', get_post_format() );
                        ?>

                    <?php endwhile; ?>

                    <?php candid_posts_pagination_nav(); ?>

                    <?php else : ?>

                        <?php get_template_part( 'template-parts/content', 'none' ); ?>

                    <?php endif; ?>

                </div> <!-- /#main -->

            </div> <!-- /.col-md-8 -->

            <?php
                /* Show sidebar with user condition*/
                if ( $candid['sidebar_layout'] == 3 ) {
                    get_sidebar();
                }
            ?>
             
        </div> <!-- /.row -->   
    </div> <!-- /.container -->  
</main> <!-- /#content -->
<?php get_footer(); ?>