<?php

function candid_color_scheme() { 
  global $candid;
    switch( $candid['candid_color_scheme'] ) {
        case 1: //80b92e
            $candid_color_scheme = "#80b92e";
            break;
        case 2: //f26d7e
            $candid_color_scheme = "#f26d7e";
            break;
        case 3: //044f67
            $candid_color_scheme = "#044f67";
            break;
        case 4: //006442
            $candid_color_scheme = "#006442";
            break;
        case 5: //1691bd
            $candid_color_scheme = "#1691bd";
            break;
        case 6: //067b82
            $candid_color_scheme = "#067b82";
            break;
        case 7: //34495e
            $candid_color_scheme = "#34495e";
            break;
        case 8: //d2527f
            $candid_color_scheme = "#d2527f";
            break;
        case 9: //turquoise
            $candid_color_scheme = $candid['candid_custom_color'];
            break;
        default:
            $candid_color_scheme = "#373e48";
            break;
    }
    $candid_custom_hover_color = $candid['candid_custom_hover_color'] 
?>
<style type="text/css"> 
    /* all color is on here */ 
::-moz-selection {
  background-color: <?php echo $candid_color_scheme;?>;
}
::selection {
  background-color: <?php echo $candid_color_scheme;?>;
}

blockquote {
  color: <?php echo $candid_color_scheme;?>;
}

.small-border {
  background: <?php echo $candid_color_scheme;?>;
}

button,
html input[type="button"],
input[type="reset"],
input[type="submit"] {
  background: <?php echo $candid_color_scheme;?>;
}
.button {
  border: 1px solid <?php echo $candid_color_scheme;?>;
}

.more-link:after {
  background: <?php echo $candid_color_scheme;?>;
}

.comment-reply-link {
  background: <?php echo $candid_color_scheme;?>;
}

.go-to {
  background: <?php echo $candid_color_scheme;?>;
}
#header .current-menu-item > a,
#header .current-page-item > a,
#header .current-menu-parent > a
{
  color: <?php echo $candid_color_scheme;?> !important;
}

.navigation .nav-links > li a:hover, .navigation .nav-links > li span:hover {
  border-color: <?php echo $candid_color_scheme;?>;
}

.navigation .nav-links > li.active a, .navigation .nav-links > li.active span {
  background: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.navigation .nav-links li:first-child.nav-previous:hover span {
  background: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.navigation .nav-links li:first-child.nav-previous:hover span:last-child {
  color: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.navigation .nav-links li:last-child.nav-next:hover span {
  background: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.navigation .nav-links li:last-child.nav-next:hover span:first-child {
  color: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

input[type="text"]:focus,
input[type="email"]:focus,
input[type="url"]:focus,
input[type="password"]:focus,
input[type="search"]:focus,
textarea:focus {
  color: <?php echo $candid_color_scheme;?>;
}

#top-search-box .top-search-inner .search-title h3 {

  color: <?php echo $candid_color_scheme;?>;
}

#top-search-box #searchform button[type="submit"] {
  color: <?php echo $candid_color_scheme;?>;
}

#top-search-box .top-search-inner .close-search:hover {
  background: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.search-form .searchform #submit-search {
  color: <?php echo $candid_color_scheme;?>;
}

a:hover, a:focus, a:active {
  color: <?php echo $candid_color_scheme;?>;
}

.navbar-default {
  background: <?php echo $candid_color_scheme;?>;
}

.navbar-default .navbar-nav > li > a:hover, .navbar-default .navbar-nav > li > a:active {
  color: <?php echo $candid_color_scheme;?>;
}

.navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:focus, .navbar-default .navbar-nav > .open > a:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.dropdown-menu > li > a:hover, .dropdown-menu > li > a:focus {
  color: <?php echo $candid_color_scheme;?>;
}

.nav-collapse {
  background-color: <?php echo $candid_color_scheme;?>;
}

.header-search:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.entry-header .entry-meta a:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.entry-header .entry-meta .byline .author a, .entry-header .entry-meta .cat-links a, .entry-header .entry-meta .comments-link a {
  color: <?php echo $candid_color_scheme;?>;
}

.featured-area .post-title span {
  color: <?php echo $candid_color_scheme;?>;
}

.featured-area .carousel-indicators li {
  border: 1px solid <?php echo $candid_color_scheme;?>;
}

.featured-area .carousel-indicators .active {
  background-color: <?php echo $candid_color_scheme;?>;
}

.section-title {
  color: <?php echo $candid_color_scheme;?>;
}

#related-post-slide .owl-controls .owl-buttons div {
  background: <?php echo $candid_color_scheme;?>;
}

.author-info .author-name {
  color: <?php echo $candid_color_scheme;?>;
}

.author-info #author-footer .authors-social a {
  color: <?php echo $candid_color_scheme;?>;
}

.author-page #main .author-name {
  color: <?php echo $candid_color_scheme;?>;
}

.author-page #author-skill h3 {
  color: <?php echo $candid_color_scheme;?>;
}

.author-page #author-skill .skillbar-bar {
  background: <?php echo $candid_color_scheme;?>;
}

.author-page #author-skill .skill-bar-percent {
  color: <?php echo $candid_color_scheme;?>;
}

.author-page #author-skill .skill-bar-percent:after {
  border-top: 12px solid <?php echo $candid_color_scheme;?>;
}

.error-page .error-description h2 {
  color: <?php echo $candid_color_scheme;?>;
}

.footer-bottom {
  background: <?php echo $candid_color_scheme;?>;
}

.topbutton {
  background: <?php echo $candid_color_scheme;?>;
}

.about-widget .author-name {
  color: <?php echo $candid_color_scheme;?>;
}

.about-widget .more-link {
  color: <?php echo $candid_color_scheme;?>;
}

.latest-widget .latest-wrap .latest-item .latest-item-meta a {
  color: <?php echo $candid_color_scheme;?>;
}

.widget_categories li:hover > a, .widget_archive li:hover > a {
  color: <?php echo $candid_color_scheme;?>;
}

.widget_categories li:hover > .count, .widget_archive li:hover > .count {
  background: <?php echo $candid_color_scheme;?>;
}

.widget_categories li.current-cat > a, .widget_categories li.current-cat-parent > a, .widget_archive li.current-cat > a, .widget_archive li.current-cat-parent > a {
  color: <?php echo $candid_color_scheme;?>;
}
.widget_categories li.current-cat > .count, .widget_categories li.current-cat-parent > .count, .widget_archive li.current-cat > .count, .widget_archive li.current-cat-parent > .count {
  background: <?php echo $candid_color_scheme;?>;
}

.widget_tag_cloud .tagcloud a:hover {
  background: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.popular-widget .popular-wrap .popular-item .popular-item-meta a {
  color: <?php echo $candid_color_scheme;?>;
}

.widget_calendar tbody a {
  background-color: <?php echo $candid_color_scheme;?>;
}

.widget_links a:hover,
.widget_meta a:hover,
.widget_nav_menu a:hover,
.widget_pages a:hover,
.widget_recent_comments a:hover,
.widget_recent_entries a:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.follow-us-area .follow-link .fa:hover {
  background-color: <?php echo $candid_color_scheme;?>;
  border-color: <?php echo $candid_color_scheme;?>;
}

.tp_recent_tweets li span a {
  color: <?php echo $candid_color_scheme;?>;
}
.tp_recent_tweets li .twitter_time:before {
  color: <?php echo $candid_color_scheme;?>;
}
.tp_recent_tweets li .twitter_time {
  color: <?php echo $candid_color_scheme;?>;
}
.single-image.link-icon .fa:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.single-image.plus-icon .fa:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.sticky:before {
  border-left: 20px solid <?php echo $candid_color_scheme;?>;
  border-right: 20px solid <?php echo $candid_color_scheme;?>;
}

.page-content a,
.entry-content a,
.entry-summary a {
  color: <?php echo $candid_color_scheme;?>;
}

.box .format-quote .entry-content span {
  color: <?php echo $candid_color_scheme;?>;
}

.entry-footer .cat-links > a:hover {
  color: <?php echo $candid_color_scheme;?>;
}

.format-link {
  background: <?php echo $candid_color_scheme;?>;
}

.format-quote .entry-content {
  color: <?php echo $candid_color_scheme;?>;
}

.format-quote .entry-content span {
  color: <?php echo $candid_color_scheme;?>;
}

.format-quote .entry-content span:before {
  color: <?php echo $candid_color_scheme;?>;
}

.format-aside .entry-content {
  color: <?php echo $candid_color_scheme;?>;
}

.format-aside .entry-content p {
  color: <?php echo $candid_color_scheme;?>;
}

.comment-reply-title {
  color: <?php echo $candid_color_scheme;?>;
}

.comments-area .comments-title {
  color: <?php echo $candid_color_scheme;?>;
}

#post-gallary-one .carousel-control .icon-prev,
#post-gallary-one .carousel-control .icon-next,
#post-gallary-one .carousel-control .glyphicon-menu-left,
#post-gallary-one .carousel-control .glyphicon-menu-right {
  background: <?php echo $candid_color_scheme;?>;
}

.full-view .owl-controls .owl-buttons div {
  background: <?php echo $candid_color_scheme;?>;
}

.list-view .owl-controls .owl-buttons div {
  background: <?php echo $candid_color_scheme;?>;
}
@media (max-width: 800px) {
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover, .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    color: <?php echo $candid_color_scheme;?> !important;
  }
  .has-submenu.highlighted {
    color: <?php echo $candid_color_scheme;?> !important;
  }
  .navbar-default .navbar-nav > li > a:focus,
  .navbar-default .navbar-nav > li > a:hover {
    color: <?php echo $candid_color_scheme;?> !important;
    background-color: #fff !important;
  }
}
blockquote cite {
    color: <?php echo $candid_color_scheme;?> !important;
    }
    blockquote cite:before {
      color: <?php echo $candid_color_scheme;?> !important;
    }

/* sub theme-hover color */
mark, ins {
  background: <?php echo $candid_custom_hover_color; ?>;
}

button:hover,
html input[type="button"]:hover,
input[type="reset"]:hover,
input[type="submit"]:hover {
  background: <?php echo $candid_custom_hover_color; ?>;
}

.comment-reply-link:hover, .comment-reply-link:focus, .comment-reply-link:active {
  background: <?php echo $candid_custom_hover_color; ?>;
}

.go-to:hover, .go-to:focus, .go-to:active {
  background: <?php echo $candid_custom_hover_color; ?>;
}

.widget_calendar tbody a:hover, .widget_calendar tbody a:focus {
  background-color: <?php echo $candid_custom_hover_color; ?>;
}
</style> 

<?php
  } // end candid_color_scheme function
  candid_color_scheme(); // here print the function
