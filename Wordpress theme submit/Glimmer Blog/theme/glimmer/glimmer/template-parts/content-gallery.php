<?php
/**
 * The template for displaying gallery post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<?php 
	global $glimmer, $post;
	$img_rul = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); 
?> 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="ribbon"><span>'.__('Sticky', 'glimmer').'</span></div>';
    	}
    ?>
	<header class="entry-header">
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php glimmer_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

		<?php 		
			if ( is_single() ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
			} else {
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
			}
		?>
	</header>

	<?php
		$meta = get_post_meta( $post->ID );
		if ( !empty( $meta["_glimmer_format_gallery"][0] ) ) :
	?>	
	<figure class="post-thumb">
		<?php         
			// if gallery style one load this script
			$meta = get_post_meta( $post->ID );
	    	( isset( $meta["_glimmer_gallery_style"][0] ) ) ? $gallery_style = $meta["_glimmer_gallery_style"][0] : $gallery_style = "" ; 
	    	if ( $gallery_style == "gallery-one"  ) {
	    	?>
	            <div class="gallery-one owl-carousel">
	                <?php              
	                    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
	                        $imgs_urls = $meta["_glimmer_format_gallery"][0];                            
	                    }  else {
	                        $imgs_urls = '';
	                    }        
	                    $imgs_url = explode( '"', $imgs_urls );
	                    for ( $x = 0; $x < count ( $imgs_url ); $x++ ) {
	                    	if($x % 2 != 0) {
	                        	?>
	                        	<div class="item">
	                           		<img class="lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>" alt="<?php the_title(); ?>"> 
	                       		</div>
	                        	<?php                  		
	                        } // end if
	                    } // end for
	                ?>  
	            </div> <!-- /.gallery-one -->
	    	<?php                      
	    	}  // end if;
	    ?>
	    <?php     
	    	// if gallery style two load this script
	    	( isset( $meta["_glimmer_gallery_style"][0] ) ) ? $gallery_style = $meta["_glimmer_gallery_style"][0] : $gallery_style = "" ; 
	    	if ( $gallery_style == "gallery-two"  ) {
	    	?>
	            <div class="gallery-two">
                    <div class="full-view owl-carousel">
	                <?php              
	                    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
	                        $imgs_urls = $meta["_glimmer_format_gallery"][0];                            
	                    }  else {
	                        $imgs_urls = '';
	                    }        
	                    $imgs_url = explode( '"', $imgs_urls );
	                    for ( $x = 0; $x < count ( $imgs_url ); $x++ ) {
	                    	if($x % 2 != 0) {
	                        	?>
	                        	<div class="item">
	                           		<img class="lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>" alt="<?php the_title(); ?>"> 
	                       		</div>
	                        	<?php                  		
	                        } // end if
	                    } // end for
	                ?>  
	            	</div> <!-- /.full-view -->
	    	<?php                      
	    	}  // end if;
	    ?>
	    <?php         
	    	( isset( $meta["_glimmer_gallery_style"][0] ) ) ? $gallery_style = $meta["_glimmer_gallery_style"][0] : $gallery_style = "" ; 
	    	if ( $gallery_style == "gallery-two"  ) {
	    	?>
	            <div class="list-view owl-carousel">
	                <?php              
	                    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
	                        $imgs_urls = $meta["_glimmer_format_gallery"][0];                            
	                    }  else {
	                        $imgs_urls = '';
	                    }        
	                    $imgs_url = explode( '"', $imgs_urls );
	                    for ( $x = 0; $x < count ( $imgs_url ); $x++ ) {
	                    	if($x % 2 != 0) {
	                        	?>
	                        	<div class="item">
	                           		<img src="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>" alt="Thumbnail"> 
	                       		</div>
	                        	<?php                  		
	                        } // end if
	                    } // end for
	                ?>  
	            </div>  <!-- /.list-view -->
			</div> <!-- /.gallery-two -->
	    	<?php                      
	    	}  // end if;
	    ?>
	</figure> <!-- /.post-thumb -->
	<?php
		endif;
	?>
	
	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
			} else {
				the_content();
			}
		?>   		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
    <?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->