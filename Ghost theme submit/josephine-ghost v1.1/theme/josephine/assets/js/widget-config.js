/**
 *
 * Josephine Widget Configuration
 *
 */
(function($) {
    "use strict";

    // Code Widget About Me
    if (About_Me_Widget['show'] === true) {
        $('aside.josephine_about_widget').removeClass('hidden');
        $('.ghost-about-title').html(About_Me_Widget['widget_title']);
        $('.josephine_about_widget_img').attr('src', About_Me_Widget['image_url']);
        $('.about-widget .author-name').html(About_Me_Widget['author_name']);
        $('.about-widget .about-description p').html(About_Me_Widget['description']);
        $('.about-widget .more-link').attr('href', About_Me_Widget['author_link']);
    }

    // Code Widget Latest Posts
    if (Latest_Posts_Widget['show'] === true) {
        $('aside.josephine_latest_post_widget').removeClass('hidden');
        $('.ghost-latest-p-title').html(Latest_Posts_Widget['widget_title']);
    }


    // Code Widget Tags
    if (Tags_Widget['show'] === true) {
        $('aside.widget_tag_cloud').removeClass('hidden');
        $('.josephine_tags_widget_title').html(Tags_Widget['widget_title']);
    }


    // Code Widget ADS
    if (Ads_Widget['show'] === true) {
        $('aside.ghost_ads_widget').removeClass('hidden');
        $('.ghost_ads_widget_title').html(Ads_Widget['widget_title']);
        $('.ads_widget_image').html("<a target='blank' href=" + Ads_Widget['ads_url'] + "><img src=" + Ads_Widget['image_url'] + "></a>");
    }


    // Code Widget Navigation
    if (Navigation_Widget['show'] === true) {
        $('aside.widget_nav_menu').removeClass('hidden');
        $('.widget_nav_menu_title').html(Navigation_Widget['widget_title']);
    }


    // Code Widget Social
    if (Social_Widget['show'] === true) {
        $('aside.widget_follow_us').removeClass('hidden');
        $('.widget_follow_us_title').html(Social_Widget['widget_title']);
    }

    for (var Url in Social_Widget) {
        if (Url == "show" || Url == "widget_title") {
            continue;
        }

        $('.widget_social').append("<a class='follow-link' target='blank' href='" + Social_Widget[Url] + "'><i class='fa fa-" + Url + "'></i></a>");

    }


    // Code Widget Instagram Feed
    if (Instagram_Widget['show'] === true) {
        $('.instafeed_widget').removeClass('hidden');
        $('.instafeed_widget_title').html(Instagram_Widget['widget_title']);
    }


    // Code Widget Facebook Page
    if (Facebook_Page_Widget['show'] === true) {
        var fb_url = Facebook_Page_Widget['page_url'];

        $('.ghost_facebook_widget').removeClass('hidden');
        $('.ghost_facebook_widget_title').html(Facebook_Page_Widget['widget_title']);


        var fb_page = '<div class="fb-page" data-href="' + fb_url + '" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore">Facebook</div></div>';
        $('.facebook-widget').append(fb_page);
    }

    // Loading script for facebook 
        var facebook_sdk_script = '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.4";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));</script>';
        $('body').append(facebook_sdk_script);


    // Code Widget Google Plus Page
    if (Googleplus_Page_Widget['show'] === true) {
        var gp_url = Googleplus_Page_Widget['page_url'];

        $('.ghost_googleplus_widget').removeClass('hidden');
        $('.ghost_googleplus_widget_title').html(Googleplus_Page_Widget['widget_title']);

        var google_plus_sdk_script = '<script src="https://apis.google.com/js/platform.js" async defer></script>';
        var googleplus_page = '<div class="g-page" data-href="' + gp_url + '" data-rel="publisher"></div>';

        $('body').append(google_plus_sdk_script);
        $('.google-plus-widget').append(googleplus_page);
    }


    // Code Widget Twitter Profile
    if (Twitter_Widget['show'] === true) {
        var tp_url = Twitter_Widget['profile_url'];
        var tp_count = Twitter_Widget['post_count'];
        var twitter_widget_id = Twitter_Widget['twitter_id'];

        $('.ghost_twitter_widget').removeClass('hidden');
        $('.ghost_twitter_widget_title').html(Twitter_Widget['widget_title']);

        var twitter_section = '<a class="twitter-timeline" href="' + tp_url + '" data-widget-id="' + twitter_widget_id + '" data-link-color="#0062CC" data-chrome="nofooter noscrollbar" data-tweet-limit="' + tp_count + '">Tweets</a>';
        twitter_section += "<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>";

        $('.twitter_profile').append(twitter_section);
    }




    // Code Widget Footer Widget One
    if (About_Widget['show'] === true) {
        $('.widget_josephine_aboutus_contact').removeClass('hidden');
        $('.widget_josephine_aboutus_contact_title').html(About_Widget['widget_title']);
        $('.widget_josephine_aboutus_contact .about-contact-area > div').append('<p>' + About_Widget['descriptions'] + '</p>');
        $('.widget_josephine_aboutus_contact li.mobile_num').html('<i class="fa fa-mobile"></i><a href="tel:' + About_Widget['phone'] + '">' + About_Widget['phone'] + '</a>');
        $('.widget_josephine_aboutus_contact li.email_addr').html('<i class="fa fa-envelope"></i><a href="mailto:' + About_Widget['email'] + '">' + About_Widget['email'] + '</a>');
    }


    // Code Widget Footer Widget Two
    if (Mailchimp_Widget['show'] === true) {
        $('.widget_subscribe').removeClass('hidden');
        $('.widget_subscribe_title').html(Mailchimp_Widget['widget_title']);
        $('#mail-description').append('<p>' + Mailchimp_Widget['descriptions'] + '</p>');
        $("#newsletter-form").attr("action", "" + Mailchimp_Widget['action_url'] + "");
        $("button.mail-submite").html(Mailchimp_Widget['button_text']);
    }


    // Code Widget Footer Widget Three
    if (Flickr_Widget['show'] === true) {
        $('.widget_flicker').removeClass('hidden');
        $('.widget_flicker .widget-title > span').html(Flickr_Widget['widget_title']);
    }


    // Code Footer Copy-Right Text
    if (Footer_CopyRight_Text['copy_right_text'] !== "") {
        $('#footer_copyright').html('<p>' + Footer_CopyRight_Text['copy_right_text'] + '</p>');
    } else {
        $('#footer_copyright').html('<p> &copy; 2015 Josephine-Blog Theme. All right reserved. All image are used only demo purpouse only and property of their owners</p>');
    }

    //Code for Footer Social
    for (var Url in Social_Site_Url) {

        $('#top-social, #mobile-social, #footer_social').append("<a href='" + Social_Site_Url[Url] + "'><i class='fa fa-" + Url + "'></i></a>");

    }

    // Code for site Pre-Loader
    if (Sticky_Post['show'] === true) {
        $('#featured').removeClass('hidden');
    }

})(jQuery);