<?php
    global $glimmer;
    $funfact = glimmer_get_custom_posts_by_custom_order("funfact", 4, "_glimmer_funfact_order");
?>   
<!-- fun-fact
================================================== --> 
<section id="fun-fact" class="fun-fact parallax-three">
	<div class="patern-overlay"></div>
	<div class="container">
		<div class="row">
			<?php
                foreach ($funfact as $post) {
                setup_postdata($post);
                $meta = get_post_meta($post->ID);
            ?>
			<div class="col-xs-12 col-sm-6 col-lg-3">
				<div class="text-center funfact-mobile">
					<div class="icon-box wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
						<i class="fa <?php if(isset($meta["_glimmer_funfact_icon"][0])) echo $meta["_glimmer_funfact_icon"][0]; ?>"></i>
					</div>
					<div class="count" data-to="<?php if(isset($meta["_glimmer_funfact_count_number"][0])) echo $meta["_glimmer_funfact_count_number"][0]; ?>">
						<h2 class="count-number"><?php if(isset($meta["_glimmer_funfact_count_number"][0])) echo $meta["_glimmer_funfact_count_number"][0]; ?></h2>
						<h4 class="count-details"><?php the_title(); ?></h4>
					</div> <!-- /.count -->    
				</div> <!-- /.text-center -->
			</div> <!-- /.col-lg-3 -->
			<?php } ?>
		</div> <!-- /.row -->
	</div> <!-- /.container -->     
</section> <!-- /.fun-fact -->

