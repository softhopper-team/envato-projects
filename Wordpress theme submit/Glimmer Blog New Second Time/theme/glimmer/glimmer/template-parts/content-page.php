<?php
/**
 * The template for displaying page content.
 *
 * @package Glimmer
 */
 global $glimmer;
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php glimmer_page_entry_header(); ?>
	</header>
	
	<?php 
		if ( has_post_thumbnail() ) {
        ?>
        <figure class="post-thumb">
			<a href="<?php the_permalink(); ?>">
				<?php
					( $glimmer['sidebar_layout_page'] == 1 ) ? $image_size = "single-full" : $image_size = "single-full-list" ;
		          	the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
		        ?>
			</a>
		</figure> <!-- /.post-thumb -->
        <?php
        }
	?>

	<div class="entry-content">
		<?php 
			the_content();
			edit_post_link( esc_html__( '(Edit Page)', 'glimmer' ), '<span class="edit-link">', '</span>' ); 
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->
    
    <?php glimmer_page_footer(); ?> 
    
</article> <!-- /.post-->