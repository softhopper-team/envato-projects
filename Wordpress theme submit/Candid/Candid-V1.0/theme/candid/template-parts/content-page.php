<?php
/**
 * The template for displaying page content.
 *
 * @package Candid
 */
?>
<?php 
	$category = get_the_category();  
	$img_rul = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
?> 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    	if ( is_sticky() ) {
			echo "<span class='sticky-post'>".__('&starf;','candid')."</span>";
		}

        if ( has_post_thumbnail() ) {
            ?>
            <div class="post-thumbnail">
                <?php
                  	the_post_thumbnail('single-full', array( 'class' => " img-responsive", 'alt' => get_the_title()));
	            ?>
	            <div class="image-extra">
                    <div class="extra-content">
                        <div class="inner-extra">
                            <a class="single-image link-icon" href="<?php the_permalink(); ?>">
                                <span class="fa fa-link"></span>
                            </a>
                            <a class="single-image plus-icon" title="<?php the_title(); ?>" rel="<?php echo $category[0]->cat_name; ?>" href="<?php echo $img_rul ?>">
                                <span class="fa fa-plus"></span>
                            </a>
                        </div> <!-- /.inner-extra -->
                    </div> <!-- /.extra-content -->
                </div> <!-- /.image-extra -->
		    </div> <!-- /.post-thumbnail -->
            <?php
        }
    ?>

    <header class="entry-header">
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php candid_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

    <div class="entry-content">
		
		<?php 
			if(is_single()) {
				the_content(); 
			} else {
				the_content( __( '<span class="read-button">'.__('Continue reading','candid').'</span>', 'candid' ) );
			}
		?>   
		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'candid' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
    
</article> <!-- /.post -->