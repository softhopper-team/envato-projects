<?php 

class Glimmer_Newsletter extends WP_Widget {

	function __construct() {
	    $params = array (
	        'description' => 'Glimmer : Mailchimp News Letter Form',
	        'name' => 'Glimmer : Mailchimp Newsletter'
	    );
	    parent::__construct('Glimmer_Newsletter','',$params);
	}	

	function widget( $args, $instance ) {
		extract( $args );
		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$description = $instance['description'];
		$action_url = $instance['action_url'];
		$first_last_name = $instance['first_last_name'];
		$button_text = $instance['button_text'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		wp_enqueue_script('mailchimp', 'http://s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js', array(), '2.8.3', false);


		?>
		<div class="glimmer-newsletter-box">
			<div class="newsletter-area">
			    <p><?php echo $description; ?></p>
			    <form action="<?php echo $action_url; ?>" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate">
			        <div class="form-newsletter">
			            <div class="row">
			            <?php if ( $first_last_name == true ) : ?>
			                <div class="col-md-6 pad-right">
			                	<p>
			                		<input type="text"  value="" placeholder="First Name" name="FNAME" class="form-controller" required="required">
			                	</p>
							</div><!-- / col-md-6 -->
			                <div class="col-md-6 pad-left">
			                	<p>
			                		<input type="text" value="" placeholder="Last Name" name="LNAME" class="form-controller"  required="required">
			                	</p>
							</div><!-- / col-md-6 -->
						<?php endif; ?>

			                <div class="mc-field-group col-md-12">
				                <p>
				                    <input type="email"  value="" placeholder="Email*" name="EMAIL" class="form-controller email"  required="required">

				                </p>
							</div><!-- / col-md-12 -->

							<div class="col-md-12 text-center">
								 <input type="submit" value="<?php echo $button_text; ?>" name="subscribe" id="mc-embedded-subscribe" class="btn btn-default">
							</div>  <!-- / col-md-12 -->

						</div>  <!-- / row -->
					</div>  <!-- / form-newsletter -->
			    </form>  <!-- / signup form -->
    			<!--  Response -->
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>
                <!-- / Response -->
			</div> <!-- / newsletter aria -->
			<script type="text/javascript">
			(function($) {
			    "use strict";
			    
			    $('#mc-embedded-subscribe-form').submit(function(e) {
			        e.preventDefault();
			        $.ajax({
			            url: '<?php echo $action_url; ?>',
			            type: 'GET',
			            data: $('#mc-embedded-subscribe-form').serialize(),
			            dataType: 'json',
			            contentType: "application/json; charset=utf-8",
			            success: function(data) {
			                if (data['result'] != "success") {
			                    console.log(data['msg']);
			                } else {
			                }
			            }
			        });
			    });
			  })(jQuery);
			</script>
		</div><!-- newsletter_box_footer-->
	<?php 
		/* After widget (defined by themes). */
		echo $after_widget;
	}
	
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$description = $instance['description'];
		$action_url = $instance['action_url'];
		$first_last_name = $instance['first_last_name'];
		$button_text = $instance['button_text'];


		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['description'] = $new_instance['description'];
		$instance['action_url'] = $new_instance['action_url'];
		$instance['first_last_name'] = $new_instance['first_last_name'];
		$instance['button_text'] = $new_instance['button_text'];
		return $instance;
	}
	
	 /** @see WP_Widget::form */
	function form( $instance ) {

			/* Set up some default widget settings. */
			$defaults = array(
				'title' => __('Mailchimp Newsletter','glimmer'),
				'action_url' => 'http://softhopper.us11.list-manage.com/subscribe/post?u=559ff170eee6949a359c40740&amp;id=fbbd18e68b',
				'description' => "Signup for our news letter and get updates of our new post in your inbox",
				'first_last_name' => true,
				'button_text' => 'subscribe',
				
	 			);
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'glimmer') ?></label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
			</p>

	    	<p>
				<label for="<?php echo $this->get_field_id( 'action_url' ); ?>"><?php _e('Form Action URL:', 'glimmer') ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'action_url' ); ?>" name="<?php echo $this->get_field_name( 'action_url' ); ?>" value="<?php echo esc_url( $instance['action_url'] ); ?>"  class="widefat" />
			</p>
	        

	    	<p>
				<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e('Description:', 'glimmer') ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>" value="<?php echo $instance['description']; ?>"  class="widefat" />
			</p>


	    	<p>
				<label for="<?php echo $this->get_field_id( 'button_text' ); ?>"><?php _e('Button Text:', 'glimmer') ?></label>
				<input type="text" id="<?php echo $this->get_field_id( 'button_text' ); ?>" name="<?php echo $this->get_field_name( 'button_text' ); ?>" value="<?php echo $instance['button_text']; ?>"  class="widefat" />
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked( $instance['first_last_name'], true ); ?> id="<?php echo $this->get_field_id( 'first_last_name' ); ?>" name="<?php echo $this->get_field_name( 'first_last_name' ); ?>" value="1"/>
				<label for="<?php echo $this->get_field_id( 'first_last_name' ); ?>"><?php _e('Show First and Last name', 'glimmer'); ?></label>
			</p>

		
	   <?php 
	}
	} //end class