(function($) {
    "use strict";

    var themeApp = {

        // glimmer preloader
        //------------------------------------------------
        glimmer_preloader: function() {            
            $(window).load(function() {
                $('.preloader').delay(500).slideUp('slow', function() {
                    $(this).remove();
                });
            });
        },

        // glimmer featured post
        //------------------------------------------------
        glimmer_featured_area: function() {
            $('#featured-item').owlCarousel({
                items: 5,
                itemsDesktop: [1368, 4],
                itemsDesktopSmall: [1240, 3],
                itemsTablet: [780, 2],
                itemsTabletSmall: [710, 1],
                itemsMobile: [479, 1],
                pagination: true,
                lazyLoad: true,
                responsiveRefreshRate: 200,
                autoPlay: true,
                addClassActive: true
            });
        },

        // glimmer Video
        //------------------------------------------------
        glimmer_video: function() {
            $(".featured-area").fitVids();
            $(".content-area").fitVids();
        },

        //gallery post carousel
        //------------------------------------------------
        glimmer_gallary: function() {
            $('.gallery-one').owlCarousel({
                singleItem: true,
                slideSpeed: 800,
                navigation: true,
                autoPlay: true,
                lazyLoad: true,
                pagination: false,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ]
            });
        },

        // gallery post carousel
        //------------------------------------------------
        glimmer_gallary_two: function() {
            var sync1 = $(".full-view");
            var sync2 = $(".list-view");

            sync1.owlCarousel({
                singleItem: true,
                slideSpeed: 800,
                navigation: true,
                autoPlay: true,
                pagination: false,
                lazyLoad: true,
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ]
            });
            var owl_item = glimmer.owl_item;
            sync2.owlCarousel({
                items: owl_item,
                itemsDesktop: [1199, 5],
                itemsDesktopSmall: [979, 5],
                itemsTablet: [768, 5],
                itemsMobile: [479, 2],
                pagination: false,
                responsiveRefreshRate: 100,
                addClassActive: true,
                stagePadding: 20,
                autoPlay: false,
                navigation: true,
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ],
                afterInit: function(el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                sync2
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced");
                if ($("#list-view").data("owlCarousel") !== undefined) {
                    center(current);
                }
            }
            sync2.on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }
                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }
            }
        },

        // widget
        //------------------------------------------------
        glimmer_widget: function() {
            $(".widget").each(function() {
                if (!$(this).prev(".widget").length && $(this).find($(".widget-title")).length) {
                    $(this).css({
                        "margin-top": "1.05em"
                    });
                }
                if ($(this).next(".widget").find(".widget-title").length) {
                    $(this).css({
                        "margin-bottom": "2.9em"
                    });
                }
            });
        },

        // some initial style
        //------------------------------------------------
        glimmer_initial: function() {
            $('[data-toggle="tooltip"]').tooltip();
            $('.comment-reply-title').append('<span class="small-border"></span>');
            $("#contact_form > p").wrap("<div class='col-md-12'></div>");
            $("#contact_form > div").wrapAll("<div class='row'></div>");
        },

        // For IE placeholder support
        //------------------------------------------------
        glimmer_placeholder: function() {
            if (!Modernizr.input.placeholder) {

                $('[placeholder]').focus(function() {
                    var input = $(this);
                    if (input.val() == input.attr('placeholder')) {
                        input.val('');
                        input.removeClass('placeholder');
                    }
                }).blur(function() {
                    var input = $(this);
                    if (input.val() == '' || input.val() == input.attr('placeholder')) {
                        input.addClass('placeholder');
                        input.val(input.attr('placeholder'));
                    }
                }).blur();
                $('[placeholder]').parents('form').submit(function() {
                    $(this).find('[placeholder]').each(function() {
                        var input = $(this);
                        if (input.val() == input.attr('placeholder')) {
                            input.val('');
                        }
                    })
                });

            }
        },

        // Related post
        //------------------------------------------------
        glimmer_related_post: function() {
            var owl_item_related = glimmer.owl_item_related;
            $('#related-post-slide').owlCarousel({
                items: owl_item_related, //item display
                itemsDesktop: [1199, 2], //item display on Desktop
                itemsDesktopSmall: [979, 2], //item display on  Small Desktop
                itemsTablet: [768, 2], //item display on  Tablet
                itemsMobile: [659, 1], //item display on  Mobile
                pagination: false, //pagination
                autoPlay: false,
                responsiveRefreshRate: 200,
                addClassActive: true,
                lazyLoad: true,
                navigation: true, //navigation
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ],
                afterInit: function(elem) {
                    var that = this
                    that.owlControls.prependTo(elem)
                }
            });
        },

        // Scroll top
        //------------------------------------------------
        glimmer_scroll_top: function() {
            $("a[href='#top']").on('click', function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
        },

        // Author Skill
        //------------------------------------------------
        glimmer_author_skill: function() {
            if ($('.skillbar').length) {
                var $skill = $('.skillbar');

                $skill.appear(function() {
                    $(this).find('.skillbar-bar, .skill-bar-shape').animate({
                        width: $(this).attr('data-percent')
                    }, 1000);
                });
            }

            $('.percent-area .skill-bar-percent').css('left', function() {
                return $(this).parent().data('percent')
            });

            $('.percent-area .skill-bar-percent').append(function() {
                return $(this).parent().data('percent')
            });
        },

        // Maps
        //------------------------------------------------
        glimmer_maps: function() {
            if ($('#gmaps').length) {
                var map;
                var lat = glimmer.lat;
                var lon = glimmer.lon;
                var map_mouse_wheel = glimmer.map_mouse_wheel;
                var map_zoom_control = glimmer.map_zoom_control;
                var map_point_img = glimmer.map_point_img;

                map = new GMaps({
                    el: '#gmaps',
                    lat: lat,
                    lng: lon,
                    scrollwheel: map_mouse_wheel,
                    zoom: 10,
                    zoomControl: map_zoom_control,
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: false,
                    overviewMapControl: false,
                    clickable: false
                });

                var image = map_point_img;
                map.addMarker({
                    lat: lat,
                    lng: lon,
                    icon: image,
                    animation: google.maps.Animation.DROP,
                    verticalAlign: 'bottom',
                    horizontalAlign: 'center'
                });


                var styles = [{
                    "featureType": "road",
                    "stylers": [{
                        "color": "#b4b4b4"
                    }]
                }, {
                    "featureType": "water",
                    "stylers": [{
                        "color": "#d8d8d8"
                    }]
                }, {
                    "featureType": "landscape",
                    "stylers": [{
                        "color": "#f1f1f1"
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#000000"
                    }]
                }, {
                    "featureType": "poi",
                    "stylers": [{
                        "color": "#d9d9d9"
                    }]
                }, {
                    "elementType": "labels.text",
                    "stylers": [{
                        "saturation": 1
                    }, {
                        "weight": 0.1
                    }, {
                        "color": "#000000"
                    }]
                }]
            }
        },

        /* WP Admin bar */
        glimmer_wp_adminbar: function() {
            // This function gets called with the user has scrolled the window.
            $(window).scroll(function() {
                if ($(this).scrollTop() > 0) {
                    // Add the scrolled class to those elements that you want changed
                    $(".overlay-slidedown.open").addClass("scroll");
                } else {
                    $(".overlay-slidedown.open").removeClass("scroll");
                }
            });
        },

        glimmer_initializ: function() {
            themeApp.glimmer_preloader();
            themeApp.glimmer_featured_area();
            themeApp.glimmer_gallary();
            themeApp.glimmer_gallary_two();
            themeApp.glimmer_video();
            themeApp.glimmer_widget();
            themeApp.glimmer_initial();
            themeApp.glimmer_placeholder();
            themeApp.glimmer_related_post();
            themeApp.glimmer_scroll_top();
            themeApp.glimmer_author_skill();
            themeApp.glimmer_maps();
            themeApp.glimmer_wp_adminbar();
        }
    };

    /* === document ready function === */
    $(document).ready(function() {
        themeApp.glimmer_initializ();
    });

})(jQuery);