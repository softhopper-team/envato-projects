<?php
/**
 * The template for displaying shortcode in post and pages
 * 
 * @package Glimmer
 * 
 */

// status shortcode for facebook and twitter
add_shortcode("status", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'url' => '',
		), $atts
	);
	extract($atts);

	$status_facebook = ( $type == "facebook" ) ? "facebook" : "";
	$status_twitter = ( $type == "twitter" ) ? "twitter" : "";

	if( !empty( $status_facebook ) || !empty( $status_twitter ) ):	?>
		<div class="post-status-wrapper">
		<?php if( !empty( $status_facebook ) ) : ?>
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;  js = d.createElement(s);
				js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-post" data-href="<?php echo $url ?>"></div>
		<?php elseif( !empty( $status_twitter ) ) : ?>
			<blockquote class="twitter-tweet"><a href="<?php echo $url ?>"></a></blockquote>
			<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		<?php endif; ?>
		</div><!-- /.post-status-wrapper -->
	<?php endif; 
	$status = ob_get_clean();
	return $status;
});

// bootstrap column grid row shortcode
add_shortcode("row", function($atts, $content = null) {
	return '<div class="row">' . do_shortcode( $content ) . '</div>';
});

// bootstrap column grid shortcode
add_shortcode("column", function($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'number' => '',
			'offset' => '',
		), $atts
	);
	extract($atts);
	if ( !empty($offset) ) {
		$offset = "col-md-offset-$offset";
	}
	return "<div class='$offset col-md-$number'>". do_shortcode( $content ) ."</div>";
});

// bootstrap progress bar shortcode 
add_shortcode("progress_bar", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'width' => '50',
			'title' => '',
			'striped' => false,
			'animation' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " progress-bar-$type " : $type = "";
	( $striped == true ) ? $striped = " progress-bar-striped " : $striped = "";
	( $animation == true ) ? $animation = " active " : $animation = "";
	?>

	<div class="progress">
		<div class="progress-bar <?php echo $type.$striped.$animation;?>" role="progressbar" aria-valuenow="<?php echo $width; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $width; ?>%">
	    <?php echo $title; ?>
	  	</div> <!-- /.progress-bar -->
	</div> <!-- /.progress -->

	<?php	
	$progress_bar = ob_get_clean();
	return $progress_bar;
});

// bootstrap button shortcode 
add_shortcode("button", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => 'primary',
			'title' => '',
			'size' => '',
			'disabled' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " btn-$type " : $type = "";
	( $disabled == true ) ? $disabled = " disabled='true' " : $disabled = "";
	( $title == null ) ? $title = __(" Button ", "glimmer") : $title = $title;
	?>

	<button type="button" class="btn <?php echo $type." ".$size; ?>" <?php echo $disabled; ?>>
	<?php echo $title; ?>
	</button>

	<?php	
	$button = ob_get_clean();
	return $button;
});


// bootstrap accordion shortcode collapse_group
add_shortcode("collapse_group", function($atts, $content = null) {
	return '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">' . do_shortcode( $content ) . '</div>';
});

// bootstrap accordion shortcode collapse 
add_shortcode("collapse", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'id' => '',
			'title' => '',
			'expanded' => false,
		), $atts
	);
	extract($atts);
	( $expanded == true ) ? $aria_expander = "true" : $aria_expander = "false";
	( $expanded == true ) ? $expanded_two = "" : $expanded_two = "collapsed";
	( $expanded == true ) ? $expanded_in = "in" : $expanded_in = "";
	?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab" >
		  <h4 class="panel-title">
		    <a class="accordion-toggle <?php  echo $expanded_two; ?>" role="button" data-toggle="collapse" href="#<?php  echo $id; ?>" aria-expanded="<?php  echo $aria_expander; ?>" >
		    <?php  echo $title; ?>
		    </a>
		  </h4>
		</div>
		<div id="<?php  echo $id; ?>" class="panel-collapse collapse <?php  echo $expanded_in; ?>" role="tabpanel"  aria-expanded="<?php  echo $aria_expander; ?>">
		  <div class="panel-body">
		  <?php  echo $content; ?>
		  </div>
		</div>
	</div>
	<?php	
	$collapse = ob_get_clean();
	return $collapse;
});


// bootstrap shortcode alert 
add_shortcode("alert", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'dismiss' => false,
		), $atts
	);
	extract($atts);
	?>

	<div class="alert alert-<?php echo $type;?> " role="alert">
	<?php
	  if ( $dismiss == true ) {
	  	?>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?php
	  }
	  echo $content;
	?>
	</div><!-- /.alert -->


	<?php	
	$alert = ob_get_clean();
	return $alert;
});

// bootstrap shortcode tooltip 
add_shortcode("tooltip", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'title' => '',
			'placement' => "top",
		), $atts
	);
	extract($atts);
	?>
	<span data-toggle="tooltip" title="<?php echo $title; ?>" data-placement="<?php echo $placement; ?>"><?php echo $content; ?></span>

	<?php	
	$tooltip = ob_get_clean();
	return $tooltip;
});

?>
