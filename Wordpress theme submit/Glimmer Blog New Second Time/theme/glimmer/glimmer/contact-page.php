<?php
/**
 * Template Name: Contact Page
 */
?>
<?php 
    get_header();
    global $glimmer;
?>
<!-- Content
================================================== -->
<div id="content" class="site-content contact-page">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-title">
                    <h2 class="section-title"><?php the_title(); ?></h2>
                    <div class="ex-small-border"></div> 
                    <!--h5 class="sub-heading">
                    <?php 
                       // if ( isset ( $glimmer['contact_subtitle'] ) ) echo $glimmer['contact_subtitle'];
                    ?>
                    </h5-->   
                </div> <!-- /.header-title --> 
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->

        <div class="row">
            <div class="col-md-5">
                <!-- main section -->
                <div id="main">                    
                    <div class="contact-details">
                        <div class="entry-content">
                            <p>
                                <?php 
                                    if ( isset ( $glimmer['contact_description'] ) ) echo $glimmer['contact_description'];
                                ?>
                            </p> 
                        </div> <!-- /.entry-content -->

                        <div class="address">
                            <?php 
                                if ( isset ( $glimmer['contact_address'] ) ) echo $glimmer['contact_address'];
                            ?>   
                        </div> 
                    </div> <!-- /.post -->
                </div> <!-- /#main -->
            </div> <!-- /.col-md-5 -->


            <div class="col-md-7">
                <div class="gmaps-area">
                    <div id="gmaps"></div>                                   
                </div>
            </div> <!-- .col-md-7 -->
        </div> <!-- /.row -->

        <div class="row">
            <div class="col-md-12">
                <div class="contact-respond" id="respond">                            
                    <?php 
                        // show contact form by contact form 7 plugin 
                        if ( isset ( $glimmer['contact_form7_shortcode'] ) ) {
                            echo do_shortcode($glimmer['contact_form7_shortcode']); 
                        }
                    ?>         
                </div><!-- #respond --> 
            </div> <!-- /.col-md-12 -->
        </div>
    </div> <!-- /.container -->
    
</div><!-- #content -->
<?php get_footer(); ?>