<?php
/**
 * The template for displaying chat post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
global $glimmer;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="sticky-icon"><i class="fa fa-thumb-tack"></i></div>';
    	}
    ?>

	<header class="entry-header">
        <?php glimmer_entry_header(); ?>
    </header> <!-- /.entry-header -->
    
    <script type="text/javascript">
		jQuery( window ).load(function() {
			jQuery('.format-chat .chat-text p:contains("more-link")').parent().parent().css('display', 'none');  
		});	
    </script>	

	<?php
		if ( has_post_thumbnail() ) {
	    ?>
	    <figure class="post-thumb">
			<?php 
        	if ( is_single() ) {
        		?>
				<?php
					( $glimmer['sidebar_layout'] == 1 ) ? $image_size = "single-full" : $image_size = "single-full-list" ;
					/* this script for demo start */
					( isset($_GET["layout"]) ) ? $layout = $_GET["layout"]  : $layout = "" ;
					if ( $layout == "full-width" ) {
						$image_size = "single-full";
					}
					/* this script for demo end */
		          	the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
		        ?>
        		<?php
        	} else {
        		?>
        		<a href="<?php the_permalink(); ?>">
					<?php
						( $glimmer['sidebar_layout'] == 1 ) ? $image_size = "single-full" : $image_size = "single-full-list" ;
						/* this script for demo start */
						( isset($_GET["layout"]) ) ? $layout = $_GET["layout"]  : $layout = "" ;
						if ( $layout == "full-width" ) {
							$image_size = "single-full";
						}
						/* this script for demo end */
			          	the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
			        ?>
				</a>
        		<?php
        	}
        	?>
		</figure> <!-- /.post-thumb -->
	    <?php
	    }
	?>
    
	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
				?>
				<?php
					if ( has_tag() ) :
					?>
					<div class="tag clearfix">
					    <span class="tags"><?php _e('Tagged In:', 'glimmer'); ?></span>
	                    <?php 
				        echo get_the_tag_list("", "", "");
				        ?>
					</div> <!-- /.tag -->
					<?php
					endif;
				?>
				<?php
			} else {
				the_content(); 
				?>
				<div id="readmore-add"></div>
				<?php
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
	<?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->