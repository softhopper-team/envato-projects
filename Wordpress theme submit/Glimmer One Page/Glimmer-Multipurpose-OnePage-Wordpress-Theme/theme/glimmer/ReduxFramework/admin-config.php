<?php
    /**
     * ReduxFramework Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "glimmer";


    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'img' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /*
     *
     * --> Action hook examples
     *
     */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');


    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Glimmer Options', 'glimmer-admin' ),
        'page_title'           => __( 'Glimmer Options', 'glimmer-admin' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 100,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => get_template_directory_uri() . "/img/menuicon/setting.png",
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

 
    

    // Add content after the form.
    $args['footer_text'] = __( '<p></p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields



    // -> START Typography
    //Redux::setSection( $opt_name, array() );
    //Redux::setSection( $opt_name, array() );
    //print_r(get_option('glimmer_sections'));
        // this array contains all animation style list
            $animation_style_list_item = array("NoAnimation","bounce","flash","pulse","rubberBand","shake","swing","tada","wobble","bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flip","flipInX","flipInY","flipOutX","flipOutY","lightSpeedIn","lightSpeedOut","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideInUp","slideInDown","slideInLeft","slideInRight","slideOutUp","slideOutDown","slideOutLeft","slideOutRight","zoomIn","zoomInDown","zoomInLeft","zoomInRight","zoomInUp","zoomOut","zoomOutDown","zoomOutLeft","zoomOutRight","zoomOutUp","hinge","rollIn","rollOut");
            foreach ($animation_style_list_item as  $value) {
                $animation_style_list_array[$value]=$value;
            }
            Redux::setSection( $opt_name, array(
                    'title' => __('Global Management', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-globe',
                    'fields' => array(
                                             
                        array(
                            'id'       => 'glimmer_sections_order',
                            'type'     => 'sortable',
                            'title'    => __( 'Sort Sections', 'glimmer-admin' ),
                            'subtitle' => __( 'Define and reorder these however you want.', 'redux-framework-demo' ),
                            'desc'     => __( 'You can change sort section field description in the input field, So that you can easily understood.', 'glimmer-admin' ),
                            'label'    => true,
                            'options'  => array(
                                'section-aboutus' => 'About Us',
                                'section-team' => 'Team',
                                'section-skill' => 'Skill',
                                'section-services' => 'Services',
                                'section-testimonial' => 'Testimonial',
                                'section-portfolio' => 'Portfolio',
                                'section-funfact' => 'Fun Fact',
                                'section-pricing' => 'Pricing',
                                'section-client' => 'Client',
                                'section-blog' => 'Blog',
                                'section-contact' => 'Contact',
                            )
                        ),   
                        array(
                            'id' => 'glimmer_color_one',
                            'type' => 'image_select',
                            'title' => __('Color Scheme One', 'glimmer-admin'),
                            'subtitle' => __('Select Predefined Color Schemes or your Own', 'glimmer-admin'),
                            'options' => array(
                                '1' => array(
                                    'alt' => 'hex-373e48.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-373e48.gif'
                                ),
                                '2' => array(
                                    'alt' => 'hex-f26d7e.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-f26d7e.gif'
                                ),
                                '3' => array(
                                    'alt' => 'hex-044f67.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-044f67.gif'
                                ),
                                '4' => array(
                                    'alt' => 'hex-006442.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-006442.gif'
                                ),
                                '5' => array(
                                    'alt' => 'hex-1691bd.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-1691bd.gif'
                                ),
                                '6' => array(
                                    'alt' => 'hex-067b82.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-067b82.gif'
                                ),
                                '7' => array(
                                    'alt' => 'hex-34495e.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-34495e.gif'
                                ),
                                '8' => array(
                                    'alt' => 'hex-d2527f.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-d2527f.gif'
                                ),
                                '9' => array(
                                    'alt' => 'Choose Your One',
                                    'img' => get_template_directory_uri() . '/img/colors/white.png'
                                )
                            ),
                            'default' => '1'
                        ),
                        array(
                            'id' => 'glimmer_custom_color_one',
                            'type' => 'color',
                            'title' => __('Your Own Theme Color', 'glimmer-admin'),
                            'subtitle' => __('Pick a custom color', 'glimmer-admin'),
                            'default' => '#FFFFFF',
                            'validate' => 'color',
                            'required' => array("glimmer_color_one", "=", "9")
                        ),
                        array(
                            'id' => 'glimmer_color_two',
                            'type' => 'image_select',
                            'title' => __('Color Scheme Two', 'glimmer-admin'),
                            'subtitle' => __('Select Predefined Color Schemes or your Own', 'glimmer-admin'),
                            'options' => array(
                                '1' => array(
                                    'alt' => 'hex-373e48.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-373e48.gif'
                                ),
                                '2' => array(
                                    'alt' => 'hex-f26d7e.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-f26d7e.gif'
                                ),
                                '3' => array(
                                    'alt' => 'hex-044f67.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-044f67.gif'
                                ),
                                '4' => array(
                                    'alt' => 'hex-006442.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-006442.gif'
                                ),
                                '5' => array(
                                    'alt' => 'hex-1691bd.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-1691bd.gif'
                                ),
                                '6' => array(
                                    'alt' => 'hex-067b82.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-067b82.gif'
                                ),
                                '7' => array(
                                    'alt' => 'hex-34495e.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-34495e.gif'
                                ),
                                '8' => array(
                                    'alt' => 'hex-d2527f.gif',
                                    'img' => get_template_directory_uri() . '/img/colors/hex-d2527f.gif'
                                ),
                                '9' => array(
                                    'alt' => 'Choose Your One',
                                    'img' => get_template_directory_uri() . '/img/colors/white.png'
                                )
                            ),
                            'default' => '2'
                        ),
                        array(
                            'id' => 'glimmer_custom_color_two',
                            'type' => 'color',
                            'title' => __('Your Own Theme Color', 'glimmer-admin'),
                            'subtitle' => __('Pick a custom color', 'glimmer-admin'),
                            'default' => '#FFFFFF',
                            'validate' => 'color',
                            'required' => array("glimmer_color_two", "=", "9")
                        ),
                        array(
                            'id' => 'custom_css',
                            'type' => 'ace_editor',
                            'title' => __('Custom CSS', 'glimmer-admin'),
                            'description' => 'Write your custom CSS code inside &lt;style> &lt;/style> block'
                        ),

                        array(
                            'id' => 'custom_ga',
                            'type' => 'ace_editor',
                            'title' => __('Google Analytics Code', 'glimmer-admin'),
                            'description' => 'Write your custom google analytics code inside &lt;script> &lt;/script> block'

                        ),
                        
                        array(
                            'id' => 'custom_main_body_fonts',
                            'type' => 'typography',
                            'title' => __('Google Font for full body', 'glimmer-admin'),
                            'google'      => true,
                            'color' => false,
                            'word-spacing'=>false,
                            'text-align'=>false,
                            'update-weekly'=>false,
                            'line-height'=>false,
                            'subsets'=>false,
                            'letter-spacing'=>false,
                            'font-style'=>false,
                            'font-backup' => false,
                            'font-size'=>false,
                            'font-weight'=>false,
                            'output'      => array('body'),
                            'units'       =>'px',
                            'default'     => array(
                                'font-family' => 'Raleway',
                                'google'      => true,
                            ),
                        ),

                    )
                ) 
            ); //global

            Redux::setSection( $opt_name, array(
                    'title' => __('Header Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-laptop',
                    'fields' => array(                       
                        array(
                            'id' => 'section_header_display',
                            'type' => 'switch',
                            'title' => __('Display Slider Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_header_nav_logo',
                            'type' => 'media',
                            'title' => __('Main Logo', 'glimmer-admin'),
                            'subtitle' => __('This image will show in the navigation', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/logo.png"),
                            'preview' => true,
                            "url" => true,
                            'required' => array('section_header_display', '=', '1')
                        ),
                        array(
                            'id' => 'section_header_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "HOME",
                            'required' => array('section_header_display', '=', '1')
                        ),

                        array(
                            'id' => 'section_header_nav_slider_img_one',
                            'type' => 'media',
                            'title' => __('Header Navigation Slider IMG 1', 'glimmer-admin'),
                            'subtitle' => __('This image will show top of navigation slider', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/header/pic-first.jpg"),
                            'preview' => true,
                            "url" => true,
                            'required' => array('section_header_display', '=', '1')
                        ),
                        array(
                            'id' => 'section_header_nav_slider_img_two',
                            'type' => 'media',
                            'title' => __('Header Navigation Slider IMG 2', 'glimmer-admin'),
                            'subtitle' => __('This image will show right of navigation slider', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/header/pic-second.jpg"),

                            'preview' => true,
                            "url" => true,
                            'required' => array('section_header_display', '=', '1')
                        ),
                        array(
                            'id' => 'section_header_nav_slider_img_three',
                            'type' => 'media',
                            'title' => __('Header Navigation Slider IMG 1', 'glimmer-admin'),
                            'subtitle' => __('This image will show left of navigation slider', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/header/pic-third.jpg"),
                            'preview' => true,
                            "url" => true,
                            'required' => array('section_header_display', '=', '1')
                        ),

                        array(
                            'id' => 'section_header_nav_top_icon',
                            'type' => 'text',
                            'title' => __('Section Header Top Navigation Icon', 'glimmer-admin'),
                            'desc' => __('This icon will show top side link', 'glimmer-admin'),
                            'default' => "icon-service",
                        ),
                        array(
                            'id' => 'section_header_nav_top_icon_url',
                            'type' => 'text',
                            'title' => __('Section Header Top Navigation Icon URL', 'glimmer-admin'),
                            'desc' => __('This icon will show top side link', 'glimmer-admin'),
                            'default' => "#our-service",
                        ),
                        array(
                            'id' => 'section_header_nav_top_icon_tooltip',
                            'type' => 'text',
                            'title' => __('Section Header Top Navigation Icon Tooltip', 'glimmer-admin'),
                            'desc' => __('This icon will show top side link', 'glimmer-admin'),
                            'default' => "Our Service",
                        ),
                        array(
                            'id' => 'section_header_nav_right_icon',
                            'type' => 'text',
                            'title' => __('Section Header Right Navigation Icon', 'glimmer-admin'),
                            'desc' => __('This icon will show right side link', 'glimmer-admin'),
                            'default' => "icon-portfolio",
                        ),
                        array(
                            'id' => 'section_header_nav_right_icon_url',
                            'type' => 'text',
                            'title' => __('Section Header Right Navigation Icon URL', 'glimmer-admin'),
                            'desc' => __('This icon will show right side link', 'glimmer-admin'),
                            'default' => "#portfolio",
                        ),
                        array(
                            'id' => 'section_header_nav_right_icon_tooltip',
                            'type' => 'text',
                            'title' => __('Section Header Right Navigation Icon Tooltip', 'glimmer-admin'),
                            'desc' => __('This icon will show right side link', 'glimmer-admin'),
                            'default' => "Portfolio",
                        ),
                        array(
                            'id' => 'section_header_nav_bottom_icon',
                            'type' => 'text',
                            'title' => __('Section Header Bottom Navigation Icon', 'glimmer-admin'),
                            'desc' => __('This icon will show bottom side link', 'glimmer-admin'),
                            'default' => "icon-contact",
                        ),
                        array(
                            'id' => 'section_header_nav_bottom_icon_url',
                            'type' => 'text',
                            'title' => __('Section Header Bottom Navigation Icon URL', 'glimmer-admin'),
                            'desc' => __('This icon will show bottom side link', 'glimmer-admin'),
                            'default' => "#contact",
                        ),
                        array(
                            'id' => 'section_header_nav_bottom_icon_tooltip',
                            'type' => 'text',
                            'title' => __('Section Header Bottom Navigation Icon Tooltip', 'glimmer-admin'),
                            'desc' => __('This icon will show bottom side link', 'glimmer-admin'),
                            'default' => "Contact",
                        ),
                        array(
                            'id' => 'section_header_nav_left_icon',
                            'type' => 'text',
                            'title' => __('Section Header Left Navigation Icon', 'glimmer-admin'),
                            'desc' => __('This icon will show left side link', 'glimmer-admin'),
                            'default' => "icon-about",
                        ),
                        array(
                            'id' => 'section_header_nav_left_icon_url',
                            'type' => 'text',
                            'title' => __('Section Header Left Navigation Icon URL', 'glimmer-admin'),
                            'desc' => __('This icon will show left side link', 'glimmer-admin'),
                            'default' => "#about-us",
                        ),
                        array(
                            'id' => 'section_header_nav_left_icon_tooltip',
                            'type' => 'text',
                            'title' => __('Section Header Left Navigation Icon Tooltip', 'glimmer-admin'),
                            'desc' => __('This icon will show left side link', 'glimmer-admin'),
                            'default' => "About Us",
                        ),                        
                        array(
                            'id'          => 'section_header_background_slider',
                            'type'        => 'slides',
                            'title'       => __( 'Header background Slider', 'glimmer-admin' ),
                            'display_value'       => __( 'Slides Optionsss', 'glimmer-admin' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => false,
                                ),                            
                            'subtitle'    => __( 'Unlimited slides with drag and drop sortings.', 'glimmer-admin' ),
                            'desc'        => __( 'This field will store all slides values into a multidimensional array to use into a foreach loop.', 'glimmer-admin' ),
                            'placeholder' => array(
                                'title'       => __( 'This is a title', 'glimmer-admin' ),
                            'required' => array('section_header_display', '=', '1')
                                
                            ),                       

                        ),


                    )
                )
            ); //header


            Redux::setSection( $opt_name, array(
                    'title' => __('About Us Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-user',
                    'fields' => array(                       
                        array(
                            'id' => 'section_aboutus_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_aboutus_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_aboutus_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "ABOUT US",
                            'required' => array('section_aboutus_display_menu', '=', '1')
                        ),

                        array(
                            'id' => "section_aboutus_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "About Glimmer",
                        ),
                        array(
                            'id' => "section_aboutus_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "Glimmer is a multi-purpose theme, that can be used for a wide variety of uses, it has clean lines and unique design that is sure to catch visitors eye!",
                        ),
                        array(
                            'id'       => 'section_aboutus_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_aboutus_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_aboutus_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ),                       
                        array(
                            'id' => 'section_aboutus_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=aboutus">here</a>', 'glimmer-admin'),
                        ),

                    )
                )
            ); //about us


            Redux::setSection( $opt_name, array(
                    'title' => __('Team Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-group',
                    'fields' => array(
                        array(
                            'id' => 'section_team_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_team_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_team_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "TEAMS",
                            'required' => array('section_team_display_menu', '=', '1')
                        ),
                        array(
                            'id' => "section_team_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "MEAT OUR TEAM",
                        ),
                        array(
                            'id' => "section_team_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "Professional And Outstanding Ideas Of Our Passionate Team Makes Us Unique In Every Sense.",
                        ),
                        array(
                            'id' => 'section_team_bg',
                            'type' => 'media',
                            'title' => __('Section Background', 'glimmer-admin'),
                            'subtitle' => __('This is section background parallax image', 'glimmer-admin'), 
                            'default' => array("url" => get_template_directory_uri() . "/images/parallax/parallax-team.jpg"),
                            'preview' => true,
                            "url" => true,

                        ),
                        array(
                            'id' => 'section_team_bg_color',
                            'type' => 'color',
                            'title' => __('Section background color', 'glimmer-admin'),
                            'default'=>''
                        ),
                        array(
                            'id'       => 'section_team_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_team_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_team_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ),                       
                        array(
                            'id' => 'section_team_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=teammember">here</a>', 'glimmer-admin'),
                        ),
                    )
                )
            ); //team

            Redux::setSection( $opt_name, array(
                    'title' => __('Skill Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-align-left',
                    'fields' => array(
                        array(
                            'id' => 'section_skill_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_skill_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 0,
                        ),
                        array(
                            'id' => 'section_skill_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "SKILL",
                            'required' => array('section_skill_display_menu', '=', '1')
                        ),
                        array(
                            'id' => "section_skill_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "OUR SKILLS",
                        ),
                        array(
                            'id' => "section_skill_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "We Are Skilled Enough To Make You Satisfied.",
                        ),
                        array(
                            'id'       => 'section_skill_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_skill_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_skill_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_skill_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=service">here</a>', 'glimmer-admin'),
                        ),

                    )
                )
            ); //skill


            Redux::setSection( $opt_name, array(
                    'title' => __('Services Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-wrench',
                    'fields' => array(
                        array(
                            'id' => 'section_services_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_services_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_services_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "SERVICE",
                            'required' => array('section_service_display_menu', '=', '1')

                        ),

                        array(
                            'id' => "section_services_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "OUR SERVICES",
                        ),
                        array(
                            'id' => "section_services_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "Our Intelligent Service Organized Customer Service By Increasing Efficiency, Ensuring Compliance And Delivering An Exceptional Experience.",
                        ),
                        array(
                            'id'       => 'section_services_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_services_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_services_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_services_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=service">here</a>', 'glimmer-admin'),
                        ),
                    )
                )
            ); //services

            Redux::setSection( $opt_name, array(
                    'title' => __('Pricing Table Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-th-list',
                    'fields' => array(
                        array(
                            'id' => 'section_pricing_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_pricing_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_pricing_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "PRICING",
                            'required' => array('section_pricing_display_menu', '=', '1')

                        ),

                        array(
                            'id' => "section_pricing_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "OUR PRICING PLANS",
                        ),
                        array(
                            'id' => "section_pricing_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "Compatible Pricing Plans To Suite Your Needs",
                        ),
                        array(
                            'id'       => 'section_pricing_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_pricing_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_pricing_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_pricing_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=pricingtable">here</a>', 'glimmer-admin'),
                        ),
                    )
                )
            ); //pricing

            Redux::setSection( $opt_name, array(
                    'title' => __('Portfolio Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-th-large',
                    'fields' => array(
                        array(
                            'id' => 'section_portfolio_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_portfolio_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_portfolio_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "PORTFOLIO",
                            'required' => array('section_portfolio_display_menu', '=', '1')
                        ),
                        array(
                            'id' => "section_portfolio_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "OUR PORTFOLIO",
                        ),
                        array(
                            'id' => "section_portfolio_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "Let's Point Out What We Can Help You With",
                        ),
                        array(
                            'id'       => 'section_portfolio_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_portfolio_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_portfolio_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_portfolio_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=portfolio">here</a>', 'glimmer-admin'),
                        ),
                    )
                )
            ); //portfolio


            Redux::setSection( $opt_name, array(
                    'title' => __('Testimonial Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-certificate',
                    'fields' => array(
                        array(
                            'id' => 'section_testimonial_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_testimonial_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_testimonial_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "TESTIMONIAL",
                            'required' => array('section_testimonial_display_menu', '=', '1')
                        ),

                        array(
                            'id' => "section_testimonial_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "OUR CLIENT SAYS",
                        ),
                        array(
                            'id' => 'section_testimonial_bg',
                            'type' => 'media',
                            'title' => __('Section Background', 'glimmer-admin'),
                            'subtitle' => __('This is section background parallax image', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/parallax/parallax-testimonial.jpg"),
                            'preview' => true,
                            "url" => true,

                        ),
                        array(
                            'id' => 'section_testimonial_bg_color',
                            'type' => 'color',
                            'title' => __('Section Background Color', 'glimmer-admin'),
                            'subtitle' => __('If you choose background color then background parallax image will not show.', 'glimmer-admin'),
                            'default'=>''
                        ),
                        array(
                            'id'       => 'section_testimonial_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ),
                        array(
                            'id' => 'section_testimonial_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ),  
                        array(
                            'id' => 'section_testimonial_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_testimonial_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=testimonial">here</a>', 'glimmer-admin'),
                        ),
                    )
                )
            ); //testimonial


            Redux::setSection( $opt_name, array(
                    'title' => __('Funfact Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-coffee',
                    'fields' => array(
                        array(
                            'id' => 'section_funfact_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_funfact_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 0,
                        ),
                        array(
                            'id' => 'section_funfact_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "FUNFACT",
                            'required' => array('section_funfact_display_menu', '=', '1')
                        ),
                        array(
                            'id' => 'section_funfact_bg',
                            'type' => 'media',
                            'title' => __('Section Background', 'glimmer-admin'),
                            'subtitle' => __('This is section background parallax image', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/parallax/parallax-funfact.jpg"),
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id' => 'section_funfact_bg_color',
                            'type' => 'color',
                            'title' => __('Section background color', 'glimmer-admin'),
                            'default'=>''
                        ),
                        array(
                            'id'       => 'section_funfact_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ),
                        array(
                            'id' => 'section_funfact_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ),  
                        array(
                            'id' => 'section_funfact_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ),
                        array(
                            'id' => 'section_funfact_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=funfact">here</a>', 'glimmer-admin'),
                        ),
                    )
                )
            ); //funfact 


            Redux::setSection( $opt_name, array(
                    'title' => __('Happy Client Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-smile-o',
                    'fields' => array(
                         array(
                        'id' => 'section_client_display',
                        'type' => 'switch',
                        'title' => __('Display Section', 'glimmer-admin'),
                        'default' => 1,
                        ),
                        array(
                            'id' => 'section_client_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 0,
                        ),
                        array(
                            'id' => 'section_client_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "CLIENT",
                            'required' => array('section_client_display_menu', '=', '1')
                        ),

                        array(
                            'id' => "section_client_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "OUR HAPPY CLIENTS",
                        ),
                        array(
                            'id' => "section_client_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "The Well-known Brands And Clients We Corporate With",
                        ),
                        array(
                            'id' => 'section_client_bg',
                            'type' => 'media',
                            'title' => __('Section Background', 'glimmer-admin'),
                            'subtitle' => __('This is section background parallax image', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/parallax/parallax-happyclient.jpg"),
                            'preview' => true,
                            "url" => true,

                        ),
                        array(
                            'id' => 'section_client_bg_color',
                            'type' => 'color',
                            'title' => __('Section background color', 'glimmer-admin'),
                            'default'=>''
                        ),
                        array(
                            'id'       => 'section_happyclient_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_happyclient_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_happyclient_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_happyclient_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=happyclient">here</a>', 'glimmer-admin'),
                        ),
                    ) 
                )
            ); //client

            Redux::setSection( $opt_name, array(
                    'title' => __('Blog Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-file-text',
                    'fields' => array(
                        array(
                            'id' => 'section_blog_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_blog_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_blog_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "BLOG",
                            'required' => array('section_blog_display_menu', '=', '1')
                        ),

                        array(
                            'id' => "section_blog_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "Blog",
                        ),
                        array(
                            'id' => "section_blog_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "Keep On Track Of The Latest News And Updates",
                        ),
                        array(
                            'id'       => 'section_blog_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_blog_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_blog_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ), 
                        array(
                            'id' => 'section_blog_info',
                            'type' => 'info',
                            'title' => __('Create new content for this section from <a href="'.site_url().'/wp-admin/post-new.php?post_type=post">here</a>', 'glimmer-admin'),
                        ),
                    )   
                )
            ); //blog

            Redux::setSection( $opt_name, array(
                    'title' => __('Contact Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-phone',
                    'fields' => array(
                        array(
                            'id' => 'section_contact_display',
                            'type' => 'switch',
                            'title' => __('Display Section', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_contact_display_menu',
                            'type' => 'switch',
                            'title' => __('Display In Menubar', 'glimmer-admin'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'section_contact_menu_text',
                            'type' => 'text',
                            'title' => __('Section Title in Menubar', 'glimmer-admin'),
                            'default' => "CONTACT",
                            'required' => array('section_contact_display_menu', '=', '1')
                        ),

                        array(
                            'id' => "section_contact_title",
                            'type' => 'text',
                            'title' => __('Section Title', 'glimmer-admin'),
                            'default' => "CONTACT",
                        ),
                        array(
                            'id' => "section_contact_subtitle",
                            'type' => 'text',
                            'title' => __('Section Subtitle', 'glimmer-admin'),
                            'default' => "We Provides A Wide Array Of Specialized Advisory And Strategic Service For Our Clients",
                        ),
                        array(
                            'id'       => 'section_contact_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for section header title and subtitle area', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_contact_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_contact_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for section header title and subtitle area', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ),
                        array(
                            'id' => 'section_contact_lat',
                            'type' => 'text',
                            'title' => __('Latitude', 'glimmer-admin'),
                            'default' => "-37.817314",
                        ),
                        array(
                            'id' => 'section_contact_lon',
                            'type' => 'text',
                            'title' => __('Longitude', 'glimmer-admin'),
                            'default' => "144.955431",
                        ),
                        array(
                            'id' => 'section_contact_map_point_img',
                            'type' => 'media',
                            'title' => __('Section Contact Map Point Image', 'glimmer-admin'),
                            'subtitle' => __('This image will show in the google map of your location point', 'glimmer-admin'),
                            'default' => array("url" => get_template_directory_uri() . "/images/map-icon.png"),
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id' => 'section_contact_receiver',
                            'type' => 'text',
                            'title' => __('Contact Email Receiver', 'glimmer-admin'),
                            'default'=> "youremail@domain.com"
                        ),
                        array(
                            'id' => 'section_contact_success_message',
                            'type' => 'text',
                            'title' => __('Contact Successful Message', 'glimmer-admin'),
                            'default'=> "Thank you for contact us. As early as possible  we will contact you"
                        ),
                         array(
                            'id'       => 'section_contact_form_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for contact form', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_contact_form_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for contact form', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_contact_form_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for contact form', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ),
                        array(
                            'id' => 'section_contact_description',
                            'type' => 'editor',
                            'title' => __('Marker Details', 'glimmer-admin'),
                            'default'=>'<header class="adress-title">
                                <h2>Where we are</h2>
                            </header> <!-- /.header -->
                            <div class="main-address">
                                <h5>Softhopper Creative Agency</h5>
                                <h5>02 No. Dharmondi, Dhaka. Bd</h5>
                            </div> <!-- /.main-address -->

                            <div class="email-me">
                                <h5>Pho: 01234567890</h5>
                                <h5>E-mail : <a href="mailto:#">softhopperbd@gmail.com</a></h5>  
                            </div> <!-- /.email-me -->',
                        ),
                         array(
                            'id'       => 'section_contact_address_animation_style',
                            'type'     => 'select',
                            'title'    => __( 'Animation Style', 'glimmer-admin' ),
                            'subtitle' => __( 'This animation for contact address', 'glimmer-admin' ),                            
                            //Must provide key => value pairs for select options
                            'options'  => $animation_style_list_array,
                            'default'  => 'fadeIn'
                        ), 
                        array(
                            'id' => 'section_contact_address_animation_duration',
                            'type' => 'text',
                            'title' => __('Animation Duration', 'glimmer-admin'),
                            'subtitle' => __( 'This animation duration for contact address', 'glimmer-admin' ),
                            'description' => __('To give animation duration use like: 2s (s for second)', 'glimmer-admin'),
                        ), 
                        array(
                            'id' => 'section_contact_address_animation_delay',
                            'type' => 'text',
                            'title' => __('Animation Delay', 'glimmer-admin'),
                            'subtitle' => __( 'This animation delay for contact address', 'glimmer-admin' ),
                            'description' => __('How much delay animation will start (s for second)', 'glimmer-admin'),
                            'default' => "0.5s",
                        ),
                    )
                )
            ); //contact

            Redux::setSection( $opt_name, array(
                    'title' => __('Footer Section', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-edit',
                    'fields' => array(  

                        array(
                            'id' => 'section_footer_theme_name',
                            'type' => 'text',
                            'title' => __('Footer Theme Name', 'glimmer-admin'),
                            'default' => 'GLIMMER'
                        ),                     
                       
                        array(
                            'id'          => 'section_footer_social_link',
                            'type'        => 'glimmer_slides',
                            'title'       => __( 'Add Unlimited Social Link', 'glimmer-admin' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),                            
                            'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'glimmer-admin' ),
                            'placeholder' => array(
                                'title'       => __( 'Add font awesome Icon Class', 'glimmer-admin' ),
                                'url'       => __( 'Social Icon Url', 'glimmer-admin' ),
                            ),                                                                     
                        ),

                        array(
                            'id' => 'section_footer_copyright_info',
                            'type' => 'textarea',
                            'title' => __('Footer Copyright Info', 'glimmer-admin'),
                            'default' => '&copy; Glimmer, All right reserverd-2015<br>
                            Designed By - <a href="#">SoftHopper</a>'
                        ),
                    )
                )
            ); //footer

            Redux::setSection( $opt_name, array(
                    'title' => __('404 Settings', 'glimmer-admin'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-question-circle',                
                    'fields' => array(
                        array(
                            'id'=>'settings_404_heading',
                            'type' => 'text',
                            'title' => __('404  Title', 'glimmer-admin'),
                            'default'=>'Whoops! Page Not Found'
                        ),
                        array(
                            'id'=>'settings_404_text',
                            'type' => 'text',
                            'title' => __('404 Text', 'glimmer-admin'),
                            'default'=>"40<span>4</span>"
                        ),
                        array(
                            'id'=>'settings_404_subheading',
                            'type' => 'text',
                            'title' => __('404 Sub Title', 'glimmer-admin'),
                            'default'=>'The page you are looking for doesn\'t exit.'
                        ),
                        
                    )
                )
            ); //404   
    /*
     * <--- END SECTIONS
     */

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    function compiler_action( $options, $css, $changed_values ) {
        echo '<h1>The compiler hook has run!</h1>';
        echo "<pre>";
        print_r( $changed_values ); // Values that have changed since the last save
        echo "</pre>";
        //print_r($options); //Option values
        //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use get_template_directory_uri() if you want to use any of the built in icons
     * */
    function dynamic_section( $sections ) {
        //$sections = array();
        $sections[] = array(
            'title'  => __( 'Section via hook', 'redux-framework-demo' ),
            'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'redux-framework-demo' ),
            'icon'   => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    function change_arguments( $args ) {
        //$args['dev_mode'] = true;

        return $args;
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    function change_defaults( $defaults ) {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }

    // Remove the demo link and the notice of integrated demo from the redux-framework plugin
    function remove_demo() {

        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_filter( 'plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2 );

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
        }
    }