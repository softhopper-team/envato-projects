<?php
/**
 * The template for displaying related posts in the single page
 *
 * @package Candid
 */
?>
<?php
    global $post;
    $tags = wp_get_post_tags($post->ID);
     
    if ($tags) {
    $tag_ids = array();
    foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
    $args=array(
        'tag__in' => $tag_ids,
        'post__not_in' => array($post->ID),
        'posts_per_page'=> 5, // Number of related posts to display.
    );
     
    $my_query = new wp_query( $args );
 
?>
<?php
    if( $my_query->have_posts() ) :
?>
<!-- related post -->
<div id="related-post">
    <h3>
        <?php _e('Related Posts','candid'); ?>
        <span class="small-border"></span>  <!-- /.small-border --> 
    </h3>  
    <div id="related-post-slide" class="owl-carousel">            
        <?php
            while( $my_query->have_posts() ) {
            $my_query->the_post();
        ?>
        <div class="item">
            <article class="post">   
                <?php
                    $meta = get_post_meta( $post->ID );
                    $post_format = get_post_format();
                    if ( false === $post_format ) {
                        $post_format = "standard";
                    } 
                    if ( $post_format == "audio" ) {
                            ?>
                            <div class="post-thumbnail">
                                <?php              
                                    if( isset ( $meta["_candid_format_audio"][0] ) ) echo $meta["_candid_format_audio"][0];
                                ?>
                            </div> <!-- /.post-thumbnail -->
                            <?php
                    } elseif ( $post_format == "video" ) {
                            ?>
                            <div class="post-thumbnail">
                                <?php              
                                    if( isset ( $meta["_candid_format_video"][0] ) ) echo $meta["_candid_format_video"][0];
                                ?>
                            </div> <!-- /.post-thumbnail -->
                            <?php
                    } else {
                            ?>
                            <div class="post-thumbnail">
                                <a href="<?php the_permalink();?>">                        
                                    <?php                                        
                                        $alt_text = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('related-posts', array ( 
                                                'class' => " img-responsive",
                                                'alt' => $alt_text
                                                )
                                            );
                                        } else {
                                        ?>
                                        <img class="img-responsive" alt="No Thumb" src="<?php echo get_template_directory_uri(); ?>/images/blog-no-medium.gif">
                                        <?php
                                        }
                                    ?>                       
                                </a> 
                            </div> <!-- /.post-thumbnail -->
                            <?php
                    }
                ?>
                <header class="entry-header">
                    <h2 class="entry-title">
                        <a rel="bookmark" href="<?php the_permalink();?>"><?php the_title(); ?></a>
                    </h2>
                    <div class="entry-meta">
                        <span class="byline">
                            <span class="author vcard">
                                <?php _e('By ','candid').the_author_posts_link(); ?>
                            </span>
                        </span>
                        <span class="cat-links">
                            <?php _e('In ','candid').the_category( ', ' ); ?>
                        </span>
                        <span class="entry-date">
                            <?php _e('On ','candid').the_time( get_option( 'date_format' ) ); ?>
                        </span>
                    </div><!-- .entry-meta -->
                    
                </header><!-- .entry-header -->

                <div class="entry-content">
                    <p>
                        <?php 
                            $content = get_the_content();
                            echo substr($content, 0, 250)."&#46;&#46;&#46;";
                        ?>
                    </p> 
                    
                    <a class="more-link button" href="<?php the_permalink()?>">
                        <span class="more-button"><?php _e('Continue Reading', 'candid'); ?></span>
                    </a>  
                </div><!-- .entry-content -->
            </article> <!-- /.post -->   
        </div> <!-- /.item --> 
        <?php   
            } // end while loop
        ?> 
    </div> <!-- /.related-post-slide -->
</div><!-- /#related-post -->
 <?php 
    endif;  
    } // end if tags condition
    wp_reset_query();
?> 