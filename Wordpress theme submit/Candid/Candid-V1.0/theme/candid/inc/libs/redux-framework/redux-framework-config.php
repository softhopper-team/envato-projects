<?php
    /**
     * Candid Theme Options
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "candid";


    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'images' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /*
     *
     * --> Action hook examples
     *
     */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');


    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'submenu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Candid Options', 'candid' ),
        'page_title'           => __( 'Candid Options', 'candid' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // remove tracking 
        'disable_tracking' => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 100,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => THEME_URL . "/images/menuicon/setting.png",
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

 
    

    // Add content after the form.
    $args['footer_text'] = __( '<p></p>', 'candid' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'candid' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'candid' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'candid' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'candid' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'candid' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields



    // -> START Typography
    //Redux::setSection( $opt_name, array() );
    //Redux::setSection( $opt_name, array() );
    //print_r(get_option('candid_sections'));
        // this array contains all animation style list
            
            Redux::setSection( $opt_name, array(
                    'title' => __('General Options', 'candid' ),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'el el-cog',
                    'fields' => array (    
                        array (
                            // this is load because if it not load other slides extension not work
                            'id'          => 'candid_hidden_slides',
                            'type'        => 'slides',
                            'title'       => __( 'Candid Hidden Slider', 'candid' ),
                        ),
                        array(
                            'id'=>'preloader',
                            'type' => 'switch',
                            'title' => __('Site Preloader', 'candid'),
                            'default' => true
                        ),
                        array(
                            'id'=>'favicon',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Favicon', 'candid'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/favicon.ico'
                            )
                        ),

                        array(
                            'id'=>'icon_iphone',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPhone Icon', 'candid'),
                            'desc' => __('Icon for Apple iPhone (57px X 57px)', 'candid'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon.png'
                            )
                        ),

                        array(
                            'id'=>'icon_iphone_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPhone Retina Icon', 'candid'),
                            'desc' => __('Icon for Apple iPhone Retina (114px X 114px)', 'candid'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon_114x114.png'
                            )
                        ),

                        array(
                            'id'=>'icon_ipad',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPad Icon', 'candid'),
                            'desc' => __('Icon for Apple iPad (72px X 72px)', 'candid'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon_72x72.png'
                            )
                        ),

                        array(
                            'id'=>'icon_ipad_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPad Retina Icon', 'candid'),
                            'desc' => __('Icon for Apple iPad Retina (144px X 144px)', 'candid'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon_144x144.png'
                            )
                        ),    
                        array(
                            'id'       => 'custom_css',
                            'type'     => 'ace_editor',
                            'title'    => __( 'Custom CSS Code', 'candid' ),
                            'subtitle' => __( 'Paste your CSS code here.', 'candid' ),
                            'mode'     => 'css',
                            'theme'    => 'monokai',
                            'default'  => "#header {\n   margin: 0 auto;\n}"
                        ),
                        array(
                            'id'       => 'custom_js',
                            'type'     => 'ace_editor',
                            'title'    => __( 'Custom JS Code', 'candid' ),
                            'subtitle' => __( 'Paste your JS code here.', 'candid' ),
                            'mode'     => 'javascript',
                            'theme'    => 'monokai',
                            'default'  => "jQuery(document).ready(function($){\n\n});"
                        ),
                        array(
                            'id'       => 'custom_ga',
                            'type'     => 'textarea',
                            'title'    => __( 'Google Analytics Code', 'candid' ),
                            'subtitle' => __( 'Paste your Google Analytics code here.', 'candid' ),
                            'desc'     => 'Write your custom google analytics code here with &lt;script> &lt;/script> tag block',
                            'default'  => ""
                        ),                      
                    )
                ) 
            ); //general

            Redux::setSection( $opt_name, array(
                    'title' => __('Layout', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-th-list',
                    'fields' => array ( 
                        array(
                            'id'       => 'rtl_support',
                            'type'     => 'switch',
                            'title'    => __( 'RTL Language Support', 'candid' ),
                            'subtitle' => __( 'You can support RTL language ( Like: Arabic, Urdu etc) from here', 'candid' ),
                            'default'  => 0
                        ),  
                        array (
                            'id' => 'layout',
                            'type' => 'image_select',
                            'title' => __('Box or Wide Layout', 'candid'),
                            'subtitle' => __('Select box or wide layout for whole site', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'box.png',
                                    'img' => THEME_URL . '/images/backend/layout/box.png'
                                ),
                                '2' => array(
                                    'alt' => 'wide.png',
                                    'img' => THEME_URL . '/images/backend/layout/wide.png'
                                ),
                            ),
                            'default' => '1'
                        ),
                        array (
                            'id' => 'sidebar_layout',
                            'type' => 'image_select',
                            'title' => __('Default Layout', 'candid'),
                            'subtitle' => __('Default layout for whole site', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),
                        array (
                            'id' => 'sidebar_layout_single',
                            'type' => 'image_select',
                            'title' => __('Single Layout', 'candid'),
                            'subtitle' => __('Default layout of single post', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),    
                        array (
                            'id' => 'sidebar_layout_page',
                            'type' => 'image_select',
                            'title' => __('Page Layout', 'candid'),
                            'subtitle' => __('Default layout of single page ', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),  

                        array (
                            'id' => 'sidebar_layout-archive',
                            'type' => 'image_select',
                            'title' => __('Archive Layout', 'candid'),
                            'subtitle' => __('Default layout of category, archive, tag page ', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),                  
                           
                        
                    )
                ) 
            ); //Layout

            Redux::setSection( $opt_name, array(
                    'title' => __('Typography', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-font',
                    'fields' => array (  
                        array(
                            'id'       => 'body-font',
                            'type'     => 'typography',
                            'title'    => __( 'Body Font', 'candid' ),
                            'subtitle' => __( 'You can Specify the body font properties.', 'candid' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('body'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '0.938em',
                                'line-height'   => '1.45em',
                                'font-family' => 'Roboto',
                                'font-weight' => '400',
                            ),
                        ), 
                        array(
                            'id'       => 'featured-post-title',
                            'type'     => 'typography',
                            'title'    => __( 'Featured Post Title Font', 'candid' ),
                            'subtitle' => __( 'You can Specify the featured post title font properties.', 'candid' ),
                            'google'   => true,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'line-height'=> true,
                            'font-weight'=>true,
                            'color'    => false,
                            'output'      => array('.featured-area .post-title'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-family' => 'Roboto',
                                'font-size' => '2.441em',
                                'line-height' => '1.45em',
                                'font-weight' => '300',
                                'font-style' => 'italic',
                                //'color' => '#666',
                            ),
                        ), 
                        array(
                            'id'       => 'post-title',
                            'type'     => 'typography',
                            'title'    => __( 'Post Title Font', 'candid' ),
                            'subtitle' => __( 'You can Specify the post title font properties.', 'candid' ),
                            'google'   => true,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'subsets'  =>false,
                            'font-style'=> false,
                            'font-backup' => false,
                            'font-size' => true,
                            'line-height'=> true,
                            'font-weight'=>true,
                            'color'    => false,
                            'output'      => array('h2.entry-title'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-family' => 'Roboto',
                                'font-size' => '1.953em',
                                'line-height' => '1.2em',
                                'font-weight' => '400',
                                //'color' => '#666',
                            ),
                        ), 
                        array(
                            'id'       => 'heading-font',
                            'type'     => 'typography',
                            'title'    => __( 'Heading Font', 'candid' ),
                            'subtitle' => __( 'You can Specify the heading font properties from h1 to h6.', 'candid' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> false,
                            'subsets'  =>false,
                            'font-style'=> false,
                            'font-backup' => false,
                            'font-size' => false,
                            'font-weight'=>false,
                            'output'      => array('h1, h2, h3, h4, h5, h6'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-family' => 'Roboto',
                            ),
                        ), 
                        array(
                            'id'       => 'site-title',
                            'type'     => 'typography',
                            'title'    => __( 'Site Title Font', 'candid' ),
                            'subtitle' => __( 'You can Specify the site title font properties.', 'candid' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> false,
                            'subsets'  =>false,
                            'font-style'=> false,
                            'font-backup' => false,
                            'font-size' => false,
                            'font-weight'=>false,
                            'output'      => array('.site-title'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-family' => 'Pacifico',
                            ),
                        ),                   
                                               
                        
                    )
                ) 
            ); //Typography

            Redux::setSection( $opt_name, array(
                    'title' => __('Style', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-life-ring',
                    'fields' => array (                        
                        array (
                            'id' => 'candid_color_scheme',
                            'type' => 'image_select',
                            'title' => __('Color Scheme', 'candid'),
                            'subtitle' => __('Select Predefined Color Schemes or your Own', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'hex-80b92e.gif',
                                    'img' => THEME_URL . '/images/colors/hex-80b92e.gif'
                                ),
                                '2' => array(
                                    'alt' => 'hex-f26d7e.gif',
                                    'img' => THEME_URL . '/images/colors/hex-f26d7e.gif'
                                ),
                                '3' => array(
                                    'alt' => 'hex-044f67.gif',
                                    'img' => THEME_URL . '/images/colors/hex-044f67.gif'
                                ),
                                '4' => array(
                                    'alt' => 'hex-006442.gif',
                                    'img' => THEME_URL . '/images/colors/hex-006442.gif'
                                ),
                                '5' => array(
                                    'alt' => 'hex-1691bd.gif',
                                    'img' => THEME_URL . '/images/colors/hex-1691bd.gif'
                                ),
                                '6' => array(
                                    'alt' => 'hex-067b82.gif',
                                    'img' => THEME_URL . '/images/colors/hex-067b82.gif'
                                ),
                                '7' => array(
                                    'alt' => 'hex-34495e.gif',
                                    'img' => THEME_URL . '/images/colors/hex-34495e.gif'
                                ),
                                '8' => array(
                                    'alt' => 'hex-d2527f.gif',
                                    'img' => THEME_URL . '/images/colors/hex-d2527f.gif'
                                ),
                                '9' => array(
                                    'alt' => 'Choose Your One',
                                    'img' => THEME_URL . '/images/colors/white.png'
                                )
                            ),
                            'default' => '1'
                        ),

                        array(
                            'id' => 'candid_custom_color',
                            'type' => 'color',
                            'title' => __('Your Own Theme Color', 'candid'),
                            'subtitle' => __('Pick a custom color', 'candid'),
                            'default' => '#FFFFFF',
                            'validate' => 'color',
                            'required' => array("candid_color_scheme", "=", "9")
                        ),

                        array(
                            'id' => 'candid_custom_hover_color',
                            'type' => 'color',
                            'title' => __('Choose a hover color', 'candid'),
                            'subtitle' => __('This is hover, focus and active color.', 'candid'),
                            'default' => '#9CCA5A',
                            'validate' => 'color',
                        ),
                                           
                       
                    )
                ) 
            ); //Style

            Redux::setSection( $opt_name, array(
                    'title' => __('Header Section', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-laptop',
                    'fields' => array(                   
                        array(
                            'id' => 'header_nav_logo',
                            'type' => 'media',
                            'title' => __('Main Logo', 'candid'),
                            'subtitle' => __('This image will show in the navigation', 'candid'),
                            'default' => array("url" => THEME_URL . "/images/logo.png"),
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id' => 'header_bg_img',
                            'type' => 'media',
                            'title' => __('Header background image', 'candid'),
                            'subtitle' => __('This header background image will show below top menu', 'candid'), 
                            'default' => array("url" => THEME_URL . "/images/header-bg.jpg"),
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id' => 'header_bg_color',
                            'type' => 'color',
                            'title' => __('Header background color', 'candid'),
                            'subtitle' => __('This background color will show when header background image not load', 'candid'),
                            'default'=>'#dfdfdf'
                        ),   
                    )
                )
            ); //header

            Redux::setSection( $opt_name, array(
                    'title' => __('Featured Post', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-star',
                    'fields' => array(                       
                        array(
                            'id' => 'featured_display',
                            'type' => 'switch',
                            'title' => __('Display Featured Post', 'candid'),
                            'default' => 1,
                        ),
                        array(
                            'id'       => 'featured_per_page',
                            'type'     => 'select',
                            'title'    => __( 'Number of featured post', 'candid' ),
                            'subtitle' => __( 'How many featured post you want to show', 'candid' ),
                            //Must provide key => value pairs for select options
                            'options'  => array(
                                '1' => '1',
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',
                                '5' => '5',
                                '6' => '6',
                                '7' => '7',
                                '8' => '8',
                                '9' => '9',
                                '10' => '10',
                            ),
                            'default'  => '3'
                        ),
                        array(
                            'id'       => 'featured_slide_delay',
                            'type'     => 'text',
                            'title'    => __( 'Featured slide interval time', 'candid' ),
                            'desc'    => __( 'Value 1000 = 1 Second', 'candid' ),
                            'subtitle' => __( 'How much delay featured post will slide', 'candid' ),
                            'default'  => '3500'
                        ),
                    )
                )
            ); //featured post


            Redux::setSection( $opt_name, array(
                    'title' => __('About Me Page', 'candid'),
                    'icon' => 'fa fa-user',
                    'fields' => array( 
                        array(
                            'id' => 'author_img',
                            'type' => 'media',
                            'title' => __('Author Image', 'candid'),
                            'subtitle' => __('This image will show in the navigation', 'candid'),
                            'default' => array("url" => THEME_URL . "/images/author/author-image.jpg"),
                            'preview' => true,
                            "url" => true,
                        ),                      
                        array(
                            'id' => 'author_name',
                            'type' => 'text',
                            'title' => __('Author Name', 'candid'),
                            'subtitle' => __( 'This author will show in about page below author image', 'candid' ),
                            'default' => "Johan Smith",
                        ),  
                        array(
                            'id'      => 'author_description',
                            'type'    => 'editor',
                            'title'   => __( 'Author description', 'candid' ),
                            'default' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis pellentesque aug ue nec rhoncus. Nam ut orci augue. Phase llus ac venenatis orci. Nullam iaculis lao reet massa, vitae tempus ante tincidunte et. Suspendisse iaculis laoreet eros, a vee natis metus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis pellentesque aug ue nec rhoncus. Nam ut orci augue. Phase llus ac venenatis orci. Nullam iaculis lao reet massa, vitae tempus ante tincidunte et. Suspendisse iaculis laoreet eros, a vee natis metus.</p>',
                            'args'    => array(
                                'wpautop'       => false,
                                'media_buttons' => false,
                                'textarea_rows' => 5,
                                'teeny'         => false,
                                'quicktags'     => false,
                            )
                        ), 
                        array (
                            'id' => 'author_sign',
                            'type' => 'media',
                            'title' => __('Author Singnature', 'candid'),
                            'subtitle' => __('This image will show in the navigation', 'candid'),
                            'default' => array("url" => THEME_URL . "/images/author/sign.png"),
                            'preview' => true,
                            "url" => true,
                        ), 
                        array(
                            'id'          => 'author_social_link',
                            'type'        => 'candid_slides',
                            'title'       => __( 'Add Follow Link', 'candid' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),
                            'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'candid' ),
                            'placeholder' => array(
                                'title'       => __( 'Add font awesome Icon Class', 'candid' ),
                                'url'       => __( 'Social Icon Url', 'candid' ),
                            ),
                            'default' => array(
                                0 => array(
                                    'title' => 'fa-facebook',
                                    'url' => '#'
                                ),
                                1 => array(
                                    'title' => 'fa-twitter',
                                    'url' => '#'
                                ),
                                2 => array(
                                    'title' => 'fa-google-plus',
                                    'url' => '#'
                                ),
                            ),
                        ), 
                        array(
                            'id'          => 'author_skills',
                            'type'        => 'candid_slides_skill',
                            'title'       => __( 'Add author skills', 'candid' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),
                            'subtitle'    => __( "Add your skills for about page skill section", 'candid' ),
                            'placeholder' => array(
                                'title'       => __( 'Add skill name', 'candid' ),
                                'url'       => __( 'Add skill percent', 'candid' ),
                            ),
                            'default' => array(
                                0 => array(
                                    'title' => 'Article Writing',
                                    'url' => '54%'
                                ),
                                1 => array(
                                    'title' => 'Wordpress',
                                    'url' => '90%'
                                ),
                                2 => array(
                                    'title' => 'HTML/CSS',
                                    'url' => '80%'
                                ),
                                3 => array(
                                    'title' => 'Pogramming',
                                    'url' => '92%'
                                ),
                            ),
                        ),                   
                    )
                )
            ); //about me

            Redux::setSection( $opt_name, array(
                    'title' => __('Contact Page', 'candid'),
                    'icon' => 'fa fa-phone',
                    'fields' => array(
                        array(
                            'id' => 'contact_lat',
                            'type' => 'text',
                            'title' => __('Latitude', 'candid'),
                            'default' => "-37.817314",
                        ),
                        array(
                            'id' => 'contact_lon',
                            'type' => 'text',
                            'title' => __('Longitude', 'candid'),
                            'default' => "144.955431",
                        ),
                        array(
                            'id' => 'map_mouse_wheel',
                            'type' => 'switch',
                            'title' => __('Map Mouse Wheel', 'candid'),
                            'default' => true
                        ),
                        array(
                            'id' => 'map_zoom_control',
                            'type' => 'switch',
                            'title' => __('Map zoomControl', 'candid'),
                            'default' => true
                        ),
                        array(
                            'id' => 'contact_map_point_img',
                            'type' => 'media',
                            'title' => __('Contact Map Point Image', 'candid'),
                            'subtitle' => __('This image will show in the google map of your location point', 'candid'),
                            'default' => array("url" => THEME_URL . "/images/map-icon.png"),
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id' => 'contact_description',
                            'type' => 'editor',
                            'title' => __('Contact Description', 'candid'),
                            'default'=> '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis pellentesque aug ue nec rhoncus. Nam ut orci augue. Phase llus ac venenatis orci. Nullam iaculis lao reet massa, vitae tempus ante tincidunte et. Suspendisse iaculis laoreet eros, a vee natis metus.</p>',
                        ),
                        array(
                            'id' => 'contact_address',
                            'type' => 'editor',
                            'title' => __('Contact Address', 'candid'),
                            'default'=>'<h4>Creative Agency, Dhaka, Bangladesh</h4>
                                    <dl class="dl-horizontal">
                                        <dt>Phone :</dt>
                                        <dd>+088(017) 1123 - 987654321</dd>
                                        <dt>Fax :</dt>
                                        <dd>+088(142) 564-2225632</dd>
                                        <dt>E-mail :</dt>
                                        <dd><a href="mailto:#">candid@domain.net</a></dd>  
                                        <dt>Web :</dt>
                                        <dd><a href="#">http://cadidcompany.net</a></dd>       
                                    </dl>',
                        ),
                        array(
                            'id' => 'contact_receiver',
                            'type' => 'text',
                            'title' => __('Contact Email Receiver', 'candid'),
                            'default'=> "youremail@domain.com"
                        ),
                        array(
                            'id' => 'contact_success_message',
                            'type' => 'text',
                            'title' => __('Contact Successful Message', 'candid'),
                            'default'=> "Thank you for contact us. As early as possible  we will contact you"
                        ),
                    )
                )
            ); //contact

            Redux::setSection( $opt_name, array(
                    'title' => __('Footer Section', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-edit',
                    'fields' => array(  
                        array(
                            'id'       => 'footer_widgets',
                            'type'     => 'switch',
                            'title'    => __( 'Enable Footer Widget', 'candid' ),
                            'subtitle' => __( 'You can off or on footer widgets', 'candid' ),
                            'default'  => true,
                        ),
                        array (
                            'id' => 'footer_widget_columns',
                            'type' => 'image_select',
                            'required' => array( 'footer_widgets', '=', '1' ),
                            'title' => __('Default Layout', 'candid'),
                            'subtitle' => __('How many sidebar you want to show on footer', 'candid'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'one-column.png',
                                    'img' => THEME_URL . '/images/backend/footer/one-column.png'
                                ),
                                '2' => array(
                                    'alt' => 'two-columns.png',
                                    'img' => THEME_URL . '/images/backend/footer/two-columns.png'
                                ),
                                '3' => array(
                                    'alt' => 'three-columns.png',
                                    'img' => THEME_URL . '/images/backend/footer/three-columns.png'
                                ), 
                                '4' => array(
                                    'alt' => 'four-columns.png',
                                    'img' => THEME_URL . '/images/backend/footer/four-columns.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),                    
                        array(
                            'id'      => 'footer_copyright_info',
                            'type'    => 'editor',
                            'title'   => __( 'Footer Copyright Info', 'candid' ),
                            'default' => 'Copyright &copy; 2015 All right reserved Candid. By <a href="#">SoftHopper</a> ',
                            'args'    => array(
                                'wpautop'       => false,
                                'media_buttons' => false,
                                'textarea_rows' => 5,
                                //'tabindex' => 1,
                                //'editor_css' => '',
                                'teeny'         => false,
                                //'tinymce' => array(),
                                'quicktags'     => false,
                            )
                        ),
                        array(
                            'id'          => 'footer_social_link',
                            'type'        => 'candid_slides',
                            'title'       => __( 'Add Social Link', 'candid' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),                            
                            'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'candid' ),
                            'placeholder' => array(
                                'title'       => __( 'Add font awesome Icon Class', 'candid' ),
                                'url'       => __( 'Social Icon Url', 'candid' ),
                            ), 
                            'default' => array(
                                0 => array(
                                    'title' => 'fa-facebook',
                                    'url' => '#'
                                ),
                                1 => array(
                                    'title' => 'fa-twitter',
                                    'url' => '#'
                                ),
                                2 => array(
                                    'title' => 'fa-google-plus',
                                    'url' => '#'
                                ),
                            ),                                                                    
                        ),
                    )
                )
            ); //footer

            Redux::setSection( $opt_name, array(
                    'title' => __('404 Settings', 'candid'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-question-circle',                
                    'fields' => array(
                        array(
                            'id' => '404_img',
                            'type' => 'media',
                            'title' => __('404  Image', 'candid'),
                            'subtitle' => __('This image will show in the navigation', 'candid'),
                            'default' => array("url" => THEME_URL . "/images/404.png"),
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id'=>'404_heading',
                            'type' => 'text',
                            'title' => __('404  Title', 'candid'),
                            'default'=>'Whoops! Page Not Found'
                        ),
                        array(
                            'id'=>'404_subheading',
                            'type' => 'text',
                            'title' => __('404 Sub Title', 'candid'),
                            'default'=>'The page you are looking for doesn\'t exit.'
                        ),
                    )
                )
            ); //404   
    /*
     * <--- END SECTIONS
     */

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    function compiler_action( $options, $css, $changed_values ) {
        echo '<h1>The compiler hook has run!</h1>';
        echo "<pre>";
        print_r( $changed_values ); // Values that have changed since the last save
        echo "</pre>";
        //print_r($options); //Option values
        //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use THEME_URL if you want to use any of the built in icons
     * */
    function dynamic_section( $sections ) {
        //$sections = array();
        $sections[] = array(
            'title'  => __( 'Section via hook', 'candid' ),
            'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'candid' ),
            'icon'   => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    function change_arguments( $args ) {
        //$args['dev_mode'] = true;

        return $args;
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    function change_defaults( $defaults ) {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }

    // Remove the demo link and the notice of integrated demo from the redux-framework plugin
    function remove_demo() {

        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_filter( 'plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2 );

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
        }
    }