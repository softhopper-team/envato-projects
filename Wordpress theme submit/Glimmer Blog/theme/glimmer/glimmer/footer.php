<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Glimmer
 */
    global $glimmer;
?>
    <!-- Footer
    ================================================== --> 
    <footer id="colophon" class="site-footer" role="contentinfo">
        
        <div id="footer-top">
            <div class="container-fluid">
                <div class="row">
                    <?php if ( ! dynamic_sidebar( 'instagram' ) ) : ?>
                    <?php endif; // end instagram widget area ?>
                </div> <!-- /.row -->
            </div> <!-- /.container-fluid -->
        </div> <!-- #footer-top" -->

        <?php if ( $glimmer['footer_widgets'] ) : ?>
        <div id="footer-middle">
            <div class="container">
                <div class="row">
                    <?php
                        // show footer widget with condition
                        $columns = intval( $glimmer['footer_widget_columns'] );
                        $col_class = 12 / max( 1, $columns );
                        $col_class_sm = 12 / max( 1, $columns );
                        if ( $columns == 4 ) {
                            $col_class_sm = 6;
                        } 
                        $col_class = "col-sm-$col_class_sm col-md-$col_class";
                        for ( $i = 1; $i <= $columns ; $i++ ) {
                            if ( $columns == 3 ) :
                                if ( $i == 3 ) {
                                    $col_class = "col-sm-12 col-md-$col_class";
                                } else {
                                    $col_class = "col-sm-6 col-md-$col_class";
                                } 
                            endif; 
                        ?>
                            <div class="widget-area footer-sidebar-<?php echo $i ?> <?php echo esc_attr( $col_class ) ?>">
                                <?php dynamic_sidebar( __( 'Footer ', 'glimmer' ) . $i ) ?>
                            </div>
                        <?php
                        }
                    ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->         
        </div> <!-- #footer-middle -->
        <?php endif; ?>

        <div id="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="scroll-top">                    
                            <a href="#top">
                                <i class="glyphicon glyphicon-menu-up"></i>
                                <span><?php _e( 'Back to top', 'glimmer' ); ?></span>
                            </a>
                        </div> <!-- /#scroll-top --> 
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->             
            </div> <!-- /.container -->

            <div class="site-footer-banner" style="background-image: url(<?php if ( isset( $glimmer['footer_bg_img']['url'] ) ) echo $glimmer['footer_bg_img']['url']; ?>)">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="site-branding">
                                <div class="site-logo">
                                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php if ( isset( $glimmer['footer_logo']['url'] ) ) echo $glimmer['footer_logo']['url']; ?>" alt="logo" class="img-responsive"></a>
                                </div>
                            </div> <!-- .site-branding -->   
                        </div>
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.site-footer -->

            <div class="site-footer-bottom"> 
                <div class="container">  
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <?php 
                                    if ( isset ( $glimmer['footer_copyright_info'] ) ) echo $glimmer['footer_copyright_info'];
                                ?>  
                            </div>
                            <div class="navbar-right">
                                <?php
                                    wp_nav_menu( array(
                                        'theme_location'    => 'footer-menu',
                                        'depth'             =>  1,
                                        'menu_class'        => 'footer-nav',
                                        'container'        => '',
                                        'fallback_cb'       => 'Glimmer_Nav_Walker::footer_nav_fallback',
                                        'walker'            => new Glimmer_Nav_Walker())
                                    );
                                ?>                                        
                                <div class="footer-social">
                                   <?php
                                        // show footer menu social link
                                        if( isset ( $glimmer['footer_social_link'] )  && ! empty ( $glimmer['footer_social_link']) ) {
                                            $social_link = $glimmer['footer_social_link'];                      
                                            foreach ( $social_link as $key => $value ) {
                                            if ( $value['title'] ) {
                                        ?>
                                        <a href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                                        <?php
                                                }
                                            }
                                        }
                                    ?>      
                                </div> <!-- /.footer-social --> 
                            </div> <!-- /.navbar-right -->  
                        </div> <!-- /.col-md-12 -->
                    </div> <!-- /.row -->
                </div> <!-- /.container -->             
            </div> <!-- /.site-footer-bottom -->
        </div> <!-- #footer-bottom -->            
    </footer><!-- #colophon -->
    <?php wp_footer(); ?>
    </body>
</html>