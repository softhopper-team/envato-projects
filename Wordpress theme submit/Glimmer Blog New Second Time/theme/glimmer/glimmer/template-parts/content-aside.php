<?php
/**
 * The template for displaying aside post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
        <?php glimmer_entry_header(); ?>
    </header> <!-- /.entry-header -->

    <div class="entry-content">
        <?php the_content(); ?>      
    </div> <!-- .entry-content -->
</article> <!-- /.post-->