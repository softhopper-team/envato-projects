<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Glimmer
 */

if ( ! function_exists( 'the_posts_navigation' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_posts_navigation() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation posts-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Posts navigation', 'glimmer' ); ?></h2>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( __( 'Older posts', 'glimmer' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( __( 'Newer posts', 'glimmer' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'the_post_navigation' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_post_navigation() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php _e( 'Post navigation', 'glimmer' ); ?></h2>
		<div class="nav-links">
			<?php
				previous_post_link( '<div class="nav-previous">%link</div>', '%title' );
				next_post_link( '<div class="nav-next">%link</div>', '%title' );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;


if ( ! function_exists( 'glimmer_pagination_nav' ) ) :
/**
 * This is for post pagination
 */
function glimmer_pagination_nav() {
	?>
	<nav>
        <ul class="pagination">
            <?php
            global $wp_query;

            $big = 999999999; // need an unlikely integer

            $links = paginate_links(array(
                'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
                'format' => '?paged=%#%',
                'current' => max(1, get_query_var('paged')),
                'total' => $wp_query->max_num_pages,
                'type' => 'array',
                'prev_next' => true,
                'prev_text' => '<i class="fa fa-angle-left"></i>',
                "next_text" => '<i class="fa fa-angle-right"></i>',
                'mid_size' => 3
            ));
            //print_r($links);
            if ($links) {
                foreach ($links as $link) {
                    if (strpos($link, "current") !== false)
                        echo '<li class="active"><a>' . $link . "</a></li>\n";
                    else
                        echo '<li>' . $link . "</li>\n";

                }
            }
            ?>
        </ul>
    </nav>     
	<?php
}
endif;

if ( ! function_exists( 'glimmer_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_posted_on() {
	$byline = sprintf(
		_x( 'By: %s', 'post author', 'glimmer' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	?>
		<ul>
            <li><?php echo $byline; ?></li>
            <li><?php _e('On: ','glimmer').the_time( get_option( 'date_format' ) ); ?></li>
            <li>
            	<?php
            		$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'glimmer' ) );
					if ( $categories_list  ) {
						printf( '<span class="cat-links"><span>%1$s </span>%2$s</span>',
							_x( 'In: ', 'Used before category names.', 'glimmer' ),
							$categories_list
						);
					}
			    ?>
            </li>
            <li><a href="<?php comments_link(); ?>"><?php comments_number( __('No comment','glimmer'), __('1 Comment','glimmer'), __('% Comments','glimmer') ); ?></a></li>
            <li><span class="total-post-viewed"><?php echo wpb_get_post_views(get_the_ID()); ?></span></li>
        </ul>  
	<?php
}
endif;


if ( ! function_exists( 'glimmer_posted_on_for_page' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_posted_on_for_page() {
	$byline = sprintf(
		_x( 'By: %s', 'post author', 'glimmer' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	?>
		<ul>
            <li><?php echo $byline; ?></li>
            <li><?php _e('On: ','glimmer').the_time( get_option( 'date_format' ) ); ?></li>
            <li><a href="<?php comments_link(); ?>"><?php comments_number( __('No comment','glimmer'), __('1 Comment','glimmer'), __('% Comments','glimmer') ); ?></a></li>
        </ul>  
	<?php
}
endif;



if ( ! function_exists( 'glimmer_posted_on_for_home' ) ) :
/**
 * This is for landing page blog section
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_posted_on_for_home() {
	$byline = sprintf(
		_x( 'By %s', 'post author', 'glimmer' ),
		'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
	);

	?>
		<ul>
            <li><?php echo $byline; ?></li>
            <li><?php the_time( get_option( 'date_format' ) ); ?></li>
            <li><?php echo wpb_get_post_views(get_the_ID()); ?></li>
        </ul>  
	<?php
}
endif;

if ( ! function_exists( 'glimmer_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function glimmer_entry_footer() {
	// Hide category and tag text for pages.
	 $posttags = get_the_tags();
      if ($posttags) {
    ?>
    <div class="tag-catagory pull-left">             
        <i class="icon-tag"></i> 
        <span>                                            
        	<?php echo get_the_tag_list("", ", ", "");; ?>
        </span>
    </div> <!-- /.tag-catagory -->
    <?php
      }	
    ?>
    <!-- Social share link -->
    <div class="social-share pull-right">
        <?php _e('Share this post - ','glimmer'); ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/home?status=<?php the_permalink(); ?>"><i class="fa fa-twitter"></i></a>
        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>"><i class="fa fa-google-plus"></i></a>
    </div>
    <?php	
}
endif;

if ( ! function_exists( 'the_archive_title' ) ) :
/**
 * Shim for `the_archive_title()`.
 *
 * Display the archive title based on the queried object.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the title. Default empty.
 * @param string $after  Optional. Content to append to the title. Default empty.
 */
function the_archive_title( $before = '', $after = '' ) {
	if ( is_category() ) {
		$title = sprintf( __( 'Category: %s', 'glimmer' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( 'Tag: %s', 'glimmer' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( 'Author: %s', 'glimmer' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( 'Year: %s', 'glimmer' ), get_the_date( _x( 'Y', 'yearly archives date format', 'glimmer' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( 'Month: %s', 'glimmer' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'glimmer' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( 'Day: %s', 'glimmer' ), get_the_date( _x( 'F j, Y', 'daily archives date format', 'glimmer' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = _x( 'Asides', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = _x( 'Galleries', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = _x( 'Images', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = _x( 'Videos', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = _x( 'Quotes', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = _x( 'Links', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = _x( 'Statuses', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = _x( 'Audio', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = _x( 'Chats', 'post format archive title', 'glimmer' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( 'Archives: %s', 'glimmer' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( __( '%1$s: %2$s', 'glimmer' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = __( 'Archives', 'glimmer' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @param string $title Archive title to be displayed.
	 */
	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) :
/**
 * Shim for `the_archive_description()`.
 *
 * Display category, tag, or term description.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the description. Default empty.
 * @param string $after  Optional. Content to append to the description. Default empty.
 */
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		/**
		 * Filter the archive description.
		 *
		 * @see term_description()
		 *
		 * @param string $description Archive description to be displayed.
		 */
		echo $before . $description . $after;
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function glimmer_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'glimmer_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'glimmer_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so glimmer_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so glimmer_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in glimmer_categorized_blog.
 */
function glimmer_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'glimmer_categories' );
}
add_action( 'edit_category', 'glimmer_category_transient_flusher' );
add_action( 'save_post',     'glimmer_category_transient_flusher' );
