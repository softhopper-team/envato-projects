<?php
function hex2rgba($color, $opacity = false) {
 
	$default = 'rgb(0,0,0)';
 
	//Return default if no color provided
	if(empty($color))
          return $default; 
 
	//Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
        	$color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
        	if(abs($opacity) > 1)
        		$opacity = 1.0;
        	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
        	$output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}

$color = '#fff';
$rgb = hex2rgba($color);
$rgba = hex2rgba($color, 0.7);
//'div.example{background: '.$rgb.'; color: '.$rgba.'; height; 400px; width; 400px; } ';
function glimmer_color_scheme() { 
global $glimmer;
    //first color
    switch($glimmer['glimmer_color_one']){
        case 1: //373e48
            $glimmer_color_one = "#373e48";
            break;
        case 2: //f26d7e
            $glimmer_color_one = "#f26d7e";
            break;
        case 3: //044f67
            $glimmer_color_one = "#044f67";
            break;
        case 4: //006442
            $glimmer_color_one = "#006442";
            break;
        case 5: //1691bd
            $glimmer_color_one = "#1691bd";
            break;
        case 6: //067b82
            $glimmer_color_one = "#067b82";
            break;
        case 7: //34495e
            $glimmer_color_one = "#34495e";
            break;
        case 8: //d2527f
            $glimmer_color_one = "#d2527f";
            break;
        case 9: //turquoise
            $glimmer_color_one = $glimmer['glimmer_custom_color_one'];
            break;
        default:
            $glimmer_color_one = "#373e48";
            break;
    }
    //second color
    switch($glimmer['glimmer_color_two']){
        case 1: //373e48
            $glimmer_color_two = "#373e48";
            break;
        case 2: //f26d7e
            $glimmer_color_two = "#f26d7e";
            break;
        case 3: //044f67
            $glimmer_color_two = "#044f67";
            break;
        case 4: //006442
            $glimmer_color_two = "#006442";
            break;
        case 5: //1691bd
            $glimmer_color_two = "#1691bd";
            break;
        case 6: //067b82
            $glimmer_color_two = "#067b82";
            break;
        case 7: //34495e
            $glimmer_color_two = "#34495e";
            break;
        case 8: //d2527f
            $glimmer_color_two = "#d2527f";
            break;
        case 9: //turquoise
            $glimmer_color_two = $glimmer['glimmer_custom_color_two'];
            break;
        default:
            $glimmer_color_two = "#f26d7e";
            break;
    }
?>
<style type="text/css">
/****************************************
  Colors
****************************************/
a:hover {
  color: <?php echo $glimmer_color_two;?>;
}
a:focus {
  color: <?php echo $glimmer_color_two;?>;
}
.heading .title {
  color: <?php echo $glimmer_color_one;?>;
}
.heading .small-border {
  background: <?php echo $glimmer_color_one;?>;
}

.button-big {
  color: <?php echo $glimmer_color_two;?>;
  border: 1px solid <?php echo $glimmer_color_two;?>;
}
.button-big:hover {
  color: <?php echo $glimmer_color_one;?>;
  border: 1px solid <?php echo $glimmer_color_one;?>;
}
.pageloader {
  background-color: <?php echo $glimmer_color_one;?>;
}
.home {
  background-color: rgba(45, 52, 62, 0.3);
}
[class*="brick-one"] {
  background: <?php echo $glimmer_color_two;?>;
}
.odd {
  background: <?php echo $glimmer_color_two;?>;
}
.blank {
  background: <?php echo $glimmer_color_one;?>;
}
.navbar-default {
  background-color: <?php echo $glimmer_color_one;?>;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-brand:focus {
  color: <?php echo $glimmer_color_two;?> !important;
}
.navbar-default .navbar-nav > .active > a,
.navbar-default .navbar-nav > .active > a:focus,
.navbar-default .navbar-nav > .active > a:hover {
  color: <?php echo $glimmer_color_two;?> !important;
}
.navbar-default .navbar-brand:hover,
.navbar-default .navbar-nav > li > a:hover {
  color: <?php echo $glimmer_color_two;?> !important;
}
.navbar-default .navbar-toggle:focus,
.navbar-default .navbar-toggle:hover {
  background-color: <?php echo $glimmer_color_two;?> !important;
}
.navbar-default .navbar-nav > li.current-menu-ancestor > a { 
    color: <?php echo $glimmer_color_two;?> !important;
}
.navbar-default.stuck .navbar-nav > li.current-menu-ancestor > a { 
    color: <?php echo $glimmer_color_two;?> !important;
}
.dropdown-menu>.current-menu-ancestor>a, .dropdown-menu>.active>a, .dropdown-menu>.active>a:focus, .dropdown-menu>.active>a:hover {
    color: <?php echo $glimmer_color_two;?> !important;  
}
.nav .open > a,
.nav .open > a:focus,
.nav .open > a:hover {
  color: <?php echo $glimmer_color_two;?> !important;
}
/*** dropdown menu ***/
.dropdown .dropdown > a:after {
  border-left-color: <?php echo $glimmer_color_two;?>;
}
.dropdown .dropdown:hover > a:after {
  border-left-color: <?php echo $glimmer_color_two;?>;
}
.dropdown-menu {
  background-color: <?php echo $glimmer_color_one;?>;
}
.dropdown-menu li > a:hover,
.dropdown-menu li > a:focus {
  color: <?php echo $glimmer_color_two;?> !important;
}
/*** sticky-wrapper ***/
.stuck .navbar-nav > li.active > a,
.stuck .navbar-nav > li.active a:focus,
.stuck .navbar-nav > li.active a:hover {
  color: <?php echo $glimmer_color_two;?>;
}
.team-heading {
  background: <?php echo $glimmer_color_one;?>;
}
.patern-overlay {
  background-color: rgba(45, 52, 62, 0.8);
}
.about-service .about-icon {
  color: <?php echo $glimmer_color_one;?>;
}
.team-member .team-bg {
  background: <?php echo $glimmer_color_one;?>;
}
.team-details .member-details .social-link i {
  color: <?php echo $glimmer_color_one;?>;
}
.team-details .member-details .social-link i:hover {
  background: <?php echo $glimmer_color_two;?>;
}
.our-skill .skillbar-bar {
  background: <?php echo $glimmer_color_two;?>;
}
.our-skill .skill-bar-percent {
  color: <?php echo $glimmer_color_two;?>;
}
.our-skill .skill-bar-percent:after {
  content: "";
  border-top: 12px solid <?php echo $glimmer_color_two;?>;
}
.our-service .service-content .service-icon {
  background: <?php echo $glimmer_color_two;?>;
}
.our-service .service-content .service-icon:after {
  -webkit-box-shadow: 0 0 0 4px <?php echo $glimmer_color_two;?>;
  box-shadow: 0 0 0 4px <?php echo $glimmer_color_two;?>;
}
.portfolio .portfolio-filter ul li a {
  border: 1px solid <?php echo $glimmer_color_one;?>;
  color: <?php echo $glimmer_color_one;?>;
}
.portfolio .portfolio-filter ul li a:hover {
  color: <?php echo $glimmer_color_two;?>;
  border: 1px solid <?php echo $glimmer_color_two;?>;
}
.portfolio .portfolio-filter ul li .current {
  border: 1px solid <?php echo $glimmer_color_two;?>;
  color: <?php echo $glimmer_color_two;?>;
}
.portfolio .portfolio-filter ul li:hover {
  color: <?php echo $glimmer_color_two;?>;
}
/**Portfolio Single**/
#single-portfolio .close-folio-item {
  color: <?php echo $glimmer_color_one;?>;
}
#single-portfolio #project-image .owl-prev,
#single-portfolio #project-image .owl-next {
  background: <?php echo $glimmer_color_two;?>;
}
.fun-fact i {
  background: <?php echo $glimmer_color_two;?>;
}
.price-table .price-table-content .price-title {
  background: <?php echo $glimmer_color_one;?>;
}
.price-table .price-table-content .price-title:after {
  border-color: rgba(47, 47, 47, 0);
  border-top-color: <?php echo $glimmer_color_one;?>;
}
.price-table .price-table-content .price-sub-title {
  background: <?php echo $glimmer_color_one;?>;
}
.price-table .price-table-content .price-table-container .price {
  color: <?php echo $glimmer_color_two;?>;
}
.price-table .price-table-content .price-table-container ul li {
  color: <?php echo $glimmer_color_one;?>;
}
.price-table .price-table-content .price-table-container .sign-up-button {
  background: <?php echo $glimmer_color_one;?>;
}
.price-table .price-table-content .pricing-content:hover .price-title,
.price-table .price-table-content .featured .price-title {
  background: <?php echo $glimmer_color_two;?>;
}
.price-table .price-table-content .pricing-content:hover .price-title:after,
.price-table .price-table-content .featured .price-title:after {
  border-top-color: <?php echo $glimmer_color_two;?>;
}
.price-table .price-table-content .pricing-content:hover .price-sub-title,
.price-table .price-table-content .featured .price-sub-title {
  background: <?php echo $glimmer_color_two;?>;
}
.price-table .price-table-content .pricing-content:hover ul > li,
.price-table .price-table-content .featured ul > li {
  color: <?php echo $glimmer_color_two;?>;
}
.price-table .price-table-content .pricing-content:hover .sign-up-button,
.price-table .price-table-content .featured .sign-up-button {
  background: <?php echo $glimmer_color_two;?>;
}
.pagination > li > a, .pagination > li > span  {
  color: <?php echo $glimmer_color_one;?> !important;
  border: 1px solid <?php echo $glimmer_color_one;?>;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  color: <?php echo $glimmer_color_two;?> !important;
  border-color: <?php echo $glimmer_color_two;?>;
}
.pagination li:first-child a:hover,
.pagination li:last-child a:hover,
.pagination li:first-child span:hover,
.pagination li:last-child span:hover,
.pagination li:first-child a:focus,
.pagination li:last-child a:focus,
.pagination li:first-child span:focus,
.pagination li:last-child span:focus {
  color: <?php echo $glimmer_color_two;?>;
  border-color: <?php echo $glimmer_color_two;?>;
}
.pagination > .active a,
.pagination > .active a:focus,
.pagination > .active a:hover,
.pagination > .active span,
.pagination > .active span:focus,
.pagination > .active span:hover {
  border-color: <?php echo $glimmer_color_two;?>;
  color: <?php echo $glimmer_color_two;?> !important;
}
.nav-previous:hover {
  border-color: <?php echo $glimmer_color_two;?>;
}
.page-links a {
  background-color: <?php echo $glimmer_color_one;?>;
  border-color: <?php echo $glimmer_color_one;?>;
}
.page-links a:hover,
.page-links a:focus {
  background-color: <?php echo $glimmer_color_two;?>;
  border: 1px solid <?php echo $glimmer_color_two;?>;
}
.post-password-form input[type="submit"]:hover {
  background: <?php echo $glimmer_color_two;?>;
}
.submit:hover,
button:hover {
  background: <?php echo $glimmer_color_two;?>;
}
input[type="text"]:focus,
input[type="password"]:focus,
input[type="datetime"]:focus,
input[type="datetime-local"]:focus,
input[type="date"]:focus,
input[type="month"]:focus,
input[type="time"]:focus,
input[type="week"]:focus,
input[type="number"]:focus,
input[type="email"]:focus,
input[type="url"]:focus,
input[type="search"]:focus,
input[type="tel"]:focus,
input[type="color"]:focus,
input[type="subject"]:focus,
textarea:focus,
select:focus {
  border: 1px solid <?php echo $glimmer_color_two;?>;
}
.searchform button:hover {
  background: <?php echo $glimmer_color_two;?>;
}
.comment-respond input[type="text"]:focus,
.comment-respond input[type="password"]:focus,
.comment-respond input[type="datetime"]:focus,
.comment-respond input[type="datetime-local"]:focus,
.comment-respond input[type="date"]:focus,
.comment-respond input[type="month"]:focus,
.comment-respond input[type="time"]:focus,
.comment-respond input[type="week"]:focus,
.comment-respond input[type="number"]:focus,
.comment-respond input[type="email"]:focus,
.comment-respond input[type="url"]:focus,
.comment-respond input[type="search"]:focus,
.comment-respond input[type="tel"]:focus,
.comment-respond input[type="color"]:focus,
.comment-respond input[type="subject"]:focus,
.comment-respond textarea:focus,
.comment-respond select:focus {
  border: 1px solid <?php echo $glimmer_color_two;?> !important;
}
.comment-respond .submit:hover {
  background: <?php echo $glimmer_color_two;?> !important;
}
.blog-featured .blog-description p > a {
  color: <?php echo $glimmer_color_two;?>;
}
.blog-featured .blog-description h2 > a {
  color: <?php echo $glimmer_color_one;?>;
}
.blog-featured .blog-description h2 > a:hover {
  color: <?php echo $glimmer_color_two;?>;
}
.blog-meta li:first-child a {
  color: <?php echo $glimmer_color_two;?>;
}
.author-info .author  a {
  color: <?php echo $glimmer_color_two;?>;
}
.blog-content .blog-details .blog-title a:hover {
  color: <?php echo $glimmer_color_two;?>;
}
.blog-content .blog-details .blog-description a {
  color: <?php echo $glimmer_color_two;?>;
}
.blog-content .blog-details .tag-catagory a:hover {
  color: <?php echo $glimmer_color_two;?>;
}
.comment-area .media .media-reply {
  color: <?php echo $glimmer_color_two;?>;
}
.reply a {
  background: <?php echo $glimmer_color_one;?>;
}
.reply a:hover {
  background: <?php echo $glimmer_color_two;?>;
}
.error-body .error span {
  color: <?php echo $glimmer_color_two;?>;
}
.footer-copyright {
  background: <?php echo $glimmer_color_one;?>;
}
.footer-copyright .social a {
  color: <?php echo $glimmer_color_one;?>;
}
.footer-copyright .social a:hover {
  background: <?php echo $glimmer_color_two;?>;
}
/***Back to top***/
.backtopbutton {
  color: <?php echo $glimmer_color_two;?>;
  border: 1px solid <?php echo $glimmer_color_two;?>;
}
.backtopbutton span {
  font-size: 18px;
}
.backtopbutton:hover,
.backtopbutton:focus {
  color: <?php echo $glimmer_color_two;?>;
}
@media (max-width: 988px) {
  .team-mobile {
    background: <?php echo $glimmer_color_one;?>;
  }
}


</style>
<?php }
add_action('wp_head','glimmer_color_scheme',200);