<?php
    global $glimmer;
    $testimonials = glimmer_get_custom_posts("testimonial", 20);
?>  
<!-- testimonial
================================================== --> 
<section id="testimonial" class="testimonial parallax-two">
    <div class="patern-overlay"></div>                 
    <div class="parallax-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <header class="heading wow <?php if (isset($glimmer['section_testimonial_animation_style'])) echo $glimmer['section_testimonial_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_testimonial_animation_duration'])) echo $glimmer['section_testimonial_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_testimonial_animation_delay'])) echo $glimmer['section_testimonial_animation_delay']; ?>">
                        <h2 class="title">
                            <?php if (isset($glimmer['section_testimonial_title'])) echo $glimmer['section_testimonial_title'];
                            ?>
                        </h2>
                            <span class="small-border-two"></span>
                    </header> <!-- /.heading -->
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.paralux-heading -->

    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="pik-carousel">

                    <div class="pik-carousel-wrapper">
                        <ul class="pik-carousel-container">
                            <?php
                                foreach ($testimonials as $post) {
                                setup_postdata($post);
                                $meta = get_post_meta($post->ID);
                            ?> 
                            <li>
                                <img class="img-responsive" src="<?php if(isset($meta["_glimmer_buyer_image"][0])) echo $meta["_glimmer_buyer_image"][0]; ?>" alt="<?php if(isset($meta["_glimmer_buyer_image"][0])) echo $meta["_glimmer_buyer_image"][0]; ?>">
                                <div class="pc-content">
                                    <h2> <?php if(isset($meta["_glimmer_buyer_description"][0])) echo $meta["_glimmer_buyer_description"][0]; ?></h2>
                                </div>
                                <div class="pc-author">
                                    <h2> <?php if(isset($meta["_glimmer_buyer_name"][0])) echo $meta["_glimmer_buyer_name"][0]; ?></h2>
                                </div>
                            </li>
                            <?php } ?>

                        </ul> <!-- /.pik-carusol-container -->

                    </div> <!-- /.pik-carusol-wrapper -->

                </div> <!-- /.pik-carusol -->

            </div> <!-- /.col-lg-12 -->
        </div> <!-- /.row -->

    </div> <!-- /.container -->     
</section> <!-- /.testimonial -->
