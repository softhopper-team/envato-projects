<?php
/**
 * The template for displaying featured posts
 *
 * Used for index.
 *
 * @package Candid
 */
?>
<?php 
    global $candid;
    if ( $candid['featured_display'] == 1 ) : 
?>
<?php
    /* Show box or wide layout with user condition*/
    $layout = '';
    if ( $candid['layout'] == 1 ) {
        $layout = 'boxed';
    } 
?>
<!-- Featured-area
================================================== -->
<section class="featured-area <?php echo $layout; ?>">
    <div class="container">
        <?php
            $query = new WP_Query ( 
                    array ( 
                            'category_name' => 'featured',
                            'posts_per_page' => $candid['featured_per_page']
                        )
                    );
            if ( $query->have_posts() ) : 
        ?>
        <div id="features-slide">
            <div id="carousel-featured" class="carousel slide" data-ride="carousel" data-interval="<?php echo $candid['featured_slide_delay']; ?>">
            
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <?php  
                        $cat_slug = get_category_by_slug('featured'); 
                        $cat_id = $cat_slug->term_id;
                        $posts = get_posts('post_type=post&category='.$cat_id); // total posts count of featured category
                        $published_posts = count($posts); 
                        $i = 0; // index id counting
                        while ( $query->have_posts() ) : $query->the_post();
                        if ( $candid['featured_per_page'] > 1 && $published_posts > 1) {
                    ?>
                        <li data-target="#carousel-featured" data-slide-to="<?php echo $i; ?>" <?php if ( $i == 0 ) echo 'class="active"'; ?>></li>
                    <?php 
                        } // end if
                        $i++;
                        endwhile; wp_reset_query(); 
                    ?> 
                </ol>

                <ul class="carousel-inner" role="listbox">
                    <?php
                        $query = new WP_Query ( array ( 
                                        'category_name' => 'featured',
                                        'posts_per_page' => $candid['featured_per_page']
                                    )
                                );
                        $i = 0;
                        while ( $query->have_posts() ) : $query->the_post();
                    ?>
                    <li class="item <?php if ( $i == 0 ) echo 'active'; ?>">
                        <div class="row">
                            <div class="col-md-6">
                                <h2 class="post-title">
                                    <a href="<?php the_permalink()?>"><?php the_title(); ?></a>         
                                </h2> <!-- /.post-title -->

                                <div class="featured-description">                                    
                                    <p>
                                        <?php 
                                            $content = get_the_content();
                                            echo substr($content, 0, 500)."&#46;&#46;&#46;";
                                        ?>
                                    </p>                                
                                    <a class="more-link button" href="<?php the_permalink()?>">
                                        <span class="more-button"><?php _e('Continue Reading','candid'); ?></span>
                                    </a>   
                                </div> <!-- /.featured-description -->   
                            </div> <!-- /.col-md-6 -->

                            <div class="col-md-6">
                                <div class="featured-image">                                   
                                    <?php                                        
                                        $alt_text = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
                                        the_post_thumbnail('fearured-img', array ( 
                                            'class' => " img-responsive",
                                            'alt' => $alt_text
                                            )
                                        );
                                    ?>
                                </div> <!-- /.featured-image -->   
                            </div> <!-- /.col-md-6 -->
                        </div> <!-- /.row -->
                    </li> <!-- /.item -->

                    <?php 
                        $i++;
                        endwhile; wp_reset_query(); 
                    ?>                  

                </ul> <!-- /.carousel-inner -->    
            </div> <!-- /.slide -->    
        </div> <!-- /.featured-area --> 
        <?php endif; ?>      
    </div> <!-- /.container -->    
</section> <!-- /.featured-area -->

<?php endif; ?>