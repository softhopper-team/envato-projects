<?php
/**
 * The template for displaying aside post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Candid
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                
    <header class="entry-header">
        <div class="entry-meta">
            <?php
                $format = get_post_format();
                
                if ( false === $format ) {
                    
                } else {
                    echo "<span class='post-formet'> ".ucfirst( $format )."</span>";
                }
            ?>        
            <span class="byline">
                <span class="author vcard">
                    <?php the_author_posts_link(); ?>
                </span>
            </span>
       
            <span class="entry-date">
                <?php _e('On ','candid').the_time( get_option( 'date_format' ) ); ?>
            </span>

        </div><!-- .entry-meta -->
    </header><!-- .entry-header -->

    <div class="entry-content">
        <?php the_content(); ?>
    </div><!-- .entry-content -->
</article>