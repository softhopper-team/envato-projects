<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="ribbon"><span>'.__('Sticky', 'glimmer').'</span></div>';
    	}
    ?>
	<header class="entry-header">
		
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php glimmer_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

		<?php 		
			if ( is_single() ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
			} else {
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
			}
		?>
	</header>

	<?php
    	$post_format = get_post_format();
    	$meta = get_post_meta( $post->ID );
		if ( false === $post_format ) {
			$post_format = "standard";
		}
		
		if ( $post_format == "link" ) {
            ?>
            <div class="post-link" style="background-image:url(<?php if( isset ( $meta["_glimmer_format_link_bg_img"][0] ) ) echo $meta["_glimmer_format_link_bg_img"][0]; ?>)"> 
            	<div class="overlay"></div>
                <div class="link-content">
                    <h2 class="link-title">
                    	<a href="#"><?php if( isset ( $meta["_glimmer_format_link_text"][0] ) ) echo $meta["_glimmer_format_link_text"][0]; ?></a>
                    </h2>
                    <a class="link" href="#"><?php if( isset ( $meta["_glimmer_format_link"][0] ) ) echo $meta["_glimmer_format_link"][0]; ?></a>
                </div> <!-- /.link-content -->
			</div> <!-- /.post-thumb -->
            <?php
        } else {
        	if ( has_post_thumbnail() ) {
            ?>
            <figure class="post-thumb">
				<a href="<?php the_permalink(); ?>">
					<?php
			          	the_post_thumbnail('single-full', array( 'class' => " img-responsive", 'alt' => get_the_title()));
			        ?>
				</a>
			</figure> <!-- /.post-thumb -->
            <?php
	        } else {
	        	echo "<hr>";
	        }
        }
    ?>
	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
			} else {
				the_excerpt();
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
	<?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->