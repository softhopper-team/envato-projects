<?php
    global $glimmer, $post;
    $pricingtable = glimmer_get_custom_posts_by_custom_order("pricingtable", 4, "_glimmer_pricing_order");
    $classes = array(0 => "", "1" => "col-lg-12", "2" => "col-lg-6", "3" => "col-lg-4", "4" => "col-lg-3");
    $class = $classes[count($pricingtable)];
?>
<!-- price table
================================================== --> 
<section id="price-table" class="price-table">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <header class="heading wow <?php if (isset($glimmer['section_pricing_animation_style'])) echo $glimmer['section_pricing_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_pricing_animation_duration'])) echo $glimmer['section_pricing_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_pricing_animation_delay'])) echo $glimmer['section_pricing_animation_delay']; ?>">
                    <h2 class="title">
                        <?php if (isset($glimmer['section_pricing_title'])) echo $glimmer['section_pricing_title'];
                            ?>
                    </h2>
                        <span class="small-border"></span>

                    <h5 class="subtitle"><?php if (isset($glimmer['section_pricing_subtitle'])) echo $glimmer['section_pricing_subtitle']; ?></h5>
                </header> <!-- /.heading -->
            </div><!--/. col-md-12--> 
        </div> <!-- /.row -->
        

        <div class="price-table-content base-padding ">
            <div class="row">                       
                <?php
                foreach ($pricingtable as $post) {
                    $meta = get_post_meta($post->ID);
                    $is_popular = false;
                    if (isset($meta['_glimmer_pricing_featured'])) $is_popular = $meta['_glimmer_pricing_featured'][0];
                    $mpclass = "";
                    if ($is_popular) $mpclass = "featured";
                ?>
                <div class="col-xs-12 col-sm-6 <?php echo $class; ?> price-padding <?php echo $mpclass; ?> wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                    <div class="pricing-content">
                        <div class="price-title text-center">
                            <h3><?php the_title(); ?></h3>
                        </div> <!-- /.price-table -->
                        <div class="price-sub-title text-center">
                            <h4><?php if(isset($meta['_glimmer_pricing_subtitle'][0])) echo $meta['_glimmer_pricing_subtitle'][0]; ?></h4>
                        </div> <!-- /.price-sub-title -->
                        <div class="price-table-container text-center">
                            <div class="price">
                                <span class="dollar"><?php if(isset($meta['_glimmer_pricing_currency_symbols'][0])) echo $meta['_glimmer_pricing_currency_symbols'][0]; ?></span><?php if(isset($meta['_glimmer_pricing_price'][0])) echo $meta['_glimmer_pricing_price'][0]; ?>
                            </div>
                            <p><?php if(isset($meta['_glimmer_pricing_duration'][0])) echo $meta['_glimmer_pricing_duration'][0]; ?></p>
                            <ul>
                                <?php
                                    $elements = $meta['_glimmer_pricing_elements'][0];
                                    $el_parts = explode("\n", $elements);
                                    foreach ($el_parts as $el) {
                                        $el = do_shortcode($el);
                                        echo "<li>{$el}</li>";
                                    }
                                ?>
                            </ul>
                            <a href="<?php if(isset($meta['_glimmer_pricing_button_link'][0])) echo $meta['_glimmer_pricing_button_link'][0]; ?>"
                               class="sign-up-button"><?php if(isset($meta['_glimmer_pricing_button'][0])) echo $meta['_glimmer_pricing_button'][0]; ?></a>   
                        </div> <!-- /.price-table-container -->
                    </div><!-- /.pricing-content -->
                </div> <!-- /.col-lg-3 -->
                <?php } ?>
            </div> <!-- /.row -->  
        </div>  <!-- /.price-table-content -->      
    </div> <!--/.container-->        
</section> <!-- /.pricetable -->
