<?php
/**
 * The template for displaying status post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<!-- this script to load facebook status -->	
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="sticky-icon"><i class="fa fa-thumb-tack"></i></div>';
    	}
    ?>

	<header class="entry-header">
        <?php glimmer_entry_header(); ?>
    </header> <!-- /.entry-header -->

	<div class="post-media">				
	<?php
    $meta = get_post_meta( $post->ID );
	$status_bg = ( isset ( $meta["_glimmer_format_status_bg"][0] ) ) ? $meta["_glimmer_format_status_bg"][0] : "";
	$status_facebook = ( isset ( $meta["_glimmer_format_status_fb"][0] ) ) ? $meta["_glimmer_format_status_fb"][0] : "";
	$status_twitter = ( isset ( $meta["_glimmer_format_status_twitter"][0] ) ) ? $meta["_glimmer_format_status_twitter"][0] : "";
	$status_gplus = ( isset ( $meta["_glimmer_format_status_gplus"][0] ) ) ? $meta["_glimmer_format_status_gplus"][0] : "";
	$status_instagram = ( isset ( $meta["_glimmer_format_status_instagram"][0] ) ) ? $meta["_glimmer_format_status_instagram"][0] : "";

	if( !empty( $status_facebook ) || !empty( $status_twitter ) || !empty( $status_gplus ) || !empty( $status_instagram )):	?>
		<div class="post-status-wrapper" style="background: url(<?php echo $status_bg; ?>);">
		<?php if( !empty( $status_facebook ) ) : ?>
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;  js = d.createElement(s);
				js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-post" data-href="<?php echo $status_facebook ?>"></div>
		<?php elseif( !empty( $status_twitter ) ) : ?>
			<blockquote class="twitter-tweet"><a href="<?php echo $status_twitter ?>"></a></blockquote>
			<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		<?php elseif( !empty( $status_gplus ) ) : ?>
			<script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
			<div class="g-post" data-href="<?php echo $status_gplus ?>"></div>
		<?php elseif( !empty( $status_instagram ) ) : ?>
			<?php echo $status_instagram ?>
		<?php endif; ?>
		</div><!-- /.post-status-wrapper -->
	<?php endif; ?>
	</div> <!-- /.post-media -->

	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
				?>
				<?php
					if ( has_tag() ) :
					?>
					<div class="tag clearfix">
					    <span class="tags"><?php _e('Tagged In:', 'glimmer'); ?></span>
	                    <?php 
				        echo get_the_tag_list("", "", "");
				        ?>
					</div> <!-- /.tag -->
					<?php
					endif;
				?>
				<?php
			} else {
				the_content(); 
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
	<?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->