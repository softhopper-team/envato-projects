
(function($) {
    "use strict";
	/*-----------------------------------
	 Quick Mobile Detection
	 -----------------------------------*/
	 var isMobile = {
	    Android: function () { 
	      return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function () {
	      return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function () {
	      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function () {
	      return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function () {
	      return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function () {
	      return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};
	/*-----------------------------------
	 Glimmer App
	 -----------------------------------*/
    var glimmerApp = {
		/* Preloader */
	    glimmar_loading : function () {
            $("#loading").delay(500).fadeOut();
            $(".pageloader").delay(1200).fadeOut("slow");

            var hash = window.location.hash;
            if (hash) { 
                $(document).scrollTop( $(hash).offset().top -56);
            }
        },
        /* Sticky Nav */
		glimmar_nav : function () {
			var width = $(window).width(); 
			if ((width <= 600  )) {
			 	$(function() {
					$(window).scroll(function() {
						if($(this).scrollTop() > 0) {
							$('.navbar').addClass( "stuck" );
							$('.blog-header').addClass( "stuck" );
						} else {
							$('.navbar').removeClass( "stuck" );
							$('.blog-header').removeClass( "stuck" );
						}
					});
				});
			}
			else {
				$(function() {
					$(window).scroll(function() {
						if($(this).scrollTop() > 300) {
							$('.navbar').addClass( "stuck" );
						} else {
							$('.navbar').removeClass( "stuck" );
						}
					});
				});
			}
			$('body').css('padding-top', parseInt($('.navbar-default').css("height")));        
			$(window).resize(function () { 
			    $('body').css('padding-top', parseInt($('.navbar-default').css("height")));
			});
		},
		/*parallax*/
		glimmar_parallax : function () {
			if(!isMobile.any()) {
				$('.parallax-one').parallax("50%", 0.5);
				$('.parallax-two').parallax("50%", 0.5);
				$('.parallax-three').parallax("50%", 0.5);
				$('.parallax-four').parallax("50%", 0.5);
				$('#parallax-five').parallax("50%", 0.5);
				$('#parallax-header').parallax("50%", 0.5);
			}
		},		
		/*Video jQuery*/ 
		glimmar_video : function () {
			$("#project-video").fitVids();
			$(".blog-content").fitVids();
			$("#comments").fitVids();
		},
		/*Contact Form*/
		glimmar_contactForm : function () {
			var form = $('#contact-us'); 
            form.submit(function (event) {
                event.preventDefault();
                var data = form.serialize();
                var form_status = $('<div class="form_status"></div>');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data: data,
                    url: $(this).attr('action'),

					beforeSend: function () {
						form.before( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
					}
				}).done(function (data) {
					form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
				});
            }); 
		},
		/*Retina Image*/
		glimmar_retina : function () {
		    if (window.devicePixelRatio > 1) {

		        var lowresImages = $('img');

		        lowresImages.each(function () {
		            var lowres = $(this).attr('src');
		            var highres = lowres.replace(".", "@2x.");
		            $(this).attr('src', highres);
		        });
		    }
		},
		/*scroll to top*/
		 glimmar_scrollTop : function () {
	 	    $(".scrollup").hide();
		    $(window).scroll(function() {
		        if ($(this).scrollTop() > 600) {
		            $('.scrollup').fadeIn();
		         } else {
		            $('.scrollup').fadeOut();
		         }
		    });
			$("a.backtopbutton[href^='#']").on('click', function(e) {
			   e.preventDefault();
			   $('html, body').stop().animate({scrollTop:0}, 1000, 'easeOutExpo');
			});
		 },
		glimmar_tooltip : function() {
		 	$('[data-toggle="tooltip"]').tooltip();
		},
		glimmar_initializ : function () {			
			glimmerApp.glimmar_nav();	
			glimmerApp.glimmar_parallax();	
			glimmerApp.glimmar_video();							
			glimmerApp.glimmar_scrollTop();
			glimmerApp.glimmar_retina();
			glimmerApp.glimmar_tooltip();	
		}	
	};
	/* === document ready function === */
	$(document).ready(function () {
		glimmerApp.glimmar_initializ();			
	});
	/* === window load function === */
	$(window).load(function () {	
		glimmerApp.glimmar_loading();
	});
	/* === function === */	
	glimmerApp.glimmar_contactForm();

})(jQuery);