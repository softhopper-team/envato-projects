<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-content" >
    	
    	<?php

    		if (is_sticky()) {
    			echo "<span class='sticky-post'>".__('Featured','glimmer')."</span>";
    		}

            if ( has_post_thumbnail() ) {
                ?>
                	<figure class="blog-image"> 
			            <?php
		                  	the_post_thumbnail('single-full', array( 'class' => "img-responsive", 'alt' => get_the_title()));
			            ?>
			        </figure> <!-- /.blog-image -->
                <?php
            } else {
                ?>
                	<div class="post-embed">
			    		<?php 
					        $meta = get_post_meta($post->ID);  
					        if(isset($meta["_glimmer_post_embed_code"][0])) echo $meta["_glimmer_post_embed_code"][0];
				        ?>
			    	</div><!-- /.post-embed -->
                <?php
            }
        ?>
        
        <div class="blog-details clearfix">
                <?php the_title( sprintf( '<h2 class="blog-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?> 

            <div class="blog-meta">
	         	<?php glimmer_posted_on(); ?>
	        </div> <!-- /.blog-meta -->	            
            <hr> <!-- /.hr -->

            <div class="blog-description">	      
            		<?php 
						if(is_single()) {
							the_content(); 
						} else {
							the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'glimmer' ) );
						}
					?>          
	                <?php
	                	wp_link_pages( array(
							'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'glimmer' ) . '</span>',
							'after'       => '</div>',
							'link_before' => '<span>',
							'link_after'  => '</span>',
							'pagelink'    => '%',
							'separator'   => '',
						) );
	                ?>	            
            </div> <!-- /.blog-description -->

            <?php glimmer_entry_footer(); ?>
            
        </div> <!-- /.blog-details-full -->
    </div><!-- /.blog-content -->
</article> <!-- /.blog-content -->

<?php 
	if(is_single()) {
		get_template_part( 'partials/content', 'authorinfo' ); 
	}
?>