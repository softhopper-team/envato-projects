<?php
/**
 * The template for displaying post format image if not set image.
 *
 * Used for both single and page.
 *
 * @package Glimmer
 */
?>
<?php
    $meta = get_post_meta( get_the_ID() );
    $post_format = get_post_format();
    if ( false === $post_format ) {
        $post_format = "standard";
    }
    
    if ( $post_format == "gallery" ) {  
        // show first image of gallery post
        if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
            $imgs_urls = $meta["_glimmer_format_gallery"][0];  
            $imgs_url = explode( '"', $imgs_urls );
            $img_crop_url = sh_crop_image_url($imgs_url[1], "trending-post");
            ?>
            <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">  
            <?php                           
        }  else {
            ?>
            <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/gallery.jpg'; ?>" alt="<?php the_title(); ?>">       
            <?php 
        }
    } elseif ( $post_format == "audio" ){
        if( isset($meta["_glimmer_format_audio_bg_img"][0]) ) {
            $img_crop_url = sh_crop_image_url($meta["_glimmer_format_audio_bg_img"][0], "trending-post");
            ?>
            <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">           
            <?php 
        } else {
            ?>
            <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/audio.jpg'; ?>" alt="<?php the_title(); ?>">    
            <?php 
        }
    } elseif ( $post_format == "video" ){
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('trending-post', array( 'alt' => get_the_title()));
        } else {
        ?>
        <img src="<?php echo THEME_URL; ?>/images/post/trending-no-thumb/video.jpg" alt="<?php the_title(); ?>" />
        <?php
        } //end else
    } elseif ( $post_format == "quote" ){
        ?>
        <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/quote.jpg'; ?>" alt="<?php the_title(); ?>">     
        <?php 
    } elseif ( $post_format == "aside" ){
        ?>
        <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/aside.jpg'; ?>" alt="<?php the_title(); ?>">     
        <?php 
    } elseif ( $post_format == "chat" ){
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('trending-post', array( 'alt' => get_the_title()));
        } else {
        ?>
        <img src="<?php echo THEME_URL; ?>/images/post/trending-no-thumb/chat.jpg" alt="<?php the_title(); ?>" />
        <?php
        } //end else
    } elseif ( $post_format == "link" ){
        if( isset($meta["_glimmer_format_link_bg_img"][0]) ) {
            $img_crop_url = sh_crop_image_url($meta["_glimmer_format_link_bg_img"][0], "trending-post");
            ?>
            <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">          
            <?php 
        } else {
            ?>
            <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/link.jpg'; ?>" alt="<?php the_title(); ?>">     
            <?php 
        }
    } elseif ( $post_format == "status" ){
        if( isset($meta["_glimmer_format_status_bg"][0]) ) {
            $img_crop_url = sh_crop_image_url($meta["_glimmer_format_status_bg"][0], "trending-post");
            ?>
            <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">          
            <?php 
        } else {
            ?>
            <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/status.jpg'; ?>" alt="<?php the_title(); ?>">       
            <?php 
        }
    } else {
        if ( has_post_thumbnail() ) {
                the_post_thumbnail('trending-post', array( 'alt' => get_the_title()));
        } else {
        ?>
        <img src="<?php echo THEME_URL; ?>/images/post/trending-no-thumb/image.jpg" alt="<?php the_title(); ?>" />
        <?php
        } //end else
    } //end else
?>