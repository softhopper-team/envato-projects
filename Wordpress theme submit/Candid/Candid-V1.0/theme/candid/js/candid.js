(function($) {
    "use strict";

    var themeApp = {

        // Preloder
        //------------------------------------------------

        candid_preloder: function() {
            function hidePreLoader() {
                $("#preloader").hide();
            }

            var preloader_bar_color = candid.preloader_bar_color;

            if ($("#preloader").length) {
                $("body").queryLoader2({
                    barColor: preloader_bar_color,
                    backgroundColor: "#fff",
                    percentage: true,
                    onLoadComplete: hidePreLoader(),
                    barHeight: 3
                });
            }
        },

        // Collapse the navbar on scroll
        //------------------------------------------------
        candid_navbar: function() {
            if ($(".navbar").offset().top > 50) {
                $(".navbar-fixed-top").addClass("top-nav-collapse");
            } else {
                $(".navbar-fixed-top").removeClass("top-nav-collapse");
            }
        },

        // Top Search
        //------------------------------------------------
        candid_top_search: function() {
            $('.header-search, .mheader-search').on('click', function() {
                $('#top-search-box').fadeIn('1000');
            });

            $('#top-search-box .close-search, #top-search-box .search-overlay').on('click', function() {
                $('#top-search-box').fadeOut('slow');
            });
        },

        // candid image hover
        //------------------------------------------------
        candid_image_hover: function() {
            if ($('.single-image').length) {
                var $images = $('.single-image');

                //single imge
                $images.filter(".plus-icon").fancybox({
                    'titleShow': true,
                    'padding': '10',
                    'transitionIn': 'fade',
                    'transitionOut': 'fade',
                    'easeingIn': 'easeOutBack',
                    'easeOut': 'easeInBack',
                    helpers: {
                        title: {
                            type: 'over'
                        }
                    }
                });
            }
        },

        // candid Video
        //------------------------------------------------
        candid_video: function() {
            $(".featured-area").fitVids();
            $("#content").fitVids();
        },

        // gallery post carousel
        //------------------------------------------------
        candid_gallary_two: function() {
            var sync1 = $(".full-view");
            var sync2 = $(".list-view");

            sync1.owlCarousel({
                singleItem: true,
                slideSpeed: 1000,
                navigation: true,
                pagination: false,
                lazyLoad: true,
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<i class='glyphicon glyphicon-menu-left'></i>",
                    "<i class='glyphicon glyphicon-menu-right'></i>"
                ]
            });
            var owl_item = candid.owl_item;
            sync2.owlCarousel({
                items: owl_item,
                itemsDesktop: [1199, 5],
                itemsDesktopSmall: [979, 5],
                itemsTablet: [768, 5],
                itemsMobile: [479, 2],
                pagination: false,
                responsiveRefreshRate: 100,
                addClassActive: true,
                stagePadding: 20,
                lazyLoad: true,
                navigation: true,
                navigationText: [
                    "<i class='glyphicon glyphicon-menu-left'></i>",
                    "<i class='glyphicon glyphicon-menu-right'></i>"
                ],
                afterInit: function(el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                sync2
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced");
                if ($("#list-view").data("owlCarousel") !== undefined) {
                    center(current);
                }
            }
            sync2.on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }
                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }
            }
        },

        // widget
        //------------------------------------------------
        candid_widget: function() {
            $(".widget").each(function() {
                if (!$(this).prev(".widget").length && $(this).find($(".widget-title")).length) {
                    $(this).css({
                        "margin-top": "1.3em"
                    });
                }
                if ($(this).next(".widget").find(".widget-title").length) {
                    $(this).css({
                        "margin-bottom": "3.3em"
                    });
                }
            });
        },

        // some initial style
        //------------------------------------------------
        candid_style: function() {

            if ($("body").find(".error-page").length) {
                $(".footer-top").css({
                    "background": "#ffffff"
                });
            }
            if (!$(".paging-navigation-post .nav-previous").length) {
                $(".paging-navigation-post").append("<style>.nav-links::before{display: none}</style>");
            }
            if (!$(".paging-navigation-post .nav-next").length) {
                $(".paging-navigation-post").append("<style>.nav-links::after{display: none}</style>");
            }
            $('.comment-reply-title').append('<span class="small-border"></span>');
            $( ".format-gallery .more-link" ).wrap( "<div class='more-link-area'></div>" );
        },

        // Related post
        //------------------------------------------------
        candid_related_post: function() {
            $('#related-post-slide').owlCarousel({
                items: 2,
                itemsDesktop: [1199, 2],
                itemsDesktopSmall: [979, 2],
                itemsTablet: [768, 1],
                itemsMobile: [479, 1],
                pagination: false,
                responsiveRefreshRate: 200,
                addClassActive: true,
                navigation: true,
                navigationText: [
                    "<i class='glyphicon glyphicon-menu-left'></i>",
                    "<i class='glyphicon glyphicon-menu-right'></i>"
                ],
                afterInit: function(elem) {
                    var that = this
                    that.owlControls.prependTo(elem)
                }
            });
        },

        // Scroll top
        //------------------------------------------------
        candid_scroll_top: function() {
            $("body").append("<a href='#top' class='topbutton'><span class='glyphicon glyphicon-menu-up'></span></a>");
            $("a[href='#top']").on('click', function() {
                $("html, body").animate({
                    scrollTop: 0
                }, "normal");
                return false;
            });
            $(".topbutton").hide();
            $(window).scroll(function() {
                if ($(this).scrollTop() < 600) {
                    $("a[href='#top']").fadeOut('fast');
                } else {
                    $("a[href='#top']").fadeIn('fast');
                }
            });
        },

        // Author Skill
        //------------------------------------------------
        candid_author_skill: function() {
            if ($('.skillbar').length) {
                var $skill = $('.skillbar');

                $skill.appear(function() {
                    $(this).find('.skillbar-bar, .skill-bar-shape').animate({
                        width: $(this).attr('data-percent')
                    }, 3000);
                });
            }

            $('.percent-area .skill-bar-percent').css('left', function() {
                return $(this).parent().data('percent')
            });

            $('.percent-area .skill-bar-percent').append(function() {
                return $(this).parent().data('percent')
            });
        },

        // Maps
        //------------------------------------------------
        candid_maps: function() {
            if ($('#gmaps').length) {
                var map;
                var lat = candid.lat;
                var lon = candid.lon;
                var map_mouse_wheel = candid.map_mouse_wheel;
                var map_zoom_control = candid.map_zoom_control;
                var map_point_img = candid.map_point_img;

                map = new GMaps({
                    el: '#gmaps',
                    lat: lat,
                    lng: lon,
                    scrollwheel: map_mouse_wheel,
                    zoom: 10,
                    zoomControl: map_zoom_control,
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: false,
                    overviewMapControl: false,
                    clickable: false
                });

                var image = map_point_img;
                map.addMarker({
                    lat: lat,
                    lng: lon,
                    icon: image,
                    animation: google.maps.Animation.DROP,
                    verticalAlign: 'bottom',
                    horizontalAlign: 'center'
                });


                var styles = [{
                    "featureType": "road",
                    "stylers": [{
                        "color": "#b4b4b4"
                    }]
                }, {
                    "featureType": "water",
                    "stylers": [{
                        "color": "#d8d8d8"
                    }]
                }, {
                    "featureType": "landscape",
                    "stylers": [{
                        "color": "#f1f1f1"
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#000000"
                    }]
                }, {
                    "featureType": "poi",
                    "stylers": [{
                        "color": "#d9d9d9"
                    }]
                }, {
                    "elementType": "labels.text",
                    "stylers": [{
                        "saturation": 1
                    }, {
                        "weight": 0.1
                    }, {
                        "color": "#000000"
                    }]
                }]
            }
        },


        /*Contact Form*/
        candid_contactForm: function() {
            var form = $('.contact-page #contact_form');
            form.submit(function(event) {
                event.preventDefault();
                var data = form.serialize();
                var form_status = $('<div class="form_status"></div>');
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    data: data,
                    url: $(this).attr('action'),

                    beforeSend: function() {
                        form.before(form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn());
                    }
                }).done(function(data) {
                    form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
                });
            });
        },

        /*Wp Admin bar*/
        candid_wp_adminbar: function() {
            // This function gets called with the user has scrolled the window.
            $(window).scroll(function() {
                if ($(this).scrollTop() > 0) {
                    // Add the scrolled class to those elements that you want changed
                    $(".navbar-fixed-top").addClass("scroll");
                } else {
                    $(".navbar-fixed-top").removeClass("scroll");
                }
            });
        },

        candid_initializ: function() {
            themeApp.candid_preloder();
            themeApp.candid_top_search();
            themeApp.candid_image_hover();
            themeApp.candid_video();
            themeApp.candid_gallary_two();
            themeApp.candid_widget();
            themeApp.candid_style();
            themeApp.candid_related_post();
            themeApp.candid_scroll_top();
            themeApp.candid_author_skill();
            themeApp.candid_maps();
            themeApp.candid_wp_adminbar();
        }
    };


    /* === document ready function === */
    $(document).ready(function() {
        themeApp.candid_initializ();
    });

    $(window).scroll(function() {
        themeApp.candid_navbar();
    });

    /* === function === */
    themeApp.candid_contactForm();

})(jQuery);