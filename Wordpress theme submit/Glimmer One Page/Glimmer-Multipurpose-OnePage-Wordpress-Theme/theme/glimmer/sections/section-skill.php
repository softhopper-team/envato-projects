<?php
    global $glimmer;
    $skill = glimmer_get_custom_posts("skill", 14);
?> 
<!-- skill
		================================================== --> 
		<section id="our-skill" class="our-skill">
			<div class="container">
				<div class="row">

					<div class="col-md-12 text-center">
						<header class="heading wow <?php if (isset($glimmer['section_skill_animation_style'])) echo $glimmer['section_skill_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_skill_animation_duration'])) echo $glimmer['section_skill_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_skill_animation_delay'])) echo $glimmer['section_skill_animation_delay']; ?>">
							<h2 class="title">
                               <?php if (isset($glimmer['section_skill_title'])) echo $glimmer['section_skill_title'];
                                    ?>
                            </h2>
                                <span class="small-border"></span>

                            <h5 class="subtitle"><?php if (isset($glimmer['section_skill_subtitle'])) echo $glimmer['section_skill_subtitle'];
                                    ?></h5>
						</header> <!-- /.heading -->
					</div><!--/. col-md-12--> 
				</div> <!-- /.row -->
	   
				<div class="row base-padding-three">
					
					<?php
                        foreach ($skill as $post) {
                        setup_postdata($post);
                        $meta = get_post_meta($post->ID);
                    ?> 
					<div class="col-sm-6 pd-big">
						<div class="skill-area">
							<div class="percent-area">
								<div class="skillbar" data-percent="<?php if(isset($meta["_glimmer_skill_percent"][0])) echo $meta["_glimmer_skill_percent"][0]; ?>">
									<div class="skillbar-title"><strong><?php the_title(); ?></strong></div>
									<div class="skillbar-bg">
										<div class="skillbar-bar"></div>	
									</div>
									<div class="skill-bar-percent wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>"></div>
								</div> <!-- /.skillbar --> 
							</div> <!-- /.percent-area -->
						</div> <!-- /.skill-area -->          
					</div> <!-- /.col-sm-6 -->
					<?php } ?>					
			   
				</div> <!-- /.row -->
			</div> <!-- /.container -->
		</section> <!-- /.skill -->