<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Glimmer
 */
?>
<?php
    global $glimmer;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" >
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

    <?php
        // Site Preloader
        if (  $glimmer['preloader'] == true ) {
        ?>
        <!-- Preloader -->
        <div class="preloader">
            <div class="preloader-logo">
                <img src="<?php if ( isset( $glimmer['preloader_logo']['url'] ) ) echo $glimmer['preloader_logo']['url']; ?>" class="img-responsive" alt="preloader">
            </div> <!-- /.preloader-logo -->
            <div class="loader">
                <?php 
                    $animated_icon = "";
                    if ( $glimmer['preloader_animated_icon'] == 1 ) {
                        $animated_icon = "fa-spinner fa-pulse";
                    } elseif ( $glimmer['preloader_animated_icon'] == 2 ) {
                        $animated_icon = "fa-spinner fa-spin";
                    } elseif ( $glimmer['preloader_animated_icon'] == 3 ) {
                        $animated_icon = "fa-circle-o-notch fa-spin";
                    } elseif ( $glimmer['preloader_animated_icon'] == 4 ) {
                        $animated_icon = "fa-refresh fa-spin";
                    } elseif ( $glimmer['preloader_animated_icon'] == 5 ) {
                        $animated_icon = "fa-cog fa-spin";
                    } 
                ?>
                <i class="fa <?php if ( isset( $animated_icon ) ) echo $animated_icon; ?>"></i>
            </div> <!-- /.loader -->
        </div> <!-- /.preloader -->
        <?php
        }
    ?>

    <!-- Header
    ================================================== -->
    <header id="masthead" role="banner">
        <div class="site-header" style="background-image: url(<?php if ( isset( $glimmer['header_bg_img']['url'] ) ) echo $glimmer['header_bg_img']['url']; ?>)">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="site-branding">
                            <div class="site-logo">
                                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php if ( isset( $glimmer['header_logo']['url'] ) ) echo $glimmer['header_logo']['url']; ?>" alt="logo" class="img-responsive"></a>
                            </div>
                        </div> <!-- .site-branding -->     
                    </div>
                </div>
            </div> <!-- /.container -->
        </div> <!-- /.site-header -->

        <nav class="main-nav" role="navigation">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="top-menu">
                        <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'main-menu',
                                'depth'             =>  0,
                                'menu_class'        => '',
                                'container'         => '',
                                'fallback_cb'       => 'Glimmer_Nav_Walker::fallback',
                                'walker'            => new Glimmer_Nav_Walker())
                            );
                        ?>
                        </div><!-- /.menu -->
                        <div id="top-search">
                            <div id="trigger-overlay">
                                <i class="ico-search"></i>
                            </div>
                            <div class="overlap overlay-slidedown">
                                <div class="search-row">
                                    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form" method="get" role="search">
                                        <button type="button" class="overlay-close"><?php _e( 'Close', 'glimmer' ); ?></button>
                                        <input name="s" value="" placeholder="<?php _e( 'Type keyword and hit Enter', 'glimmer' ); ?>" type="text">
                                        <?php if ( $glimmer['search_only_form_post'] == 1 ) : ?>
                                            <input type="hidden" value="post" name="post_type" id="post_type">
                                        <?php endif; ?>
                                    </form>
                                </div>
                            </div> 
                        </div> <!-- /.top-search -->

                        <div id="top-social">
                            <?php
                                // show social link in menu
                                if( isset ( $glimmer['header_menu_social_link'] )  && !empty ( $glimmer['header_menu_social_link']) ) {
                                    $social_link = $glimmer['header_menu_social_link'];                      
                                    foreach ( $social_link as $key => $value ) {
                                    if ( $value['title'] ) {
                                ?>
                                <a target="_blank" href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                                <?php
                                        }
                                    }
                                }
                            ?>  
                        </div> <!-- /.top-social -->   
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->

            </div> <!-- /.container -->
        </nav> <!-- #site-nav -->
    </header> <!-- #masthead -->