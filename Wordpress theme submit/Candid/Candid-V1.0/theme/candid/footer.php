<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Candid
 */
    global $candid;
?>
    <?php
        /* Show box or wide layout with user condition*/
        $layout = '';
        if ( $candid['layout'] == 1 ) {
            $layout = 'boxed';
        } 
    ?>
	<!-- Footer
    ================================================== --> 
    <footer id="footer">
        <?php if ( $candid['footer_widgets'] ) : ?>

        <div class="footer-top <?php echo $layout; ?>">
            <div class="container">
             
                <div class="row">  
                     
                    <?php
                        $columns = intval( $candid['footer_widget_columns'] );
                        $col_class = 12 / max( 1, $columns );
                        $col_class = "col-sm-$col_class col-md-$col_class";
                        for ( $i = 1; $i <= $columns ; $i++ ) {
                        ?>
                            <div class="widget-area footer-sidebar-<?php echo $i ?> <?php echo esc_attr( $col_class ) ?>">
                                <?php dynamic_sidebar( __( 'Footer ', 'candid' ) . $i ) ?>
                            </div>
                        <?php
                        }
                    ?>
                </div> <!-- /.row -->    
            </div> <!-- /.container -->    
        </div> <!-- /.footer-top -->

        <?php endif; ?>

        <div class="footer-bottom">
            <div class="container">   
                <div class="copyright">
                    <?php 
                        if ( isset ( $candid['footer_copyright_info'] ) ) echo $candid['footer_copyright_info'];
                    ?>  
                </div>
                <div class="navbar-right">
                    
                    <?php
                        wp_nav_menu( array(
                            'theme_location'    => 'footer-menu',
                            'depth'             =>  1,
                            'menu_class'        => 'footer-nav',
                            'container'        => '',
                            'fallback_cb'       => 'Candid_Nav_Walker::footer_nav_fallback',
                            'walker'            => new Candid_Nav_Walker())
                        );
                    ?>

                    <div class="footer-social">
                    <?php
                        if( isset ( $candid['footer_social_link'] )  && ! empty ( $candid['footer_social_link']) ) {
                            $social_link = $candid['footer_social_link'];                      
                            foreach ( $social_link as $key => $value ) {
                            if ( $value['title'] ) {
                    ?>
                        <a href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                    <?php
                                }
                            }
                        }
                    ?>    
                    </div> <!-- /.footer-social --> 
                </div> <!-- /.navbar-right -->
            </div> <!-- /.container -->
        </div> <!-- /.footer-bottom -->
    </footer>    

    <?php wp_footer(); ?>
    </body>
</html>