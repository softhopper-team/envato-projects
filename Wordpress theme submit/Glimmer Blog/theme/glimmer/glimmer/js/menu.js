(function($) {

    $.fn.menumaker = function(options) {

        var menu = $(this),
            settings = $.extend({
                format: "dropdown"
            }, options);

        return this.each(function() {
            menu.prepend('<div id="menu-button"></div>');
            $(this).find("#menu-button").on('click', function() {
                $(this).toggleClass('menu-opened');
                var mainmenu = $('#menu-top-menu');
                if (mainmenu.hasClass('open')) {
                    mainmenu.slideUp("fast").removeClass('open');
                } else {
                    mainmenu.slideDown("slow").addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });

            menu.find('li ul').parent().addClass('has-sub');

            multiTg = function() {
                menu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                menu.find('.submenu-button').on('click', function() {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide(400);
                    } else {
                        $(this).siblings('ul').addClass('open').show(400);
                    }
                });
            };

            if (settings.format === 'multitoggle') multiTg();
            else menu.addClass('dropdown');


            resizeFix = function() {
                if ($(window).width() > 992) {
                    menu.find('ul').show();
                }

                if ($(window).width() <= 992) {
                    menu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            return $(window).on('resize', resizeFix);
        });
    };

    $(document).ready(function() {
        $(".top-menu").menumaker({
            format: "multitoggle"
        });
        $(".top-menu").prepend("<div id='menu-line'></div>");

        $('#menu-line, #menu-button').wrapAll( "<div class='menu-button-area'></div>" );
        
        var foundActive = false,
            activeElement, linePosition = 0,
            menuLine = $(".top-menu #menu-line"),
            lineWidth, defaultPosition, defaultWidth;

        $(".top-menu > ul > li").each(function() {
            if ($(this).hasClass('active')) {
                activeElement = $(this);
                foundActive = true;
            }
        });

        if (foundActive === false) {
            activeElement = $(".top-menu > ul > li").first();
        }

        defaultWidth = lineWidth = activeElement.width();

        defaultPosition = linePosition = activeElement.position().left;

        menuLine.css("width", lineWidth);
        menuLine.css("left", linePosition);

        $(".top-menu > ul > li").hover(function() {
                activeElement = $(this);
                lineWidth = activeElement.width();
                linePosition = activeElement.position().left;
                menuLine.css("width", lineWidth);
                menuLine.css("left", linePosition);
            },
            function() {
                menuLine.css("left", defaultPosition);
                menuLine.css("width", defaultWidth);
            });

        var windowWidth = $(window).width();
        $(window).resize(function() {
            if ( windowWidth <= 992) {
                $('.top-menu').find('#menu-button').removeClass('menu-opened');
                $('.top-menu').find('.submenu-button').removeClass('submenu-opened');
            }
        });
    });
})(jQuery);
