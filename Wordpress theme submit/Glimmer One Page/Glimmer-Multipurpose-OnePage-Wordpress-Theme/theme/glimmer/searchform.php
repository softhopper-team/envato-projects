<?php
/**
 * The template for displaying search form.
 *
 * @package Glimmer
 */
?>
<div class="searchform">
    <form class="search-form" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
        <input class="col-xs-10" type="text" name="s" placeholder="<?php _e( 'Search here...', 'glimmer' ); ?>">
        <button id="submit-search" class="col-xs-2 button submit" type="submit">
            <i class="icon-magnifying"></i>
        </button>
    </form> 
</div> <!-- /.search-box -->  
