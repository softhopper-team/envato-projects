<?php
/**
 * The template for displaying aside post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>

<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-content" >
        <div class="blog-details clearfix">
        	<p class="aside-meta"><?php the_author(); ?><?php _e(' @ ','glimmer').the_time( get_option( 'date_format' ) ); ?></p>
            <?php the_content(); ?>		            
        </div> <!-- /.blog-details-full -->
    </div><!-- /.blog-content -->
</div> <!-- /.blog-content -->