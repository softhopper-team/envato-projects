<?php 

class Glimmer_Login_Form extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => 'Glimmer : Customized Login Form',
            'name' => 'Glimmer : Login Form'
        );
        parent::__construct('Glimmer_Login_Form','',$params);
    }   

    function widget( $args, $instance ) {
        extract( $args );
        /* User-selected settings. */
        $title = apply_filters('widget_title', $instance['title'] );

        /* Before widget (defined by themes). */
        echo $before_widget;

        /* Title of widget (before and after defined by themes). */
        if ( $title ) {
            echo $before_title . $title . $after_title;
        }

        ?>
        <div class="glimmer-login-form">

        <?php 

            if ( ! is_user_logged_in() ) {

                wp_login_form( $args );
                echo'<p class="lost-pass"><a href="'. wp_lostpassword_url().'" title="Lost Password">Lost Your Password?</a></p>';
                wp_register( "", "", true );

            } else {
                ?>
                    <div class="glimmer-login-board">
                       <div class="left-thumb">
                           <?php echo get_avatar( get_the_author_meta('ID')); ?>
                       </div> <!-- / . left-thumb -->
                       <div class="right-details">
                           <p><?php _e( "Welcome:","glimmer" );?> <strong><?php echo get_the_author_meta('display_name');?></strong></p>
                           <a href="<?php echo home_url() ; ?>/wp-admin"><?php _e( "Dashboard","glimmer" );?></a>
                           <a href="<?php echo get_edit_user_link( get_the_author_meta('ID') ) ?>"><?php _e( "Profile","glimmer" );?></a>
                           <a href="<?php echo wp_logout_url( home_url() ); ?>"><?php _e( "Logout","glimmer" );?></a>
                       </div> <!-- / . right-details -->
                   </div> <!-- / .glimmer-login-board -->
                <?php
            }

       echo "</div> <!-- / .glimmer-login-form -->";

        /* After widget (defined by themes). */
        echo $after_widget;
    }
    
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        /* Strip tags (if needed) and update the widget settings. */
        $instance['title'] = strip_tags( $new_instance['title'] );
        return $instance;
    }
    
     /** @see WP_Widget::form */
    function form( $instance ) {

            /* Set up some default widget settings. */
            $defaults = array(
                'title' => __('Login Form', 'glimmer'),
                );
            $instance = wp_parse_args( (array) $instance, $defaults ); ?>
        
            <p>
                <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'glimmer') ?></label>
                <input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
            </p>
        
       <?php 
    }
} //end class