<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Candid
 */
?>
<?php
    global $candid;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" >
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
    <?php
        // Preloader
        if (  $candid['preloader'] == true ) {
            echo '<div id="preloader"></div>';
        }
    ?>
    <!-- Header
    ================================================== -->
    <header id="header">

        <!-- Menu -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <img src="<?php if ( isset( $candid['header_nav_logo']['url'] ) ) echo $candid['header_nav_logo']['url']; ?>" alt="logo"/>
                    </a>

                    <ul class="mobile-nav-search">
                        <li>
                            <div class="header-search">
                                <div class="btn-search"><span class="icon icon-Search"></span></div>
                            </div> 
                        </li>
                    </ul>
                </div>

                <div class="navbar-collapse collapse">
                    <!-- Left nav -->
                    <div class="menu-left">
                        <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'main-menu',
                                'depth'             =>  0,
                                'menu_class'        => 'nav navbar-nav',
                                'container'        => '',
                                'fallback_cb'       => 'Candid_Nav_Walker::fallback',
                                'walker'            => new Candid_Nav_Walker())
                            );
                        ?>
                        <ul class="nav-search">
                            <li>
                                <div class="header-search">
                                    <div class="btn-search"><span class="icon icon-Search"></span></div>
                                </div> 
                            </li>
                        </ul>
                    </div>
                </div><!--/.nav-collapse -->

            </div> <!-- /.container -->
        </nav> <!-- /.nabvar -->

        <div id="top-search-box">
            <div class="search-overlay"></div>
            <div class="top-search-pos">
                <div class="top-search-inner">
                    <div class="search-title text-center">
                        <h3><?php _e( 'Do you want Search?', 'candid' ); ?></h3>
                    </div>
                    <form method="GET" class="top-search" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                        <input type="search" placeholder="<?php _e( 'Type and hit enter &#46;&#46;&#46;', 'candid' ); ?>" value="" name="s">
                        <span><button type="submit" class="search-submit"><i class="icon icon-Search"></i></button></span>
                    </form>
                    <span class="close-search"><?php _e( 'Close', 'candid' ); ?></span>
                </div> <!-- /.top-search-inner -->   
            </div> <!-- /.top-search-pos -->
        </div> <!-- /#top-search-box -->

        <!-- Blog Introduction -->
        <div class="site-header">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="site-wraper">
                            <div id="box">
                                <div class="site-branding">
                                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                                    <h4 class="site-description"><?php bloginfo( 'description' ); ?></h4>  <!-- /#site-description -->   
                                </div> <!-- /.site-branding -->     
                            </div> <!-- /#box -->                
                        </div> <!-- /#site-wraper -->    
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->        
            </div> <!-- /.container -->                
        </div> <!-- /.site-header -->
                     
    </header> <!-- /#header -->