<?php
/**
 * Hooks for template header
 *
 * @package Candid
 */

/**
 * Get favicon and home screen icons
 *
 * @since  1.0
 */
function candid_header_icons() {
	global $candid;
	$favicon = $candid['favicon']['url'];
	$header_icons =  ( $favicon ) ? '<link rel="shortcut icon" type="image/x-ico" href="' . esc_url( $favicon ) . '" />' : '';

	$icon_iphone = $candid['icon_iphone']['url'];
	$header_icons .= ( $icon_iphone ) ? '<link rel="apple-touch-icon" sizes="57x57"  href="' . esc_url( $icon_iphone ) . '" />' : '';

	$icon_iphone_retina = $candid['icon_iphone_retina']['url'];
	$header_icons .= ( $icon_iphone_retina ) ? '<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url( $icon_iphone_retina ). '" />' : '';

	$icon_ipad = $candid['icon_ipad']['url'];
	$header_icons .= ( $icon_ipad ) ? '<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( $icon_ipad ) . '" />' : '';

	$icon_ipad_retina = $candid['icon_ipad_retina']['url'];
	$header_icons .= ( $icon_ipad_retina ) ? '<link rel="apple-touch-icon" sizes="144x144" href="' . esc_url( $icon_ipad_retina ) . '" />' : '';

	echo $header_icons;
}
add_action( 'wp_head', 'candid_header_icons' );

/**
 * Custom scripts and styles on header
 *
 * @since  1.0
 */
function candid_header_scripts() {
	global $candid, $post;
	// Include color schemer code
	require THEME_DIR . '/inc/frontend/color-schemer.php';
	
	$custom_background = '';                          
	$custom_background = get_theme_mod( 'background_color' ); 
	if ( $custom_background == "") {
	    if ( $candid['layout'] == 1 ) {
	        $custom_background = "#f2f2f2";
	    } else {
	    	$custom_background = "#ffffff";
	    }
	}
	?>
	<style type="text/css"> 
		body {
			background: <?php echo $custom_background; ?>;
		}
		#header .site-header {
		  background: <?php if ( isset( $candid['header_bg_color'] ) ) echo $candid['header_bg_color']; ?> url(<?php if ( isset( $candid['header_bg_img']['url'] ) ) echo $candid['header_bg_img']['url']; ?>) !important;		  
	      background-size: cover !important;
		}
	</style> 						
	<?php
	// Custom CSS
	if ( isset( $post->ID ) ) $meta = get_post_meta( $post->ID );
	$inline_css='';
	isset( $meta["_candid_custom_css"][0] ) ? $candid_custom_css = $meta["_candid_custom_css"][0] : $candid_custom_css = '';
	isset( $candid['custom_css'] ) ? $custom_css = $candid['custom_css'] : $custom_css = '';
	$inline_css = $candid_custom_css . $custom_css;
	
	$inline_css = '<style type="text/css">' . $inline_css . '</style>';
	
	if( $inline_css ) {
		echo $inline_css;
	}	
}
add_action( 'wp_head', 'candid_header_scripts', 300 );
