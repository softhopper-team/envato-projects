<?php
class Glimmer_Aboutus_Contact extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => 'Glimmer : About Us &amp; Contact Info',
            'name' => 'Glimmer : About Us &amp; Contact'
        );
        parent::__construct('Glimmer_Aboutus_Contact','',$params);
    }

    /** @see WP_Widget::form */
    public function form( $instance) {
        extract($instance);
        ?>        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Widget Title:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p> 
        <p>
            <label for="<?php echo $this->get_field_id('about_title'); ?>"><?php _e('About Us &amp; Contact Title:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('about_title'); ?>"
                name="<?php echo $this->get_field_name('about_title'); ?>"
                value="<?php if( isset($about_title) ) echo esc_attr($about_title); ?>" />
        </p>        
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:','glimmer'); ?></label>
            <textarea 
                class="widefat" 
                rows="6" 
                cols="20" 
                id="<?php echo $this->get_field_id('description'); ?>" 
                name="<?php echo $this->get_field_name('description'); ?>"><?php if( isset($description) ) echo esc_attr($description); ?></textarea>
        </p> 
        <p>
            <label for="<?php echo $this->get_field_id('mobile'); ?>"><?php _e('Mobile:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('mobile'); ?>"
                name="<?php echo $this->get_field_name('mobile'); ?>"
                value="<?php if( isset($mobile) ) echo esc_attr($mobile); ?>" />
        </p> 
        <p>
            <label for="<?php echo $this->get_field_id('email_id'); ?>"><?php _e('Email Id:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('email_id'); ?>"
                name="<?php echo $this->get_field_name('email_id'); ?>"
                value="<?php if( isset($email_id) ) echo sanitize_email($email_id); ?>" />
        </p> 
        
      <?php       
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['about_title'] = $new_instance['about_title'];
        $instance['description'] = $new_instance['description'];
        $instance['mobile'] = strip_tags( $new_instance['mobile'] );
        $instance['email_id'] = strip_tags( $new_instance['email_id'] );
     
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters('title', $title);
        $about_title = apply_filters('about_title', $about_title);
        $description = apply_filters('description', $description);
        $mobile = apply_filters('mobile', $mobile);
        $email_id = apply_filters('email_id', $email_id);
        
       
        echo $before_widget;
            if ( !empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <div class="about-contact-area">
                <?php 
                    if ( !empty( $about_title ) ) {
                        echo "<h3>$about_title</h3>";
                    }

                    if ( !empty( $description ) ) {
                        echo "<p>$description</p>";
                    }
                ?>
                
                <ul class="about-mail">
                    <?php 
                    if ( !empty( $mobile ) ) {
                        ?>
                        <li>
                            <i class="fa fa-mobile"></i>
                            <span><?php echo $mobile; ?></span>
                        </li>
                        <?php
                    }
                    if ( !empty( $email_id ) ) {
                        ?>
                        <li>
                            <i class="fa fa-envelope"></i>
                            <a href="mailto:<?php echo $email_id; ?>"><?php echo $email_id; ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>                
            </div><!-- /.about-contact-area -->
            <?php
        echo $after_widget;
    } // end widget function
} // class Cycle Widget
?>
