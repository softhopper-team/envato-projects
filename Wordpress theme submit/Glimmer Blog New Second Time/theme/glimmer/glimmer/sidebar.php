<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Glimmer
 */
?>
<div class="col-md-4">
	<!-- Sidebar -->
	<div id="secondary" class="widget-area" role="complementary">
    	<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

            <?php the_widget('WP_Widget_Search', '', 'before_widget=<aside class="widget clearfix widget_search">&before_title=<div class="widget-title-area"><span class="widget-title-inner"></span><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); ?>
            
            <?php the_widget('WP_Widget_Categories', '', 'before_widget=<aside class="widget clearfix widget_categories">&before_title=<div class="widget-title-area"><span class="widget-title-inner"></span><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); ?>

            <?php the_widget('WP_Widget_Archives', '', 'before_widget=<aside class="widget clearfix widget_archive">&before_title=<div class="widget-title-area"><span class="widget-title-inner"></span><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); ?>

            <?php the_widget('WP_Widget_Tag_Cloud', '', 'before_widget=<aside class="widget clearfix widget_tag_cloud">&before_title=<div class="widget-title-area"><span class="widget-title-inner"></span><h5 class="widget-title"><span>&after_title=</span></h5></div>&after_widget=</aside>'); ?>

    	<?php endif; // end sidebar widget area ?>
    </div> <!-- #secondary -->	
</div> <!-- .col-md-4 --> 