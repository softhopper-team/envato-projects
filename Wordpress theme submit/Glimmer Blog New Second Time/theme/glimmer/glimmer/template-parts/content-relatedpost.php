<?php
/**
 * The template for displaying related posts in the single page
 *
 * @package Glimmer
 */
global $glimmer, $post;
?>
<?php if ( $glimmer['related_post'] ) : ?>
<?php
    $query_type = $glimmer['related_query'];
    $related_no = 2;
    if ( $query_type == 'author' ) {
        $args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no, 'no_found_rows'=> 1, 'author'=> get_the_author_meta( 'ID' ));
    } elseif ( $query_type == 'tag' ) {
        $tags = wp_get_post_tags($post->ID);
        $tags_ids = array();
        foreach($tags as $individual_tag) $tags_ids[] = $individual_tag->term_id;
        $args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no, 'no_found_rows'=> 1, 'tag__in'=> $tags_ids );
    } else {
        $categories = get_the_category($post->ID);
        $category_ids = array();
        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
        $args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no, 'no_found_rows'=> 1, 'category__in'=> $category_ids );
    }       
    
    $my_query = new wp_query( $args );
?>
<?php
    if( $my_query->have_posts() ) :
?>
    <!-- related post -->
    <div class="related-post clearfix">
        <h3 class="related-post-title"><?php _e('You may also like','glimmer'); ?></h3>
        <div class="row">
            <?php
                while( $my_query->have_posts() ) {
                $my_query->the_post();
            ?>
            <div class="col-md-4 col-sm-6">
                <div class="related-post-item">
                    <?php
                        $meta = get_post_meta( get_the_ID() );
                        $post_format = get_post_format();
                        if ( false === $post_format ) {
                            $post_format = "standard";
                        }
                        
                        if ( $post_format == "gallery" ) {  
                            // show first image of gallery post
                            if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
                                $imgs_urls = $meta["_glimmer_format_gallery"][0];  
                                $imgs_url = explode( '"', $imgs_urls );
                                $img_crop_url = sh_crop_image_url($imgs_url[1], "related-posts");
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php                           
                            }  else {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo THEME_URL.'/images/post/no-media-big/gallery.jpg'; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            }
                        } elseif ( $post_format == "audio" ){
                            if( isset($meta["_glimmer_format_audio_bg_img"][0]) ) {
                                $img_crop_url = sh_crop_image_url($meta["_glimmer_format_audio_bg_img"][0], "related-posts");
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            } else {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo THEME_URL.'/images/post/no-media-big/audio.jpg'; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            }
                        } elseif ( $post_format == "video" ){
                            if ( has_post_thumbnail() ) {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                            the_post_thumbnail('related-posts', array( 'class' => "latest-item-thumb", 'alt' => get_the_title()));
                                        ?>
                                    </a>
                                </div>
                                <?php
                            } else {
                            ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                       <img src="<?php echo THEME_URL; ?>/images/post/no-media-big/video.jpg" alt="<?php the_title(); ?>" />
                                    </a>
                                </div>
                            <?php
                            } //end else
                        } elseif ( $post_format == "quote" ){
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo THEME_URL.'/images/post/no-media-big/quote.jpg'; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                        } elseif ( $post_format == "aside" ){
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo THEME_URL.'/images/post/no-media-big/aside.jpg'; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                        } elseif ( $post_format == "chat" ){
                            if ( has_post_thumbnail() ) {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                            the_post_thumbnail('related-posts', array( 'class' => "latest-item-thumb", 'alt' => get_the_title()));
                                        ?>
                                    </a>
                                </div>
                                <?php
                            } else {
                            ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                       <img src="<?php echo THEME_URL; ?>/images/post/no-media-big/chat.jpg" alt="<?php the_title(); ?>" />
                                    </a>
                                </div>
                            <?php
                            } //end else
                        } elseif ( $post_format == "link" ){
                            if( isset($meta["_glimmer_format_link_bg_img"][0]) ) {
                                $img_crop_url = sh_crop_image_url($meta["_glimmer_format_link_bg_img"][0], "related-posts");
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            } else {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo THEME_URL.'/images/post/no-media-big/link.jpg'; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            }
                        } elseif ( $post_format == "status" ){
                            if( isset($meta["_glimmer_format_status_bg"][0]) ) {
                                $img_crop_url = sh_crop_image_url($meta["_glimmer_format_status_bg"][0], "related-posts");
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            } else {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo THEME_URL.'/images/post/no-media-big/status.jpg'; ?>" alt="<?php the_title(); ?>"> 
                                    </a>                            
                                </div>
                                <?php 
                            }
                        } else {
                            if ( has_post_thumbnail() ) {
                                ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                        <?php
                                            the_post_thumbnail('related-posts', array( 'class' => "latest-item-thumb", 'alt' => get_the_title()));
                                        ?>
                                    </a>
                                </div>
                                <?php
                            } else {
                            ?>
                                <div class="post-media">
                                    <a href="<?php the_permalink(); ?>">
                                       <img src="<?php echo THEME_URL; ?>/images/post/no-media-big/image.jpg" alt="<?php the_title(); ?>" />
                                    </a>
                                </div>
                            <?php
                            } //end else
                        } //end else
                    ?>
                    <div class="post-body">
                        <div class="entry-date">
                            <?php the_time( get_option( 'date_format' ) ); ?>
                        </div>
                        <?php       
                            the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
                        ?>
                    </div>
                </div> <!-- /.related-post-item -->  
            </div> <!-- /.col-md-4 -->           
            <?php   
                } // end while loop
            ?> 
        </div> <!-- /.row -->    
    </div> <!-- /.related-post -->
    <?php 
    endif;  
    wp_reset_query();
?> 
<?php endif; ?>