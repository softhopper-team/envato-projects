<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Glimmer
 */
?>
<?php 
    get_header(); 
    global $glimmer;
?>
<!-- Content
================================================== -->
<div id="content" class="site-content error-page">
    <div class="container">
        <div class="row">
            <div class="col md-12">
                <div class="error-description">
                    <h2><?php if ( isset ( $glimmer['404_heading'] ) ) echo $glimmer['404_heading']; ?></h2> 
                    <h5><?php if ( isset ( $glimmer['404_subheading'] ) ) echo $glimmer['404_subheading']; ?></h5>   
                </div> <!-- /.error-description -->
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->

        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <div class="error-image clearfix">
                    <img src="<?php if ( isset ( $glimmer['404_img']['url'] ) ) echo $glimmer['404_img']['url']; ?>" alt="404" class="img-responsive">
                </div> <!-- /.error-image -->
                <span class="border-left"></span>   
            </div> <!-- /.col-md-8 -->

            <div class="col-md-6">
                <div class="search">
                    <?php get_search_form(); ?>                               
                </div> <!-- /.search-form -->

                <div class="goto-button">                
                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="go-to">
                        <span class="go-button"><?php esc_html_e( 'Go to Homepage', 'glimmer' ) ?></span>
                    </a>
                    <?php 
                        //get contact page template url by template slug
                        $pages = get_pages(array(
                            'meta_key' => '_wp_page_template',
                            'meta_value' => 'contact-page.php'
                        ));
                        $contact_page_url = '';
                        foreach($pages as $page){
                            $contact_page_url = $page->ID;
                        }
                        ( !empty($contact_page_url) ) ? $contact_page_url  = get_permalink( $contact_page_url ) : $contact_page_url = "#";
                    ?>
                    <a href="<?php echo $contact_page_url; ?>" class="go-to">
                        <span class="go-button"><?php esc_html_e( 'Contact', 'glimmer' ) ?></span>
                    </a>
                </div> <!-- /.goto-button -->   
            </div> <!-- /.col-md-4 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->    
</div><!-- #content -->
<?php get_footer(); ?>