<?php
/**
 * The template for displaying search form.
 *
 * @package Candid
 */
?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form" method="get" role="search">
    <label>
        <span class="screen-reader-text"><?php _e( 'Search for:', 'candid' ); ?></span>
        <input type="search" title="Search for:" name="s" value="" placeholder="<?php _e( 'Search here...', 'candid' ); ?>" class="search-field">
    </label>
    <input type="submit" value="Search" class="search-submit screen-reader-text">
</form>
