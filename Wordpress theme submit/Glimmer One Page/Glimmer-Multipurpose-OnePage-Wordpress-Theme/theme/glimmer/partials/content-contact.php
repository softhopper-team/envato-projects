<?php
	global $glimmer;
?>	
<?php
	$contact_blog_bg = "";
	if( get_current_template() == "landing-page.php" ) {
		$contact_blog_bg = "";
	} else {
		$contact_blog_bg = "contact-blog-bg";
	}
?>
<!-- contact
================================================== --> 
<section id="contact" class="contact-us <?php echo $contact_blog_bg; ?>">

	<div class="container">

		<div class="row">
			<div class="col-md-12 text-center">
				<header class="heading wow <?php if (isset($glimmer['section_contact_animation_style'])) echo $glimmer['section_contact_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_contact_animation_duration'])) echo $glimmer['section_contact_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_contact_animation_delay'])) echo $glimmer['section_contact_animation_delay']; ?>">
                    <h2 class="title">
                        <?php 
                            if (isset($glimmer['section_contact_title'])) echo $glimmer['section_contact_title'];
                        ?>
                    </h2>
                    <span class="small-border"></span>
                    <h5 class="subtitle">
                        <?php 
                            if (isset($glimmer['section_contact_subtitle'])) echo $glimmer['section_contact_subtitle'];
                        ?>
                    </h5>
                </header> <!-- /.heading -->
			</div> <!-- /.col-md-12 -->
		</div> <!-- /.row -->

	</div> <!-- /.container -->
	<?php
		if( get_current_template() == "landing-page.php" ) {
	?>
		<div id="find-on-maps" class="find-on-maps base-padding-two">
			<div class="maps-container wow fadeInUp" data-wow-duration="2s">
				<div id="gmap">					
				</div>              
			</div> <!-- /.maps-container -->
		</div> <!-- /.find-on-maps -->
	<?php
		} 
	?>

	<div class="container">
		<div class="row base-padding">
			<div class="col-md-7 clearfix">
				<form id="contact-us" class="contact-form wow <?php if (isset($glimmer['section_contact_form_animation_style'])) echo $glimmer['section_contact_form_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_contact_form_animation_duration'])) echo $glimmer['section_contact_form_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_contact_form_animation_delay'])) echo $glimmer['section_contact_form_animation_delay']; ?>" method="post" action="<?php echo get_stylesheet_directory_uri(); ?>/partials/sendmail.php">

					<p class="input-block">
						<input class="col-xs-12" type="text" name="name" id="name" required="required" placeholder="<?php _e('Your Name :','glimmer') ?>" />		
					</p>

					<p class="input-block">
						<input class="col-xs-12" type="email" name="email" id="email" required="required" placeholder="<?php _e('Your Email :','glimmer') ?>" />						
					</p>

					<p class="input-block">
						<input class="col-xs-12" type="text" name="subject" id="subject" required="required" placeholder="<?php _e('Subject :','glimmer') ?>" />						
					</p>

					<p class="input-block">
						<textarea class="col-xs-12" name="message" id="message" placeholder="<?php _e('Your Text :','glimmer') ?>"></textarea>  
					</p>  

					<p class="input-block">
						<button class="col-xs-12 button submit" type="submit" id="submit"><?php _e('Submit','glimmer') ?></button>
					</p>
				</form><!--/ .contact-form-->   
			</div> <!-- /.col-md-7 -->

			<div class="col-md-5 clearfix pd-bigger">
				<div class="where-we wow <?php if (isset($glimmer['section_contact_address_animation_style'])) echo $glimmer['section_contact_address_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_contact_address_animation_duration'])) echo $glimmer['section_contact_address_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_contact_address_animation_delay'])) echo $glimmer['section_contact_address_animation_delay']; ?>">
					<?php if (isset($glimmer['section_contact_description'])) echo $glimmer['section_contact_description'];
                    ?>
				</div> <!-- /.what we are --> 
			</div> <!-- /.col-md-5 -->
		</div>
	</div> <!-- /.container -->
</section> <!-- /.contact-us -->