<?php
function glimmer_scripts() {
    global $glimmer;
	wp_enqueue_script("jquery");

    if( get_current_template() == "landing-page.php" ) {
        //enqueue for landing page
        //Include all css file
        wp_enqueue_style('google-railway-font', "//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900");
        wp_enqueue_style('font-awesome', get_template_directory_uri() . "/style/css/font-awesome.css");
        wp_enqueue_style('softhopper-icon', get_template_directory_uri() . "/style/css/softhopper-icon.css");
        wp_enqueue_style('bootstrap-min', get_template_directory_uri() . "/style/css/bootstrap.min.css");
        wp_enqueue_style('smartmenus-min', get_template_directory_uri() . "/style/css/jquery.smartmenus.bootstrap.css");
        wp_enqueue_style('animation', get_template_directory_uri() . "/style/css/animation.min.css");
        wp_enqueue_style('headeranimation', get_template_directory_uri() . "/style/css/headeranimation.css");
        wp_enqueue_style('owl-carousel', get_template_directory_uri() . "/style/css/owl.carousel.css");
        wp_enqueue_style('owl-theme', get_template_directory_uri() . "/style/css/owl.theme.css");
        wp_enqueue_style('pik', get_template_directory_uri() . "/style/css/pik.css");
        wp_enqueue_style('glimmer-main', get_template_directory_uri() . "/style/css/glimmer.min.css");
        wp_enqueue_style('supersized', get_template_directory_uri() . "/style/css/supersized.css");
        wp_enqueue_style('supersized-shutter', get_template_directory_uri() . "/style/css/supersized.shutter.css");
        wp_enqueue_style( 'glimmer-style', get_stylesheet_uri() );

        // //Include all js file
        wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.0.min.js', array(), '2.8.0', false);
        wp_enqueue_script('jquery-new', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', array(), '1.11.1', false);
        wp_enqueue_script('bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.min.js', array("jquery"), '1', true);
        wp_enqueue_script('smartmenus-min', get_template_directory_uri() . '/js/jquery.smartmenus.min.js', array("jquery"), '1', true);
        wp_enqueue_script('smartmenus-bootstrap', get_template_directory_uri() . '/js/jquery.smartmenus.bootstrap.min.js', array("jquery"), '1', true);
        wp_enqueue_script('supersizedjs', get_template_directory_uri() . '/js/supersized.3.2.7.min.js', array("jquery"), '1', true);
        wp_enqueue_script('supersized-shutter-js', get_template_directory_uri() . '/js/supersized.shutter.min.js', array("jquery"), '1', true);
        wp_enqueue_script('waypoints-js', get_template_directory_uri() . '/js/jquery.waypoints.min.js', array("jquery"), '1', true);
        wp_enqueue_script('sticky-min-js', get_template_directory_uri() . '/js/sticky.min.js', array("jquery"), '1', true); 
        wp_enqueue_script('wow-min-js', get_template_directory_uri() . '/js/wow.min.js', array("jquery"), '1', true);
        wp_enqueue_script('parallax-js', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array("jquery"), '1', true);
        wp_enqueue_script('appear-js', get_template_directory_uri() . '/js/jquery.appear.js', array("jquery"), '1', true);
        wp_enqueue_script('countTo-js', get_template_directory_uri() . '/js/jquery.countTo.js', array("jquery"), '1', true);
        wp_enqueue_script('isotope-js', get_template_directory_uri() . '/js/jquery.isotope.min.js', array("jquery"), '1', true);
        wp_enqueue_script('owl-carousel-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array("jquery"), '1', true);
        wp_enqueue_script('googleapis-js', 'http://maps.googleapis.com/maps/api/js?sensor=true', array("jquery"), '1', true);
        wp_enqueue_script('gmaps-js', get_template_directory_uri() . '/js/gmaps.min.js', array("jquery"), '1', true);
        wp_enqueue_script('plugins-js', get_template_directory_uri() . '/js/plugins.js', array("jquery"), '1', true);
        wp_enqueue_script('glimmer-js', get_template_directory_uri() . '/js/glimmer.js', array("jquery"), '1', true); 
         
        if (isset($glimmer['section_contact_lat']) && isset($glimmer['section_contact_lon'])) {
            wp_localize_script("glimmer-js", "geo", array("lat" => $glimmer['section_contact_lat'], "lon" => $glimmer['section_contact_lon'], "map_point_img" => $glimmer['section_contact_map_point_img'], "home_uri" => get_template_directory_uri()));
        }
    } else {
        //enqueue without landing page
        // Include all css file
        wp_enqueue_style('google-railway-font', "//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900");
        wp_enqueue_style('font-awesome', get_template_directory_uri() . "/style/css/font-awesome.css");
        wp_enqueue_style('softhopper-icon', get_template_directory_uri() . "/style/css/softhopper-icon.css");
        wp_enqueue_style('bootstrap-min', get_template_directory_uri() . "/style/css/bootstrap.min.css");
        wp_enqueue_style('smartmenus-min', get_template_directory_uri() . "/style/css/jquery.smartmenus.bootstrap.css");
        wp_enqueue_style('animation', get_template_directory_uri() . "/style/css/animation.min.css");
        wp_enqueue_style('glimmer-main', get_template_directory_uri() . "/style/css/glimmer.min.css");
        wp_enqueue_style( 'glimmer-style', get_stylesheet_uri() );

        // Include all js file
        wp_enqueue_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.0.min.js', array(), '2.8.0', false);
        wp_enqueue_script('jquery-new', '//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js', array(), '1.11.1', false);
        wp_enqueue_script('bootstrap-min-js', get_template_directory_uri() . '/js/bootstrap.js', array("jquery"), '1', true);
        wp_enqueue_script('smartmenus-min', get_template_directory_uri() . '/js/jquery.smartmenus.min.js', array("jquery"), '1', true);
        wp_enqueue_script('smartmenus-bootstrap', get_template_directory_uri() . '/js/jquery.smartmenus.bootstrap.min.js', array("jquery"), '1', true);
        wp_enqueue_script('waypoints-js', get_template_directory_uri() . '/js/jquery.waypoints.min.js', array("jquery"), '1', true);
        wp_enqueue_script('sticky-min-js', get_template_directory_uri() . '/js/sticky.min.js', array("jquery"), '1', true); 
        wp_enqueue_script('wow-min-js', get_template_directory_uri() . '/js/wow.min.js', array("jquery"), '1', true);
        wp_enqueue_script('parallax-js', get_template_directory_uri() . '/js/jquery.parallax-1.1.3.js', array("jquery"), '1', true);
        wp_enqueue_script('plugins-js', get_template_directory_uri() . '/js/plugins.js', array("jquery"), '1', true);
        wp_enqueue_script('glimmer-blog-js', get_template_directory_uri() . '/js/glimmer-blog.js', array("jquery"), '1', true); 
         
        if (isset($glimmer['section_contact_lat']) && isset($glimmer['section_contact_lon'])) {
            wp_localize_script("glimmer-blog-js", "geo", array("lat" => $glimmer['section_contact_lat'], "lon" => $glimmer['section_contact_lon'], "map_point_img" => $glimmer['section_contact_map_point_img'], "home_uri" => get_template_directory_uri()));
        }
    }

  

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'glimmer_scripts' );



function addAndOverridePanelCSS() {
  //wp_dequeue_style( 'redux-admin-css' );
    wp_enqueue_style('redux-font-awesome', get_template_directory_uri() . "/style/css/font-awesome.css");
    wp_register_style(
    'redux-custom-css',
    get_template_directory_uri() . '/ReduxFramework/glimmer-custom-assets/glimmer-redux-custom.css',
    array( 'farbtastic' ), // Notice redux-admin-css is removed and the wordpress standard farbtastic is included instead
    time(),
    'all'
  );    
  wp_enqueue_style('redux-custom-css');
}
// This example assumes your opt_name is set to redux_demo, replace with your opt_name value
add_action( 'redux/page/glimmer/enqueue', 'addAndOverridePanelCSS' );

function glimmer_redux_custom_js() {
    wp_enqueue_script('glimmer-custom-js', get_template_directory_uri() . '/ReduxFramework/glimmer-custom-assets/glimmer-redux-custom.js', array("jquery"), '1', true);
}
add_action( 'admin_enqueue_scripts', 'glimmer_redux_custom_js' );