<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB2 directory)
 *
 * Be sure to replace all instances of 'candid_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Demo_CMB2
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
}

/**
 * Conditionally displays a metabox when used as a callback in the 'show_on_cb' cmb2_box parameter
 *
 * @param  CMB2 object $cmb CMB2 object
 *
 * @return bool             True if metabox should show
 */
function candid_show_if_front_page( $cmb ) {
	// Don't show this metabox if it's the front page template
	if ( $cmb->object_id !== get_option( 'page_on_front' ) ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a field when used as a callback in the 'show_on_cb' field parameter
 *
 * @param  CMB2_Field object $field Field object
 *
 * @return bool                     True if metabox should show
 */
function candid_hide_if_no_cats( $field ) {
	// Don't show this field if not in the cats category
	if ( ! has_tag( 'cats', $field->object_id ) ) {
		return false;
	}
	return true;
}

/**
 * Conditionally displays a message if the $post_id is 2
 *
 * @param  array             $field_args Array of field parameters
 * @param  CMB2_Field object $field      Field object
 */
function candid_before_row_if_2( $field_args, $field ) {
	if ( 2 == $field->object_id ) {
		echo '<p>Testing <b>"before_row"</b> parameter (on $post_id 2)</p>';
	} else {
		echo '<p>Testing <b>"before_row"</b> parameter (<b>NOT</b> on $post_id 2)</p>';
	}
}

add_action( 'cmb2_init', 'candid_register_demo_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */
function candid_register_demo_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_candid_';

	/**
	 * Sample metabox to demonstrate each field type included
	 */
	$cmb_post_format = new_cmb2_box( array(
		'id'            => $prefix . 'post_format_details',
		'title'         => __( 'Format Details', 'candid' ),
		'object_types'  => array( 'page', 'post' ), // Post type
		//'show_on_cb'    => 'candid_show_if_front_page', // function should return a bool value
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );
	$cmb_post_format->add_field( array(
	    'name'    => 'Gallery Style',
	    'id'      => $prefix . 'gallery_style',
	    'desc'      => __('You can change gallery style from here', 'candid' ),
	    'type'    => 'radio_inline',
	    'row_classes'   => 'gallery radio-img',
	    'default'   => 'gallery-one',
	    'options' => array(
	        'gallery-one' => __( '<img src="'.THEME_URL . '/images/backend/meta-box/gallery-one.jpg'.'">', 'candid' ),
	        'gallery-two'   => __( '<img src="'.THEME_URL . '/images/backend/meta-box/gallery-two.jpg'.'">', 'candid' ),	        
	    ),
	) );
	$cmb_post_format->add_field( array(
        'name' => __('Add your gallery images', 'candid' ),
        'desc' => __('Image size should 750x338', 'candid' ),
        'id' => $prefix . 'format_gallery',
        'type' => 'file_list',
        'row_classes'   => 'gallery',
        'preview_size' => array( 100, 100 ),
    ) );
	$cmb_post_format->add_field( array(
		'name' => __( 'Audio embed code', 'candid' ),
		'desc' => __( 'Paster your audio embed code here', 'candid' ),
		'id'   => $prefix . 'format_audio',
		'type' => 'textarea_code',
		'row_classes'   => 'audio',
	) );
    $cmb_post_format->add_field( array(
		'name' => __( 'Video embed code', 'candid' ),
		'desc' => __( 'Paster your video embed code here', 'candid' ),
		'id'   => $prefix . 'format_video',
		'type' => 'textarea_code',
		'row_classes'   => 'video',
	) ); 

	$cmb_post_format->add_field( array(
		'name' => __( 'Quote', 'candid' ),
		'id'   => $prefix . 'format_quote',
		'type' => 'textarea_small',
		'row_classes'   => 'quote',
	) );
	$cmb_post_format->add_field( array(
		'name' => __( 'Author', 'candid' ),
		'id'   => $prefix . 'format_quote_author',
		'type' => 'text',
		'row_classes'   => 'quote',
	) );
	$cmb_post_format->add_field( array(
		'name' => __( 'URL', 'candid' ),
		'id'   => $prefix . 'format_quote_url',
		'type' => 'text',
		'row_classes'   => 'quote',
	) );
	$cmb_post_format->add_field( array(
		'name' => __( 'Link', 'candid' ),
		'id'   => $prefix . 'format_link',
		'type' => 'text',
	) );
	$cmb_post_format->add_field( array(
		'name' => __( 'Text', 'candid' ),
		'id'   => $prefix . 'format_link_text',
		'type' => 'text',
	) );

	$cmb_display_settings = new_cmb2_box( array(
		'id'            => $prefix . 'display_settings',
		'title'         => __( 'Display Settings', 'candid' ),
		'object_types'  => array( 'page', 'post' ), // Post type
		//'show_on_cb'    => 'candid_show_if_front_page', // function should return a bool value
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // true to keep the metabox closed by default
	) );
	$cmb_display_settings->add_field( array(
	    'name'    => 'Custom Layout',
	    'id'      => $prefix . 'custom_layout',
	    'type'    => 'checkbox',
	    'default'   => false,
	) );
	$cmb_display_settings->add_field( array(
	    'name'    => 'Layout',
	    'id'      => $prefix . 'layout',
	    'row_classes'   => 'radio-img',
	    'desc'      => __('You can change layout', 'candid' ),
	    'type'    => 'radio_inline',
	    'default'   => 'content-sidebar',
	    'options' => array(
	        'full-content' => __( '<img src="'.THEME_URL . '/images/backend/sidebars/empty.png'.'">', 'candid' ),
	        'sidebar-content'   => __( '<img src="'.THEME_URL . '/images/backend/sidebars/single-left.png'.'">', 'candid' ),
	        'content-sidebar'   => __( '<img src="'.THEME_URL . '/images/backend/sidebars/single-right.png'.'">', 'candid' ),	        
	    ),
	) );
	$cmb_display_settings->add_field( array(
		'name' => __( 'Custom CSS', 'candid' ),
		'desc' => __( 'Write your custom CSS code here without &lt;style&gt; &lt;/style&gt; tag block', 'candid' ),
		'id'   => $prefix . 'custom_css',
		'type' => 'textarea_small',
	) ); 
	$cmb_display_settings->add_field( array(
		'name' => __( 'Custom JS', 'candid' ),
		'desc' => __( 'Write your custom JS code here without &lt;script&gt; &lt;/script&gt; tag block', 'candid' ),
		'id'   => $prefix . 'custom_js',
		'type' => 'textarea_small',
	) ); 

}


add_action( 'cmb2_init', 'candid_register_user_profile_metabox',6 );
/**
 * Hook in and add a metabox to add fields to the user profile pages
 */
function candid_register_user_profile_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_candid_user_';
	/**
	 * Metabox for the user profile screen
	 */
	$cmb_user = new_cmb2_box( array(
		'id'               => $prefix . 'edit',
		'title'            => __( 'User Profile Metabox', 'candid' ),
		'object_types'     => array( 'user' ), // Tells CMB2 to use user_meta vs post_meta
		'show_names'       => true,
		'new_user_section' => 'add-new-user', // where form will show on new user page. 'add-existing-user' is only other valid option.
	) );

	$cmb_user->add_field( array(
		'name'     => __( 'Extra Info', 'candid' ),
		'id'       => $prefix . 'extra_info',
		'type'     => 'title',
		'on_front' => false,
	) );
	
	$cmb_user->add_field( array(
		'name' => __( 'Author Position', 'candid' ),
		'desc' => __( 'This info will show in post author theme area', 'candid' ),
		'id'   => $prefix . 'author_position',
		'type' => 'text',
	) );

	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
	$group_field_id = $cmb_user->add_field( array(
		'id'          => $prefix . 'social_link',
		'type'        => 'group',
		'options'     => array(
			'group_title'   => __( 'Social Link {#}', 'candid' ), // {#} gets replaced by row number
			'add_button'    => __( 'Add New Social Link', 'candid' ),
			'remove_button' => __( 'Remove Social Link', 'candid' ),
			'sortable'      => true, // beta
		),
	) );
	$cmb_user->add_group_field( $group_field_id, array(
		'name'       => __( 'Font awesome icon class', 'candid' ),
		'id'         => 'social_icon',
		'type'       => 'text',
	) );

	$cmb_user->add_group_field( $group_field_id, array(
		'name'       => __( 'Social link url', 'candid' ),
		'id'         => 'social_url',
		'type'       => 'text',
	) );
}
