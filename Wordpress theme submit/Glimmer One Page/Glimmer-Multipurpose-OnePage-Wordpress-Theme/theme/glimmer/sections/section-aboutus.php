<?php
    global $glimmer;
    $aboutus = glimmer_get_custom_posts("aboutus", 3);
    $classes = array(0 => "", "1" => "col-md-12", "2" => "col-md-6", "3" => "col-md-4", "4" => "col-md-3");
    $class = $classes[count($aboutus)];

    $aboutus_second_row = glimmer_get_custom_posts_with_offset("aboutus", 3, 3);
    $second_row_classes = array(0 => "", "1" => "col-md-12", "2" => "col-md-6", "3" => "col-md-4", "4" => "col-md-3");
    $second_row_class = $second_row_classes[count($aboutus_second_row)];
?> 
<!-- about
================================================== --> 
<section id="about-us" class="about-us">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <header class="heading wow <?php if (isset($glimmer['section_aboutus_animation_style'])) echo $glimmer['section_aboutus_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_aboutus_animation_duration'])) echo $glimmer['section_aboutus_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_aboutus_animation_delay'])) echo $glimmer['section_aboutus_animation_delay']; ?>">
                    <h2 class="title">
                        <?php 
                            if (isset($glimmer['section_aboutus_title'])) echo $glimmer['section_aboutus_title'];
                        ?>
                    </h2>
                    <span class="small-border"></span>
                    <h5 class="subtitle">
                        <?php 
                            if (isset($glimmer['section_aboutus_subtitle'])) echo $glimmer['section_aboutus_subtitle'];
                        ?>
                    </h5>
                </header> <!-- /.heading -->
            </div><!--/. col-lg-12-->   
        </div> <!-- /.row -->
    </div> <!-- /.container -->

    <div class="about-content base-padding">
        <div class="container">
            <div class="row">
                <?php
                    foreach ($aboutus as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?> 
                <div class="<?php echo $class; ?> pd-big">
                    <div class="about-service text-center wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                        <div class="about-icon">
                            <i class="fa <?php if(isset($meta["_glimmer_icon_code"][0])) echo $meta["_glimmer_icon_code"][0]; ?>"></i> 
                        </div> <!-- /.about-icon -->
                        
                        <div class="about-description">
                            <h4><?php the_title(); ?></h4>
                                <?php the_content(); ?>
                        </div> <!-- /.about-description -->
                    </div> <!-- /.about-service -->
                </div> <!--/.col-md-4-->
                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </div> <!--End row-->    

            <div class="row">
                <?php
                    foreach ($aboutus_second_row as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?> 
                <div class="<?php echo $second_row_class; ?> pd-big margin-top">
                    <div class="about-service text-center wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                        <div class="about-icon">
                            <i class="fa <?php if(isset($meta["_glimmer_icon_code"][0])) echo $meta["_glimmer_icon_code"][0]; ?>"></i> 
                        </div> <!-- /.about-icon -->
                        
                        <div class="about-description">
                            <h4><?php the_title(); ?></h4>
                                <?php the_content(); ?>
                        </div> <!-- /.about-description -->
                    </div> <!-- /.about-service -->
                </div> <!--/.col-md-4-->
                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </div> <!--End row-->  
        </div> <!-- /.container -->
    </div> <!-- /.about-content -->
</section> <!-- /.about section -->

