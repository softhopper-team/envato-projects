<?php

function glimmer_get_enabled_sections(){
    global $glimmer;
    $sections = array();
    $sections['section-aboutus'] = "";
    $sections['section-team'] = "";
    $sections['section-skill'] = "";
    $sections['section-services'] = "";
    $sections['section-testimonial'] = "";
    $sections['section-portfolio'] = "";
    $sections['section-funfact'] = "";
    $sections['section-pricing'] = "";
    $sections['section-client'] = "";
    $sections['section-blog'] = "";
    $sections['section-contact'] = "";
    if($glimmer['section_aboutus_display']) $sections['section-aboutus'] = "About Us";
    if($glimmer['section_team_display']) $sections['section-team'] = "Team";
    if($glimmer['section_skill_display']) $sections['section-skill'] = "Skill";
    if($glimmer['section_services_display']) $sections['section-services'] = "Services";
    if($glimmer['section_testimonial_display']) $sections['section-testimonial'] = "Testimonial";
    if($glimmer['section_portfolio_display']) $sections['section-portfolio'] = "Portfolio";
    if($glimmer['section_funfact_display']) $sections['section-funfact'] = "Fan Fact";
    if($glimmer['section_pricing_display']) $sections['section-pricing'] = "Pricing";
    if($glimmer['section_client_display']) $sections['section-client'] = "Client";
    if($glimmer['section_blog_display']) $sections['section-blog'] = "Blog";
    if($glimmer['section_contact_display']) $sections['section-contact'] = "Contact";

    return ($sections);
}

function glimmer_get_all_sections(){
    $sections = array();
    $sections['section-aboutus'] = "About Us";
    $sections['section-team'] = "Team";
    $sections['section-skill'] = "Skill";
    $sections['section-services'] = "Services";
    $sections['section-testimonial'] = "Testimonial";
    $sections['section-portfolio'] = "Portfolio";
    $sections['section-funfact'] = "Fun Fact";
    $sections['section-pricing'] = "Pricing";
    $sections['section-client'] = "Client";
    $sections['section-blog'] = "Blog";
    $sections['section-contact'] = "Contact";
    return $sections;
}