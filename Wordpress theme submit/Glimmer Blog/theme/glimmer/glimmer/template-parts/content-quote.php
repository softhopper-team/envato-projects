<?php
/**
 * The template for displaying quote post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<?php              
    $meta = get_post_meta( $post->ID );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('format-quote'); ?>>
    <div class="entry-content">
        <blockquote>
            <p>
            <?php              
                if( isset ( $meta["_glimmer_format_quote"][0] ) ) echo $meta["_glimmer_format_quote"][0];
            ?>
            </p>            
            <?php
            if ( isset ( $meta["_glimmer_format_quote_author"][0] ) ) :
                if( isset ( $meta["_glimmer_format_quote_url"][0] ) ) {
                    ?>                           
                        <cite><a href="<?php if( isset ( $meta["_glimmer_format_quote_url"][0] ) ) echo $meta["_glimmer_format_quote_url"][0]; ?>"><?php if( isset ( $meta["_glimmer_format_quote_author"][0] ) ) echo $meta["_glimmer_format_quote_author"][0]; ?></a></cite>
                    <?php
                    } else {
                    ?>                        
                    <cite><?php if( isset ( $meta["_glimmer_format_quote_author"][0] ) ) echo $meta["_glimmer_format_quote_author"][0]; ?></cite>
                    <?php
                } 
            endif;
            ?>
        </blockquote>   
    </div> <!-- .entry-content -->
</article> <!-- /.post-->