<?php
/**
 * The template for displaying all archive posts.
 *
 * @package Glimmer
 */
get_header(); ?>
<?php
    global $glimmer, $post;
?>
<?php get_template_part("blog","header");?>

        <!-- blog list
        ================================================== -->
        <div class="blog-list">
            <div class="container">

                <div class="row base-padding-blog">
                    <div class="col-md-8">
                            <?php if ( have_posts() ) : ?>

							<header class="page-header">
								<?php
									the_archive_title( '<h1 class="page-title">', '</h1>' );
									the_archive_description( '<div class="taxonomy-description">', '</div>' );
								?>
							</header><!-- .page-header -->

							<?php /* Start the Loop */ ?>
							<?php while ( have_posts() ) : the_post(); ?>

								<?php
								/**
								 * Run the loop for the search to output the results.
								 * If you want to overload this in a child theme then include a file
								 * called content-search.php and that will be used instead.
								 */
								get_template_part( 'content', get_post_format() );
								?>

							<?php endwhile; ?>

							<?php glimmer_pagination_nav(); ?>

						<?php else : ?>

							<?php get_template_part( 'content', 'none' ); ?>

						<?php endif; ?>
         
                    </div> <!-- /.col-md-8 -->

                    <?php get_sidebar(); ?>                    

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.blog-list -->
    
<?php get_template_part("blog", "footer"); ?>
<?php get_footer(); ?>