<?php
/**
 * The template for displaying link post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Candid
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header class="entry-header">
    	<?php              
			$meta = get_post_meta( $post->ID );
    	?>    	 
    	<h2 class="entry-title">
            <a rel="bookmark" href="<?php if ( isset ( $meta["_candid_format_link"][0] ) ) echo $meta["_candid_format_link"][0]; ?>"><?php if ( isset ( $meta["_candid_format_link_text"][0] ) ) echo $meta["_candid_format_link_text"][0]; ?></a>
        </h2>
        <span class="entry-link">
            <a href="<?php if ( isset ( $meta["_candid_format_link"][0] ) ) echo $meta["_candid_format_link"][0]; ?>"><?php if ( isset ( $meta["_candid_format_link"][0] ) ) echo $meta["_candid_format_link"][0]; ?></a>
        </span>
    </header><!-- .entry-header -->
</article> <!-- /.post format-link -->