<?php
function glimmer_comment_form($args) {
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );

	$args['fields'] = array(
      'author' =>
        '<div class="col-md-6"><p><input id="author" class="col-xs-12" name="author" required="required" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
        '" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' placeholder="' . __( 'Your Name', 'glimmer' ) . ( $req ? '*' : '' ) . '" /></p></div>',

      'email' =>
        '<div class="col-md-6"><p><input id="email" class="col-xs-12" name="email" required="required" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
        '" size="30"' . ( $req ? " aria-required='true'" : '' ) . ' placeholder="' . __( 'Your Email', 'glimmer' ) . ( $req ? '*' : '' ) . '" /></p></div>',

      'url' =>
        '<div class="col-md-12"><p><input id="url" class="col-xs-12" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
        '" size="30" placeholder="' . __( 'Got a Website?', 'glimmer' ) . '" /></p></div>'
      );
	$args['id_form'] = "commentform";
	//$args['class_form'] = "commentform";
	$args['id_submit'] = "submit";
	$args['class_submit'] = "submit";
	$args['name_submit'] = "submit";
	$args['title_reply'] = __( 'Leave a Reply', 'glimmer' );
	$args['title_reply_to'] = __( 'Leave a Reply to %s', 'glimmer' );
	$args['cancel_reply_link'] = __( 'Cancel Reply', 'glimmer' );
	$args['comment_notes_before'] = "";
	$args['comment_notes_after'] = "";
	$args['label_submit'] = __( 'Submit', 'glimmer' );
	$args['comment_field'] = '<div class="col-md-12"><textarea id="comment" name="comment" aria-required="true" placeholder="'. __( 'Your Comment here...', 'glimmer' ) .'" ></textarea></div>';
	return $args;
}

add_filter('comment_form_defaults', 'glimmer_comment_form');

function glimmer_comment_list($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
	<<?php echo $tag ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>

	
	<div class="comment-meta">
		<div class="comment-author vcard">	
			<div class="author-img">
				<?php //if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>		<?php echo get_avatar($comment,$size='80'); ?>				
			</div><!-- /.author-img -->
			
		</div><!-- /.comment-author .vcard -->

		<div class="comment-metadata">

			<?php printf( __( '<b class="fn">%s</b> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
		<span class="date">
			<?php
				/* translators: 1: date, 2: time */
				printf( __('%1$s at %2$s','glimmer'), get_comment_date(),  get_comment_time() ); ?><?php edit_comment_link( __( '(Edit)','glimmer' ), '  ', '' );
			?>
		</span>
			
		</div><!-- /.comment-metadata -->
	</div><!-- /.comment-meta -->

	

	<div class="comment-details">		
		<div class="comment-content">
			<?php comment_text(); ?>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<p><em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.','glimmer' ); ?></em>
				</p>
			<?php endif; ?>
		</div><!-- /.comment-content -->

		<div class="reply">
		<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
		</div><!-- /.reply -->
	</div><!-- /.comment-details -->
	
	<?php if ( 'div' != $args['style'] ) : ?>
	</div><!-- /.comment-body -->
	<?php endif; ?>
<?php
}
/*get avater*/

// function add_gravatar_class($class) {
//     $class = str_replace("class='avatar", "class='avatar media-object", $class);
//     return $class;
// }
// add_filter('get_avatar','add_gravatar_class');


// function replace_reply_link_class($class){
//     $class = str_replace("class='comment-reply-link", "class='media-reply", $class);
//     return $class;
// }
// add_filter('comment_reply_link', 'replace_reply_link_class');
