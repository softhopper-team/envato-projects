<?php
    global $glimmer;
    $teammembers = glimmer_get_custom_posts_by_custom_order("teammember", 4, "_glimmer_team_order");
     $classes = array(0 => "", "1" => "col-md-12", "2" => "col-md-6", "3" => "col-md-4", "4" => "col-md-3");
    $class = $classes[count($teammembers)];
?>   
<!-- team
================================================== --> 
<section id="our-team" class="team parallax-one">   
    <div class="parallax-heading">
        <div class="patern-overlay"></div>
        <div class="team-heading">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <header class="heading wow <?php if (isset($glimmer['section_team_animation_style'])) echo $glimmer['section_team_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_team_animation_duration'])) echo $glimmer['section_team_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_team_animation_delay'])) echo $glimmer['section_team_animation_delay']; ?>">
                            <h2 class="title">
                                <?php if (isset($glimmer['section_team_title'])) echo $glimmer['section_team_title']; ?>
                            </h2>
                                <span class="small-border-two"></span>

                            <h5 class="subtitle"><?php if (isset($glimmer['section_team_subtitle'])) echo $glimmer['section_team_subtitle']; ?></h5>
                        </header> <!-- /.heading -->            
                    </div> <!-- /.col-lg-12 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->                     
        </div> <!-- /.heading-two -->
    </div> <!-- /.parallax-heading -->  
   

    <div class="team-member">
        <div class="container">
            <div class="row">
                <?php
                    $team_i = 0;
                    foreach ($teammembers as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?> 
                <div class="<?php echo $class; ?> wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                    <div class="team-bg">
                        <div class="team-details">
                            <div class="member-image <?php if( $team_i>0 ) echo 'team-mobile'; ?>">
                                 <img class="img-responsive" src="<?php if(isset($meta["_glimmer_teammember_photo"][0])) echo $meta["_glimmer_teammember_photo"][0]; ?>" alt="<?php the_title(); ?>">   
                            </div>
                            
                             <div class="member-details text-center">
                                <h4><?php the_title(); ?></h4>
                                <p><?php 
                                        if ( isset($meta["_glimmer_position"][0]) ) {
                                            echo $meta["_glimmer_position"][0];
                                        }
                                    ?>
                                </p>
                                <div class="social-link">                                 
                                <?php
                                for ($i = 1; $i < 4; $i++) {
                                    ?>
                                    <a href="<?php if(isset($meta["_glimmer_social_url_{$i}"][0])) echo $meta["_glimmer_social_url_{$i}"][0]; ?>"><i class="fa <?php if(isset($meta["_glimmer_social_icon_{$i}"][0])) echo $meta["_glimmer_social_icon_{$i}"][0]; ?>"></i></a>                
                                <?php
                                    }
                                ?>
                                </div>
                            </div>    
                        </div> <!-- /.team-details -->
                    </div> <!-- /.team-bg -->
                </div> <!-- /.col-md-4 -->
                <?php 
                    $team_i++;
                    } 
                ?>

            </div> <!-- /.row -->
        </div> <!-- /.container -->
    </div> <!-- /.team-member -->
</section>   <!-- /.our-team -->
