<?php
function glimmer_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar Widget', 'glimmer-admin' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget  %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'glimmer_widgets_init' );