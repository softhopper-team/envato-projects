<?php
/**
 * The template for displaying search form.
 *
 * @package Glimmer
 */
global $glimmer;
?>
<form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="search-form" method="get" role="search">
    <div class="input-group">
        <input type="search" name="s" value="<?php _e( 'Search here &hellip;', 'glimmer' ); ?>" class="form-controller">
        <?php if ( $glimmer['search_only_form_post'] == 1 ) : ?>
            <input type="hidden" value="post" name="post_type" id="post_type">
        <?php endif; ?>
        <span class="input-group-btn">
            <button type="submit" class="btn btn-search"><i class="ico-search"></i></button>
        </span>
    </div>
</form>
