<?php
// glimmer breadcrumbs for archive page
function glimmer_custom_breadcrumbs() {
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '<span class="divider">/</span>'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<li class="active">'; // tag before the current crumb
  $after = '</li>'; // tag after the current crumb

  global $post;
  $homeLink = esc_url( home_url() ) ;

  if (is_home() || is_front_page()) {

  if ($showOnHome == 1) echo '<ul class="breadcrumb"><li><a href="' . $homeLink . '">' . $home . '</a></li></ul>';

  } else {

  echo '<ul class="breadcrumb">';

  if ( is_category() ) {
  echo '<li>'.__('Browsing Category','glimmer').'</li><li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li>';
  $thisCat = get_category(get_query_var('cat'), false);
  if ($thisCat->parent != 0) echo '<li>'.get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ').'</li>';
  echo $before . '' . single_cat_title('', false) . '' . $after;

  } elseif ( is_search() ) {
  echo '<li>'.__('Search results for','glimmer').'</li>';
  echo $before . get_search_query() . $after;

  } elseif ( is_day() ) {
  echo '<li>'.__('Browsing Day','glimmer').'</li><li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li>';
  echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . '</li> ';
  echo '<li><a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . '</li> ';
  echo $before . get_the_time('d') . $after;

  } elseif ( is_month() ) {
  echo '<li>'.__('Browsing Month','glimmer').'</li><li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li>';
  echo '<li><a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . '</li> ';
  echo $before . get_the_time('F') . $after;

  } elseif ( is_year() ) {
  echo '<li>'.__('Browsing Year','glimmer').'</li><li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li>';
  echo $before . get_the_time('Y') . $after;

  } elseif ( is_tag() ) {
  echo '<li>'.__('Browsing Tag','glimmer').'</li><li><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . '</li>';
  echo $before . single_tag_title('', false) . $after;
  } 

  if ( get_query_var('paged') ) {
  if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo '<li class="paged">(';
  echo __('Page', 'glimmer') . ' ' . get_query_var('paged');
  if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')</li>';
  }

  echo '</ul>';

  }
} // end glimmer_custom_breadcrumbs()
?>