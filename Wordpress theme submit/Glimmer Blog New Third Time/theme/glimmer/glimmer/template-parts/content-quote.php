<?php
/**
 * The template for displaying quote post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<?php              
    $meta = get_post_meta( $post->ID );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('format-quote'); ?>>
    <h2 class="entry-title">Some hide content</h2>
    <div class="quote-content">
        <?php
            if ( is_single() ) {
                ?>
                <i class="dashicons dashicons-format-quote"></i>
                <?php
            } else {
                ?>
                <a href="<?php the_permalink(); ?>"><i class="dashicons dashicons-format-quote"></i></a>
                <?php
            }
        ?>        
        <blockquote>
            <span class="screen-reader-text"></span>
            <p>
            <?php              
                if( isset ( $meta["_glimmer_format_quote"][0] ) ) echo $meta["_glimmer_format_quote"][0];
            ?>
            </p>
        </blockquote> <!-- /.quote-content -->
        <div class="author">
            <?php
            if ( isset ( $meta["_glimmer_format_quote_author"][0] ) ) :
                echo "<span>&#x02015;</span>";
                if( isset ( $meta["_glimmer_format_quote_url"][0] ) ) {
                    ?>                           
                        <a href="<?php if( isset ( $meta["_glimmer_format_quote_url"][0] ) ) echo $meta["_glimmer_format_quote_url"][0]; ?>"><?php if( isset ( $meta["_glimmer_format_quote_author"][0] ) ) echo $meta["_glimmer_format_quote_author"][0]; ?></a>
                    <?php
                    } else {
                    ?>                        
                    <?php if( isset ( $meta["_glimmer_format_quote_author"][0] ) ) echo $meta["_glimmer_format_quote_author"][0]; ?>
                    <?php
                    } 
            endif;
            ?>
        </div> <!-- /.author -->
    </div> <!-- /.quote-content -->
</article> <!-- /.post-->