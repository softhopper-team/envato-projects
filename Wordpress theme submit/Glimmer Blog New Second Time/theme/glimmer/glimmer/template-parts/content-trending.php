<?php
/**
 * The template for displaying trending posts
 *
 * Used for index.
 *
 * @package Candid
 */
?>
<?php 
    global $glimmer;
   
    $display_trending_post = "";
    ( isset($_GET["display_trending"]) ) ? $display_trending = $_GET["display_trending"]  : $display_trending = "" ;
    ( $display_trending == "no" ) ? $display_trending_post = false : $display_trending_post = $glimmer['trending_display'];
    if ( $display_trending_post ) : 
    if ( !is_paged() ) : 
?>
<!-- Trending Post
================================================== -->        
<div class="trending-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    if ( isset($glimmer['trending_section_title']) ) :
                ?>                    
                <h2 class="area-title">
                    <span><?php echo $glimmer['trending_section_title']; ?></span>    
                </h2>
                <?php 
                    endif;
                ?>
                <div id="trending-post" class="owl-carousel">
                    <?php
                        $glimmer_trending_post_limit = $glimmer['trending_post_per_page'];
                        $glimmer_trending_post = new WP_Query( array(
                        'posts_per_page' => $glimmer_trending_post_limit,
                        'meta_key' => 'softhopper_post_views_count', 
                        'ignore_sticky_posts' => true, 
                        'orderby' => 'meta_value_num', 
                        'order' => 'DESC'  ) );
                    ?>
                    <?php while ( $glimmer_trending_post->have_posts() ) : $glimmer_trending_post->the_post(); ?>

                    <div class="item">                     
                        <figure class="trending-content">                            
                            <?php
                                $meta = get_post_meta( get_the_ID() );
                                $post_format = get_post_format();
                                if ( false === $post_format ) {
                                    $post_format = "standard";
                                }
                                
                                if ( $post_format == "gallery" ) {  
                                    // show first image of gallery post
                                    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
                                        $imgs_urls = $meta["_glimmer_format_gallery"][0];  
                                        $imgs_url = explode( '"', $imgs_urls );
                                        $img_crop_url = sh_crop_image_url($imgs_url[1], "trending-post");
                                        ?>
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">  
                                        <?php                           
                                    }  else {
                                        ?>
                                        <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/gallery.jpg'; ?>" alt="<?php the_title(); ?>">       
                                        <?php 
                                    }
                                } elseif ( $post_format == "audio" ){
                                    if( isset($meta["_glimmer_format_audio_bg_img"][0]) ) {
                                        $img_crop_url = sh_crop_image_url($meta["_glimmer_format_audio_bg_img"][0], "trending-post");
                                        ?>
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">           
                                        <?php 
                                    } else {
                                        ?>
                                        <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/audio.jpg'; ?>" alt="<?php the_title(); ?>">    
                                        <?php 
                                    }
                                } elseif ( $post_format == "video" ){
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('trending-post', array( 'alt' => get_the_title()));
                                    } else {
                                    ?>
                                    <img src="<?php echo THEME_URL; ?>/images/post/trending-no-thumb/video.jpg" alt="<?php the_title(); ?>" />
                                    <?php
                                    } //end else
                                } elseif ( $post_format == "quote" ){
                                    ?>
                                    <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/quote.jpg'; ?>" alt="<?php the_title(); ?>">     
                                    <?php 
                                } elseif ( $post_format == "aside" ){
                                    ?>
                                    <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/aside.jpg'; ?>" alt="<?php the_title(); ?>">     
                                    <?php 
                                } elseif ( $post_format == "chat" ){
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail('trending-post', array( 'alt' => get_the_title()));
                                    } else {
                                    ?>
                                    <img src="<?php echo THEME_URL; ?>/images/post/trending-no-thumb/chat.jpg" alt="<?php the_title(); ?>" />
                                    <?php
                                    } //end else
                                } elseif ( $post_format == "link" ){
                                    if( isset($meta["_glimmer_format_link_bg_img"][0]) ) {
                                        $img_crop_url = sh_crop_image_url($meta["_glimmer_format_link_bg_img"][0], "trending-post");
                                        ?>
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">          
                                        <?php 
                                    } else {
                                        ?>
                                        <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/link.jpg'; ?>" alt="<?php the_title(); ?>">     
                                        <?php 
                                    }
                                } elseif ( $post_format == "status" ){
                                    if( isset($meta["_glimmer_format_status_bg"][0]) ) {
                                        $img_crop_url = sh_crop_image_url($meta["_glimmer_format_status_bg"][0], "trending-post");
                                        ?>
                                        <img src="<?php if(isset($img_crop_url)) echo $img_crop_url; ?>" alt="<?php the_title(); ?>">          
                                        <?php 
                                    } else {
                                        ?>
                                        <img src="<?php echo THEME_URL.'/images/post/trending-no-thumb/status.jpg'; ?>" alt="<?php the_title(); ?>">       
                                        <?php 
                                    }
                                } else {
                                    if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('trending-post', array( 'alt' => get_the_title()));
                                    } else {
                                    ?>
                                    <img src="<?php echo THEME_URL; ?>/images/post/trending-no-thumb/image.jpg" alt="<?php the_title(); ?>" />
                                    <?php
                                    } //end else
                                } //end else
                            ?>
                            <figcaption>
                                <div class="td-wrapper">        
                                    <span class="entry-date">
                                        <?php the_time( get_option( 'date_format' ) ); ?>
                                    </span>
                                    <?php
                                        the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
                                    ?>
                                    <div class="meta">
                                        <div class="post-comment">
                                            <a href="<?php comments_link(); ?>" class="comments-link">
                                                <span><?php comments_number( '0 Comment', '1 Comments', '% Comments' ); ?></span>
                                            </a>
                                        </div>
                                        <div class="post-view">
                                            <a href="<?php the_permalink(); ?>" class="view-link">
                                                <span><?php echo softhopper_get_post_views(get_the_ID()); ?></span>
                                            </a>
                                        </div>
                                    </div> <!-- /.meta -->
                                </div> <!-- /.td-wrapper -->
                                <a class="td-more" href="<?php the_permalink(); ?>">View more</a>
                            </figcaption>           
                        </figure>
                    </div> <!-- /.item -->
                    <?php
                        endwhile;
                    ?>
                </div> <!-- #trending-post -->
            </div>          
        </div>
    </div>
</div> <!-- /.trending-area -->
<?php endif; endif; ?>

