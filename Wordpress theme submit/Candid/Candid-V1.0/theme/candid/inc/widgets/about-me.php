<?php
class Candid_About_Me extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => 'Candid About Me Info',
            'name' => 'Candid About Me'
        );
        parent::__construct('Candid_About_Me','',$params);
    }

    public function form( $instance) {
        extract($instance);
        ?>
        <script type="text/javascript">
            jQuery( document ).ready( function($) {
                $(document).on('click', 'button.about-me-photo', function(){
                    image_field = $('.photo-url');
                    tb_show('', 'media-upload.php?type=image&TB_iframe=true');
                    return false;
                });
                window.send_to_editor = function(html) {
                    imgurl = $('img', html).attr('src');
                    image_field.val(imgurl);
                    tb_remove();
                }
            });
        </script>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','candid'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p>  

        <p>
            <label for="<?php echo $this->get_field_id('name'); ?>"><?php _e('Name:','candid'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('name'); ?>"
                name="<?php echo $this->get_field_name('name'); ?>"
                value="<?php if( isset($name) ) echo esc_attr($name); ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:','candid'); ?></label>
            <textarea 
                class="widefat" 
                rows="6" 
                cols="20" 
                id="<?php echo $this->get_field_id('description'); ?>" 
                name="<?php echo $this->get_field_name('description'); ?>"><?php if( isset($description) ) echo esc_attr($description); ?></textarea>
        </p>        

        <p>
            <label for="<?php echo $this->get_field_id('photo'); ?>"><?php _e('Photo:','candid'); ?></label>
            <input
                class="widefat photo-url"
                type="text"
                id="<?php echo $this->get_field_id('photo'); ?>"
                name="<?php echo $this->get_field_name('photo'); ?>"
                value="<?php if( isset($photo) ) echo esc_attr($photo); ?>" />
            <button class="button about-me-photo">Upload Your Photo</button>
        </p>

        <p>
            <label for="<?php echo $this->get_field_id('page_url'); ?>"><?php _e('About Me Page URL:','candid'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('page_url'); ?>"
                name="<?php echo $this->get_field_name('page_url'); ?>"
                value="<?php if( isset($page_url) ) echo esc_url($page_url); ?>" />
        </p>

        <?php
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters('widget_title',$title);
        $name = apply_filters('widget_name',$name);
        $description = apply_filters('widget_description',$description);
        $page_url = apply_filters('widget_page_url',$page_url);
        $photo = apply_filters('widget_photo',$photo);
        
       
        echo $before_widget;
            if ( !empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <div class="about-widget">
                <div class="author-image-home">
                    
                    <?php 
                        if ( !empty( $photo ) ) {
                            echo "<img class='img-responsive' src='$photo' alt='Author Image' />";
                        }
                    ?>

                </div> <!-- /.author-image-home -->

                <div class="about-description">
                    <?php 
                        if ( !empty( $name ) ) {
                            echo "<h4 class='author-name'>$name</h4>";
                        }

                        if ( !empty( $description ) ) {
                            echo "<p>$description</p>";
                        }
                    ?>
                    <?php 
                        if ( !empty( $page_url ) ) {
                            echo "<a class='more-link button' href='$page_url'>
                                <span class='read-button'>".__('Continue','candid')."</span>
                            </a>";
                        }
                    ?>
                    

                </div> <!-- /.about-description -->  
            </div> <!-- /.about-widget -->   
                
            <?php
        echo $after_widget;
    }
}
