<?php
/*-----------------------------------------------------------------------------------*/
# Soundcloud Function
/*-----------------------------------------------------------------------------------*/
function glimmer_soundcloud($url , $autoplay = 'false' ) {
    global $glimmer, $post;
    $color = '';
    switch( $glimmer['glimmer_color_scheme'] ) {
        case 1: //C69F73
            $glimmer_color_scheme = "#C69F73";
            break;
        case 2: //1ABC9C
            $glimmer_color_scheme = "#1ABC9C";
            break;
        case 3: //D2527F
            $glimmer_color_scheme = "#D2527F";
            break;
        case 4: //F26D7E
            $glimmer_color_scheme = "#F26D7E";
            break;
        case 5: //CC6054
            $glimmer_color_scheme = "#CC6054";
            break;
        case 6: //667A61
            $glimmer_color_scheme = "#667A61";
            break;
        case 7: //A74C5B
            $glimmer_color_scheme = "#A74C5B";
            break;
        case 8: //95A5A6
            $glimmer_color_scheme = "#95A5A6";
            break;
        case 9: //turquoise
            $glimmer_color_scheme = $glimmer['glimmer_custom_color'];
            break;
        default:
            $glimmer_color_scheme = "#C69F73";
            break;
    }   
    
    if( !empty( $glimmer_color_scheme ) ){
        $glimmer_color_scheme = str_replace ( '#' , '' , $glimmer_color_scheme );
        $color = '&amp;color='.$glimmer_color_scheme;
    }
    return '<iframe style="width:100%" height="166" src="https://w.soundcloud.com/player/?url='.$url.$color.'&amp;auto_play='.$autoplay.'&amp;show_artwork=true"></iframe>';
}

/*-----------------------------------------------------------------------------------*/
# Get Post Video  
/*-----------------------------------------------------------------------------------*/
function glimmer_vedio() {
    global $post;
    $meta = get_post_meta( $post->ID );  
    if( isset( $meta["_glimmer_format_video_url"][0] ) && !empty( $meta["_glimmer_format_video_url"][0] ) ) {
        $video_url = $meta["_glimmer_format_video_url"][0];
        $video_link = @parse_url($video_url);
        if ( $video_link['host'] == 'www.youtube.com' || $video_link['host']  == 'youtube.com' ) {
            parse_str( @parse_url( $video_url, PHP_URL_QUERY ), $my_array_of_vars );
            $video =  $my_array_of_vars['v'] ;
            $video_code ='<iframe width="600" height="325" src="http://www.youtube.com/embed/'.$video.'?rel=0&wmode=opaque" allowfullscreen="allowfullscreen"></iframe>';
        } elseif ( $video_link['host'] == 'www.vimeo.com' || $video_link['host']  == 'vimeo.com' ){
            $video = (int) substr(@parse_url($video_url, PHP_URL_PATH), 1);
            $video_code='<iframe width="600" height="325" src="http://player.vimeo.com/video/'.$video.'" allowfullscreen="allowfullscreen"></iframe>';
        } elseif ( $video_link['host'] == 'www.youtu.be' || $video_link['host']  == 'youtu.be' ){
            $video = substr(@parse_url($video_url, PHP_URL_PATH), 1);
            $video_code ='<iframe width="600" height="325" src="http://www.youtube.com/embed/'.$video.'?rel=0" allowfullscreen="allowfullscreen"></iframe>';
        } elseif ( $video_link['host'] == 'www.dailymotion.com' || $video_link['host']  == 'dailymotion.com' ){
            $video = substr(@parse_url($video_url, PHP_URL_PATH), 7);
            $video_id = strtok($video, '_');
            $video_code='<iframe width="600" height="325" src="http://www.dailymotion.com/embed/video/'.$video_id.'"></iframe>';
        }
    } elseif( isset( $meta["_glimmer_format_embed_code"][0] ) ) {
        $embed_code = $meta["_glimmer_format_embed_code"][0];
        $video_code = htmlspecialchars_decode( $embed_code); 
    } else { 
        $mp4 = isset( $meta["_glimmer_format_video_mp4"][0] ) ? $meta["_glimmer_format_video_mp4"][0] : '';
        $ogv = isset( $meta["_glimmer_format_video_ogv"][0] ) ? $meta["_glimmer_format_video_ogv"][0] : '';
        $mov = isset( $meta["_glimmer_format_video_mov"][0] ) ? $meta["_glimmer_format_video_mov"][0] : '';
        $video_code = '<div class="post-video-player">'.do_shortcode('[video mp4="'.$mp4.'" ogv="'.$ogv.'" mov="'.$mov.'"]').'</div>';
    }
    if ( isset($video_code) ) echo $video_code;
} // end glimmer_vedio()
