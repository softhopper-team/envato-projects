<?php
    global $glimmer;
    $clients = glimmer_get_custom_posts("happyclient", 10);
?>    
				<!-- clients
		================================================== --> 
		<section id="our-clients" class="our-clients parallax-four">
			<div class="patern-overlay"></div>
			<div class="parallax-heading">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<header class="heading wow <?php if (isset($glimmer['section_happyclient_animation_style'])) echo $glimmer['section_happyclient_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_happyclient_animation_duration'])) echo $glimmer['section_happyclient_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_happyclient_animation_delay'])) echo $glimmer['section_happyclient_animation_delay']; ?>">                       
	                            <h2 class="title">
	                                <?php if (isset($glimmer['section_client_title'])) echo $glimmer['section_client_title'];
	                                ?>
	                            </h2>
	                                <span class="small-border-two"></span>

	                            <h5 class="subtitle"><?php if (isset($glimmer['section_client_subtitle'])) echo $glimmer['section_client_subtitle'];
	                                    ?></h5>
                        	</header> <!-- /.heading -->
						</div> <!-- /.col-md-12 -->
					</div> <!-- /.row -->

					<div class="row">
						<div class="col-lg-12">
							<div id="clients" class="our-client owl-carousel text-center">
								<?php
			                        foreach ($clients as $post) {
			                        setup_postdata($post);
			                        $meta = get_post_meta($post->ID);
			                    ?>  
								<div class="item wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
									<a href="<?php if(isset($meta["_glimmer_happy_client_url"][0])) echo $meta["_glimmer_happy_client_url"][0]; ?>"><img class="img-responsive" src="<?php if(isset($meta["_glimmer_happy_client_logo"][0])) echo $meta["_glimmer_happy_client_logo"][0]; ?>" alt="<?php if(isset($meta["_glimmer_happy_client_logo"][0])) echo $meta["_glimmer_happy_client_logo"][0]; ?>"></a>
								</div>
								<?php } ?>                                                                                                                
							</div> <!-- /.our-client -->
						</div> <!-- /.col-lg-12 -->
					</div> <!-- /.row -->
				</div> <!-- /.container -->
			</div> <!-- /.paralux-heading -->
		  
		</section> <!-- /.our-client -->