<?php
/**
 * The template used for displaying page content
 *
 * @package Glimmer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-content" >
        <?php
            if ( has_post_thumbnail() ) {
                ?>
                    <figure class="blog-image"> 
                        <?php
                            the_post_thumbnail('single-full', array( 'class' => "img-responsive", 'alt' => get_the_title()));
                        ?>
                    </figure> <!-- /.blog-image -->
                <?php
            } else {
                ?>
                    <div class="post-embed">
                        <?php 
                            $meta = get_post_meta($post->ID);  
                            if(isset($meta["_glimmer_post_embed_code"][0])) echo $meta["_glimmer_post_embed_code"][0];
                        ?>
                    </div><!-- /.post-embed -->
                <?php
            }
        ?>

        <div class="blog-details clearfix">
                <?php the_title( sprintf( '<h1 class="blog-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?> 

            <div class="blog-meta">
	         	<?php glimmer_posted_on_for_page(); ?>
	        </div> <!-- /.blog-meta -->	            
            <hr> <!-- /.hr -->

            <div class="blog-description">
                <?php the_content(); ?>
            </div> <!-- /.blog-description -->

            <?php glimmer_entry_footer(); ?>
            
        </div> <!-- /.blog-details-full -->
    </div><!-- /.blog-content -->
</article> <!-- /.blog-content -->

<?php get_template_part( 'partials/content', 'authorinfo' ); ?>