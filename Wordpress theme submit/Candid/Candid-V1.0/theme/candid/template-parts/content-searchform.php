<?php
/**
 * The template part for custom search form
 *
 * @package Candid
 */
?>
<div class="search-form clearfix">
    <form action="<?php echo esc_url( home_url( '/' ) ); ?>" method="get" class="searchform">
        <input type="text" placeholder="<?php _e( 'Search here...', 'candid' ); ?>" name="s" class="col-xs-10">
        <button type="submit" class="col-xs-2" id="submit-search">
            <i class="icon icon-Search"></i>
        </button>
    </form>                                     
</div> <!-- /.search-form -->