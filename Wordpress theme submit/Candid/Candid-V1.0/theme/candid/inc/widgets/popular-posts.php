<?php
class Candid_Popular_Posts extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => 'Candid Popular Posts',
			'name' => 'Candid Popular Posts'
		);
		parent::__construct('Candid_Popular_Posts','',$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','candid'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('candid_popular_post_limit'); ?>"><?php _e('Number of posts to show:','candid'); ?></label>
			<input 
				id="<?php echo $this->get_field_id('candid_popular_post_limit'); ?>" 
				type="text" 
				name="<?php echo $this->get_field_name('candid_popular_post_limit'); ?>"
				value="<?php if( isset($candid_popular_post_limit) ) echo esc_attr($candid_popular_post_limit); ?>"
				size="3" />
		</p>
		<?php

	}

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$candid_popular_post_limit = apply_filters('widget_candid_popular_post_limit',$candid_popular_post_limit);
		if ( empty($candid_popular_post_limit) ) $candid_popular_post_limit = 5;

		echo $before_widget;
			if ( !empty( $title ) ) {
				echo $before_title . $title . $after_title;
			}
			?>
			
				<?php 
					$candid_popular_post = new WP_Query( array( 'posts_per_page' => $candid_popular_post_limit, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value', 'order' => 'DESC'  ) );
				?>
				<div class="popular-widget">
                    <ul class="popular-newsfeed">
						<?php while ( $candid_popular_post->have_posts() ) : $candid_popular_post->the_post(); ?>
						<li class="popular-wrap">
	                        <div class="popular-item">          
	                            <div class="popular-image">
		                            <?php
								    	$meta = get_post_meta( get_the_ID() );
		                            	$post_format = get_post_format();
										if ( false === $post_format ) {
											$post_format = "standard";
										}
										
										if ( $post_format == "gallery" ) {	
				                            if ( isset ( $meta["_candid_format_gallery"][0] ) ) {
				                                $imgs_urls = $meta["_candid_format_gallery"][0];
				                            }  else {
				                                $imgs_urls = '';
				                            }        
				                            $imgs_url = explode( '"', $imgs_urls );
		                                	?>
		                                	<a href="<?php the_permalink(); ?>">
					                            <figure class="fit-img">
					                                <img class="popular-item-thumb" src="<?php if(isset($imgs_url[1])) echo $imgs_url[1]; ?>" alt="<?php if(isset($imgs_url[1])) echo $imgs_url[1]; ?>"> 
					                            </figure>
					                        </a>                          	
		                                	<?php 
								        } else {
					                        if ( has_post_thumbnail() ) {
					                            ?>
						                        <a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <?php
						                                	the_post_thumbnail('popular-post', array( 'class' => "popular-item-thumb", 'alt' => get_the_title()));
						                                ?>
						                            </figure>
						                        </a>
					                            <?php
					                        } else {
					                        ?>
						                        <a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                               <img class="popular-item-thumb" src="<?php echo get_template_directory_uri(); ?>/images/no-thumb.gif" alt="No Thumbnail" />
						                            </figure>
						                        </a>
					                        <?php
					                        } //end else
					                    } //end else
				                    ?>  
	                            </div> <!-- /.popular-image -->  

	                            <div class="popular-item-text">
	                                <span class="popular-item-meta"><?php the_time( 'j M, Y' ); ?></span>,
	                                <span class="popular-item-meta"><?php echo wpb_get_post_views(get_the_ID()); ?></span>
	                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	                            </div> <!-- /.popular-item-text -->
	                        </div>  <!-- /.popular-item --> 
                    	</li> <!-- /.popular-wrap -->
		                <?php
							endwhile;
						?>
					</ul> <!-- /.popular-newsfeed -->
                </div> <!-- /.popular-widget -->  
			<?php
		echo $after_widget;
	}
}