<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Glimmer
 */
    global $glimmer;
?>
    <!-- Footer
    ================================================== --> 
    <footer id="colophon" class="site-footer">

        <div id="footer-top">
            <?php if ( ! dynamic_sidebar( 'instagram' ) ) : ?>
            <?php endif; // end instagram widget area ?>        
        </div> <!-- #footer-top" -->

        <div id="footer-middle">
            <div class="container">
                <div class="row">
                    <?php
                        // show footer widget with condition
                        $columns = intval( $glimmer['footer_widget_columns'] );
                        $col_class = 12 / max( 1, $columns );
                        $col_class_sm = 12 / max( 1, $columns );
                        if ( $columns == 4 ) {
                            $col_class_sm = 6;
                        } 
                        $col_class = "col-sm-$col_class_sm col-md-$col_class";
                        for ( $i = 1; $i <= $columns ; $i++ ) {
                            if ( $columns == 3 ) :
                                if ( $i == 3 ) {
                                    $col_class = "col-sm-12 col-md-$col_class";
                                } else {
                                    $col_class = "col-sm-6 col-md-$col_class";
                                } 
                            endif; 
                        ?>
                            <div class="widget-area footer-sidebar-<?php echo $i ?> <?php echo esc_attr( $col_class ) ?>">
                                <?php dynamic_sidebar( __( 'Footer ', 'glimmer' ) . $i ) ?>
                            </div>
                        <?php
                        }
                    ?>
                </div> <!-- /.row -->
            </div> <!-- /.container -->         
        </div> <!-- #footer-middle -->

        <div id="footer-bottom">
            <div class="container">
                <div class="copyright">
                    <?php 
                        if ( isset ( $glimmer['footer_copyright_info'] ) ) echo $glimmer['footer_copyright_info'];
                    ?>     
                </div> <!-- /.copyright -->

                <div class="footer-social">
                   <?php
                        // show footer menu social link
                        if ( isset ( $glimmer['footer_social_link'] )  && ! empty ( $glimmer['footer_social_link']) ) {
                            $social_link = $glimmer['footer_social_link'];                      
                            foreach ( $social_link as $key => $value ) {
                            if ( $value['title'] ) {
                        ?>
                        <a target="_blank" href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                        <?php
                                }
                            }
                        }
                    ?>      
                </div> <!-- /.footer-social --> 
            </div> <!-- /.container -->
        </div> <!-- /#footer-bottom -->

    </footer><!-- #colophon -->
    <?php wp_footer(); ?>
    </body>
</html>