<?php
	global $glimmer;
?>	
<!-- footer
================================================== -->     
<footer class="footer-copyright text-center">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 footer-padding">	
				<h1 class="title">
						<?php if (isset($glimmer['section_footer_theme_name'])) echo $glimmer['section_footer_theme_name'];
                        ?>
				</h1>
			
				<div class="social">
					
					<?php
                	if(isset($glimmer['section_footer_social_link'])  && !empty($glimmer['section_footer_social_link']) ){
                		$social_link = $glimmer['section_footer_social_link']; 	              		
				        foreach ($social_link as $key => $value) {
				        if($value['title']){
		        	?>
		        		<a href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
			        <?php
			    				}
					        }
                    	}
                    ?>
				</div>

				<div class="copyright">					
					<p><?php if (isset($glimmer['section_footer_copyright_info'])) echo $glimmer['section_footer_copyright_info'];
                            ?>
					</p>
				</div> 

			</div> <!-- /.col-lg-12 -->
		</div> <!-- /.row -->
	</div> <!-- /.container -->
</footer> <!-- /.footer-copyright -->




<!-- scroll to top
================================================== --> 
<div class="scrollup">
	<a class="backtopbutton" href="#">
		<i class="icon-arrow-up"></i>
	</a>
</div> <!-- /.scrollup -->