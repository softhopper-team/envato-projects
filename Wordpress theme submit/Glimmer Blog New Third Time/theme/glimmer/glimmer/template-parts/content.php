<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
 global $glimmer;
 	$post_format = get_post_format();
	$meta = get_post_meta( $post->ID );
	if ( false === $post_format ) {
		$post_format = "standard";
	}
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="sticky-icon"><i class="fa fa-thumb-tack"></i></div>';
    	}
    ?>	
	
    <header class="entry-header">
        <?php glimmer_entry_header(); ?>
    </header> <!-- /.entry-header -->

	<?php
		if ( $post_format == "link" ) {
            ?>
			<div class="post-link">
                <div class="post-link-wrapper">					
					<div class="tb">
					    <div class="icon-area tb-cell">
					        <i class="fa fa-link"></i>
					    </div>
					    <div class="link-content tb-cell">
					        <h2>
                            	<a href="<?php if( isset ( $meta["_glimmer_format_link"][0] ) ) echo $meta["_glimmer_format_link"][0]; ?>"><?php if( isset ( $meta["_glimmer_format_link_text"][0] ) ) echo $meta["_glimmer_format_link_text"][0]; ?>
                            	</a>
                            </h2>
					        <a href="<?php if( isset ( $meta["_glimmer_format_link"][0] ) ) echo $meta["_glimmer_format_link"][0]; ?>" target="_blank"><?php if( isset ( $meta["_glimmer_format_link"][0] ) ) echo $meta["_glimmer_format_link"][0]; ?></a>
					    </div>
					</div> <!-- /.tb -->

                </div><!-- /.post-link-wrapper -->
                <div class="images">
                <img alt="<?php the_title(); ?>" src="<?php if( isset ( $meta["_glimmer_format_link_bg_img"][0] ) ) echo $meta["_glimmer_format_link_bg_img"][0]; ?>">
                </div>
            </div><!-- /.post-link -->
            <?php
        } else {
        	if ( has_post_thumbnail() ) {
            ?>
            <figure class="post-thumb">
            	<?php 
            	if ( is_single() ) {
            		?>
					<?php
						( $glimmer['sidebar_layout'] == 1 ) ? $image_size = "single-full" : $image_size = "single-full-list" ;
						/* this script for demo start */
						( isset($_GET["layout"]) ) ? $layout = $_GET["layout"]  : $layout = "" ;
						if ( $layout == "full-width" ) {
							$image_size = "single-full";
						}
						/* this script for demo end */
			          	the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
			        ?>
            		<?php
            	} else {
            		?>
            		<a href="<?php the_permalink(); ?>">
						<?php
							( $glimmer['sidebar_layout'] == 1 ) ? $image_size = "single-full" : $image_size = "single-full-list" ;
							/* this script for demo start */
							( isset($_GET["layout"]) ) ? $layout = $_GET["layout"]  : $layout = "" ;
							if ( $layout == "full-width" ) {
								$image_size = "single-full";
							}
							/* this script for demo end */
				          	the_post_thumbnail( $image_size, array( 'class' => " img-responsive", 'alt' => get_the_title() ));
				        ?>
					</a>
            		<?php
            	}
            	?>
				
			</figure> <!-- /.post-thumb -->
            <?php
	        }
        }
    ?>
    
	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
				?>
				<?php
					if ( has_tag() ) :
					?>
					<div class="tag clearfix">
					    <span class="tags"><?php _e('Tagged In:', 'glimmer'); ?></span>
	                    <?php 
				        echo get_the_tag_list("", "", "");
				        ?>
					</div> <!-- /.tag -->
					<?php
					endif;
				?>
				<?php
			} else {
				the_excerpt();
			}
		?>  
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer clearfix">
        <?php
			if ( is_single() ) {
				glimmer_entry_footer_single();
			} else { 
				glimmer_entry_footer(); 
			}
		?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->