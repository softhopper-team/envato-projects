<?php
/**
 * The template for displaying search results pages.
 *
 * @package Glimmer
 */
get_header();
global $glimmer;
?>
<!-- Content
================================================== -->
<div id="content" class="site-content">
    <div class="container">
        <div class="row">
        <?php
            /* Show sidebar with user condition */
            $columns_grid = 8;
            $columns_offset  = '';
            if ( $glimmer['sidebar_layout_search'] == 2 ) {
                get_sidebar();
            } elseif ( $glimmer['sidebar_layout_search'] == 1 ) {
                $columns_grid = 10;
                $columns_offset = 'col-md-offset-1';
            }
        ?>
        <div class="<?php echo $columns_offset; ?> col-md-<?php echo $columns_grid; ?>">
                <!-- Content Area -->
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">                              
                        <?php                             
                        if ( have_posts() ) : ?>

                        <header class="page-header">
                            <h1 class="page-title"><?php printf( esc_html__( 'Search Results for: %s', 'glimmer' ), '<span>' . get_search_query() . '</span>' ); ?></h1>

                            <?php if ( $glimmer['search_form_in_search_page'] == 1 ) : ?>
                                <div class="search">
                                    <?php get_search_form(); ?>                               
                                </div> <!-- /.search-form -->
                            <?php endif; ?>
                        </header><!-- .page-header -->

                        <?php /* Start the Loop */ ?>
                        <?php while ( have_posts() ) : the_post(); ?>

                            <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'template-parts/content', get_post_format() );
                            ?>

                        <?php endwhile; ?>

                        <?php glimmer_posts_pagination_nav(); ?>

                        <?php else : ?>

                            <?php get_template_part( 'template-parts/content', 'none' ); ?>

                        <?php endif; ?>                             

                    </main> <!-- #main -->
                </div> <!-- #primary -->
            </div> <!-- /.col-md-8 -->
            <?php
                /* Show sidebar with user condition */
                if ( $glimmer['sidebar_layout_search'] == 3 ) {
                    get_sidebar();
                }
            ?>                      
        </div> <!-- /.row -->
    </div> <!-- /.container -->     
</div><!-- #content -->
<?php get_footer(); ?>