<?php
/**
 * The template for displaying featured posts
 *
 * Used for index.
 *
 * @package Glimmer
 */
?>
<?php 
    global $glimmer;
    if ( $glimmer['featured_display'] == 1 ) :
    if ( !is_paged() ) : 
?>
<!-- Featured Area
================================================== -->
<div id="featured" class="feature-area">
    <div id="featured-content">
        <div class="container">
            <div class="row">                
                <div class="col-md-12">                
                <div id="featured-item" class="owl-carousel">                   
                    <?php
                        $query = new WP_Query ( array ( 
                                    'category_name' => 'featured',
                                    'posts_per_page' => $glimmer['featured_per_page']
                                )
                            );
                        while ( $query->have_posts() ) : $query->the_post();
                    ?> 
                    <?php 
                        $featured_img_big = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-img-big' ); 
                    ?>
                    <div class="full-wrapper" style="background: url('<?php echo $featured_img_big[0]; ?>')">
                        <div class="image-extra">
                            <div class="extra-content">
                                <div class="inner-extra">
                                    <span class="entry-date">
                                        <?php the_time( get_option( 'date_format' ) ); ?>
                                    </span>
                                    <?php       
                                        the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
                                    ?>
                                    <div class="hr-line"></div>
                                    <div class="entry-meta">
                                        <span class="cat-links">
                                        <?php
                                            $categories = get_the_category(); //get all categories for this post
                                            $cat_no =  count( $categories );
                                            if ( $cat_no > 1 ) {
                                                _e('In ','glimmer');
                                                $cat_counter = 1;
                                                foreach( $categories as $categorie ):
                                                    if ( $categorie->slug == 'featured' ) continue;
                                                    $cat_counter++;
                                                    ( $cat_counter == $cat_no ) ? $cat_separator = "" : $cat_separator = ", ";
                                                    echo '<a href="' . get_category_link( $categorie->term_id ) . '" ' . '>' . $categorie->name.'</a>'.$cat_separator;
                                                endforeach;  
                                            } else {
                                                _e('In ','glimmer').the_category( ', ' );
                                            }
                                        ?>
                                        </span>
                                        <span class="devider">/</span>
                                        <span class="byline">
                                            <span class="author vcard">
                                                <?php _e('By ','glimmer').the_author_posts_link(); ?>
                                            </span>
                                        </span>
                                    </div> <!-- .entry-meta -->
                                    <div class="fet-more-wrapper">  
                                        <a href="<?php the_permalink(); ?>" class="fet-read-more"><?php _e('Continue Reading', 'glimmer'); ?></a>
                                    </div>
                                </div> <!-- /.inner-extra -->
                            </div> <!-- /.extra-content -->
                        </div> <!-- /.image-extra -->
                    </div> <!-- /.full-wrapper -->
                    <?php 
                        endwhile;
                        wp_reset_query(); 
                    ?> 
                </div> <!-- #featured-item -->
                
                <?php
                $featured_thumb_display = "";
                ( isset($_GET["featured_thumbnail"]) ) ? $featured_thumbnail = $_GET["featured_thumbnail"]  : $featured_thumbnail = "" ;
                ( $featured_thumbnail == "with_thumbnail" ) ? $featured_thumb_display = true : $featured_thumb_display = $glimmer['featured_thumb_display'];
                if ( $featured_thumb_display ) : ?>
                <div id="full-sync" class="owl-carousel">
                    <?php
                        $query = new WP_Query ( array ( 
                                    'category_name' => 'featured',
                                    'posts_per_page' => $glimmer['featured_per_page']
                                )
                            );
                        while ( $query->have_posts() ) : $query->the_post();
                    ?> 
                    <?php 
                        $featured_img_big = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'featured-img-big' ); 
                    ?>
                    <div class="full-control">
                        <?php
                            the_post_thumbnail('featured-img-small', array( 'alt' => get_the_title()));
                        ?>
                    </div> <!-- /.full-control -->
                    <?php 
                        endwhile;
                        wp_reset_query(); 
                    ?> 
                </div> <!-- /.owl-carousel -->
                <?php endif; 
                ?>
                
            </div>  <!-- /.col-md-12 -->
            </div>  <!-- /.row -->
        </div> <!-- /.container-fluid -->
    </div> <!-- #featured-content -->
</div> <!-- /#featured -->
<?php endif; endif; ?>