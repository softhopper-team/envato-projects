<?php

function glimmer_hex_2_rgba($color, $opacity = false) {
     $default = 'rgb(0,0,0)';
    //Return default if no color provided
    if(empty($color))
          return $default; 
 
    //Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}

function glimmer_color_scheme() { 
  global $glimmer;
    switch( $glimmer['glimmer_color_scheme'] ) {
        case 1: //C69F73

            // add a condition to show demo color scheme by url
            ( isset($_GET["color_scheme_color"]) ) ? $color_scheme_color = $_GET["color_scheme_color"]  : $color_scheme_color = "" ;
            if (preg_match('/^[A-Z0-9]{6}$/i', $color_scheme_color)) {
              $demo_color_scheme = $_GET['color_scheme_color'];
            }
            else {
               $demo_color_scheme = "ef9d87";
            }
            $glimmer_color_scheme = "#".$demo_color_scheme;
            
            break;
        case 2: //1ABC9C
            $glimmer_color_scheme = "#1ABC9C";
            break;
        case 3: //D2527F
            $glimmer_color_scheme = "#D2527F";
            break;
        case 4: //F26D7E
            $glimmer_color_scheme = "#F26D7E";
            break;
        case 5: //CC6054
            $glimmer_color_scheme = "#CC6054";
            break;
        case 6: //667A61
            $glimmer_color_scheme = "#667A61";
            break;
        case 7: //A74C5B
            $glimmer_color_scheme = "#A74C5B";
            break;
        case 8: //95A5A6
            $glimmer_color_scheme = "#95A5A6";
            break;
        case 9: //turquoise
            $glimmer_color_scheme = $glimmer['glimmer_custom_color'];
            break;
        default:
            $glimmer_color_scheme = "#ef9d87";
            break;
    }
    //rgba color
    $glimmer_rgba = glimmer_hex_2_rgba($glimmer_color_scheme, 0.8);
?>
<style type="text/css"> 
::-moz-selection{background:<?php echo $glimmer_color_scheme;?>}::selection{background:<?php echo $glimmer_color_scheme;?>}a:hover,a:focus,a:active{color:<?php echo $glimmer_color_scheme;?>}blockquote:before{color:<?php echo $glimmer_color_scheme;?>}blockquote cite,blockquote a,blockquote span{color:<?php echo $glimmer_color_scheme;?>}.more-link:hover{color:<?php echo $glimmer_color_scheme;?>}.more-link:before,.more-link:after{border-color:<?php echo $glimmer_color_scheme;?>}.small-border{background:<?php echo $glimmer_color_scheme;?>}.ex-small-border::after{background:<?php echo $glimmer_color_scheme;?>}.comment-reply-link{border-color:<?php echo $glimmer_color_scheme;?>}.comment-reply-link:hover{background:<?php echo $glimmer_color_scheme;?>}button,input[type="button"],input[type="reset"],input[type="submit"]{border-color:<?php echo $glimmer_color_scheme;?>}button:hover,input[type="button"]:hover,input[type="reset"]:hover,input[type="submit"]:hover{background:<?php echo $glimmer_color_scheme;?>}.btn-search{background:<?php echo $glimmer_color_scheme;?>}.btn-search:focus,.btn-search:active:focus{background:<?php echo $glimmer_color_scheme;?> !important}.continue-read{color:<?php echo $glimmer_color_scheme;?>}.continue-read:hover{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.go-button{border-color:<?php echo $glimmer_color_scheme;?>}.go-button:hover{background:<?php echo $glimmer_color_scheme;?>}#featured-item .owl-item.active .image-extra .inner-extra .fet-read-more:hover{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.top-menu .menu-list > li a:hover{color:<?php echo $glimmer_color_scheme;?>}.top-menu .menu-list > li > a::before{background:<?php echo $glimmer_color_scheme;?>}.menu-submenu > li > a:hover,.menu-submenu .menu-submenu > li > a:hover,.menu-submenu .menu-submenu .menu-submenu > li > a:hover{color:<?php echo $glimmer_color_scheme;?> !important}.top-menu .menu-list > li.current-menu-item > a,.top-menu .menu-list > li.current-menu-ancestor > a{color:<?php echo $glimmer_color_scheme;?> !important}.menu-submenu .current-menu-item > a,.current-menu-ancestor .current-menu-ancestor > a{color:<?php echo $glimmer_color_scheme;?> !important}@media (max-width: 992px){.main-nav .top-menu > ul > li:hover > a,.main-nav .top-menu > ul > li.active > a{color:<?php echo $glimmer_color_scheme;?> !important}}.feature-area .full-control{background:<?php echo $glimmer_color_scheme;?>}.feature-area #featured-header .entry-title span{color:<?php echo $glimmer_color_scheme;?>}.feature-area #featured-item .item .image-extra .extra-content span span{color:<?php echo $glimmer_color_scheme;?>}.feature-area .owl-theme .owl-controls .owl-page span{background:<?php echo $glimmer_color_scheme;?>}.inner-extra .entry-meta .cat-links:before,.inner-extra .entry-meta .author:before{color:<?php echo $glimmer_color_scheme;?>}#trending-post .item .td-wrapper .entry-title a:hover {color:<?php echo $glimmer_color_scheme;?>}.content-area .sticky-icon{background:<?php echo $glimmer_color_scheme;?>}.content-area .entry-header .entry-meta a:hover{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-header .entry-meta .cat-links:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-header .entry-meta .entry-date:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-header .entry-meta .author:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-header .entry-meta .view-link:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-content .edit-link:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-footer .button:hover{background:<?php echo $glimmer_color_scheme;?>}.content-area .entry-footer .comments-link:hover{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-footer .share-area a:hover{color:<?php echo $glimmer_color_scheme;?>}.content-area .entry-footer .tags-links span:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .format-link .post-link .link-content{color:<?php echo $glimmer_color_scheme;?>}.content-area .format-link .post-link .link-content a{color:<?php echo $glimmer_color_scheme;?>}.content-area .format-quote .quote-content i{background:<?php echo $glimmer_color_scheme;?>}.content-area .format-quote .quote-content:before{color:<?php echo $glimmer_color_scheme;?>}.content-area .format-quote .quote-content .author a{color:<?php echo $glimmer_color_scheme;?>}.content-area .ribbon span{background:<?php echo $glimmer_color_scheme;?>;background:-webkit-gradient(linear, left top, left bottom, from(<?php echo $glimmer_color_scheme;?>), to(<?php echo $glimmer_color_scheme;?>));background:-webkit-linear-gradient(<?php echo $glimmer_color_scheme;?> 0%, <?php echo $glimmer_color_scheme;?> 100%);background:linear-gradient(<?php echo $glimmer_color_scheme;?> 0%, <?php echo $glimmer_color_scheme;?> 100%)}.content-area .ribbon span:before{border-left:3px solid <?php echo $glimmer_color_scheme;?>;border-top:3px solid <?php echo $glimmer_color_scheme;?>}.content-area .ribbon span:after{border-right:3px solid <?php echo $glimmer_color_scheme;?>;border-top:3px solid <?php echo $glimmer_color_scheme;?>}.single-post .entry-content .tag a:hover{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.single-post .entry-footer .post-format{color:<?php echo $glimmer_color_scheme;?>}.single-post .entry-footer .post-author a:hover{color:<?php echo $glimmer_color_scheme;?>}.navigation .nav-links > li.active a,.navigation .nav-links > li.active span{background:<?php echo $glimmer_color_scheme;?>}.navigation .nav-links > li a:hover{background:<?php echo $glimmer_color_scheme;?>}.navigation .nav-links li:first-child.nav-previous:hover span{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.navigation .nav-links li:first-child.nav-previous:hover span:last-child{color:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.navigation .nav-links li:last-child.nav-next:hover span{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.navigation .nav-links li:last-child.nav-next:hover span:first-child{color:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.navigation > li a:hover,.navigation > li span:hover{border-color:<?php echo $glimmer_color_scheme;?>}.navigation > li.active a,.navigation > li.active span{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.gallery-one .owl-controls .owl-buttons div i,.gallery-two .owl-controls .owl-buttons div i{font-size:12px;color:<?php echo $glimmer_color_scheme;?>}.gallery-two .list-view .owl-controls .owl-buttons div{background:<?php echo $glimmer_color_scheme;?>}.gallery-two .list-view .owl-item.synced .item::after{border-color:<?php echo $glimmer_color_scheme;?>}.author-info .authors-social a:hover{color:<?php echo $glimmer_color_scheme;?>}.follow-us-area .follow-link .fa:hover{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.latest-item-meta a,.popular-item-meta a{color:<?php echo $glimmer_color_scheme;?>}.widget_categories li:hover > a,.widget_archive li:hover > a{color:<?php echo $glimmer_color_scheme;?>}.widget_categories li:hover > .count,.widget_archive li:hover > .count{background:<?php echo $glimmer_color_scheme;?>}.widget_categories li.current-cat > a,.widget_categories li.current-cat-parent > a,.widget_archive li.current-cat > a,.widget_archive li.current-cat-parent > a{color:<?php echo $glimmer_color_scheme;?>}.widget_categories li.current-cat > .count,.widget_categories li.current-cat-parent > .count,.widget_archive li.current-cat > .count,.widget_archive li.current-cat-parent > .count{background:<?php echo $glimmer_color_scheme;?>}.widget_tag_cloud .tagcloud a:hover{background:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>}.widget_calendar caption{background:<?php echo $glimmer_color_scheme;?>}.widget_calendar tbody a{background:<?php echo $glimmer_color_scheme;?>}.widget_links li a:hover,.widget_meta li a:hover,.widget_nav_menu li a:hover,.widget_pages li a:hover,.widget_recent_comments li a:hover,.widget_recent_entries li a:hover{color:<?php echo $glimmer_color_scheme;?>}.widget_links li span:hover:before,.widget_meta li span:hover:before,.widget_nav_menu li span:hover:before,.widget_pages li span:hover:before,.widget_recent_comments li span:hover:before,.widget_recent_entries li span:hover:before{color:<?php echo $glimmer_color_scheme;?>}.tp_recent_tweets li span a{color:<?php echo $glimmer_color_scheme;?>}.tp_recent_tweets li .twitter_time:before{color:<?php echo $glimmer_color_scheme;?>}.tp_recent_tweets li .twitter_time{color:<?php echo $glimmer_color_scheme;?>}.widget_glimmer_aboutus_contact .about-contact-area h3 span{color:<?php echo $glimmer_color_scheme;?>}.form-newsletter #mc-embedded-subscribe:hover{border-color:<?php echo $glimmer_color_scheme;?>;color:<?php echo $glimmer_color_scheme;?>}.about-me .entry-footer h3,.about-me .entry-footer .follow-link a{color:<?php echo $glimmer_color_scheme;?>}.about-me #author-skill .skillbar-bar{background:<?php echo $glimmer_color_scheme;?>}.about-me #author-skill .skill-bar-percent{color:<?php echo $glimmer_color_scheme;?>}.about-me #author-skill .skill-bar-percent:after{border-top:12px solid <?php echo $glimmer_color_scheme;?>}#footer-top .widget .widget-title::before,#footer-top .widget .widget-title::after{border-color:<?php echo $glimmer_color_scheme;?>}#footer-middle .widget_tag_cloud .tagcloud a:hover{border-color:<?php echo $glimmer_color_scheme;?>;background:<?php echo $glimmer_color_scheme;?>}#footer-middle .widget_categories ul li .count:hover,#footer-middle .widget_archive ul li .count:hover{background:<?php echo $glimmer_color_scheme;?>;border:1px solid <?php echo $glimmer_color_scheme;?>}.topbutton:hover{color:<?php echo $glimmer_color_scheme;?>;border-color:<?php echo $glimmer_color_scheme;?>} </style>
<?php
} // end glimmer_color_scheme function
glimmer_color_scheme(); // here print the function
