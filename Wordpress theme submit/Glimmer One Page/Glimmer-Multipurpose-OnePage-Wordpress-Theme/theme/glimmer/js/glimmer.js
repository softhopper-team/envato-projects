
(function($) {
    "use strict";

	/*-----------------------------------
	 Quick Mobile Detection
	 -----------------------------------*/
	 var isMobile = {
	    Android: function () { 
	      return navigator.userAgent.match(/Android/i);
	    },
	    BlackBerry: function () {
	      return navigator.userAgent.match(/BlackBerry/i);
	    },
	    iOS: function () {
	      return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	    },
	    Opera: function () {
	      return navigator.userAgent.match(/Opera Mini/i);
	    },
	    Windows: function () {
	      return navigator.userAgent.match(/IEMobile/i);
	    },
	    any: function () {
	      return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
	    }
	};

	/*-----------------------------------
	 Glimmer App
	 -----------------------------------*/
    var glimmerApp = {
		/* Preloader */
	    glimmar_loading : function () {
            $("#loading").delay(500).fadeOut();
            $(".pageloader").delay(1200).fadeOut("slow");

            var hash = window.location.hash;
            if (hash) { 
                $(document).scrollTop( $(hash).offset().top -56);
            } 
        },

		/* Sticky Nav */
		glimmar_nav : function () {		
			var sticky = new Waypoint.Sticky({
			  element: $('.navbar')[0]
			});
		},
		/* Menu easing */
		glimmar_menu : function () {
			$('.navbar-nav').onePageNav({
				currentClass: 'active',
				easing: 'easeOutExpo'
			});
		},
		/*go section*/
		glimmar_goSection : function () {
	    
			$('.go-service, .go-about, .go-portfolio, .go-contact').each(function () {
			
			    $(this).on('click', function () {
					$.target = $($(this).attr('href')).offset().top-50;
					
					$('body, html').animate({scrollTop : $.target}, 1500, 'easeOutExpo');
					return false;
			    });
			});
		},
		/*parallax*/
		glimmar_parallax : function () {
			if(!isMobile.any()) {
				$('.parallax-one').parallax("50%", 0.5);
				$('.parallax-two').parallax("50%", 0.5);
				$('.parallax-three').parallax("50%", 0.5);
				$('.parallax-four').parallax("50%", 0.5);
				$('#parallax-five').parallax("50%", 0.5);
				$('#parallax-header').parallax("50%", 0.5);
			}
		},		
		/* Skill Bar */
		glimmar_skill : function () {
		  	$('.skillbar').appear(function () {
			    $(this).find('.skillbar-bar, .skill-bar-shape').animate({
			      width:$(this).attr('data-percent')
			    },3000);
			});
			$('.percent-area .skill-bar-percent').css('left', function () {
				return $(this).parent().data('percent');
			});
			$('.percent-area .skill-bar-percent').append(function () {
				return $(this).parent().data('percent');
			});	
		},

		/* testimonial carousel*/
		glimmar_testimonial : function () {
			$('.pik-carousel').carousel({
				autoSlideshow: false,
				itemWidth: 260,
				itemHeight: 260,
				distance: 10,
				selectedItemDistance: 65,
				selectedItemZoomFactor: 0.5,
				unselectedItemZoomFactor: 0.3,
				unselectedItemAlpha: 0.6,
				motionStartDistance: 215,
				topMargin: 115,
				selectByClick: true,
	        	gradientOverlaySize: 215,
	        	gradientOverlayVisible: false,
		        reflectionAlpha: 0.2,
		        slideSpeed: 0.45,
		        preload: false,
		        enableMouseWheel: false
			});
		},
		/*portfolio filter jQuery*/ 
		glimmar_porfolio : function () {
		    var container = $('.portfolio-items');
		    container.isotope({
		        filter: '*',
		        itemSelector: '.col-lg-3',
		  		layoutMode: 'fitRows',	
		        animationOptions: {
		            duration: 750,
		            easing: 'linear',
		            queue: false
		        }
		    });			 
		    $('.portfolio-filter a').on('click', function () {
		        $('.portfolio-filter .current').removeClass('current');
		        $(this).addClass('current');
		 
		        var selector = $(this).attr('data-filter');
		        container.isotope({
		            filter: selector,
		            animationOptions: {
		                duration: 750,
		                easing: 'linear',
		                queue: false
		            }
		        });
		        return false;
		    }); 
		},
		/*Portfolio Single View*/ 
		glimmar_portfolioSingle : function () {
			$('.portfolio-items').on('click', '.read-more', function(event) {
				event.preventDefault();
				var link = $(this).data('single-url'),
					full_url = '#portfolio-single-wrap',
					parts = full_url.split("#"),
					trgt = parts[1],
					target_top = $("#"+trgt).offset().top;
				$('html, body').animate({scrollTop:target_top}, 1200);
				$('#portfolio-single').slideUp(1000, function() {
					$(this).load(link, function() {
						$(this).slideDown(1000, function () {
							$('#project-image').owlCarousel({
    							singleItem:true,
    							lazyLoad: true,
    							navigation:true,
    							pagination: false,
    							navigationText: [
									"<i class='icon-arrow-left'></i>",
									"<i class='icon-arrow-right'></i>"
								]
  							});	
						});
					});
				});	
			});
			// Close Portfolio Single View
			$('#portfolio-single-wrap').on('click', '.close-folio-item', function () {
				var full_url = '#portfolio',
					parts = full_url.split("#"),
					trgt = parts[1],
					target_offset = $("#"+trgt).offset(),
					target_top = target_offset.top;
				$('html, body').animate({scrollTop:target_top}, 1400);
				$("#portfolio-single").slideUp(1000);
				return false;
			});
		},
		/*Video jQuery*/ 
		glimmar_video : function () {
			$("#project-video").fitVids();
			$(".blog-content").fitVids();
		},
		/*Fun-fact counter*/
		glimmar_counter : function () {
		  	$(".count").appear(function() {
		  		$('.count').each(function() {
		      		var count = $(this).attr('data-to');
		  			$(this).find('.count-number').delay(6000).countTo({
				        from: 50,
				        to: count,
				        speed: 3000,
				        refreshInterval: 50  
		      		});  
		    	});
		  	});
		},
		/*client*/
		glimmar_client : function () {
		 	$('.our-client').owlCarousel({
				items : 4,
				itemsDesktop : [1199,4],
			    itemsDesktopSmall : [980,3],
			    itemsTablet: [768,2],
			    itemsMobile : [479,1],
		 		stagePadding: 20,
    			loop: true,
    			margin: 20,
    			autoPlay: true,
    			stopOnHover: true,
				navigation: false,
				pagination: false,
				paginationSpeed : 800
			});
		},
		/*Google Map Customization*/
		glimmar_maps : function () {
			var map;
			var lat = geo.lat;
    		var lon = geo.lon;
    		var map_point_img = geo.map_point_img;
			map = new GMaps({
				el: '#gmap',
				lat: lat,
				lng: lon,
				scrollwheel:false,
				zoom: 10,
				zoomControl : true,
				panControl : false,
				streetViewControl : false,
				mapTypeControl: false,
				overviewMapControl: false,
				clickable: false
			});
			var image = map_point_img;
			map.addMarker({
				lat: lat,
				lng: lon,
				icon: image,
				animation: google.maps.Animation.DROP,
				verticalAlign: 'bottom',
				horizontalAlign: 'center',
				backgroundColor: '#3e8bff'
			});
			var styles = [ 
			{
				"featureType": "road",
				"stylers": [
				{ "color": "#b4b4b4" }
				]
			},
			{
				"featureType": "water",
				"stylers": [
				{ "color": "#d8d8d8" }
				]
			},
			{
				"featureType": "landscape",
				"stylers": [
				{ "color": "#f1f1f1" }
				]
			},
			{
				"elementType": "labels.text.fill",
				"stylers": [
				{ "color": "#000000" }
				]
			},
			{
				"featureType": "poi",
				"stylers": [
				{ "color": "#d9d9d9" }
				]
			},
			{
				"elementType": "labels.text",
				"stylers": [
				{ "saturation": 1 },
				{ "weight": 0.1 },
				{ "color": "#000000" }
				]
			}
			];
			map.addStyle({
				styledMapName:"Styled Map",
				styles: styles,
				mapTypeId: "map_style"  
			});
			map.setStyle("map_style");
		},
		/*Contact Form*/
		glimmar_contactForm : function () {
			var form = $('#contact-us'); 
			form.submit(function (event) {
				event.preventDefault();
				var data = form.serialize();
				var form_status = $('<div class="form_status"></div>');
				$.ajax({
					type: "POST",
					dataType: "json",
					data: data,
					url: $(this).attr('action'),

					beforeSend: function () {
						form.before( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
					}
				}).done(function (data) {
					form_status.html('<p class="text-success">' + data.message + '</p>').delay(3000).fadeOut();
				});
            });  
		},
		/*Retina Image*/
		glimmar_retina : function () {
		    if (window.devicePixelRatio > 1) {

		        var lowresImages = $('img');

		        lowresImages.each(function () {
		            var lowres = $(this).attr('src');
		            var highres = lowres.replace(".", "@2x.");
		            $(this).attr('src', highres);
		        });
		    }
		},
		/*scroll to top*/
		 glimmar_scrollTop : function () {
	 	    $(".scrollup").hide();
		    $(window).scroll(function() {
		        if ($(this).scrollTop() > 600) {
		            $('.scrollup').fadeIn();
		         } else {
		            $('.scrollup').fadeOut();
		         }
		    });
			$("a.backtopbutton[href^='#']").on('click', function(e) {
			   e.preventDefault();
			   $('html, body').stop().animate({scrollTop:0}, 1000, 'easeOutExpo');
			});
		 },

		glimmar_tooltip : function() {
		 	$('[data-toggle="tooltip"]').tooltip();
		},
		glimmar_initializ : function () {			
			glimmerApp.glimmar_nav();														
			glimmerApp.glimmar_menu();
			glimmerApp.glimmar_goSection();
			glimmerApp.glimmar_parallax();	
			glimmerApp.glimmar_skill(); 	
		  	glimmerApp.glimmar_portfolioSingle();
			glimmerApp.glimmar_counter();							
			glimmerApp.glimmar_video();							
			glimmerApp.glimmar_client();
			glimmerApp.glimmar_scrollTop();
			glimmerApp.glimmar_maps();			
			glimmerApp.glimmar_retina();
			glimmerApp.glimmar_tooltip();		
		}
	};
	/* === document ready function === */
	$(document).ready(function () {
		glimmerApp.glimmar_initializ();			
	});
	/* === window load function === */
	$(window).load(function () {	
		glimmerApp.glimmar_loading();
		glimmerApp.glimmar_porfolio(); 	 	
	});
	/* === function === */	
	glimmerApp.glimmar_testimonial();	
	glimmerApp.glimmar_contactForm();
})(jQuery);