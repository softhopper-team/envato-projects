<?php
/**
 * Template Name: About Me
 */
?>
<?php 
    get_header();
    global $candid;
?>
<!-- Content
================================================== -->
<main id="content" class="author-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-title">
                    <h2 class="section-title"><?php the_title(); ?></h2>
                    <div class="ex-small-border"></div>    
                </div> <!-- /.header-title -->

                <!-- main section -->
                <div id="main">
                    
                    <article class="post">
                        <div class="post-thumbnail">
                            <img class="img-responsive" src="<?php if ( isset ( $candid['author_img']['url'] ) ) echo $candid['author_img']['url']; ?>" alt="Author Img" >
                        </div> <!-- /.post-thumbnail -->

                        <h2 class="author-name">
                        <?php 
                            if ( isset ( $candid['author_name'] ) ) echo $candid['author_name'];
                        ?>
                        </h2> <!-- /.author-name -->

                        <div class="entry-content">
                            <?php 
                                if ( isset ( $candid['author_description'] ) ) echo $candid['author_description'];
                            ?>

                            <footer class="entry-footer clearfix">
                               <div class="author-sign">
                                    <img src="<?php if ( isset ( $candid['author_sign']['url'] ) ) echo $candid['author_sign']['url']; ?>" alt="Sign" >
                                    <h3>
                                    <?php 
                                        if ( isset ( $candid['author_name'] ) ) echo $candid['author_name'];
                                    ?>
                                    </h3>
                               </div>
                               <div class="follow-link">
                                    <b><?php _e('Follow me -', 'candid' ); ?></b>
                                    <?php
                                        if( isset ( $candid['author_social_link'] )  && ! empty ( $candid['author_social_link']) ) {
                                            $social_link = $candid['author_social_link'];                      
                                            foreach ( $social_link as $key => $value ) {
                                            if ( $value['title'] ) {
                                    ?>
                                        <a href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                                    <?php
                                                }
                                            }
                                        }
                                    ?>     
                               </div> 
                            </footer> <!-- /.entry-footer -->    
                        </div>  
                    </article>
                </div> <!-- /#main -->
            </div> <!-- /.col-md-12 --> 
        </div> <!-- /.row --> 

        <div id="author-skill">
            <h3><?php _e('Skills', 'candid' ); ?></h3>
            <div class="row">
                <?php
                    if( isset ( $candid['author_skills'] )  && ! empty ( $candid['author_skills']) ) {
                        $social_link = $candid['author_skills'];                      
                        foreach ( $social_link as $key => $value ) {
                        if ( $value['title'] ) {
                    ?>
                    <div class="col-sm-6 pd-bottom">
                        <div class="skill-area">

                            <div class="percent-area">
                                <div class="skillbar" data-percent="<?php echo $value['url']; ?>">
                                    <div class="skillbar-title"><strong><?php echo $value['title'];?></strong></div>
                                    <div class="skillbar-bg">
                                        <div class="skillbar-bar"></div>    
                                    </div>
                                    <div class="skill-bar-percent"></div>
                                </div> <!-- /.skillbar --> 
                            </div> <!-- /.percent-area -->
                        </div> <!-- /.skill-area --> 
                    </div> <!-- /.col-sm-6 -->
                    <?php
                            }
                        }
                    }
                ?>
                

                

               
                
              
            </div> <!-- /.row --> 

        </div> <!-- /#author-skill -->  
    </div> <!-- /.container -->  
</main> <!-- /#content -->
<?php get_footer(); ?>