<?php
/**
 * Glimmer functions and definitions
 *
 * @package Glimmer
 */

/**
 * Define theme's constant
 */
if ( ! defined( 'THEME_VERSION' ) ) {
	define( 'THEME_VERSION', '1.0' );
}
if ( ! defined( 'THEME_DIR' ) ) {
	define( 'THEME_DIR', get_template_directory() );
}
if ( ! defined( 'THEME_URL' ) ) {
	define( 'THEME_URL', get_template_directory_uri() );
}

/**
 * Include the Redux theme options framework
 */

//Include the Redux theme options framework extension

if ( !class_exists( 'glimmer_redux_register_custom_extension_loader' ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-extensions-loader/loader.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-extensions-loader/loader.php' );
}

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/ReduxCore/framework.php' );
}

if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-framework-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-framework-config.php' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'glimmer_setup' ) ) :

function glimmer_setup() {
	global $glimmer;
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Glimmer, use a find and replace
	 * to change 'glimmer' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'glimmer', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main-menu' => esc_html__( 'Top Menu', 'glimmer' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) 
	);
	/* Define image size */
	//add_image_size( 'featured-img', 336, 254, true );
	add_image_size( 'featured-img-small', 386, 254, true );
	add_image_size( 'trending-post', 283, 232, true );
	add_image_size( 'single-full', 885, 455, true );
	add_image_size( 'single-full-list', 690, 410, true );
	add_image_size( 'popular-post', 66, 66, true );
	add_image_size( 'latest-post', 66, 66, true );
	add_image_size( 'related-posts', 370, 250, true );
	
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'image', 'gallery', 'audio', 'video', 'quote', 'link', 'aside', 'status', 'chat'
		) 
	);
        
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'glimmer_custom_background_args', array (
		'default-color' => 'f2f2f2',
		'default-image' => '',
	) ) );

	// Create custom category when theme setup
	if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
        require_once (ABSPATH.'/wp-admin/includes/taxonomy.php'); 
        if ( ! get_cat_ID( 'Featured' ) ) {
            wp_create_category( 'Featured' );
        }
    }
}
endif; // glimmer_setup
add_action( 'after_setup_theme', 'glimmer_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if ( ! isset( $content_width ) ) {
	$content_width = 960; /* pixels */
}


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function glimmer_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Home Page', 'glimmer' ),
		'id'            => 'sidebar-home-page',
		'description'   => 'This sidebar is only for home page',
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
		'after_title'   => '</span></h5><div class="hr-line"></div></div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar Default', 'glimmer' ),
		'id'            => 'sidebar-default',
		'description'   => 'This sidebar is for single post, single page, archive page and others pages.',
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
		'after_title'   => '</span></h5><div class="hr-line"></div></div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Instagram Widget Area', 'glimmer' ),
		'id'            => 'instagram',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h2 class="widget-title">',
		'after_title'   => '</h2></div>',
	) );
	// Register footer sidebars
	register_sidebars( 4, array(
		'name'          => __( 'Footer %d', 'glimmer' ),
		'id'            => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget clearfix %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h5 class="widget-title"><span>',
		'after_title'   => '</span></h5></div>',
	) );
}
add_action( 'widgets_init', 'glimmer_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function glimmer_scripts() {
	global $wp_scripts, $glimmer;
	$protocol = is_ssl() ? 'https' : 'http';
	// enqueue style
	wp_enqueue_style( 'dashicons' );
	wp_enqueue_style('font-awesome', THEME_URL . "/fonts/font-awsome/css/font-awesome.min.css");
	wp_enqueue_style('bootstrap', THEME_URL . "/lib/bootstrap/css/bootstrap.min.css");
	wp_enqueue_style('glimmer-icon', THEME_URL . "/fonts/glimmer-icon/glimmer-icon.css");
	wp_enqueue_style('owl-carousel', THEME_URL . "/lib/owl-carousel/owl.carousel.min.css");
	wp_enqueue_style('owl-theme', THEME_URL . "/lib/owl-carousel/owl.theme.min.css");
	wp_enqueue_style('magnific-popup', THEME_URL . "/lib/magnific-popup/magnific-popup.min.css");
	wp_enqueue_style('justified-gallery', THEME_URL . "/lib/justifiedgallery/justifiedGallery.min.css");
	wp_enqueue_style( 'glimmer-style', get_stylesheet_uri() );

	// enqueue scripts
	wp_enqueue_script('modernizr', THEME_URL . '/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', false);
	wp_enqueue_script('bootstrap-js', THEME_URL . "/lib/bootstrap/js/bootstrap.min.js", array("jquery"), false, true);
	wp_enqueue_script('plugins-js', THEME_URL . '/js/plugins.js', array("jquery"), false, true);
	wp_enqueue_script('owl-carousel-js', THEME_URL . '/lib/owl-carousel/owl.carousel.min.js', array("jquery"), false, true);

	//jquery.appear.js load only about page
	if ( glimmer_get_current_template() == "aboutme-page.php" ) {
		wp_enqueue_script('appear-js', THEME_URL . '/js/jquery.appear.js', array("jquery"), false, true);
	}
	//google map load only contact page
	if ( glimmer_get_current_template() == "contact-page.php" ) {
		wp_enqueue_script('googleapis-js', "$protocol://maps.googleapis.com/maps/api/js?sensor=true", array("jquery"), false, true);
		wp_enqueue_script('gmaps-js', THEME_URL . '/js/gmaps.min.js', array("jquery"), false, true);
	}
	wp_enqueue_script('magnific-popup-js', THEME_URL . '/lib/magnific-popup/jquery.magnific-popup.min.js', array("jquery"), false, true);
	wp_enqueue_script('justified-gallery-js', THEME_URL . '/lib/justifiedgallery/jquery.justifiedGallery.min.js', array("jquery"), false, true);
	wp_enqueue_script('glimmer-js', THEME_URL . '/js/glimmer.js', array("jquery"), false, true);
	wp_enqueue_script( 'glimmer-navigation', THEME_URL . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'glimmer-skip-link-focus-fix', THEME_URL . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( isset( $glimmer['contact_lat'] ) && isset( $glimmer['contact_lon'] ) ) {
		( $glimmer['sidebar_layout'] == 1 ) ? $owl_item = 7 : $owl_item = 5;
        wp_localize_script("glimmer-js", "glimmer", array (
	        	"lat" => $glimmer['contact_lat'],
	        	"lon" => $glimmer['contact_lon'],
	        	"map_mouse_wheel" => $glimmer['map_mouse_wheel'],
	        	"map_zoom_control" => $glimmer['map_zoom_control'],
	        	"map_point_img" => $glimmer['contact_map_point_img']['url'],
	        	"featured_auto_slide" => $glimmer['featured_post_auto_slide'],
	        	"featured_slide_speed" => $glimmer['featured_slide_speed'],
	        	"featured_thumb_per_page" => $glimmer['featured_thumb_per_page'],
	        	"trending_auto_slide" => $glimmer['trending_post_auto_slide'],
	        	"trending_slide_speed" => $glimmer['trending_slide_speed'],
	        	"tiled_gallery_row_height" => $glimmer['tiled_gallery_row_height'],
	        	"owl_item" => $owl_item,
	        	"theme_uri" => THEME_URL 
        	)
        );
    }

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'glimmer_scripts' );

/**
 * Enqueue scripts and styles for WordPress admin panel.
 */
function glimmer_admin_panel_scripts($hook) {
	// enqueue style
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style('admin-font-awesome', THEME_URL . "/fonts/font-awsome/css/font-awesome.min.css");
	wp_enqueue_style('admin-custom', THEME_URL . "/css/backend/custom.css");
	wp_enqueue_style('redux-custom', THEME_URL . "/css/backend/redux-custom.css");	
	if( $hook == 'widgets.php' ) {
		wp_enqueue_style('thickbox');
	}

	// enqueue scripts
	wp_enqueue_script('glimmer-backend-js', THEME_URL . '/js/backend/admin.js', array("jquery"), false, true);
	if( $hook == 'widgets.php' ) {		 
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('widget-js', THEME_URL . '/js/backend/widget.js', null, null, true);
	}
}
add_action( 'admin_enqueue_scripts', 'glimmer_admin_panel_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require THEME_DIR . '/inc/functions/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require THEME_DIR . '/inc/functions/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require THEME_DIR . '/inc/functions/extras.php';

/**
 * Customizer additions.
 */
require THEME_DIR . '/inc/functions/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require THEME_DIR . '/inc/functions/jetpack.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require THEME_DIR . '/inc/libs/tgm-plugin-activation/tgm-admin-config.php';

/**
 * Configure CMB2 Meta Box
 */
require THEME_DIR . '/inc/libs/cmb2/cmb2-config.php';

/**
 * Load custom widget
 */
require THEME_DIR . '/inc/widgets/widgets.php';

/**
 * Register Custom Navigation Walker
 */
require THEME_DIR . '/inc/functions/glimmer-nav-walker.php';

/**
 * Wordpress comment seciton override 
 */
require THEME_DIR . '/inc/functions/wp-comment-section-override.php';

/**
 * Query function to get post
 */
require THEME_DIR . '/inc/functions/function-for-post.php';

/**
 * Glimmer Shortcode Function 
 */
require THEME_DIR . '/inc/shortcodes/shortcodes.php';

/**
 * Popular Post functions
 */
require THEME_DIR . '/inc/functions/popular-post.php';

/**
 * Include header, Hooks for template header
 */
require THEME_DIR . '/inc/frontend/header.php';

/**
 * Include header, Hooks for template header
 */
require THEME_DIR . '/inc/frontend/footer.php';

/**
 * Include chat-post-modify
 */
require THEME_DIR . '/inc/functions/chat-post-modify.php';

/**
 * Include override functions
 */
require THEME_DIR . '/inc/functions/wordpress-override.php';

/**
 * Include html minifier function so that improve page loading speed
 */
//require THEME_DIR . '/inc/functions/html-minifier.php';
