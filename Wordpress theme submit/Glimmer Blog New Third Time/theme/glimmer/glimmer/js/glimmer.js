/* ---------------------------------------------
 common scripts
 --------------------------------------------- */
(function($) {
    "use strict"; // use strict to start

    var glimmerApp = {
        /* ---------------------------------------------
         Preloader
         --------------------------------------------- */
        preloader: function() {
            $(window).on('load', function() {
                $("body").imagesLoaded(function() {
                    $('.preloader').delay(500).slideUp('slow', function() {
                        $(this).remove();
                    });
                });
            });
        },
        /* ---------------------------------------------
         Placeholder
         --------------------------------------------- */
        placeholder: function() {
            var $ph = $('input[type="search"], input[type="text"], input[type="email"], textarea');
            $ph.each(function() {
                var value = $(this).val();
                $(this).focus(function() {
                    if ($(this).val() === value) {
                        $(this).val('');
                    }
                });
                $(this).blur(function() {
                    if ($(this).val() === '') {
                        $(this).val(value);
                    }
                });
            });
        },
        /* ---------------------------------------------
         Header Social And Search
         --------------------------------------------- */
        social_search: function() {
            $('.share-button').on('click', function() {
                $('#top-social').animate({width: "toggle"}, 250);
            });
            $('#search-submit').on('click', function() {
                $('#dsearch').animate({width:'toggle'},500);
                $(this).toggleClass("td");
            });
        },

        /* ---------------------------------------------
         Menu fix
         --------------------------------------------- */
        menu: function() {
            var items = $('.overlapblackbg, .slideLeft');
            var menucontent = $('.menucontent');

            var menuopen = function() {
                $(items).removeClass('menuclose').addClass('menuopen');
            };
            var menuclose = function() {
                $(items).removeClass('menuopen').addClass('menuclose');
            };
            $('#navToggle').on('click', function() {
                if (menucontent.hasClass('menuopen')) {
                    $(menuclose);
                } else {
                    $(menuopen);
                }
            });
            menucontent.on('click', function() {
                if (menucontent.hasClass('menuopen')) {
                    $(menuclose);
                }
            });
            $('#navToggle,.overlapblackbg').on('click', function() {
                $('.menucontainer').toggleClass("mrginleft");
            });
            $('.menu-list li').has('.menu-submenu').prepend('<span class="menu-click"><i class="menu-arrow fa fa-plus"></i></span>');
            $('.menu-mobile').on('click', function() {
                $('.menu-list').slideToggle('slow');
            });
            $('.menu-click').on('click', function() {
                $(this).siblings('.menu-submenu').slideToggle('slow');
                $(this).children('.menu-arrow').toggleClass('menu-extend');
            });
        },

        /* ---------------------------------------------
         smooth scroll
         --------------------------------------------- */
        smoothscroll: function() {
            if (typeof smoothScroll == 'object') {
                smoothScroll.init();
            }
        },
        /* ---------------------------------------------
         Featured Slider
         --------------------------------------------- */
        featured_area: function() {
            var sync1 = $("#featured-item");
            var sync2 = $("#full-sync");
            var featured_auto_slide = glimmer.featured_auto_slide;
            var featured_thumb_per_page = glimmer.featured_thumb_per_page;            
            if ( featured_auto_slide == 1 ) {
                var featured_post_auto_slide = true;
            } else {
                var featured_post_auto_slide = false;
            }            
            var featured_slide_speed = glimmer.featured_slide_speed;            
            sync1.owlCarousel({
                singleItem: true,
                navigation: true,
                autoPlay: featured_post_auto_slide,
                pagination: false,
                stopOnHover: true,
                navigationText: [
                    "<span class='fa fa-angle-left'></span>",
                    "<span class='fa fa-angle-right'></span>"
                ],
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
                addClassActive: true,
                slideSpeed: featured_slide_speed,
                paginationSpeed: featured_slide_speed
            });

            sync2.owlCarousel({
                items: featured_thumb_per_page,
                itemsDesktop: [1199, 5],
                itemsDesktopSmall: [979, 5],
                itemsTablet: [768, 4],
                itemsMobile: [479, 2],
                pagination: false,
                responsiveRefreshRate: 100,
                afterInit: function(el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                $("#full-sync")
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced")
                if ($("#full-sync").data("owlCarousel") !== undefined) {
                    center(current)
                }
            }

            $("#full-sync").on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }

                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }

            }
        },

        /* ---------------------------------------------
         Trending Post
         --------------------------------------------- */       

        trending_area: function() {
            var trending_auto_slide = glimmer.trending_auto_slide;
            if ( trending_auto_slide == 1 ) {
                var trending_post_auto_slide = true;
            } else {
                var trending_post_auto_slide = false;
            }            
            var trending_slide_speed = glimmer.trending_slide_speed;  

            $('#trending-post').owlCarousel({
                items: 2,
                itemsDesktop: [1199, 2],
                itemsDesktopSmall: [980, 1],
                itemsTablet: [768, 1],
                itemsMobile: [479, 1],
                navigation: false,
                autoPlay: trending_post_auto_slide,
                pagination: true,
                responsiveRefreshRate: 200,
                addClassActive: true,
                slideSpeed: trending_slide_speed,
                paginationSpeed: trending_slide_speed
            });
        },

        /* ---------------------------------------------
         Video Responsive fix
         --------------------------------------------- */
        video: function() {
            $(".content-area").fitVids();
        },

        /* ---------------------------------------------
         Tooltip
         --------------------------------------------- */
        tooltip: function() {
           $('[data-toggle="tooltip"]').tooltip();
        },

        /* ---------------------------------------------
         Gallery Carousel One
         --------------------------------------------- */
        gallary: function() {
            $('.gallery-one').owlCarousel({
                singleItem: true,
                slideSpeed: 800,
                navigation: true,
                pagination: false,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ],
                afterInit: function(el) {
                    el.magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        closeOnContentClick: false,
                        closeBtnInside: false,
                        mainClass: 'mfp-with-zoom mfp-img-mobile',
                        image: {
                            verticalFit: true
                        },
                        gallery: {
                            enabled: true
                        },
                        zoom: {
                            enabled: true,
                            duration: 300,
                            opener: function(element) {
                                return element.find('img');
                            }
                        }
                    });
                }
            });
        },
        /* ---------------------------------------------
         Gallery Justified
         --------------------------------------------- */
        gallery_justified: function() {            
            var tiled_gallery_row_height = glimmer.tiled_gallery_row_height; 
            if ($('.glimmer-tiled-gallery').length) {
                var tiledItemSpacing = 4;
                $('.glimmer-tiled-gallery').wrap('<div class="glimmer-tiled-gallery-row"></div>');
                $('.glimmer-tiled-gallery').parent().css('margin', -tiledItemSpacing);
                $('.glimmer-tiled-gallery').justifiedGallery({
                    rowHeight: tiled_gallery_row_height,
                    maxRowHeight: '200%',
                    lastRow: 'justify',
                    margins: tiledItemSpacing,
                    waitThumbnailsLoad: false
                });
            }
            $('.glimmer-tiled-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: false,
                mainClass: 'pp-gallery mfp-with-zoom mfp-img-mobile',
                image: {
                    verticalFit: true
                },
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300,
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });
        },
        /* ---------------------------------------------
         Gallery Carousel Two
         --------------------------------------------- */
        gallary_two: function() {
            var sync1 = $(".full-view");
            var sync2 = $(".list-view");

            sync1.owlCarousel({
                singleItem: true,
                slideSpeed: 800,
                navigation: true,
                pagination: false,
                lazyLoad: true,
                afterAction: syncPosition,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ],
                afterInit: function(el) {
                    el.magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        closeOnContentClick: false,
                        closeBtnInside: false,
                        mainClass: 'mfp-with-zoom mfp-img-mobile',
                        image: {
                            verticalFit: true
                        },
                        gallery: {
                            enabled: true
                        },
                        zoom: {
                            enabled: true,
                            duration: 300,
                            opener: function(element) {
                                return element.find('img');
                            }
                        }
                    });
                }
            });
            var owl_item = glimmer.owl_item;
            sync2.owlCarousel({
                items: owl_item,
                itemsDesktop: [1199, 5],
                itemsDesktopSmall: [979, 5],
                itemsTablet: [768, 5],
                itemsMobile: [479, 2],
                pagination: false,
                responsiveRefreshRate: 100,
                addClassActive: true,
                stagePadding: 20,
                lazyLoad: true,
                navigation: false,
                afterInit: function(el) {
                    el.find(".owl-item").eq(0).addClass("synced");
                }
            });

            function syncPosition(el) {
                var current = this.currentItem;
                sync2
                    .find(".owl-item")
                    .removeClass("synced")
                    .eq(current)
                    .addClass("synced");
                if ($("#list-view").data("owlCarousel") !== undefined) {
                    center(current);
                }
            }
            sync2.on("click", ".owl-item", function(e) {
                e.preventDefault();
                var number = $(this).data("owlItem");
                sync1.trigger("owl.goTo", number);
            });

            function center(number) {
                var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
                var num = number;
                var found = false;
                for (var i in sync2visible) {
                    if (num === sync2visible[i]) {
                        var found = true;
                    }
                }
                if (found === false) {
                    if (num > sync2visible[sync2visible.length - 1]) {
                        sync2.trigger("owl.goTo", num - sync2visible.length + 2)
                    } else {
                        if (num - 1 === -1) {
                            num = 0;
                        }
                        sync2.trigger("owl.goTo", num);
                    }
                } else if (num === sync2visible[sync2visible.length - 1]) {
                    sync2.trigger("owl.goTo", sync2visible[1])
                } else if (num === sync2visible[0]) {
                    sync2.trigger("owl.goTo", num - 1)
                }
            }
        },
        /* ---------------------------------------------
         Comment From
         --------------------------------------------- */
        comment_section: function() {
            $('.comment-reply-title').append('<span class="small-border"></span>');
            $("#contact_form > p").wrap("<div class='col-md-12'></div>");
            $("#contact_form > div").wrapAll("<div class='row'></div>");
        },
        /* ---------------------------------------------
         Chat More Button fix
         --------------------------------------------- */
        chat_more_button: function() {
            if ($('.chat-text p .more-link').length) {
                $('.chat-text p .more-link').detach().appendTo('#readmore-add');
            }
        },
        /* ---------------------------------------------
         Scroll Top
         --------------------------------------------- */
        scroll_top: function() {
            $("body").append("<a href='#top' class='topbutton'><span class='glyphicon glyphicon-menu-up'></span></a>");
            $("a[href='#top']").on('click', function() {
                $("html, body").animate({
                    scrollTop: 0
                }, "normal");
                return false;
            });
            $(".topbutton").hide();
            $(window).scroll(function() {
                if ($(this).scrollTop() < 600) {
                    $("a[href='#top']").fadeOut('fast');
                } else {
                    $("a[href='#top']").fadeIn('fast');
                }
            });
        },
        /* ---------------------------------------------
         Author Skill
         --------------------------------------------- */
        author_skill: function() {
            if ($('.skillbar').length) {
                var $skill = $('.skillbar');

                $skill.appear(function() {
                    $(this).find('.skillbar-bar, .skill-bar-shape').animate({
                        width: $(this).attr('data-percent')
                    }, 1000);
                });
            }

            $('.percent-area .skill-bar-percent').css('left', function() {
                return $(this).parent().data('percent')
            });

            $('.percent-area .skill-bar-percent').append(function() {
                return $(this).parent().data('percent')
            });
        },
        /* ---------------------------------------------
         Google Maps
         --------------------------------------------- */
        maps: function() {
            if ($('#gmaps').length) {
                var map;
                var lat = glimmer.lat;
                var lon = glimmer.lon;
                var map_mouse_wheel = glimmer.map_mouse_wheel;
                var map_zoom_control = glimmer.map_zoom_control;
                var map_point_img = glimmer.map_point_img;
                map = new GMaps({
                    el: '#gmaps',
                    lat: lat,
                    lng: lon,
                    scrollwheel: map_mouse_wheel,
                    zoom: 10,
                    zoomControl: map_zoom_control,
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: false,
                    overviewMapControl: false,
                    clickable: false
                });
                
                var image = map_point_img;
                map.addMarker({
                    lat: lat,
                    lng: lon,
                    icon: image,
                    animation: google.maps.Animation.DROP,
                    verticalAlign: 'bottom',
                    horizontalAlign: 'center'
                });


                var styles = [{
                    "featureType": "road",
                    "stylers": [{
                        "color": "#b4b4b4"
                    }]
                }, {
                    "featureType": "water",
                    "stylers": [{
                        "color": "#d8d8d8"
                    }]
                }, {
                    "featureType": "landscape",
                    "stylers": [{
                        "color": "#f1f1f1"
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#000000"
                    }]
                }, {
                    "featureType": "poi",
                    "stylers": [{
                        "color": "#d9d9d9"
                    }]
                }, {
                    "elementType": "labels.text",
                    "stylers": [{
                        "saturation": 1
                    }, {
                        "weight": 0.1
                    }, {
                        "color": "#000000"
                    }]
                }]
            }
        },

        /*Wp Admin bar*/
        wp_adminbar: function() {
            // This function gets called with the user has scrolled the window.
            $(window).scroll(function() {
                if ($(this).scrollTop() > 0) {
                    // Add the scrolled class to those elements that you want changed
                    $(".top-menu, .animated-arrow.menuopen").addClass("scroll");
                } else {
                    $(".top-menu, .animated-arrow.menuopen").removeClass("scroll");
                }
            });
        },

        initializ: function() {
            glimmerApp.preloader();
            glimmerApp.placeholder();
            glimmerApp.social_search();
            glimmerApp.menu();
            glimmerApp.smoothscroll();
            glimmerApp.featured_area();
            glimmerApp.trending_area();
            glimmerApp.gallary();
            glimmerApp.gallery_justified();
            glimmerApp.gallary_two();
            glimmerApp.video();
            glimmerApp.tooltip();
            glimmerApp.comment_section();
            glimmerApp.chat_more_button();
            glimmerApp.scroll_top();
            glimmerApp.author_skill();
            glimmerApp.maps();
            glimmerApp.wp_adminbar();
        }
    };

    /* === document ready function === */
    $(document).ready(function() {
        glimmerApp.initializ();
    });

})(jQuery);