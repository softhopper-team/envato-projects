<?php
class Glimmer_Popular_Posts extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => 'Glimmer Popular Posts',
			'name' => 'Glimmer Popular Posts'
		);
		parent::__construct('Glimmer_Popular_Posts','',$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
			<input
				class="widefat"
				type="text"
				id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('glimmer_popular_post_limit'); ?>">Number of posts to show:</label>
			<input 
				id="<?php echo $this->get_field_id('glimmer_popular_post_limit'); ?>" 
				type="text" 
				name="<?php echo $this->get_field_name('glimmer_popular_post_limit'); ?>"
				value="<?php if( isset($glimmer_popular_post_limit) ) echo esc_attr($glimmer_popular_post_limit); ?>"
				size="3" />
		</p>
		<?php

	}

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$glimmer_popular_post_limit = apply_filters('widget_glimmer_popular_post_limit',$glimmer_popular_post_limit);
		if ( empty($glimmer_popular_post_limit) ) $glimmer_popular_post_limit = 5;

		echo $before_widget;
			echo $before_title . $title . $after_title;
			?>
			
				<?php 
					$glimmer_popular_post = new WP_Query( array( 'posts_per_page' => $glimmer_popular_post_limit, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
					while ( $glimmer_popular_post->have_posts() ) : $glimmer_popular_post->the_post();	
				?>

                <div class="popular-wrap">

                    <?php
                        if ( has_post_thumbnail() ) {
                            ?>
                            <div class="popular-post-thumb">
		                        <a href="<?php the_permalink(); ?>">
		                            <figure class="fit-img ripple">
		                                <?php
		                                	the_post_thumbnail('popular-post', array( 'class' => "", 'alt' => get_the_title()));
		                                ?>
		                            </figure>
		                        </a>
		                    </div> <!-- /.popular-post-thumb -->
                            <?php
                        } else {
                        ?>
                        	<div class="popular-post-thumb">
		                        <a href="<?php the_permalink(); ?>">
		                            <figure class="fit-img ripple">
		                               <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog/popular-no-thumb.gif" alt="No Thumbnail" />
		                            </figure>
		                        </a>
		                    </div> <!-- /.popular-post-thumb -->
                        <?php
                        }
                    ?>  

                    <div class="popular-post-detail">
                        <div class="blog-meta">
                            <ul>
                                <li><?php the_time('j M, Y'); ?></li>
                        		<li><?php echo wpb_get_post_views(get_the_ID()); ?></li>
                            </ul>       
                        </div> <!-- /.blog-meta -->
                        <div class="popular-description">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div> <!-- /.popular-description -->
                    </div> <!-- /.popular-post-detail -->
                </div> <!-- /.popular-wrap -->
                <?php
					endwhile;
				?>
			<?php
		echo $after_widget;
	}
}

add_action('widgets_init','glimmer_register_widget');
function glimmer_register_widget() {
	register_widget('Glimmer_Popular_Posts');
}