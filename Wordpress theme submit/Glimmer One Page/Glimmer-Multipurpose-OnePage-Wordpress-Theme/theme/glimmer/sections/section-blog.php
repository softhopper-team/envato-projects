<?php
    global $glimmer;
    $posts = glimmer_get_custom_posts("post", 3);
?>  
<!-- Blog
================================================== --> 
<section id="blog-featured" class="blog-featured">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <header class="heading wow <?php if (isset($glimmer['section_blog_animation_style'])) echo $glimmer['section_blog_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_blog_animation_duration'])) echo $glimmer['section_blog_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_blog_animation_delay'])) echo $glimmer['section_blog_animation_delay']; ?>">                       
                        <h2 class="title">
                            <?php if (isset($glimmer['section_blog_title'])) echo $glimmer['section_blog_title'];
                            ?>
                        </h2>
                            <span class="small-border"></span>

                        <h5 class="subtitle"><?php if (isset($glimmer['section_blog_subtitle'])) echo $glimmer['section_blog_subtitle'];
                                ?></h5>
                    </header> <!-- /.heading -->                        
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
        
        <div class="base-padding">
            <div class="row">
                <?php
                    foreach ($posts as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?> 
                <div class="col-sm-6 col-md-4">
                    <div class="blog-latest-img">
                        <?php
                            if ( has_post_thumbnail() ) {
                                  the_post_thumbnail('section-blog', array( 'class' => "img-responsive", 'alt' => get_the_title()));
                            } else {
                            ?>
                                <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog/blog-no-thumb.gif" alt="No Thumbnail" />
                            <?php
                            }
                        ?>    
                    </div> <!-- /.blog-latest-img -->
                    <div class="blog-section-content">                               
                        <div class="blog-description">
                            <h2>
                                <a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2> 
                             <div class="blog-meta">
                                <?php glimmer_posted_on_for_home(); ?>
                            </div> <!-- /.blog-meta -->
                            <hr> <!-- /.hr -->                                
                                <?php the_excerpt(); ?>                                   
                        </div> <!-- /.blog-description -->
                    </div>  <!-- /.blog-section-content -->

                </div> <!-- /.col-md-4 -->
                <?php } ?>  

            </div> <!-- /.row -->

            <div class="row">
                <div class="col-md-12 text-center base-padding-top">
                    <a class="button-big" href="<?php echo glimmer_get_blog_link(); ?>">More Blog</a>
                </div> <!-- /.col-md-12 -->
            </div> <!-- /.row -->                   
        </div> <!-- /.base-padding -->

    </div> <!-- /.container -->

</section> <!-- /.blog -->