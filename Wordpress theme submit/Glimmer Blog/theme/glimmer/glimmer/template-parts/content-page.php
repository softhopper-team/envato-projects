<?php
/**
 * The template for displaying page content.
 *
 * @package Glimmer
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php 		
			the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
		?>
	</header>
	
	<?php 
		if ( has_post_thumbnail() ) {
        ?>
        <figure class="post-thumb">
			<a href="<?php the_permalink(); ?>">
				<?php
		          	the_post_thumbnail('single-full', array( 'class' => " img-responsive", 'alt' => get_the_title()));
		        ?>
			</a>
		</figure> <!-- /.post-thumb -->
        <?php
        } else {
        	echo "<hr>";
        }
	?>

	<div class="entry-content">
		<?php 
			the_content();
			edit_post_link( esc_html__( '(Edit Page)', 'glimmer' ), '<span class="edit-link">', '</span>' ); 
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->
    
    <?php glimmer_page_footer(); ?> 
    
</article> <!-- /.post-->