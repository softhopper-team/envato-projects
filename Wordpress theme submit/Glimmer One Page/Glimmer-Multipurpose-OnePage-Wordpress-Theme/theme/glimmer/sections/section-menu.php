<?php
    global $glimmer;
?>	 

<!-- Menu
================================================== --> 
<div class="navbar navbar-default " role="navigation">
	<div class="container">
	  	<div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
	    	</button>
	    	<a class="navbar-brand" href="<?php echo site_url(); ?>">
            </a>
	  	</div>

	  	<div class="navbar-collapse collapse">
	    <!-- Right nav -->
            <?php if (!has_nav_menu("primary")) {
            ?>
                <ul class="nav navbar-nav one-page-nav navbar-right">
                <?php 
                    if ($glimmer['section_header_display']) {
                ?>
                    <li class="active"><a href="#header">
                        <?php 
                            if (isset($glimmer['section_header_menu_text'])) echo $glimmer['section_header_menu_text'];
                        ?>
                    </a>
                    </li>
                <?php
                    }
                    else {
                ?>
                    <li class="active"><a href="<?php echo site_url(); ?>"><?php _e('HOME','glimmer') ?></a></li>
                <?php
                    }
                ?>
                <?php
                    $sections = $glimmer['glimmer_sections_order'];
                    if (!is_array($sections)) {
                        $sections = glimmer_get_enabled_sections();
                    }
                    if (empty($sections)) {
                        $sections = glimmer_get_all_sections();
                    }
                    $section_ids = array(
                        "section_aboutus" => "#about-us",
                        "section_team" => "#our-team",
                        "section_services" => "#our-service",
                        "section_pricing" => "#price-table",
                        "section_funfact" => "#fun-fact",
                        "section_client" => "#our-clients",
                        "section_skill" => "#our-skill",
                        "section_testimonial" => "#testimonial",
                        "section_portfolio" => "#portfolio",
                        "section_blog" => "#blog-featured",
                        "section_contact" => "#contact",
                    );
                    foreach ($sections as $section => $name) {
                        $ssection = str_replace("section-", "section_", $section);                            
                            $menu_key = "{$ssection}_menu_text";
                        if ($ssection !== "section_parallax") {
                            $section_id = $section_ids[$ssection];
                            ?>
                            <?php if ($glimmer["{$ssection}_display_menu"]) { ?>
                                <li>
                                    <a href="<?php echo $section_id; ?>"><?php echo $glimmer[$menu_key]; ?></a>
                                </li>
                            <?php
                            }
                        }
                    }
                    ?>                     
                </ul> <!--/.navbar-nav -->
            <?php
                } else {
            ?>
                <?php
                    wp_nav_menu( array(
                        'theme_location'    => 'primary',
                        'depth'             =>  0,
                        'menu_class'        => 'nav navbar-nav one-page-nav navbar-right',
                        'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                        'walker'            => new wp_bootstrap_navwalker())
                    );
                ?>
            <?php
                } 
            ?>
		    
	  	</div><!--/.nav-collapse -->
	</div> <!-- /.container -->
</div> <!-- /.nabvar -->