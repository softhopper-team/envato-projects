<?php
/**
 * The template for displaying link post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-content"> 
          <?php the_content(); ?>        
    </div><!-- /.blog-content -->
</div> <!-- /.blog-content -->