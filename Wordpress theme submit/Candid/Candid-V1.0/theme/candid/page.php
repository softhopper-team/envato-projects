<?php 
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package Candid
 */
    get_header();
    global $candid, $post;
?>
<!-- Content
================================================== -->
<?php
    /* Show box or wide layout with user condition*/
    $layout = '';
    if ( $candid['layout'] == 1 ) {
        $layout = 'box';
    }
?>
<main id="content" class="<?php echo $layout; ?>">
    <div class="container">
        <div class="row">

            <?php
                /* Show sidebar with user condition*/
                $meta = get_post_meta( $post->ID );
                $columns_grid = 8;  
                if ( isset($meta["_candid_custom_layout"][0])) {
                    if ( $meta["_candid_layout"][0] == 'sidebar-content' ) {
                        get_sidebar();
                    } elseif ( $meta["_candid_layout"][0] == 'full-content' ) {
                        $columns_grid = 12;
                    }
                } elseif ( $candid['sidebar_layout_page'] == 2 ) {
                    get_sidebar();
                } elseif ( $candid['sidebar_layout_page'] == 1 ) {
                    $columns_grid = 12;
                }
            ?>
            <div class="col-md-<?php echo $columns_grid; ?>">
                <!-- main section -->
                <div id="main">
                    <?php if ( have_posts() ) : ?>

                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', 'page' );
                        ?>

                    <?php endwhile; ?>

                    <?php else : ?>

                        <?php get_template_part( 'template-parts/content', 'none' ); ?>

                    <?php endif; ?>

                    <!-- Include authorinfo, relatedpost template part -->
                    <?php get_template_part( 'template-parts/content', 'pagination' ); ?>
                    <?php get_template_part( 'template-parts/content', 'authorinfo' ); ?>

                    <?php
						// If comments are open or we have at least one comment, load up the comment template
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
					?>

                </div> <!-- /#main -->

            </div> <!-- /.col-md-8 -->

            <?php
                /* Show sidebar with user condition*/
                $meta = get_post_meta( $post->ID );
                if ( isset($meta["_candid_custom_layout"][0])) {
                    if ( $meta["_candid_layout"][0] == 'content-sidebar') {
                        get_sidebar();
                    } 
                } elseif ( $candid['sidebar_layout_page'] == 3 ) {
                    get_sidebar();
                }
            ?>
             
        </div> <!-- /.row -->   
    </div> <!-- /.container -->  
</main> <!-- /#content -->
<?php get_footer(); ?>