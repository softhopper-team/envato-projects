
var image_field;
jQuery( document ).ready( function($) {
	// redux framework custom js code 
	jQuery('#glimmer-glimmer_hidden_slides').parent().parent('tr').addClass( "glimmer-glimmer_hidden_slides" );

	//glimmer about me
	
	 
	// Show/hide settings for post format when choose post format
	var $format = $( '#post-formats-select' ).find( 'input.post-format' ),
		$formatBox = $( '#_glimmer_post_format_details' );
		// add a link class default because in cmb2 link class not add
		$('.cmb2-id--glimmer-format-link, .cmb2-id--glimmer-format-link-text, .cmb2-id--glimmer-format-link-bg-img').addClass('link');

	$format.on( 'change', function() {
		var	type = $format.filter( ':checked' ).val();

		$formatBox.hide();
		if( $formatBox.find( '.cmb-row' ).hasClass( type ) ) {
			$formatBox.show();
		}

		$formatBox.find( '.cmb-row' ).slideUp();
		$formatBox.find( '.' + type ).slideDown();
	} );
	$format.filter( ':checked' ).trigger( 'change' );

	// Show/hide settings for custom layout settings
	$( '#_glimmer_custom_layout' ).on( 'change', function() {
		if( $( this ).is( ':checked' ) ) {
			$( '.cmb2-id--glimmer-layout' ).slideDown();
		}
		else {
			$( '.cmb2-id--glimmer-layout' ).slideUp();
		}
	} ).trigger( 'change' );
} );
