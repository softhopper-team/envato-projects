<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Candid
 */
?>
<?php 
	$category = get_the_category();  
	$img_rul = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
?> 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
    	$post_format = get_post_format();
    	$meta = get_post_meta( $post->ID );
		if ( false === $post_format ) {
			$post_format = "standard";
		}
		
		if ( $post_format == "audio" ) {
            ?>
            <div class="post-thumbnail">
                <?php              
					if( isset ( $meta["_candid_format_audio"][0] ) ) echo $meta["_candid_format_audio"][0];
				?>
            </div> <!-- /.post-thumbnail -->
            <?php
        } elseif ( $post_format == "video" ) {
            ?>
            <div class="post-thumbnail">
                <?php              
					if( isset ( $meta["_candid_format_video"][0] ) ) echo $meta["_candid_format_video"][0];
				?>
		    </div> <!-- /.post-thumbnail -->
            <?php
        } else {
	            ?>
	            <div class="post-thumbnail">
	                <?php
	                  	the_post_thumbnail('single-full', array( 'class' => " img-responsive", 'alt' => get_the_title()));
		            ?>
		            <div class="image-extra">
	                    <div class="extra-content">
	                        <div class="inner-extra">
	                            <a class="single-image link-icon" href="<?php the_permalink(); ?>">
	                                <span class="fa fa-link"></span>
	                            </a>
	                            <a class="single-image plus-icon" title="<?php the_title(); ?>" href="<?php echo $img_rul ?>">
	                                <span class="fa fa-plus"></span>
	                            </a>
	                        </div> <!-- /.inner-extra -->
	                    </div> <!-- /.extra-content -->
	                </div> <!-- /.image-extra -->
			    </div> <!-- /.post-thumbnail -->
	            <?php
	        }
    	?>

    <header class="entry-header">

		<?php 		
			if ( is_single() ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
			} else {
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
			}
		?>

		<div class="entry-meta">
			<?php candid_posted_on(); ?>
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

    <div class="entry-content">		
		<?php 
			if ( is_single() ) {
				the_content(); 
			} else {
				the_excerpt( __( '<span class="read-button">'.__( 'Continue reading',  'candid').'</span>', 'candid' ) );
				echo '<a href="'.get_the_permalink().'" class="more-link button"><span class="read-button">'.__( 'Continue reading',  'candid').'</span></a>';
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'candid' ),
				'after'  => '</div>',
			) );
		?>
		<?php 
			 candid_entry_footer();
		?>		
	</div><!-- .entry-content -->
</article> <!-- /.post -->