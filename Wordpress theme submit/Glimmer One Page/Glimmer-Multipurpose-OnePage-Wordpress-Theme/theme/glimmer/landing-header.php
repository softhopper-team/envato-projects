<?php
/**
 * The template for displaying one page header.
 *
 * @package Glimmer
 */
global $glimmer;
?>
<?php get_header(); ?>
		    
<?php
	if ( $glimmer['section_header_display'] ) {
			get_template_part("sections/section-slider");
	}			
	get_template_part("sections/section-menu");
?>