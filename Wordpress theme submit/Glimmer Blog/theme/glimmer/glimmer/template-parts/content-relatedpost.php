<?php
/**
 * The template for displaying related posts in the single page
 *
 * @package Glimmer
 */
global $glimmer, $post;
?>
<?php if ( $glimmer['related_post'] ) : ?>
<?php
    $query_type = $glimmer['related_query'];
    $related_no = $glimmer['related_post_number'] ? $glimmer['related_post_number'] : 5;
    if ( $query_type == 'author' ) {
        $args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no , 'no_found_rows'=> 1, 'author'=> get_the_author_meta( 'ID' ));
    } elseif ( $query_type == 'tag' ) {
        $tags = wp_get_post_tags($post->ID);
        $tags_ids = array();
        foreach($tags as $individual_tag) $tags_ids[] = $individual_tag->term_id;
        $args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no , 'no_found_rows'=> 1, 'tag__in'=> $tags_ids );
    } else {
        $categories = get_the_category($post->ID);
        $category_ids = array();
        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
        $args=array('post__not_in' => array($post->ID),'posts_per_page'=> $related_no , 'no_found_rows'=> 1, 'category__in'=> $category_ids );
    }       
    
    $my_query = new wp_query( $args );
?>
<?php
    if( $my_query->have_posts() ) :
?>
<!-- related post -->
<div id="related-post">
    <h3>
        <?php _e('Related Posts','glimmer'); ?>
        <span class="small-border"></span>  <!-- /.small-border --> 
    </h3>  
    <div id="related-post-slide" class="owl-carousel">
        <?php
            while( $my_query->have_posts() ) {
            $my_query->the_post();
        ?>
        <div class="item">
            <?php
                $meta = get_post_meta( get_the_ID() );
                $post_format = get_post_format();
                if ( false === $post_format ) {
                    $post_format = "standard";
                }
                
                if ( $post_format == "gallery" ) {  
                    // show first image of gallery post
                    if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
                        $imgs_urls = $meta["_glimmer_format_gallery"][0];  
                        $imgs_url = explode( '"', $imgs_urls );
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php if(isset($imgs_url[1])) echo $imgs_url[1]; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php                           
                    }  else {
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/gallery.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php 
                    }
                } elseif ( $post_format == "audio" ){
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/audio.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php 
                } elseif ( $post_format == "video" ){
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/video.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php 
                } elseif ( $post_format == "quote" ){
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/quote.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php 
                } elseif ( $post_format == "aside" ){
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/aside.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php 
                } elseif ( $post_format == "chat" ){
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/chat.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail --> 
                        <?php 
                } elseif ( $post_format == "link" ){
                    if( $meta["_glimmer_format_link_bg_img"][0] ) {
                        ?>                       
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo $meta["_glimmer_format_link_bg_img"][0]; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail -->
                        <?php 
                    } else {
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/link.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>                            
                        </div> <!-- /.post-thumbnail -->
                        <?php 
                    }
                } elseif ( $post_format == "status" ){
                        ?>
                        <div class="post-thumbnail">
                            <a href="<?php the_permalink(); ?>">
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo THEME_URL.'/images/post/no-media-big/status.jpg'; ?>" alt="<?php the_title(); ?>"> 
                            </a>    
                        </div> <!-- /.post-thumbnail -->                        
                        <?php 
                } else {
                    ?>
                    <div class="post-thumbnail">
                        <a href="<?php the_permalink();?>">                        
                            <?php                                        
                                $alt_text = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail('related-posts', array ( 
                                        'class' => " img-responsive",
                                        'alt' => $alt_text
                                        )
                                    );
                                } else {
                                ?>
                                <img class="img-responsive lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo get_template_directory_uri(); ?>/images/post/no-media-big/image.jpg" alt="<?php the_title(); ?>">
                                <?php
                                }
                            ?>                       
                        </a> 
                    </div> <!-- /.post-thumbnail -->
                    <?php
                } //end else
            ?>  
            <a href="<?php the_permalink(); ?>" class="image-extra">
                <div class="extra-content">
                    <div class="inner-extra">
                        <div class="entry-meta">
                            <span class="cat-links">
                                <?php _e( 'In ', 'glimmer' ); ?><span rel="category tag">
                                <?php
                                    $categories = get_the_category($post->ID);
                                    foreach ($categories as $category ) {
                                        echo $category->name.', ' ;
                                    }
                                ?>
                                </span>
                            </span>
                            <span class="entry-date">
                                <?php the_time( get_option( 'date_format' ) ); ?>
                            </span>
                        </div> <!-- .entry-meta -->
                        <h2 class="entry-title"><?php echo custom_post_excerpt( get_the_title(), 50, '&hellip;'); ?> </h2>
                        <!-- content -->
                    </div> <!-- /.inner-extra -->
                </div> <!-- /.extra-content -->
            </a> <!-- /.image-extra -->
        </div> <!-- /.item -->
        <?php   
            } // end while loop
        ?> 
    </div> <!-- /.related-post-slide -->
</div> <!-- #related-post -->
 <?php 
    endif;  
    wp_reset_query();
?> 
<?php endif; ?>