<?php
class Candid_Latest_Posts extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => 'Candid Latest Posts',
			'name' => 'Candid Latest Posts'
		);
		parent::__construct('Candid_Latest_Posts','',$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'candid'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('candid_latest_post_limit'); ?>"><?php _e('Number of posts to show:', 'candid'); ?></label>
			<input 
				id="<?php echo $this->get_field_id('candid_latest_post_limit'); ?>" 
				type="text" 
				name="<?php echo $this->get_field_name('candid_latest_post_limit'); ?>"
				value="<?php if( isset($candid_latest_post_limit) ) echo esc_attr($candid_latest_post_limit); ?>"
				size="3" />
		</p>
		<?php

	}

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$candid_latest_post_limit = apply_filters('widget_candid_latest_post_limit',$candid_latest_post_limit);
		if ( empty($candid_latest_post_limit) ) $candid_latest_post_limit = 5;

		echo $before_widget;
			if ( !empty( $title ) ) {
				echo $before_title . $title . $after_title;
			}
			?>
			
				<?php 
					$candid_latest_post = new WP_Query( array( 'posts_per_page' => $candid_latest_post_limit,  'order' => 'DESC'  ) );
				?>
				<div class="latest-widget">
                    <ul class="latest-newsfeed">
						<?php while ( $candid_latest_post->have_posts() ) : $candid_latest_post->the_post(); ?>
						<li class="latest-wrap">
	                        <div class="latest-item">          
	                            <div class="latest-image">
		                            <?php
								    	$meta = get_post_meta( get_the_ID() );
		                            	$post_format = get_post_format();
										if ( false === $post_format ) {
											$post_format = "standard";
										}
										
										if ( $post_format == "gallery" ) {	
				                            if ( isset ( $meta["_candid_format_gallery"][0] ) ) {
				                                $imgs_urls = $meta["_candid_format_gallery"][0];                            
				                            }  else {
				                                $imgs_urls = '';
				                            }        
				                            $imgs_url = explode( '"', $imgs_urls );
		                                	?>
		                                	<a href="<?php the_permalink(); ?>">
					                            <figure class="fit-img">
					                                <img class="latest-item-thumb" src="<?php if(isset($imgs_url[1])) echo $imgs_url[1]; ?>" alt="<?php if(isset($imgs_url[1])) echo $imgs_url[1]; ?>"> 
					                            </figure>
					                        </a>                          	
		                                	<?php 
								        } else {
					                        if ( has_post_thumbnail() ) {
					                            ?>
						                        <a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <?php
						                                	the_post_thumbnail('latest-post', array( 'class' => "latest-item-thumb", 'alt' => get_the_title()));
						                                ?>
						                            </figure>
						                        </a>
					                            <?php
					                        } else {
					                        ?>
						                        <a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                               <img class="latest-item-thumb" src="<?php echo get_template_directory_uri(); ?>/images/no-thumb.gif" alt="No Thumbnail" />
						                            </figure>
						                        </a>
					                        <?php
					                        } //end else
					                    } //end else
				                    ?>  
	                            </div> <!-- /.latest-image -->  

	                            <div class="latest-item-text">
	                                <span class="latest-item-meta">
	                                <?php
							    		$categories_list = get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'candid' ) );
										if ( $categories_list  ) {
											printf( '<span class="cat-links">%1$s %2$s</span>',
												_x( 'In ', 'Used before category names.', 'candid' ),
												$categories_list
											);
										}
								    ?></span>,
	                                <span class="latest-item-meta"><?php the_time( 'j M, Y' ); ?></span>
	                                <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
	                            </div> <!-- /.latest-item-text -->
	                        </div>  <!-- /.latest-item --> 
                    	</li> <!-- /.latest-wrap -->
		                <?php
							endwhile;
						?>
					</ul> <!-- /.latest-newsfeed -->
                </div> <!-- /.latest-widget -->  
			<?php
		echo $after_widget;
	}
}