<?php

function glimmer_hex_2_rgba($color, $opacity = false) {
     $default = 'rgb(0,0,0)';
    //Return default if no color provided
    if(empty($color))
          return $default; 
 
    //Sanitize $color if "#" is provided 
        if ($color[0] == '#' ) {
            $color = substr( $color, 1 );
        }
 
        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
                $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
        } elseif ( strlen( $color ) == 3 ) {
                $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
        } else {
                return $default;
        }
 
        //Convert hexadec to rgb
        $rgb =  array_map('hexdec', $hex);
 
        //Check if opacity is set(rgba or rgb)
        if($opacity){
            if(abs($opacity) > 1)
                $opacity = 1.0;
            $output = 'rgba('.implode(",",$rgb).','.$opacity.')';
        } else {
            $output = 'rgb('.implode(",",$rgb).')';
        }
 
        //Return rgb(a) color string
        return $output;
}

function glimmer_color_scheme() { 
  global $glimmer;
    switch( $glimmer['glimmer_color_scheme'] ) {
        case 1: //C69F73
            $glimmer_color_scheme = "#C69F73";
            break;
        case 2: //1ABC9C
            $glimmer_color_scheme = "#1ABC9C";
            break;
        case 3: //D2527F
            $glimmer_color_scheme = "#D2527F";
            break;
        case 4: //F26D7E
            $glimmer_color_scheme = "#F26D7E";
            break;
        case 5: //CC6054
            $glimmer_color_scheme = "#CC6054";
            break;
        case 6: //667A61
            $glimmer_color_scheme = "#667A61";
            break;
        case 7: //A74C5B
            $glimmer_color_scheme = "#A74C5B";
            break;
        case 8: //95A5A6
            $glimmer_color_scheme = "#95A5A6";
            break;
        case 9: //turquoise
            $glimmer_color_scheme = $glimmer['glimmer_custom_color'];
            break;
        default:
            $glimmer_color_scheme = "#C69F73";
            break;
    }
    //rgba color
    $glimmer_rgba = glimmer_hex_2_rgba($glimmer_color_scheme, 0.8);
?>
<style type="text/css"> 
::-moz-selection {
  background: <?php echo $glimmer_color_scheme;?>;
}

::selection {
  background: <?php echo $glimmer_color_scheme;?>;
}

/* link color */
a:hover, a:focus, a:active {
  color: <?php echo $glimmer_color_scheme;?>;
}

blockquote:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
blockquote cite, blockquote a, blockquote span {
  color: <?php echo $glimmer_color_scheme;?>;
}

.small-border {
  background: <?php echo $glimmer_color_scheme;?>;
}

.ex-small-border::after {
  background: <?php echo $glimmer_color_scheme;?>;
}

.read-more {
  color: <?php echo $glimmer_color_scheme;?>;
}

.comment-reply-link {
  color: <?php echo $glimmer_color_scheme;?>;
}
.comment-reply-link:hover {
  background: <?php echo $glimmer_color_scheme;?>;
}

button,
input[type="button"],
input[type="reset"],
input[type="submit"] {
  background: <?php echo $glimmer_color_scheme;?>;
}
button:hover,
input[type="button"]:hover,
input[type="reset"]:hover,
input[type="submit"]:hover {
  background: <?php echo $glimmer_rgba; ?>;
}
select option:hover, 
select option:focus {
  -webkit-box-shadow: 0 0 10px 100px <?php echo $glimmer_color_scheme;?> inset;
  box-shadow: 0 0 10px 100px <?php echo $glimmer_color_scheme;?> inset;
  background-color: <?php echo $glimmer_color_scheme;?>;
}
select option[selected] {
  background: <?php echo $glimmer_color_scheme;?>;
  -webkit-box-shadow: 0 0 10px 100px <?php echo $glimmer_color_scheme;?> inset;
  box-shadow: 0 0 10px 100px <?php echo $glimmer_color_scheme;?> inset;
}
select option[checked] {
  background: <?php echo $glimmer_color_scheme;?>;
}
.continue-read {
  color: <?php echo $glimmer_color_scheme;?>;
}
.continue-read:hover {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}

.go-button {
  background: <?php echo $glimmer_color_scheme;?>;
}
.go-button:hover {
  background: <?php echo $glimmer_rgba; ?>;
}
.comment-navigation .nav-links a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.comment-navigation .nav-links a:hover {
  background-color: <?php echo $glimmer_color_scheme;?>;
}

/* Breadcrumb */
.breadcrumb {
  border: 1px solid <?php echo $glimmer_color_scheme;?>;
}
.breadcrumb li:first-child {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}
.breadcrumb > li + li::before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.site-header, .site-footer-banner {
  background-color: <?php echo $glimmer_color_scheme;?>;
}

/* preloder */
/*.preloader-header .preloader-loader svg path.preloader-loader-circle {
  stroke: <?php echo $glimmer_color_scheme;?>;
}*/
.preloader .loader i {
  color: <?php echo $glimmer_color_scheme;?>;
}
/* Menu */
.main-nav {
  background: <?php echo $glimmer_color_scheme;?>;
}

@media (max-width: 992px) {
  .main-nav .top-menu > ul > li:hover > a,
  .main-nav .top-menu > ul > li.active > a {
    color: <?php echo $glimmer_color_scheme;?> !important;
  }
  .main-nav .top-menu .current-menu-ancestor > a {
    color: <?php echo $glimmer_color_scheme;?> !important;
  }
  .main-nav .top-menu .current-menu-item > a {
    color: <?php echo $glimmer_color_scheme;?> !important;
  } 
}
/* featured area */
.feature-area #featured-header .entry-title span {
  color: <?php echo $glimmer_color_scheme;?>;
}
.feature-area #featured-item .item .image-extra .extra-content span span {
  color: <?php echo $glimmer_color_scheme;?>;
}
.feature-area .owl-theme .owl-controls .owl-page span {
  background: <?php echo $glimmer_color_scheme;?>;
}

/* Main content */
.content-area .entry-header .entry-meta a:hover {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-content p a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-header .entry-meta .cat-links:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-header .entry-meta .entry-date:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-header .entry-meta .author:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-header .entry-meta .view-link:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-content .edit-link:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-footer table th .button:hover {
  background: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-footer table th .comments-link:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-footer table th .comments-link:hover {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-footer table th .share-button:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-footer table th .share-button a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .entry-footer table th .tags-links span:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .format-link .post-link .link-content {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .format-link .post-link .link-content a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .quote-thumb .quote-content:before {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .quote-thumb .quote-content .author a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.content-area .ribbon span {
  background: <?php echo $glimmer_color_scheme;?>;
  background: linear-gradient(<?php echo $glimmer_color_scheme;?> 0%, <?php echo $glimmer_color_scheme;?> 100%);
}
.content-area .ribbon span:before {
  border-left: 3px solid <?php echo $glimmer_color_scheme;?>;
  border-top: 3px solid <?php echo $glimmer_color_scheme;?>;
}
.content-area .ribbon span:after {
  border-right: 3px solid <?php echo $glimmer_color_scheme;?>;
  border-top: 3px solid <?php echo $glimmer_color_scheme;?>;
}
.header-title .section-title {
  color: <?php echo $glimmer_color_scheme;?>;
}
.panel-heading .panel-title a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.panel-heading .accordion-toggle:after {        
  color: <?php echo $glimmer_color_scheme;?>;        
}

/* pagination */
.navigation .nav-links > li a, .navigation .nav-links > li span {
  border: 1px solid <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links > li.active a, .navigation .nav-links > li.active span {
  background: <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links > li a:hover {
  background: <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li.nav-previous span {
  border: 1px solid <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li.nav-previous span:last-child {
  border: 1px solid <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li.nav-next span {
  border: 1px solid <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li.nav-next span:first-child {
  border: 1px solid <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li:first-child.nav-previous:hover span {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li:first-child.nav-previous:hover span:last-child {
  color: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li:last-child.nav-next:hover span {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}
.navigation .nav-links li:last-child.nav-next:hover span:first-child {
  color: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}
.navigation > li a:hover, .navigation > li span:hover {
  border-color: <?php echo $glimmer_color_scheme;?>;
}
.navigation > li.active a, .navigation > li.active span {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}

.single-post .entry-footer .single-navigation .nav-previous:hover span:first-child {
  background: <?php echo $glimmer_color_scheme;?>;
}
.single-post .entry-footer .single-navigation .nav-next:hover span:last-child {
  background: <?php echo $glimmer_color_scheme;?>;
}

/* gallery */
.gallery-one .owl-controls .owl-buttons div i, .gallery-two .owl-controls .owl-buttons div i {
  font-size: 12px;
  color: <?php echo $glimmer_color_scheme;?>;
}

.gallery-two .list-view .owl-controls .owl-buttons div {
  background: <?php echo $glimmer_color_scheme;?>;
}

.author-info .author-name {
  color: <?php echo $glimmer_color_scheme;?>;
}
.author-info .authors-social a {
  color: <?php echo $glimmer_color_scheme;?>;
}

#related-post .image-extra .entry-meta .cat-links span {
  color: <?php echo $glimmer_color_scheme;?>;
}
#related-post #related-post-slide .owl-controls .owl-buttons div:hover {
  background: <?php echo $glimmer_color_scheme;?>;
}

/* Comments Section */
.comments-area .fn {
  color: <?php echo $glimmer_color_scheme;?>;
}

/* Widget */
.about-widget .about-description .author-name {
  color: <?php echo $glimmer_color_scheme;?>;
}

.follow-us-area .follow-link .fa:hover {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}

.latest-item-meta a, .popular-item-meta a {
  color: <?php echo $glimmer_color_scheme;?>;
}

.widget_categories li:hover > a,
.widget_archive li:hover > a {
  color: <?php echo $glimmer_color_scheme;?>;
}

.widget_categories li:hover > .count,
.widget_archive li:hover > .count {
  background: <?php echo $glimmer_color_scheme;?>;
}

.widget_categories li.current-cat > a,
.widget_categories li.current-cat-parent > a,
.widget_archive li.current-cat > a,
.widget_archive li.current-cat-parent > a {
  color: <?php echo $glimmer_color_scheme;?>;
}

.widget_categories li.current-cat > .count,
.widget_categories li.current-cat-parent > .count,
.widget_archive li.current-cat > .count,
.widget_archive li.current-cat-parent > .count {
  background: <?php echo $glimmer_color_scheme;?>;
}

.widget_tag_cloud .tagcloud a:hover {
  background: <?php echo $glimmer_color_scheme;?>;
  border-color: <?php echo $glimmer_color_scheme;?>;
}

.widget_calendar caption {
  background: <?php echo $glimmer_color_scheme;?>;
}

.widget_calendar tbody a {
  background: <?php echo $glimmer_color_scheme;?>;
}

.widget_links li a:hover,
.widget_meta li a:hover,
.widget_nav_menu li a:hover,
.widget_pages li a:hover,
.widget_recent_comments li a:hover,
.widget_recent_entries li a:hover {
  color: <?php echo $glimmer_color_scheme;?>;
}

.tp_recent_tweets li span a {
  color: <?php echo $glimmer_color_scheme;?>;
}

.widget_glimmer_aboutus_contact .about-contact-area h3 span {
  color: <?php echo $glimmer_color_scheme;?>;
}
.widget_glimmer_aboutus_contact .about-contact-area .about-mail i {
  color: <?php echo $glimmer_color_scheme;?>;
}

.tp_recent_tweets li .twitter_time:before {
    color: <?php echo $glimmer_color_scheme;?>;
}
.tp_recent_tweets li .twitter_time {
    color: <?php echo $glimmer_color_scheme;?>;
}

/* author page */
.about-me .entry-footer h3, .about-me .entry-footer .follow-link a {
  color: <?php echo $glimmer_color_scheme;?>;
}
.about-me #author-skill .skillbar-bar {
  background: <?php echo $glimmer_color_scheme;?>;
}
.about-me #author-skill .skill-bar-percent {
  color: <?php echo $glimmer_color_scheme;?>;
}
.about-me #author-skill .skill-bar-percent:after {
  border-top: 12px solid <?php echo $glimmer_color_scheme;?>;
}

/* Footer */
#footer-top .widget .widget-title::before,
#footer-top .widget .widget-title::after {
  border-color: <?php echo $glimmer_color_scheme;?>;
}

#footer-bottom #scroll-top i, #footer-bottom #scroll-top span {
  background-color: <?php echo $glimmer_color_scheme;?>;
}

.site-footer-bottom {
  background-color: <?php echo $glimmer_color_scheme;?>;
}

</style> 

<?php
} // end glimmer_color_scheme function
glimmer_color_scheme(); // here print the function

function glimmer_chat_post_scheme() { 
  global $glimmer;
  $speaker_one = isset( $glimmer['chat_bg_color']['speaker_one'] ) ? $glimmer['chat_bg_color']['speaker_one'] : '';
  $speaker_two = isset( $glimmer['chat_bg_color']['speaker_two'] ) ? $glimmer['chat_bg_color']['speaker_two'] : '';
  $speaker_three = isset( $glimmer['chat_bg_color']['speaker_three'] ) ? $glimmer['chat_bg_color']['speaker_three'] : '';
  $speaker_four = isset( $glimmer['chat_bg_color']['speaker_four'] ) ? $glimmer['chat_bg_color']['speaker_four'] : '';
  $speaker_five = isset( $glimmer['chat_bg_color']['speaker_five'] ) ? $glimmer['chat_bg_color']['speaker_five'] : '';
  $speaker_six = isset( $glimmer['chat_bg_color']['speaker_six'] ) ? $glimmer['chat_bg_color']['speaker_six'] : '';
?>
<style type="text/css">
  .content-area .format-chat .chat-row cite {
    color: <?php echo $speaker_one; ?>;
  }
  .content-area .format-chat .chat-text {
    background: <?php echo $speaker_one; ?>;
    color: #fff;
  }
  .content-area .format-chat .chat-text:before {
    border-right-color: <?php echo $speaker_one; ?>;
  }
  .content-area .format-chat .chat-speaker-2 .chat-text {
    background: <?php echo $speaker_two; ?>;
    color: #ffffff;
  }
  .content-area .format-chat .chat-speaker-2 .chat-text:before {
    border-right-color: <?php echo $speaker_two; ?>;
  }
  .content-area .format-chat .chat-speaker-2 cite {
    color: <?php echo $speaker_two; ?>;
  }
  .content-area .format-chat .chat-speaker-2.chat-row:nth-child(2n+2) .chat-text:before {
    border-left-color: <?php echo $speaker_two; ?>;
  }
  .content-area .format-chat .chat-speaker-3 .chat-text {
    background: <?php echo $speaker_three; ?>;
    color: #ffffff;
  }
  .content-area .format-chat .chat-speaker-3 .chat-text:before {
    border-right-color: <?php echo $speaker_three; ?>;
  }
  .content-area .format-chat .chat-speaker-3 cite {
    color: <?php echo $speaker_three; ?>;
  }
  .content-area .format-chat .chat-speaker-3.chat-row:nth-child(2n+2) .chat-text:before {
    border-left-color: <?php echo $speaker_three; ?>;
  }
  .content-area .format-chat .chat-speaker-4 .chat-text {
    background: <?php echo $speaker_four; ?>;
    color: #ffffff;
  }
  .content-area .format-chat .chat-speaker-4 .chat-text:before {
    border-right-color: <?php echo $speaker_four; ?>;
  }
  .content-area .format-chat .chat-speaker-4 cite {
    color: <?php echo $speaker_four; ?>;
  }
  .content-area .format-chat .chat-speaker-4.chat-row:nth-child(2n+2) .chat-text:before {
    border-left-color: <?php echo $speaker_four; ?>;
  }
  .content-area .format-chat .chat-speaker-5 .chat-text {
    background: <?php echo $speaker_five; ?>;
    color: #ffffff;
  }
  .content-area .format-chat .chat-speaker-5 .chat-text:before {
    border-right-color: <?php echo $speaker_five; ?>;
  }
  .content-area .format-chat .chat-speaker-5 cite {
    color: <?php echo $speaker_five; ?>;
  }
  .content-area .format-chat .chat-speaker-5.chat-row:nth-child(2n+2) .chat-text:before {
    border-left-color: <?php echo $speaker_five; ?>;
  }
  .content-area .format-chat .chat-speaker-6 .chat-text {
    background: <?php echo $speaker_six; ?>;
    color: #ffffff;
  }
  .content-area .format-chat .chat-speaker-6 .chat-text:before {
    border-right-color: <?php echo $speaker_six; ?>;
  }
  .content-area .format-chat .chat-speaker-6 cite {
    color: <?php echo $speaker_six; ?>;
  }
  .content-area .format-chat .chat-speaker-6.chat-row:nth-child(2n+2) .chat-text:before {
    border-left-color: <?php echo $speaker_six; ?>;
  }
</style>
<?php
} // end glimmer_chat_post_scheme function
glimmer_chat_post_scheme(); // here print the function

