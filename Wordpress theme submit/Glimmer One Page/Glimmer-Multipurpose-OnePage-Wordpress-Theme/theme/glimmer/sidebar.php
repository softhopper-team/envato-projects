<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Glimmer
 */
?>
<!-- sidebar
================================================== -->             
<div class="col-md-4">
    <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
        <aside class="widget widget_search">
            <?php get_search_form(); ?>
        </aside> <!-- /.search-box -->   
   
        <aside class="widget widget_glimmer_popular_posts">
            <h2 class="widget-title"><?php _e( 'Popular Post', 'glimmer' ); ?></h2>
                <?php 
                    $glimmer_popular_post = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
                    while ( $glimmer_popular_post->have_posts() ) : $glimmer_popular_post->the_post();  
                ?>
                <div class="popular-wrap">
                    <?php
                        if ( has_post_thumbnail() ) {
                            ?>
                            <div class="popular-post-thumb">
                                <a href="<?php the_permalink(); ?>">
                                    <figure class="fit-img ripple">
                                        <?php
                                            the_post_thumbnail('popular-post', array( 'class' => "", 'alt' => get_the_title()));
                                        ?>
                                    </figure>
                                </a>
                            </div> <!-- /.popular-post-thumb -->
                            <?php
                        } else {
                        ?>
                            <div class="popular-post-thumb">
                                <a href="<?php the_permalink(); ?>">
                                    <figure class="fit-img ripple">
                                       <img class="img-responsive" src="<?php echo get_stylesheet_directory_uri(); ?>/images/blog/popular-no-thumb.gif" alt="No Thumbnail" />
                                    </figure>
                                </a>
                            </div> <!-- /.popular-post-thumb -->
                        <?php
                        }
                    ?>  

                    <div class="popular-post-detail">
                        <div class="blog-meta">
                            <ul>
                                <li><?php the_time('j M, Y'); ?></li>
                                <li><?php echo wpb_get_post_views(get_the_ID()); ?></li>
                            </ul>       
                        </div> <!-- /.blog-meta -->
                        <div class="popular-description">
                            <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                        </div> <!-- /.popular-description -->
                    </div> <!-- /.popular-post-detail -->
                </div> <!-- /.popular-wrap -->
                <?php
                    endwhile;
                ?>
        </aside> <!-- /.most-populer -->   
                     
        <aside class="widget">
            <h2 class="widget-title"><?php _e( 'Tags', 'glimmer' ); ?></h2>
            <div class="tagcloud">
               <?php wp_tag_cloud(); ?>                                
            </div>            
        </aside> <!-- /.tag -->
     
        <aside class="widget">
            <h2 class="widget-title"><?php _e( 'Archives', 'glimmer' ); ?></h2>
            <ul>
                <?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
            </ul>
        </aside> <!-- /.archives --> 
                                  
    <?php endif; // end sidebar widget area ?>
</div> <!-- /.col-md-4 -->
