(function($) {
    "use strict";

    var josephineApp = {
        /* ---------------------------------------------
         Preloader
         --------------------------------------------- */
        preloader: function() {
            $(window).on('load', function() {
                $("body").imagesLoaded(function() {
                    $('#preloader').delay(500).slideUp('slow', function() {
                        $(this).remove();
                    });
                });
            });

        },

        /* ---------------------------------------------
         Theme configure
         --------------------------------------------- */
        themeconfig: function() {

            function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                    sURLVariables = sPageURL.split('&'),
                    sParameterName,
                    i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };

            var layout = getUrlParameter('layout');

            if ( layout === 'sidebar-left' ) {
                $('#main-layout').removeClass('col-md-10');
                $('#main-layout').removeClass('col-md-offset-1');
                $('#main-layout').addClass('col-md-push-4');
                $('#main-layout').addClass('col-md-8');
                $('#sidebar-layout').show();
                $('#sidebar-layout').addClass('col-md-pull-8');
            }
            else if ( layout === 'sidebar-right' ) {
                $('#main-layout').removeClass('col-md-10');
                $('#main-layout').removeClass('col-md-offset-1');
                $('#main-layout').removeClass('col-md-push-4');
                $('#main-layout').addClass('col-md-8');
                $('#sidebar-layout').show();
                $('#sidebar-layout').removeClass('col-md-pull-8');
            }
            else if ( layout === 'full-width' ) {
                $('#main-layout').removeClass('col-md-8');
                $('#main-layout').removeClass('col-md-push-4');
                $('#main-layout').removeClass('col-md-pull-8');
                $('#main-layout').addClass('col-md-10 col-md-offset-1');
                $('#sidebar-layout').hide();
            }

            else if ( theme_layout === 'sidebar-left' ) {
                $('#main-layout').removeClass('col-md-10');
                $('#main-layout').removeClass('col-md-offset-1');
                $('#main-layout').addClass('col-md-push-4');
                $('#main-layout').addClass('col-md-8');
                $('#sidebar-layout').show();
                $('#sidebar-layout').addClass('col-md-pull-8');
            }
            else if ( theme_layout === 'sidebar-right' ) {
                $('#main-layout').removeClass('col-md-10');
                $('#main-layout').removeClass('col-md-offset-1');
                $('#main-layout').removeClass('col-md-push-4');
                $('#main-layout').addClass('col-md-8');
                $('#sidebar-layout').show();
                $('#sidebar-layout').removeClass('col-md-pull-8');
            }
            else if ( theme_layout === 'full-width' ) {
                $('#main-layout').removeClass('col-md-8');
                $('#main-layout').removeClass('col-md-push-4');
                $('#main-layout').removeClass('col-md-pull-8');
                $('#main-layout').addClass('col-md-10 col-md-offset-1');
                $('#sidebar-layout').hide();
            }
        },

        /* ---------------------------------------------
         Placeholder
         --------------------------------------------- */
        placeholder: function() {
            var $ph = $('input[type="search"], input[type="text"], input[type="email"], textarea');
            $ph.each(function() {
                var value = $(this).val();
                $(this).focus(function() {
                    if ($(this).val() === value) {
                        $(this).val('');
                    }
                });
                $(this).blur(function() {
                    if ($(this).val() === '') {
                        $(this).val(value);
                    }
                });
            });
        },

        /* ---------------------------------------------
         Header Share
         --------------------------------------------- */
        header_share: function() {
            $('.share-button').on('click', function() {
                $('#top-social').animate({
                    width: "toggle"
                }, 250);
            });
            $('#search-button, #mbsearch-button').on('click', function() {
                $('#page-search').fadeIn();
            });
            $('#search-close').on('click', function() {
                $('#page-search').fadeOut();
            });
        },

        /* ---------------------------------------------
         Menu
         --------------------------------------------- */
        menu: function() {
            var items = $('.overlapblackbg, .slideLeft');
            var menucontent = $('.menucontent');

            var menuopen = function() {
                $(items).removeClass('menuclose').addClass('menuopen');
            };
            var menuclose = function() {
                $(items).removeClass('menuopen').addClass('menuclose');
            };

            $('#navToggle').on('click', function() {
                if (menucontent.hasClass('menuopen')) {
                    $(menuclose);
                } else {
                    $(menuopen);
                }
            });
            menucontent.on('click', function() {
                if (menucontent.hasClass('menuopen')) {
                    $(menuclose);
                }
            });
            $('#navToggle,.overlapblackbg').on('click', function() {
                $('.menucontainer').toggleClass("mrginleft");
            });

            $('.menu-list li').has('.menu-submenu').prepend('<span class="menu-click"><i class="menu-arrow fa fa-angle-down"></i></span>');

            $('.menu-mobile').on('click', function() {
                $('.menu-list').slideToggle('slow');
            });
            $('.menu-click').on('click', function() {
                $(this).siblings('.menu-submenu').slideToggle('slow');
                $(this).children('.menu-arrow').toggleClass('menu-rotate');
                $(this).siblings('.menu-submenu-sub').slideToggle('slow');
            });
            $('ul.menu-list li a').each(function() {
                if ($(this).attr('href') + "/" === document.URL || $(this).attr('href') === document.URL) {
                    $(this).addClass('active');
                }
            });
        },

        /* ---------------------------------------------
         smooth scroll
         --------------------------------------------- */
        smoothscroll: function() {
            if (typeof smoothScroll == 'object') {
                smoothScroll.init();
            }
        },
        /* ---------------------------------------------
         Video Fix
         --------------------------------------------- */
        video: function() {
            $(".content-area").fitVids();
        },
        /* ---------------------------------------------
         Related Post
         --------------------------------------------- */
        relatedpost: function() {
            $('#related-post-wrap').ghostRelated({
                limit: 3
            });
        },
        /* ---------------------------------------------
         Post Next
         --------------------------------------------- */
        postnext: function() {
            $('#post_prev_next').find(function() {
                if (!$('.post-previous').length || !$('.post-next').length) {
                    $('#post_prev_next').addClass('no-next-link');
                }
            });
        },
        /* ---------------------------------------------
         Search Header
         --------------------------------------------- */
        header_search: function() {
            $("#dsearch").ghostHunter({
                results: "#dsearch-result",
                onKeyUp: true,
                zeroResultsInfo: true,
                info_template: "<p>Number of posts found: {{amount}}</p>",
                result_template: "<li><a href='{{link}}'><p><h2>{{title}}</h2></a></li>"
            });
        },
        /* ---------------------------------------------
         Gallery Post
         --------------------------------------------- */
        gallary: function() {
            $('.gallery-images').owlCarousel({
                singleItem: true,
                slideSpeed: 400,
                navigation: true,
                pagination: false,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<i class='ico-left-angle'></i>",
                    "<i class='ico-right-angle'></i>"
                ]
            });
        },

        /* ---------------------------------------------
         Scroll Top
         --------------------------------------------- */
        scroll_top: function() {
            $("body").append("<a href='#top' class='topbutton'><span class='glyphicon glyphicon-menu-up'></span></a>");
            $("a[href='#top']").on('click', function() {
                $("html, body").animate({
                    scrollTop: 0
                }, "normal");
                return false;
            });
            $(".topbutton").hide();
            $(window).scroll(function() {
                if ($(this).scrollTop() < 600) {
                    $("a[href='#top']").fadeOut('fast');
                } else {
                    $("a[href='#top']").fadeIn('fast');
                }
            });
        },
        /* ---------------------------------------------
         Maps
         --------------------------------------------- */
        maps: function() {
            //Latitude Callback Option 
            var Map_Latitude = (!Contact_Info['map_latitude'] || Contact_Info['map_latitude'] === "") ? '43.04446' : Contact_Info['map_latitude'];

            //Latitude Callback Option 
            var Map_Longitude = (!Contact_Info['map_longitude'] || Contact_Info['map_longitude'] === "") ? '-76.130791' : Contact_Info['map_longitude'];

            //Map Icon Callback Option 
            var Map_Icon = (!Contact_Info['map_icon'] || Contact_Info['map_icon'] === "") ? 'http://greenarmywky.org/images/map-icon.png' : Contact_Info['map_icon'];

            if ($('#gmaps').length) {
                var map;
                map = new GMaps({
                    el: '#gmaps',
                    lat: Map_Latitude,
                    lng: Map_Longitude,
                    scrollwheel: false,
                    zoom: 10,
                    zoomControl: true,
                    panControl: false,
                    streetViewControl: false,
                    mapTypeControl: false,
                    overviewMapControl: false,
                    clickable: false
                });

                var image = Map_Icon;
                map.addMarker({
                    lat: Map_Latitude,
                    lng: Map_Longitude,
                    icon: image,
                    animation: google.maps.Animation.DROP,
                    verticalAlign: 'bottom',
                    horizontalAlign: 'center'
                });

                var styles = [{
                    "featureType": "road",
                    "stylers": [{
                        "color": "#b4b4b4"
                    }]
                }, {
                    "featureType": "water",
                    "stylers": [{
                        "color": "#d8d8d8"
                    }]
                }, {
                    "featureType": "landscape",
                    "stylers": [{
                        "color": "#f1f1f1"
                    }]
                }, {
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#000000"
                    }]
                }, {
                    "featureType": "poi",
                    "stylers": [{
                        "color": "#d9d9d9"
                    }]
                }, {
                    "elementType": "labels.text",
                    "stylers": [{
                        "saturation": 1
                    }, {
                        "weight": 0.1
                    }, {
                        "color": "#000000"
                    }]
                }];
            }
        },
        /* ---------------------------------------------
         Contact Page
         --------------------------------------------- */
        contact_page: function() {
            //Address Callback Option 
            var full_Address = (!Contact_Info['full_addess'] || Contact_Info['full_addess'] === "") ? 'Creative Agency, Melborn, Australia' : Contact_Info['full_addess'];

            var site_Phone = (!Contact_Info['phone'] || Contact_Info['phone'] === "") ? '' : Contact_Info['phone'];

            var site_Fax = (!Contact_Info['fax'] || Contact_Info['fax'] === "") ? '' : Contact_Info['fax'];

            var site_Email = (!Contact_Info['email'] || Contact_Info['email'] === "") ? '' : Contact_Info['email'];

            var site_Web = (!Contact_Info['web'] || Contact_Info['web'] === "") ? '' : Contact_Info['web'];

            $('#company-address').html(full_Address);
            $('#company-phone').html(site_Phone);
            $('#company-fax').append(site_Fax);
            $('#company-email').append('<a href="mailto:' + site_Email + '">' + site_Email + '</a>');
            $('#company-web').append('<a target="_blank" href="' + site_Web + '">' + site_Web + '</a>');
        },
        /* ---------------------------------------------
         About page Options
         --------------------------------------------- */
        about_page: function() {

            $('.about-me h2.author-name').append( About_Info['author_name'] );
            $('.about-me div.author-sign img').attr('src', About_Info['signature_url'] );
            $('.about-me div.author-sign img, .about-me .author-image img').attr('alt', About_Info['author_name'] );
            $('.about-me div.author-sign h3').append( About_Info['author_name'] );
            $('.about-me .author-image img').attr('src', About_Info['profile_pic_url'] );

            //This code for skillbar
            for (var percent in About_Skills) {

                $('#author-skill div.row').append('<div class="col-sm-4 pd-bottom"> <div class="skill-area"> <div class="percent-area"> <div class="skillbar" data-percent="'+About_Skills[percent]+'%"> <h5 class="skillbar-title">'+ percent +'</h5> <div class="skillbar-bg"> <div class="skillbar-bar"></div> </div> <div class="skill-bar-percent"></div> </div></div></div></div> ');
            }

            //This code for social Profile
            for (var Url in About_social) {
                $('#about-follow-social').append("<a href='" + About_social[Url] + "'><i class='fa fa-" + Url + "'></i></a>");
            }

            // This code to active Skillbar
            if ($('.skillbar').length) {
                var $skill = $('.skillbar');
                $skill.appear(function() {
                    $(this).find('.skillbar-bar, .skill-bar-shape').animate({
                        width: $(this).attr('data-percent')
                    }, 1000);
                });
            }

            $('.percent-area .skill-bar-percent').css('left', function() {
                return $(this).parent().data('percent')
            });

            $('.percent-area .skill-bar-percent').append(function() {
                return $(this).parent().data('percent')
            });
        },
        /* ---------------------------------------------
         Instafeed Jquery
         --------------------------------------------- */
        instafeed: function() {
            var imageLimit = Instagram_Widget['image_limit'];
            var feedId = Instagram_Widget['user_id'];
            var feedToken = Instagram_Widget['access_token'];
            var userFeed = new Instafeed({
                limit: imageLimit,
                get: 'user',
                userId: feedId,
                accessToken: '' + feedToken + '',
                resolution: 'standard_resolution',
                template: '<div class="list"><a target="_blank" href="{{link}}"><img src="{{image}}" /></a></div>'
            });
            userFeed.run();
        },
        /* ---------------------------------------------
         Flicker widget
         --------------------------------------------- */
        flickr_widget: function() {
            var josephine_flickr_Id = Footer_Flickr_Widget['flickr_id'];
            var josephine_photo_count = Footer_Flickr_Widget['photo_count'];

            $('#josephine_flickr').jflickrfeed({
                    limit: josephine_photo_count,
                    qstrings: {
                        id: josephine_flickr_Id
                    },
                    itemTemplate: '<li><a href="{{link}}"><img src="{{image_t}}" alt="{{title}}" /></a></li>'
                },
                function(data) {
                    $('#josephine_flickr li').hover(function() {
                            $(this).children('div').show();
                        },

                        function() {
                            $(this).children('div').hide();
                        });
                });
        },
        /* ---------------------------------------------
         Mailchip widget
         --------------------------------------------- */
        mailchimp_widget: function() {
            var josephine_success_btn_text = Footer_Mailchimp_Widget['success_btn_text'];
            $('#newsletter-form').formchimp({
                'buttonText': "" + josephine_success_btn_text + "",
            });
        },
        /* ---------------------------------------------
         Syntax height-lighter
         --------------------------------------------- */
        highlighter: function() {
            $('pre code').each(function(i, block) {
                hljs.highlightBlock(block);
            });
        },
        /* ---------------------------------------------
         Comments count
         --------------------------------------------- */
        comment_count: function() {
            var s = document.createElement('script');
            s.async = true;
            s.type = 'text/javascript';
            s.src = '//' + disqus_shortname + '.disqus.com/count.js';
            (document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);

        },

        josephine_initializ: function() {
            josephineApp.preloader();
            josephineApp.themeconfig();
            josephineApp.placeholder();
            josephineApp.header_share();
            josephineApp.menu();
            josephineApp.smoothscroll();
            josephineApp.gallary();
            josephineApp.video();
            josephineApp.relatedpost();
            josephineApp.postnext();
            josephineApp.scroll_top();
            josephineApp.maps();
            josephineApp.instafeed();
            josephineApp.flickr_widget();
            josephineApp.mailchimp_widget();
            josephineApp.contact_page();
            josephineApp.about_page();
            josephineApp.header_search();
            josephineApp.highlighter();
            josephineApp.comment_count();
        }
    };

    /* === document ready function === */
    $(document).ready(function() {
        josephineApp.josephine_initializ();
    });

})(jQuery);