<?php
/*
	Glimmer Shortcode Module for Glimmer themes
	Version:  1.0.0
*/

if ( is_admin() ) {
	/*-----------------------------------------------------------------------------------*/
	# Register main Scripts and Styles
	/*-----------------------------------------------------------------------------------*/
	function glimmer_admin_register() {

		wp_enqueue_script( 'shortcodes-js', THEME_URL . '/inc/shortcodes/js/shortcodes.js', array( 'jquery' ) , false , false );  

			$glimmer_lang = array( 				
				"shortcode_glimmerlabs"			=> __( 'Glimmer Shortcodes', 'glimmer' ), 
				"shortcode_right"			=> __( 'Right', 'glimmer' ), 
				"shortcode_left"			=> __( 'Left', 'glimmer' ), 
				"shortcode_top"				=> __( 'Top', 'glimmer' ), 
				"shortcode_bottom"			=> __( 'Bottom', 'glimmer' ), 
				"shortcode_center"			=> __( 'Center', 'glimmer' ), 
				"shortcode_width"			=> __( 'Width', 'glimmer' ), 
				"shortcode_content"			=> __( 'Content', 'glimmer' ), 
				"shortcode_button"			=> __( 'Button', 'glimmer' ), 
				"shortcode_type"			=> __( 'Type', 'glimmer' ), 
				"shortcode_default"			=> __( 'Default', 'glimmer' ), 
				"shortcode_primary"			=> __( 'Primary', 'glimmer' ), 
				"shortcode_success"			=> __( 'Success', 'glimmer' ), 
				"shortcode_info"			=> __( 'Info', 'glimmer' ), 
				"shortcode_warning"			=> __( 'Warning', 'glimmer' ), 
				"shortcode_danger"			=> __( 'Danger', 'glimmer' ),
				"shortcode_size"			=> __( 'Size', 'glimmer' ),
				"shortcode_size_large"		=> __( 'Large', 'glimmer' ), 
				"shortcode_size_default" 	=> __( 'Default', 'glimmer' ), 
				"shortcode_size_small"		=> __( 'Small', 'glimmer' ), 
				"shortcode_size_ex_small"	=> __( 'Extra Small', 'glimmer' ), 
				"shortcode_progressbar"		=> __( 'Progress Bar', 'glimmer' ), 
				"shortcode_link"			=> __( 'Link', 'glimmer' ), 
				"shortcode_text"			=> __( 'Text', 'glimmer' ), 
				"shortcode_title"			=> __( 'Title', 'glimmer' ), 
				"shortcode_state"			=> __( 'State', 'glimmer' ), 
				"shortcode_status"			=> __( 'Status', 'glimmer' ), 
				"shortcode_url"				=> __( 'URL', 'glimmer' ), 
				"shortcode_background_img"	=> __( 'Background Image', 'glimmer' ), 
				"shortcode_opened"			=> __( 'Opened', 'glimmer' ), 
				"shortcode_closed"			=> __( 'Closed', 'glimmer' ), 
				"shortcode_true"			=> __( 'True', 'glimmer' ), 
				"shortcode_false"			=> __( 'False', 'glimmer' ), 
				"shortcode_alert"			=> __( 'Alert', 'glimmer' ), 
				"shortcode_dismiss"			=> __( 'Dismiss', 'glimmer' ), 
				"shortcode_striped"			=> __( 'Striped', 'glimmer' ), 
				"shortcode_animation"		=> __( 'Animation', 'glimmer' ), 
				"shortcode_height"			=> __( 'Height:', 'glimmer' ), 
				"shortcode_video"			=> __( 'Video', 'glimmer' ), 
				"shortcode_video_url"		=> __( 'Video URL:', 'glimmer' ), 
				"shortcode_audio"			=> __( 'Audio', 'glimmer' ), 
				"shortcode_mp3"				=> __( 'MP3 file URL', 'glimmer' ), 
				"shortcode_m4a"				=> __( 'M4A file URL', 'glimmer' ), 
				"shortcode_ogg"				=> __( 'OGG file URL', 'glimmer' ), 
				"shortcode_tooltip"			=> __( 'ToolTip', 'glimmer' ), 
				"shortcode_direction"		=> __( 'Direction', 'glimmer' ), 
				"shortcode_facebook"		=> __( 'Facebook', 'glimmer' ), 
				"shortcode_tweeter"			=> __( 'Tweeter', 'glimmer' ), 
				"shortcode_google_plus"		=> __( 'Google Plus', 'glimmer' ),
				"shortcode_dropcap"			=> __( 'Dropcap', 'glimmer' ),
				"shortcode_columns"			=> __( 'Columns', 'glimmer' ),
				"shortcode_accordion"		=> __( 'Accordion', 'glimmer' ),
				"shortcode_tab"				=> __( 'Tab', 'glimmer' ),
 			);		
			wp_localize_script( 'shortcodes-js', 'glimmerLang', $glimmer_lang );	
		}
	add_action( 'admin_enqueue_scripts', 'glimmer_admin_register' ); 
} // end is_admin()
 
add_action('admin_head', 'glimmer_add_mce_button');
function glimmer_add_mce_button() {
	if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
		return;
	}
	if ( 'true' == get_user_option( 'rich_editing' ) ) {
		add_filter( 'mce_external_plugins', 'glimmer_add_tinymce_plugin' );
		add_filter( 'mce_buttons', 'glimmer_register_mce_button' );
	}
}

// Declare script for new button
function glimmer_add_tinymce_plugin( $plugin_array ) {
	$plugin_array['glimmer_mce_button'] = get_template_directory_uri().'/inc/shortcodes/mce.js';
	return $plugin_array;
}

// Register new button in the editor
function glimmer_register_mce_button( $buttons ) {
	array_push( $buttons, 'glimmer_mce_button' );
	return $buttons;
}


// status shortcode for facebook and twitter
add_shortcode("status", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'url' => '',
			'background' => '',
		), $atts
	);
	extract($atts);

	( !empty($background) ) ? $background = $background : $background = "";
	$status_facebook = ( $type == "facebook" ) ? "facebook" : "";
	$status_twitter = ( $type == "twitter" ) ? "twitter" : "";
	$status_gplus = ( $type == "google_plus" ) ? "google_plus" : "";

	if( !empty( $status_facebook ) || !empty( $status_twitter ) || !empty( $status_gplus ) ):	?>
		<div class="post-status-wrapper" style="background: url(<?php echo $background; ?>);">
		<?php if( !empty( $status_facebook ) ) : ?>
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;  js = d.createElement(s);
				js.id = id;  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3";
				fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-post" data-href="<?php echo $url ?>"></div>
		<?php elseif( !empty( $status_twitter ) ) : ?>
			<blockquote class="twitter-tweet"><a href="<?php echo $url ?>"></a></blockquote>
			<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
		<?php elseif( !empty( $status_gplus ) ) : ?>
			<script type="text/javascript" src="//apis.google.com/js/plusone.js"></script>
			<div class="g-post" data-href="<?php echo $url ?>"></div>
		<?php endif; ?>
		</div><!-- /.post-status-wrapper -->
	<?php endif; 
	$status = ob_get_clean();
	return $status;
});

// bootstrap column grid row shortcode
add_shortcode("row", function($atts, $content = null) {
	return '<div class="row">' . do_shortcode( $content ) . '</div>';
});

// bootstrap column grid shortcode
add_shortcode("column", function($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'number' => '',
			'offset' => '',
		), $atts
	);
	extract($atts);
	if ( !empty($offset) ) {
		$offset = "col-md-offset-$offset";
	}
	return "<div class='$offset col-md-$number'>". do_shortcode( $content ) ."</div>";
});

// bootstrap progress bar shortcode 
add_shortcode("progress_bar", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => '',
			'width' => '50',
			'title' => '',
			'striped' => false,
			'animation' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " progress-bar-$type " : $type = "";
	( $striped == true ) ? $striped = " progress-bar-striped " : $striped = "";
	( $animation == true ) ? $animation = " active " : $animation = "";
	?>

	<div class="progress">
		<div class="progress-bar <?php echo $type.$striped.$animation;?>" role="progressbar" aria-valuenow="<?php echo $width; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $width; ?>%">
	    <?php echo $title; ?>
	  	</div> <!-- /.progress-bar -->
	</div> <!-- /.progress -->

	<?php	
	$progress_bar = ob_get_clean();
	return $progress_bar;
});

// bootstrap button shortcode 
add_shortcode("button", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'type' => 'primary',
			'text' => '',
			'size' => '',
			'disabled' => false,
		), $atts
	);
	extract($atts);

	( !empty($type) ) ? $type = " btn-$type " : $type = "";
	( $disabled == true ) ? $disabled = " disabled='true' " : $disabled = "";
	( $text == null ) ? $text = __(" Button ", "glimmer") : $text = $text;
	?>

	<button type="button" class="btn <?php echo $type." ".$size; ?>" <?php echo $disabled; ?>>
	<?php echo $text; ?>
	</button>

	<?php	
	$button = ob_get_clean();
	return $button;
});


// bootstrap accordion shortcode collapse_group
add_shortcode("collapse_group", function($atts, $content = null) {
	return '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">' . do_shortcode( $content ) . '</div>';
});

// bootstrap accordion shortcode collapse 
add_shortcode("collapse", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'id' => '',
			'title' => '',
			'expanded' => "false",
		), $atts
	);
	extract($atts);
	( $expanded == "true" ) ? $aria_expander = "true" : $aria_expander = "false";
	( $expanded == "true" ) ? $expanded_two = "" : $expanded_two = "collapsed";
	( $expanded == "true" ) ? $expanded_in = "in" : $expanded_in = "";
	?>
	<div class="panel panel-default">
		<div class="panel-heading" role="tab">
		  <h4 class="panel-title">
		    <a class="accordion-toggle <?php echo $expanded_two; ?>" role="button" data-toggle="collapse" href="#<?php  echo $id; ?>" aria-expanded="<?php echo $aria_expander; ?>" >
		    <?php  echo $title; ?>
		    </a>
		  </h4>
		</div>
		<div id="<?php echo $id; ?>" class="panel-collapse collapse <?php echo $expanded_in; ?>" role="tabpanel"  aria-expanded="<?php echo $aria_expander; ?>">
		  <div class="panel-body">
		  <?php  echo $content; ?>
		  </div>
		</div>
	</div>
	<?php	
	$collapse = ob_get_clean();
	return $collapse;
});

// bootstrap shortcode alert 
add_shortcode("alert", function($atts, $content = null) {
	ob_start();	
	$atts = shortcode_atts(
		array(
			'type' => '',
			'dismiss' => false,
		), $atts
	);
	extract($atts);
	?>
	<div class="alert alert-<?php echo $type;?> " role="alert">
	<?php
	  if ( $dismiss == true ) {
	  	?>
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	  	<?php
	  }
	  echo $content;
	?>
	</div><!-- /.alert -->
	<?php	
	$alert = ob_get_clean();
	return $alert;
});

// bootstrap shortcode tooltip 
add_shortcode("tooltip", function($atts, $content = null) {
	ob_start();	

	$atts = shortcode_atts(
		array(
			'text' => '',
			'direction' => "top",
		), $atts
	);
	extract($atts);
	?>
	<span data-toggle="tooltip" title="<?php echo $text; ?>" data-placement="<?php echo $direction; ?>"><?php echo $content; ?></span>

	<?php	
	$tooltip = ob_get_clean();
	return $tooltip;
});

// bootstrap shortcode dropcap 
add_shortcode("dropcap", function($atts, $content = null) {
	ob_start();	
	?>
	<span class="dropcap"><?php echo $content; ?></span>
	<?php	
	$dropcap = ob_get_clean();
	return $dropcap;
});

// bootstrap tabs shortcode 
$tabs_divs = '';

function tabs_group($atts, $content = null ) {
    global $tabs_divs;

    $tabs_divs = '';

    $output = '<div id="tab-side-container"><ul class="nav nav-tabs"';
    $output.='>'.do_shortcode($content).'</ul>';
    $output.= '<div class="tab-content">'.$tabs_divs.'</div></div>';

    return $output;  
}  


function tab($atts, $content = null) {  
    global $tabs_divs;

    extract(shortcode_atts(array(  
        'id' => '',
        'title' => '', 
        'active' => '', 
    ), $atts));  

    if(empty($id))
        $id = 'side-tab'.rand(100,999);

    $output = '
        <li class="'.$active.'">
            <a href="#'.$id.'" data-toggle="tab">'.$title.'</a>
        </li>
    ';

    $tabs_divs.= '<div class="tab-pane '.$active.'" id="'.$id.'">'.$content.'</div>';

    return $output;
}

add_shortcode('tabs', 'tabs_group');
add_shortcode('tab', 'tab');