<?php
class Glimmer_About_Me extends WP_Widget {

    function __construct() {
        $params = array (
            'description' => 'Glimmer : About Me Info',
            'name' => 'Glimmer : About Me'
        );
        parent::__construct('Glimmer_About_Me','',$params);
    }

    /** @see WP_Widget::form */
    public function form( $instance) {
        extract($instance);
        ?>        
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>"
                value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
        </p> 
        <p>
           <label class="ad-img-lebel"><?php _e('Image:','glimmer'); ?></label>
       
        <?php
          $arg = array(       
            'parent_div_class'=> 'custom-image-upload',                    
            'field_name' => $this->get_field_name('image'),
            'field_id' => 'upload_logo',
            'field_class' => 'upload_image_field',
            
            'upload_button_id' => 'upload_logo_button',
            'upload_button_class' => 'upload_logo_button',
            'upload_button_text' => 'Upload',
            
            'remove_button_id' => 'remove_logo',
            'remove_button_class' => 'remove_logo_button',
            'remove_button_text' => 'Remove'            
            );
            if ( empty($image) ) $image = NULL;
           glimmer_add_about_me_media_custom($arg,false,$image);
        ?>
        </p> 

        <p>
            <label for="<?php echo $this->get_field_id('name'); ?>"><?php _e('Name:','glimmer'); ?></label>
            <input
                class="widefat"
                type="text"
                id="<?php echo $this->get_field_id('name'); ?>"
                name="<?php echo $this->get_field_name('name'); ?>"
                value="<?php if( isset($name) ) echo esc_attr($name); ?>" />
        </p>
       
        <p>
            <label for="<?php echo $this->get_field_id('description'); ?>"><?php _e('Description:','glimmer'); ?></label>
            <textarea 
                class="widefat" 
                rows="6" 
                cols="20" 
                id="<?php echo $this->get_field_id('description'); ?>" 
                name="<?php echo $this->get_field_name('description'); ?>"><?php if( isset($description) ) echo esc_attr($description); ?></textarea>
        </p> 
       
        <p>            
            <label for="<?php echo $this->get_field_id('aboutme_page_url'); ?>"><?php _e('Show About Me Page URL:','glimmer'); ?></label>
            &nbsp;&nbsp;<input class="checkbox" <?php if ( empty($aboutme_page_url) ) $aboutme_page_url = 0; checked( $aboutme_page_url, 1 ); ?>  value="1" type="checkbox" id="<?php echo $this->get_field_id('aboutme_page_url'); ?>" name="<?php echo $this->get_field_name('aboutme_page_url'); ?>"> 
        </p>
       
      <?php       
    } // end form function

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['image'] = strip_tags( $new_instance['image'] );
        $instance['name'] = strip_tags( $new_instance['name'] );
        $instance['description'] = strip_tags( $new_instance['description'] );
        $instance['aboutme_page_url'] = strip_tags( $new_instance['aboutme_page_url'] );
     
        return $instance;
    }

    public function widget($args, $instance) {
        extract($args);
        extract($instance);
        $title = apply_filters('widget_title',$title);
        $name = apply_filters('widget_name',$name);
        $description = apply_filters('widget_description',$description);
        $image = apply_filters('widget_image',$image);
        
       
        echo $before_widget;
            if ( !empty( $title ) ) {
                echo $before_title . $title . $after_title;
            }
            ?>
            <div class="about-widget">
                <div class="author-image-home">
                    
                    <?php 
                        if ( !empty( $image ) ) {
                            echo "<img class='img-responsive' src='$image' alt='Author Image' />";
                        }
                    ?>

                </div> <!-- /.author-image-home -->

                <div class="about-description">
                    <?php 
                        if ( !empty( $name ) ) {
                            echo "<h4 class='author-name'>$name</h4>";
                        }

                        if ( !empty( $description ) ) {
                            echo "<p>$description</p>";
                        }
                    ?>
                    <?php 
                        //get aboutme page template url by template slug
                        $pages = get_pages(array(
                            'meta_key' => '_wp_page_template',
                            'meta_value' => 'aboutme-page.php'
                        ));
                        $aboutmepage_url = '';
                        foreach($pages as $page){
                            $aboutmepage_url = $page->ID;
                        }
                        ( !empty($aboutmepage_url) ) ? $aboutmepage_url  = get_permalink( $aboutmepage_url ) : $aboutmepage_url = "#";
                        if ( !empty( $aboutme_page_url ) ) {
                            echo "<a class='more-link' href='".$aboutmepage_url."'>".__('Continue','glimmer')."</a>";
                        }
                    ?>

                </div> <!-- /.about-description -->  
            </div> <!-- /.about-widget -->   
                
            <?php
        echo $after_widget;
    } // end widget function
    

} // class Cycle Widget

?>
<?php function glimmer_add_about_me_media_custom( $arg, $use_custom_buttons = false, $value = "" ){
    
    $defaults = array(
        'useid' => false ,
        'hidden' => true,
        
        'parent_div_class'=> 'custom-image-upload',
        
        'field_label' => 'upload_image_field_label',        
        'field_name' => 'upload_image_field',
        'field_id' => 'upload_image_field',
        'field_class' => 'upload_image_field',
        
        'upload_button_id' => 'upload_logo_button',
        'upload_button_class' => 'upload_logo_button',
        'upload_button_text' => 'Upload',
        
        'remove_button_id' => 'remove_logo_button',
        'remove_button_class' => 'remove_logo_button',
        'remove_button_text' => 'Remove',
        
        'preview_div_class' => 'preview',
        'preview_div_class2' => 'preview remove_box',
        'preview_div_id' => 'preview',
        
        'height' => '100',
        'width' => '100'
                    );
        $arguments = wp_parse_args($arg,$defaults);
        
        extract($arguments);
        wp_enqueue_media();
    ?>                                   
   <?php if( ! $use_custom_buttons ): ?>
   <div class="<?php echo $parent_div_class; ?>" id="<?php echo $parent_div_class; ?>">
   
        <input name="<?php echo $field_name; ?>" id="<?php echo $field_id; ?>" class="<?php echo $field_class; ?>" <?php if($hidden): ?>  type="hidden" <?php else: ?> type="text" <?php endif; ?> value="<?php if ( $value != "") { echo stripslashes($value); }  ?>" />
        
        <input type="button" class="button button-primary <?php echo $upload_button_class; ?>" id="<?php echo $upload_button_id; ?>"  value="<?php echo $upload_button_text; ?>">
        
        <input type="button" class="button button-primary <?php echo $remove_button_class; ?>" id="<?php echo $remove_button_id; ?>" <?php  if ( $value == "") {  ?> disabled="disabled" <?php } ?> value="<?php echo $remove_button_text; ?>">
        
        <div class="<?php echo $preview_div_class; ?>" style="float: none; <?php  if ( $value == "") { ?> display: none; <?php } ?>">
            <img src="<?php  echo stripslashes($value);  ?>" style="margin: 10px;" width="150" height="100" alt="">
        </div>   
        <div style="clear: both;"></div>
    </div>
   <?php endif; ?>
    <?php
        $usesep = ($useid) ? "#" : ".";
        if($useid):
        
         $field_class = $field_id;
         $upload_button_class = $upload_button_id;
         $remove_button_class = $remove_button_id;
         $preview_div_class = $preview_div_id;
            
        endif;  
    ?>
    <script type="text/javascript">

    jQuery(document).ready(function($){
        $('<?php echo $usesep.$remove_button_class; ?>').on('click', function(e) {
            <?php if(!$useid): ?>
           $(this).parent().find("<?php echo $usesep.$field_class; ?>").val(""); 
           $(this).parent().find("<?php echo $usesep.$preview_div_class; ?> img").attr("src","").fadeOut("fast");
           <?php else: ?>
           $("<?php echo $usesep.$field_class; ?>").val(""); 
           $("<?php echo $usesep.$preview_div_class; ?> img").attr("src","").fadeOut("fast");
           <?php endif; ?>
           $(this).attr("disabled","disabled");
         return false;   
        });
        var _custom_media = true,
          _orig_send_attachment = wp.media.editor.send.attachment;

      $('<?php echo $usesep.$upload_button_class; ?>').on('click', function(e) {
        var send_attachment_bkp = wp.media.editor.send.attachment;
        var button = $(this);
        var id = button.attr('id').replace('_button', '');
        _custom_media = true;
        wp.media.editor.send.attachment = function(props, attachment){
          if ( _custom_media ) {
              
              <?php if(!$useid): ?>
            button.parent().find("<?php echo $usesep.$field_class; ?>").val(attachment.url);
            button.parent().find("<?php echo $usesep.$preview_div_class; ?> img").attr("src",attachment.url).fadeIn("fast");
            button.parent().find("<?php echo $usesep.$remove_button_class; ?>").removeAttr("disabled");
            if($('.preview img').length > 0){ $('.preview').css('display','block'); };
            <?php else: ?>
            $("<?php echo $usesep.$field_class; ?>").val(attachment.url);
            $("<?php echo $usesep.$preview_div_class; ?> img").attr("src",attachment.url).fadeIn("fast");        
            $("<?php echo $usesep.$remove_button_class; ?>").removeAttr("disabled");
            <?php endif; ?>
          } else {
            return _orig_send_attachment.apply( this, [props, attachment] );
          };
          $('.preview').removeClass('remove_box');
        }

        wp.media.editor.open(button);
        return false;
      });
    });        
    </script>
   <?php  
}