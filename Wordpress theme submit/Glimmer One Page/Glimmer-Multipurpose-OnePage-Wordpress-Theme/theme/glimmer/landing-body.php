<?php
/**
 * The template for displaying landing page.
 *
 * @package glimmer
 */
	global $glimmer;
	$sections = $glimmer['glimmer_sections_order'];
	if (!is_array($sections)) {
	    $sections = glimmer_get_enabled_sections();
	}
	if (empty($sections)) {
	    $sections = glimmer_get_all_sections();
	}
	foreach ($sections as $section_id => $name) {
	    $sections_id = str_replace("_", "-", $section_id);
	    $sectionss_id = str_replace("-", "_", $sections_id);
	    if (file_exists(__DIR__ . "/sections/{$sections_id}.php")){
			if ($glimmer["{$sectionss_id}_display"]) {
	       		get_template_part("sections/{$sections_id}");				
			}
	    }
	}
?>
