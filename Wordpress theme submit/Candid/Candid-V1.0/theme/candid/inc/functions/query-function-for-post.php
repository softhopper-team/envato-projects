<?php

function candid_get_custom_posts($type, $limit = 20, $order = "DESC")
{
    //wp_reset_postdata();
    $args = array(
        "posts_per_page" => $limit,
        "post_type" => $type,
        "orderby" => "ID",
        "order" => $order,
    );
    $custom_posts = get_posts($args);
    return $custom_posts;
}

function candid_get_custom_posts_with_offset($type, $limit = 20, $offset, $order = "DESC")
{
    //wp_reset_postdata();
    $args = array(
        "posts_per_page" => $limit,
        "offset"=> $offset,
        "post_type" => $type,
        "orderby" => "ID",
        "order" => $order,
    );
    $custom_posts = get_posts($args);
    return $custom_posts;
}

function candid_get_custom_posts_by_custom_order($type, $limit = 20, $meta_order_key = "_candid_section_order", $order = "ASC")
{
    $args = array(
        "posts_per_page" => $limit,
        "post_type" => $type,
        "orderby" => "meta_value_num",
        "order" => "ASC",
        "meta_key" => $meta_order_key
    );
    $custom_posts = get_posts($args);
    return $custom_posts;
}


function candid_get_blog_link(){
    $blog_post = get_option("page_for_posts");
    if($blog_post){
        $permalink = get_permalink($blog_post);
    }
    else
        $permalink = site_url();
    return $permalink;
}

