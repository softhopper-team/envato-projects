<?php
/**
 * Template Name: Contact Page
 */
?>
<?php 
    get_header();
    global $candid;
?>
<!-- Content
================================================== -->
<main id="content" class="contact-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-title">
                    <h2 class="section-title"><?php the_title(); ?></h2>
                    <div class="ex-small-border"></div>  
                </div> <!-- /.header-title -->

                <!-- main section -->
                <div id="main">
                    
                    <article class="post">

                        <div class="row">
                            <div class="col-md-12">
                                <div id="gmaps"></div>
                            </div>
                            <div class="col-md-6">
                                <div class="entry-content">
                                    <?php 
                                        if ( isset ( $candid['contact_description'] ) ) echo $candid['contact_description'];
                                    ?>
                                </div> 
                            </div>
                            <div class="col-md-4 col-md-offset-1">
                                <div class="address">
                                    <?php 
                                        if ( isset ( $candid['contact_address'] ) ) echo $candid['contact_address'];
                                    ?>   
                                </div>  
                            </div>

                            <div class="col-md-12">
                                <div class="contact-respond" id="respond">                            
                                    <form name="contactForm" id='contact_form' method="post" action="<?php echo get_template_directory_uri(); ?>/inc/frontend/sendmail.php">

                                        <div class="row">
                                            <div class="col-md-4 padding-right">
                                                <p>
                                                    <input type='text' name='name' id='name' class="form-controlar"  aria-required="true" placeholder="Name*">
                                                </p>
                                            </div>
                                            <div class="col-md-4 padding-left-right">
                                                <p>
                                                    <input type='text' name='email' id='email' class="form-controlar"  aria-required="true" placeholder="Email*">
                                                </p>
                                            </div>
                                            <div class="col-md-4 padding-left">
                                                <p>
                                                    <input type='text' name='subject' id='subject' class="form-controlar"  aria-required="true" placeholder="Subject*">
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <p>
                                                    <textarea name='message' id='message' class="form-controlar" aria-required="true" rows="8" cols="45" placeholder="Write a Comment...."></textarea>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                                <p class="form-submit">
                                                    <input type="submit" value="Submit" id="submit" name="submit">
                                                </p>
                                            </div>
                                        </div><!-- /.row -->
                                    </form>         
                                                    
                                </div><!-- #respond --> 
                            </div> <!-- /.col-md-12 -->
                        </div> <!-- /.row -->
                    </article>
                </div> <!-- /#main -->
            </div> <!-- /.col-md-12 --> 
        </div> <!-- /.row --> 
    </div> <!-- /.container -->  
</main> <!-- /#content -->
<?php get_footer(); ?>