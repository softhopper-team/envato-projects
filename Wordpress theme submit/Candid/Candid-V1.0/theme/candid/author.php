<?php 
/**
 * The template for displaying author page.
 *
 * @package Candid
 */
get_header(); ?>
<!-- Content
================================================== -->
<?php
    /* Show box or wide layout with user condition*/
    $layout = '';
    if ( $candid['layout'] == 1 ) {
        $layout = 'box';
    }
?>
<main id="content" class="<?php echo $layout; ?>">
    <div class="container">
        <div class="row">
            <?php
                /* Show sidebar with user condition*/
                $columns_grid = 8;
                if ( $candid['sidebar_layout'] == 2 ) {
                    get_sidebar();
                } elseif ( $candid['sidebar_layout'] == 1 ) {
                    $columns_grid = 12;
                }
            ?>
            <div class="col-md-<?php echo $columns_grid; ?>">
                <!-- main section -->
                <div id="main">

                    <?php get_template_part( 'template-parts/content', 'authorinfo' ); ?>

                    <?php if ( have_posts() ) : ?>

                    <?php /* Start the Loop */ ?>
                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'template-parts/content', get_post_format() );
                        ?>

                    <?php endwhile; ?>

                    <?php candid_posts_pagination_nav(); ?>

                    <?php else : ?>

                        <?php get_template_part( 'template-parts/content', 'none' ); ?>

                    <?php endif; ?>

                </div> <!-- /#main -->

            </div> <!-- /.col-md-8 -->

            <?php
                /* Show sidebar with user condition*/
                $meta = get_post_meta( $post->ID );
                if ( $candid['sidebar_layout'] == 3 ) {
                    get_sidebar();
                }
            ?>
             
        </div> <!-- /.row -->   
    </div> <!-- /.container -->  
</main> <!-- /#content -->
<?php get_footer(); ?>