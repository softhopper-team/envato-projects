<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="sticky-icon"><i class="fa fa-thumb-tack"></i></div>';
    	}
    ?>
	
	<header class="entry-header">
        <?php glimmer_entry_header(); ?>
    </header> <!-- /.entry-header -->

    <div class="post-media">
		<?php glimmer_vedio(); ?>
	</div>

	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
				?>
				<?php
					if ( has_tag() ) :
					?>
					<div class="tag clearfix">
					    <span class="tags"><?php _e('Tagged In:', 'glimmer'); ?></span>
	                    <?php 
				        echo get_the_tag_list("", "", "");
				        ?>
					</div> <!-- /.tag -->
					<?php
					endif;
				?>
				<?php
			} else {
				the_content(); 
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
	<?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->