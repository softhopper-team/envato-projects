<?php
/**
 * The template for displaying all posts.
 *
 * @package Glimmer
 */
    global $glimmer, $post;
?>
<?php get_template_part("blog","header");?>
        <!-- blog list
        ================================================== -->
        <div class="blog-list">
            <div class="container">
               
                <div class="row base-padding">
                    <div class="col-md-8">    
                        <?php
                            if ( is_author() ) {
                                get_template_part( 'partials/content', 'author_page_info' ); 
                            }
                        ?>                    
                        <?php
                            if ( have_posts() ) :
                                while ( have_posts() ) : the_post();                                
                        ?>
                        <?php
                            /* Include the Post-Format-specific template for the content.
                             * If you want to override this in a child theme, then include a file
                             * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                             */
                            get_template_part( 'content', get_post_format() );
                        ?>
                        <?php endwhile; ?>

                        <?php glimmer_pagination_nav(); ?>
                        
                        <?php else : ?>

                            <?php get_template_part( 'content', 'none' ); ?>

                        <?php endif; ?>
                            
                    </div> <!-- /.col-md-8 -->

                    <?php get_sidebar(); ?>

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.blog-list -->
    
<?php get_template_part("blog", "footer"); ?>