<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Glimmer
 */
?>
<?php get_header(); ?>
<?php get_template_part( 'blog', 'header' ); ?>
 <section class="head-container">
    <div class="container">
        <div class="error-head">
            <div class="row">
                <div class="col-md-3 col-md-offset-3">
                    <h2><?php _e('Error 404','glimmer'); ?></h2>
                    <div>                        
                        <p><a href="<?php echo glimmer_get_blog_link(); ?>"><?php _e('Home','glimmer'); ?></a> <?php _e('Error 404','glimmer'); ?></p>
                    </div>
                </div> <!-- /.col-md-3 -->

                <div class="col-md-4 col-md-offset-1">
                        <?php get_search_form(); ?>
                </div> <!-- /.col-lg-4 -->
            </div> <!-- /.row -->                
        </div> <!-- /.error-head -->
    </div> <!-- /.container -->
</section> <!-- /.head-container -->

<section class="error-body">
    <div class="container">
       <div class="row">
           <div class="col-lg-12">
                <div class="error-content">
                    <h2><?php if(isset($glimmer['settings_404_heading'])) echo $glimmer['settings_404_heading'];?></h2>
                    <div class="error">
                        <?php if(isset($glimmer['settings_404_text'])) echo $glimmer['settings_404_text'];?>   
                    </div>
                    <h5><?php if(isset($glimmer['settings_404_subheading'])) echo $glimmer['settings_404_subheading'];?></h5>                        
                </div> <!-- /.content -->
           </div> <!-- /.col-lg-12 -->
       </div> <!-- /.row -->
    </div> <!-- /.container -->  
</section> <!-- /.error-body -->
<?php get_template_part( 'partials/content', 'footer' ); ?>
<?php get_footer(); ?>
