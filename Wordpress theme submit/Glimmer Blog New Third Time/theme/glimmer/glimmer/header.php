<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Glimmer
 */
?>
<?php
    global $glimmer;
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js" >
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
        <?php        
        // Site Preloader
        $preloader = "";
        ( isset($_GET["site_preloader"]) ) ? $site_preloader = $_GET["site_preloader"]  : $site_preloader = "" ;
        ( $site_preloader == "on" ) ? $preloader = true : $preloader = $glimmer['preloader'];
        if ( $preloader ) {
        ?>
        <!-- Preloader -->
        <div class="preloader">
            <div class="preloader-logo">
                <img src="<?php if ( isset( $glimmer['preloader_logo']['url'] ) ) echo $glimmer['preloader_logo']['url']; ?>" class="img-responsive" alt="preloader">
            </div> <!-- /.preloader-logo -->
            <div class="loader">
                <?php 
                    $animated_icon = "";
                    if ( $glimmer['preloader_animated_icon'] == 1 ) {
                        $animated_icon = "fa-spinner fa-pulse";
                    } elseif ( $glimmer['preloader_animated_icon'] == 2 ) {
                        $animated_icon = "fa-spinner fa-spin";
                    } elseif ( $glimmer['preloader_animated_icon'] == 3 ) {
                        $animated_icon = "fa-circle-o-notch fa-spin";
                    } elseif ( $glimmer['preloader_animated_icon'] == 4 ) {
                        $animated_icon = "fa-refresh fa-spin";
                    } elseif ( $glimmer['preloader_animated_icon'] == 5 ) {
                        $animated_icon = "fa-cog fa-spin";
                    } 
                ?>
                <i class="fa <?php if ( isset( $animated_icon ) ) echo $animated_icon; ?>"></i>
            </div> <!-- /.loader -->
        </div> <!-- /.preloader -->
        <?php
        }
    ?>
    
    <!-- Header
    ================================================== -->
    <header id="masthead" class="site-header">

        <div class="header-mobile">
            <div class="container">

                <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-logo-mobile" rel="home"><img src="<?php if ( isset( $glimmer['header_logo']['url'] ) ) echo $glimmer['header_logo']['url']; ?>" alt="<?php echo bloginfo('name'); ?>"></a>

                <div class="mbsearch-social">                    
                    <?php if ( $glimmer['header_right_search_form'] ) : ?>
                    <div class="search default">
                        <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="searchform" method="get">
                            <div class="input-group">
                                <input type="search" name="s" value="<?php _e( 'Search here &hellip;', 'glimmer' ); ?>" class="form-control">
                                <span class="input-group-btn">
                                <?php if ( $glimmer['search_only_form_post'] == 1 ) : ?>
                                    <input type="hidden" value="post" name="post_type">
                                <?php endif; ?>
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div> <!-- /.search --> 
                    <?php endif; ?> 
                </div> <!-- /.search-social --> 
            </div> <!-- /.container -->             
        </div> <!-- /.header-mobile -->

        <div class="header-content center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="site-logo" rel="home"><img src="<?php if ( isset( $glimmer['header_logo']['url'] ) ) echo $glimmer['header_logo']['url']; ?>" alt="<?php echo bloginfo('name'); ?>"></a>

                        <div class="search-social">                          

                            <?php if ( $glimmer['header_left_social'] ) : ?>
                            <button class="share-button" aria-label="Toggle Navigation" type="button">
                                <i class="fa fa-share-alt"></i>
                            </button>
                            
                            <div id="top-social">
                                <?php
                                    // show footer menu social link
                                    if( isset ( $glimmer['header_left_social_link'] )  && ! empty ( $glimmer['header_left_social_link']) ) {
                                        $social_link = $glimmer['header_left_social_link'];                      
                                        foreach ( $social_link as $key => $value ) {
                                        if ( $value['title'] ) {
                                    ?>
                                    <a target="_blank" href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                                    <?php
                                            }
                                        }
                                    }
                                ?> 
                            </div> <!-- /.top-social --> 
                            <?php endif; ?> 

                            <?php if ( $glimmer['header_right_search_form'] ) : ?>
                            <div class="search default">
                                <form action="<?php echo esc_url( home_url( '/' ) ); ?>" class="searchform" method="get">
                                    <div class="input-group">
                                        <input type="search" name="s" value="<?php _e( 'Search here &hellip;', 'glimmer' ); ?>" class="form-control" id="dsearch">
                                        <span class="input-group-btn">
                                        <?php if ( $glimmer['search_only_form_post'] == 1 ) : ?>
                                            <input type="hidden" value="post" name="post_type">
                                        <?php endif; ?>
                                            <button type="button" id="search-submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div>
                                </form>                                
                            </div> <!-- /.search --> 
                            <?php endif; ?> 

                        </div> <!-- /.search-social -->  
                    </div> <!-- /.col-md-12 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.site-header -->

        <!-- Menu -->
        <div class="menucontent overlapblackbg"></div>
        <div class="menuexpandermain slideRight">
            <a id="navToggle" class="animated-arrow slideLeft"><span></span></a>
            <span id="menu-marker"><?php _e('Menu', 'glimmer'); ?></span>
        </div>
        <nav class="top-menu slideLeft clearfix">
            <div class="menu-wrapper">
                <div class="container">
                    <div class="row">   
                        <?php
                            wp_nav_menu( array(
                                'theme_location'    => 'main-menu',
                                'depth'             =>  0,
                                'menu_class'        => 'mobile-sub menu-list',
                                'container'         => '',
                                'fallback_cb'       => 'Glimmer_Nav_Walker::fallback',
                                'walker'            => new Glimmer_Nav_Walker()
                                )
                            );
                        ?>             
                        <div id="mobile-social">
                            <span><?php _e('Follow us on', 'glimmer'); ?></span>
                            <?php
                                // show footer menu social link
                                if( isset ( $glimmer['header_left_social_link'] )  && ! empty ( $glimmer['header_left_social_link']) ) {
                                    $social_link = $glimmer['header_left_social_link'];                      
                                    foreach ( $social_link as $key => $value ) {
                                    if ( $value['title'] ) {
                                ?>
                                <a target="_blank" href="<?php echo $value['url']; ?>"><i class="fa <?php echo $value['title'];?>"></i></a>
                                <?php
                                        }
                                    }
                                }
                            ?> 
                        </div> <!-- /#mobile-social --> 
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div> <!-- /.menu-wrapper -->
        </nav> <!-- /.menu  -->

    </header> <!-- #masthead -->
