var urlLoc = document.getElementById("site-logo-main").getAttribute("href");
var themeApp = {
    gatherData: function(e, t, a) {
        var e = e || 1,
            t = t || "",
            a = a || [],
            i = this,
            s = "" + urlLoc + "/rss/" + e + "/";
        $.ajax({
            url: s,
            type: "get",
            success: function(s) {
                var n = $(s).find("item > guid").text();
                t != n ? ($(s).find("item").each(function() {
                    a.push(this)
                }), i.gatherData(e + 1, n, a)) : (i.specialPostsSetOne(a), i.recentPosts(a), i.tagcloud(a))
            }
        })
    },
    taggedPost: function(e, t) {
        var a = [];
        return $.each(e, function(e, i) {
            var s = [],
                n = [];
            $(i).find("category").each(function() {
                s.push($(this).text()), n.push($(this).text().toLowerCase())
            }), n.indexOf(t.toLowerCase()) >= 0 && a.push({
                link: $(i).find("link").text(),
                title: $(i).find("title").text(),
                content: $(i).find("content\\:encoded, encoded").text(),
                author: $(i).find("dc\\:creator, creator").text(),
                category: $(i).find("category"),
                published_date: $(i).find("pubDate").text(),
                image_link: $(this).find("media\\:content, content").attr("url"),
            })
        }), a
    },
    specialPostsSetOne: function(e) {
        var special_tag_one = 'sticky';
        var tag_one_post_count = Sticky_Post['post_count'];
        if ($("#featured").length && "undefined" != typeof special_tag_one && "undefined" != typeof tag_one_post_count) {
            var t = themeApp.taggedPost(e, special_tag_one),
                a = "";
            if (t.length > 0) {
                for (a = '<div class="container"><div class="row default-layout"><div class="col-md-12" id="mulitple-grid"><div id="featured-item" class="owl-carousel">', i = 0; i < t.length; i++)
                    if (i < tag_one_post_count) {
                        var s = t[i].title,
                            n = t[i].link,
                            o = t[i].image_link,
                            m = t[i].author,
                            c = t[i].category,
                            r = themeApp.formatDate(t[i].published_date),
                            l = $(t[i].content).text().replace("<code>", "&lt;code&gt;").replace("<", "&lt;").replace(">", "&gt;"),
                            l = l.split(/\s+/).slice(0, 50).join(" "),
                            d = "",
                            p = "";
                        "undefined" != typeof c && $.each(c, function(e, t) {
                            var a = $(t).text(),
                                i = a.toLowerCase().replace(/ /g, "-");
                            p += '<a href="' + urlLoc + '/tag/' + i + '/">' + a + "</a>"
                        }), d = "undefined" != typeof o ? '<div class="image-extra"><div class="extra-content"><div class="inner-extra"><span class="entry-date">' + r + '</span><h2 class="entry-title"><a href="' + n + '">' + s + '</a></h2><div class="hr-line"></div><div class="entry-meta"><span class="cat-links">In&nbsp;-&nbsp;&nbsp;' + p + '</span><span class="devider">/</span><span class="author">By&nbsp;-&nbsp;&nbsp;' + m + '</span></div><div class="fet-more-wrapper"><a href="' + n + '" class="fet-read-more">Continue Reading</a></div></div></div></div></div>' : '<div class="featured-media"><div class="tag-list">' + p + "</div></div>", a += '<!-- start post --><div class="full-wrapper" style="background: url(' + o + ')">' + d + '</div>'
                    }
                a += "</div></div></div></div></div></div>", $("#featured").append(a) && $('.full-wrapper').appendTo('#featured-item');
            }
            var featcarousel = $('#featured-item');
            featcarousel.owlCarousel({
                singleItem: true,
                slideSpeed: 400,
                navigation: true,
                pagination: false,
                addClassActive: true,
                responsiveRefreshRate: 200,
                navigationText: [
                    "<span class='fa fa-angle-left'></span>",
                    "<span class='fa fa-angle-right'></span>"
                ]
            });
        }
    },

    recentPosts: function(e) {
        var recent_post_count = Latest_Posts_Widget['post_count'];
        var container = $(".latest-widget");
        if (container.length && typeof recent_post_count !== 'undefined') {
            var string = '';
            $(e).slice(0, recent_post_count).each(function() {
                var link = $(this).find('link').text();
                var title = $(this).find('title').text();
                var tag = $(this).find("category").text();
                var published_date = themeApp.formatDate($(this).find('pubDate').text());
                var image_link = $(this).find('media\\:content, content').attr('url');
                if (typeof image_link !== 'undefined') {
                    var image = '<figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + image_link + '"></figure>';
                    var helper_class = 'have-image';
                } else {
                    if (tag === 'image') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>';                        
                    } else if (tag === 'video') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>'; 
                    } else if (tag === 'status') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>';                         
                    } else if (tag === 'link') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>'; 
                    } else if (tag === 'gallery') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>'; 
                    } else if (tag === 'audio') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>'; 
                    } else if (tag === 'quote') {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image-' + tag + '.jpg"></figure></div>'; 
                    } else {
                        var image = '<div class="post-thumb"><figure class="fit-img"><img alt="' + title + '" class="latest-item-thumb" src="' + urlLoc + '/assets/images/no-media/image.jpg"></figure></div>';
                    }
                    var helper_class = '';
                }
                string += '<div class="latest-wrap ' + helper_class + '"><div class="latest-item clearfix">\
                \
                <div class="latest-image"><a href="' + link + '">' + image + '</a></div>\
                <div class="latest-item-text"><h5><a href="' + link + '">' + title + '</a></h5>\
                <span class="latest-item-meta">' + published_date + '</span></div>\
                </div></div>'
            });
            container.append(string);
        }
    },
    formatDate: function(e) {
        var t = new Date(e),
            a = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            i = a[t.getMonth()],
            s = t.getDate(),
            n = t.getFullYear(),
            o = i + " " + s + ", " + n;
        return o
    },
    tagcloud: function(e) {
        var t = [];
        $(e).find("category").each(function() {
            var e = $(this).text(); - 1 == $.inArray(e, t) && t.push(e)
        });
        for (var a = "", i = 0; i < t.length; i += 1) {
            var s = t[i],
                n = s.toLowerCase().replace(/ /g, "-");

                if (n === "full-width" || n === "sidebar-left" || n === "sidebar-right") {
                    continue;
                }

            a += '<a href="' + urlLoc + '/tag/' + n + '">' + s + "</a>"
        }
        $(".tagcloud").append(a)
    },

    init: function() {
        themeApp.gatherData()
    }
};
$(document).ready(function() {
    themeApp.init()
});