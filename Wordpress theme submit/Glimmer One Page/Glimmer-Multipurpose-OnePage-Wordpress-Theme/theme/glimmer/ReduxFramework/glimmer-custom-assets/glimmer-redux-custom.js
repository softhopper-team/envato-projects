jQuery(document).ready(function($) {
	//top
	jQuery('#glimmer-section_header_nav_top_icon').parent().parent('tr').addClass( "redux-glimmer-section-header_nav_top_icon" );
    jQuery('#glimmer-section_header_nav_top_icon_url').parent().parent('tr').addClass( "glimmer-section_header_nav_top_icon_url" );	
    jQuery('#glimmer-section_header_nav_top_icon_tooltip').parent().parent('tr').addClass( "glimmer-section_header_nav_top_icon_tooltip" );	
    //right
    jQuery('#glimmer-section_header_nav_right_icon').parent().parent('tr').addClass( "redux-glimmer-section-header_nav_right_icon" );
    jQuery('#glimmer-section_header_nav_right_icon_url').parent().parent('tr').addClass( "glimmer-section_header_nav_right_icon_url" );	
    jQuery('#glimmer-section_header_nav_right_icon_tooltip').parent().parent('tr').addClass( "glimmer-section_header_nav_right_icon_tooltip" );	
    //bottom
    jQuery('#glimmer-section_header_nav_bottom_icon').parent().parent('tr').addClass( "redux-glimmer-section-header_nav_bottom_icon" );
    jQuery('#glimmer-section_header_nav_bottom_icon_url').parent().parent('tr').addClass( "glimmer-section_header_nav_bottom_icon_url" );	
    jQuery('#glimmer-section_header_nav_bottom_icon_tooltip').parent().parent('tr').addClass( "glimmer-section_header_nav_bottom_icon_tooltip" );	
    //left
    jQuery('#glimmer-section_header_nav_left_icon').parent().parent('tr').addClass( "redux-glimmer-section-header_nav_left_icon" );
    jQuery('#glimmer-section_header_nav_left_icon_url').parent().parent('tr').addClass( "glimmer-section_header_nav_left_icon_url" );
    jQuery('#glimmer-section_header_nav_left_icon_tooltip').parent().parent('tr').addClass( "glimmer-section_header_nav_left_icon_tooltip" );		
});