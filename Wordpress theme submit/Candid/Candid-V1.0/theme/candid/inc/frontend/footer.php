<?php
/**
 * Hooks for template footer
 *
 * @package Candid
 */

/**
 * Custom scripts  on footer
 *
 * @since  1.0
 */
function candid_footer_scripts() {
	global $candid, $post;
	// Custom javascript
	if ( isset( $post->ID ) ) $meta = get_post_meta( $post->ID );
	$inline_js = '';
	isset( $meta["_candid_custom_js"][0] ) ? $candid_custom_js = $meta["_candid_custom_js"][0] : $candid_custom_js = '';
	isset( $candid['custom_js'] ) ? $custom_js = $candid['custom_js'] : $custom_js = '';
	
	$js_custom = $candid_custom_js. $custom_js;
	$inline_js =  '<script type="text/javascript">' . $js_custom . '</script>' ;

	if( $inline_js ) {
		echo $inline_js;
	}
	if( isset ( $candid['custom_ga'] )  && ! empty ( $candid['custom_ga']) ) {
        echo $candid['custom_ga'];
    } 
}
add_action( 'wp_footer', 'candid_footer_scripts', 200 );
