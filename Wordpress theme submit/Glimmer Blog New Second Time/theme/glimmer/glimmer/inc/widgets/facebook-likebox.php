<?php 

class Glimmer_Fb_Likebox extends WP_Widget {
	
	function __construct() {
	    $params = array (
	        'description' => 'Glimmer : Facebook Like Box',
	        'name' => 'Glimmer : Facebook Likebox'
	    );
	    parent::__construct('Glimmer_Fb_Likebox','',$params);
	}

	function widget( $args, $instance ) {
		extract( $args );
		/* User-selected settings. */
		$title = apply_filters('widget_title', $instance['title'] );
		$width = $instance['width'];
		$height = $instance['height'];
		$color = $instance['color'];
		$faces = $instance['faces'];
		$stream = $instance['stream'];
		$header = $instance['header'];
		$borderc = $instance['borderc'];
		$page = $instance['page'];

		/* Before widget (defined by themes). */
		echo $before_widget;

		/* Title of widget (before and after defined by themes). */
		if ( $title )
			echo $before_title . $title . $after_title;
		?>
			<div class="glimmer-facebook-likebox" style="<?php if($borderc != '') {echo 'border:1px solid '.$borderc.';';} ?>">

				<iframe src="//www.facebook.com/plugins/likebox.php?href=<?php echo $page ; ?>&amp;width=<?php echo $width ; ?>&amp;height=<?php echo $height; ?>&amp;colorscheme=<?php echo $color; ?>&amp;show_faces=<?php if($faces != 'on') {echo 'false';}else{echo 'true';} ?>&amp;show_border=false&amp;stream=<?php if($stream != 'on') {echo 'false';}else{echo 'true';} ?>&amp;header=<?php if($header != 'on') {echo 'false';}else{echo 'true';} ?>" style="margin-bottom: -9px;border:none; overflow:hidden; width:<?php echo $width; ?>px; height:<?php echo $height; ?>px;"></iframe>

			</div><!-- like_box_footer-->

		<?php 
		/* After widget (defined by themes). */
		echo $after_widget;
	}


	 /** @see WP_Widget::update */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		/* Strip tags (if needed) and update the widget settings. */
		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['width'] = $new_instance['width'];
		$instance['height'] = $new_instance['height'];
		$instance['color'] = $new_instance['color'];
		$instance['faces'] = $new_instance['faces'];
		$instance['stream'] = $new_instance['stream'];
		$instance['header'] = $new_instance['header'];
		$instance['borderc'] = $new_instance['borderc'];
		$instance['page'] = $new_instance['page'];

		return $instance;
	}


	 /** @see WP_Widget::form */
	function form( $instance ) {

			/* Set up some default widget settings. */
			$defaults = array(
				'title' => __('Facebook','glimmer'),
				'page' => 'http://www.facebook.com/envato',
				'width' => 295,
				'height' => 217,
				'faces' => 'on',
				'stream' => '',
				'header' => '',
				'borderc' => '#e5e5e5',
				'color' => 'light'
				
	 			);
			$instance = wp_parse_args( (array) $instance, $defaults ); ?>
		
			<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e('Title:', 'glimmer') ?></label>
			<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

	    	<p>
			<label for="<?php echo $this->get_field_id( 'page' ); ?>"><?php _e('Facebook Page URL:', 'glimmer') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'page' ); ?>" name="<?php echo $this->get_field_name( 'page' ); ?>" value="<?php echo $instance['page']; ?>"  class="widefat" />
			</p>
	        

	    	<p>
			<label for="<?php echo $this->get_field_id( 'width' ); ?>"><?php _e('Width:', 'glimmer') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'width' ); ?>" name="<?php echo $this->get_field_name( 'width' ); ?>" value="<?php echo $instance['width']; ?>"  class="widefat" />
			</p>

	    	<p>
			<label for="<?php echo $this->get_field_id( 'height' ); ?>"><?php _e('Height:', 'glimmer') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'height' ); ?>" name="<?php echo $this->get_field_name( 'height' ); ?>" value="<?php echo $instance['height']; ?>"  class="widefat" />
			</p>

			<p>
			<label for="<?php echo $this->get_field_id( 'color' ); ?>"><?php _e('Color Scheme', 'glimmer') ?></label>
			<select id="<?php echo $this->get_field_id( 'color' ); ?>" name="<?php echo $this->get_field_name( 'color' ); ?>" class="widefat">
			<option value="light" <?php if ( 'light' == $instance['color'] ) echo 'selected="selected"'; ?>><?php _e('Light', 'glimmer'); ?></option>
			<option value="dark" <?php if ( 'dark' == $instance['color'] ) echo 'selected="selected"'; ?>><?php _e('Dark', 'glimmer'); ?></option>
			</select>
			</p>
	    	<p>
			<label for="<?php echo $this->get_field_id( 'borderc' ); ?>"><?php _e('Box border Color:', 'glimmer') ?></label>
			<input type="text" id="<?php echo $this->get_field_id( 'borderc' ); ?>" name="<?php echo $this->get_field_name( 'borderc' ); ?>" value="<?php echo $instance['borderc']; ?>"  class="widefat" />
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked( $instance['faces'], 'on' ); ?> id="<?php echo $this->get_field_id( 'faces' ); ?>" name="<?php echo $this->get_field_name( 'faces' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'faces' ); ?>"><?php _e('Show faces', 'glimmer'); ?></label>
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked( $instance['stream'], 'on' ); ?> id="<?php echo $this->get_field_id( 'stream' ); ?>" name="<?php echo $this->get_field_name( 'stream' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'stream' ); ?>"><?php _e('Show stream', 'glimmer'); ?></label>
			</p>

			<p>
				<input class="checkbox" type="checkbox" <?php checked( $instance['header'], 'on' ); ?> id="<?php echo $this->get_field_id( 'header' ); ?>" name="<?php echo $this->get_field_name( 'header' ); ?>" />
				<label for="<?php echo $this->get_field_id( 'header' ); ?>"><?php _e('Show header', 'glimmer'); ?></label>
			</p>

		
	   <?php 
	}
} //end class