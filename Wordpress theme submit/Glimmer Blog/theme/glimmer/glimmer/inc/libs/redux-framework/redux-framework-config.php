<?php
    /**
     * Glimmer Theme Options
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }


    // This is your option name where all the Redux data is stored.
    $opt_name = "glimmer";


    /*
     *
     * --> Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
     *
     */

    $sampleHTML = '';
    if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
        Redux_Functions::initWpFilesystem();

        global $wp_filesystem;

        $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
    }

    // Background Patterns Reader
    $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
    $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
    $sample_patterns      = array();

    if ( is_dir( $sample_patterns_path ) ) {

        if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) {
            $sample_patterns = array();

            while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                    $name              = explode( '.', $sample_patterns_file );
                    $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                    $sample_patterns[] = array(
                        'alt' => $name,
                        'images' => $sample_patterns_url . $sample_patterns_file
                    );
                }
            }
        }
    }

    /*
     *
     * --> Action hook examples
     *
     */

    // If Redux is running as a plugin, this will remove the demo notice and links
    //add_action( 'redux/loaded', 'remove_demo' );

    // Function to test the compiler hook and demo CSS output.
    // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
    //add_filter('redux/options/' . $opt_name . '/compiler', 'compiler_action', 10, 3);

    // Change the arguments after they've been declared, but before the panel is created
    //add_filter('redux/options/' . $opt_name . '/args', 'change_arguments' );

    // Change the default value of a field after it's been set, but before it's been useds
    //add_filter('redux/options/' . $opt_name . '/defaults', 'change_defaults' );

    // Dynamically add a section. Can be also used to modify sections/fields
    //add_filter('redux/options/' . $opt_name . '/sections', 'dynamic_section');


    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'submenu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Glimmer Options', 'glimmer' ),
        'page_title'           => __( 'Glimmer Options', 'glimmer' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => false,
        // remove tracking 
        'disable_tracking' => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-admin-generic',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 100,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => false,
        // Show the time the page took to load, etc
        'update_notice'        => false,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => false,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => THEME_URL . "/images/menuicon/setting.png",
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
        'system_info'          => false,
        // REMOVE

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

 
    

    // Add content after the form.
    $args['footer_text'] = __( '<p></p>', 'glimmer' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */


    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'glimmer' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'glimmer' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'glimmer' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'glimmer' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'glimmer' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START Basic Fields



    // -> START Typography
    //Redux::setSection( $opt_name, array() );
    //Redux::setSection( $opt_name, array() );
    //print_r(get_option('glimmer_sections'));
        // this array contains all animation style list
            
            Redux::setSection( $opt_name, array(
                    'title' => __('General Options', 'glimmer' ),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'el el-cog',
                    'fields' => array (    
                        array (
                            // this is load because if it not load other slides extension not work
                            'id'          => 'glimmer_hidden_slides',
                            'type'        => 'slides',
                            'title'       => __( 'Glimmer Hidden Slider', 'glimmer' ),
                        ),
                        array(
                            'id'=>'preloader',
                            'type' => 'switch',
                            'title' => __('Site Preloader', 'glimmer'),
                            'default' => false
                        ),
                        array(
                            'id'=>'preloader_logo',
                            'type' => 'media',
                            'url'=> true,
                            'required' => array( 'preloader', '=', '1' ),
                            'readonly' => false,
                            'title' => __('Preloader Logo', 'glimmer'),
                            'subtitle' => __('This logo image will show in preloader', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/preloader.png'
                            )
                        ),  
                        array(
                            'id'=>'preloader_logo_retina',
                            'type' => 'media',
                            'url'=> true,
                            'required' => array( 'preloader', '=', '1' ),
                            'readonly' => false,
                            'title' => __('Preloader logo for retina ', 'glimmer'),
                            'subtitle' => __('2x logo size, for screens with high DPI.
                            Use the exact same filename and add @2x after the name. example: preloader@2x.png', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/preloader@2x.png'
                            )
                        ),
                        array(
                            'id'       => 'preloader_animated_icon',
                            'type'     => 'select',
                            'title'    => __( 'Preloader Animated Icon', 'glimmer' ),
                            'subtitle' => __( 'This animated icon will show in preloader', 'glimmer' ),
                            'required' => array( 'preloader', '=', '1' ),
                            //Must provide key => value pairs for select options
                            'options'  => array(
                                '1' => 'fa-spinner fa-pulse',
                                '2' => 'fa-spinner fa-spin',
                                '3' => 'fa-circle-o-notch fa-spin',
                                '4' => 'fa-refresh fa-spin',
                                '5' => 'fa-cog fa-spin',
                            ),
                            'description' => __( "These animated icon used from font awesome. See demo <a href='http://fortawesome.github.io/Font-Awesome/examples/'>here</a> from Animated Icons List", "glimmer" ),
                            'default'  => '1'
                        ),
                        array(
                            'id'=>'favicon',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Favicon', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/favicon.ico'
                            )
                        ),
                        array(
                            'id'=>'icon_iphone',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPhone Icon', 'glimmer'),
                            'desc' => __('Icon for Apple iPhone (57px X 57px)', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon.png'
                            )
                        ),

                        array(
                            'id'=>'icon_iphone_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPhone Retina Icon', 'glimmer'),
                            'desc' => __('Icon for Apple iPhone Retina (114px X 114px)', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon_114x114.png'
                            )
                        ),

                        array(
                            'id'=>'icon_ipad',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPad Icon', 'glimmer'),
                            'desc' => __('Icon for Apple iPad (72px X 72px)', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon_72x72.png'
                            )
                        ),

                        array(
                            'id'=>'icon_ipad_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Apple iPad Retina Icon', 'glimmer'),
                            'desc' => __('Icon for Apple iPad Retina (144px X 144px)', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/favicon/apple-touch-icon_144x144.png'
                            )
                        ),    
                        array(
                            'id'       => 'custom_css',
                            'type'     => 'ace_editor',
                            'title'    => __( 'Custom CSS Code', 'glimmer' ),
                            'subtitle' => __( 'Paste your CSS code here.', 'glimmer' ),
                            'mode'     => 'css',
                            'theme'    => 'monokai',
                            'default'  => "#header {\n   margin: 0 auto;\n}"
                        ),
                        array(
                            'id'       => 'custom_js',
                            'type'     => 'ace_editor',
                            'title'    => __( 'Custom JS Code', 'glimmer' ),
                            'subtitle' => __( 'Paste your JS code here.', 'glimmer' ),
                            'mode'     => 'javascript',
                            'theme'    => 'monokai',
                            'default'  => "jQuery(document).ready(function($){\n\n});"
                        ),
                        array(
                            'id'       => 'tracking_code_for_head',
                            'type'     => 'textarea',
                            'title'    => __( 'Tracking Code For Header', 'glimmer' ),
                            'subtitle' => __( 'Paste your Google Analytics (or other) tracking code here. This will be added into the header template in &lt;head&gt; tag of your theme.', 'glimmer' ),
                            'default'  => ""
                        ), 
                        array(
                            'id'       => 'tracking_code_for_footer',
                            'type'     => 'textarea',
                            'title'    => __( 'Tracking Code For Footer', 'glimmer' ),
                            'subtitle' => __( 'Paste your Google Analytics (or other) tracking code here. This will be added into the footer template befor &lt;/body&gt; tag of your theme.', 'glimmer' ),
                            'default'  => ""
                        ),                     
                    )
                ) 
            ); //general

            Redux::setSection( $opt_name, array(
                    'title' => __('Layout', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-th-list',
                    'fields' => array ( 
                        array (
                            'id' => 'sidebar_layout',
                            'type' => 'image_select',
                            'title' => __('Default Layout', 'glimmer'),
                            'subtitle' => __('Default layout for whole site', 'glimmer'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),
                        array (
                            'id' => 'sidebar_layout_single',
                            'type' => 'image_select',
                            'title' => __('Single Layout', 'glimmer'),
                            'subtitle' => __('Default layout of single post', 'glimmer'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),    
                        array (
                            'id' => 'sidebar_layout_page',
                            'type' => 'image_select',
                            'title' => __('Page Layout', 'glimmer'),
                            'subtitle' => __('Default layout of single page ', 'glimmer'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),
                        array (
                            'id' => 'sidebar_layout_archive',
                            'type' => 'image_select',
                            'title' => __('Archive Layout', 'glimmer'),
                            'subtitle' => __('Default layout of category, archive, tag page ', 'glimmer'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),
                    )
                ) 
            ); //Layout

            Redux::setSection( $opt_name, array(
                    'title' => __('Typography', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-font',
                    'fields' => array (  
                        array (
                            'id'       => 'body-font',
                            'type'     => 'typography',
                            'title'    => __( 'Body Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the body font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'all_styles'=>true,                            
                            'units'     => 'em',
                            'output'      => array('html, body'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '1em',
                                'line-height'   => '1.45em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ),     
                        array (
                            'id'       => 'featured-post-section-title',
                            'type'     => 'typography',
                            'title'    => __( 'Featured Post Section Title', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the featured post section title font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'all_styles'=>true,  
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('.featured-head-content .entry-title'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '2.441em',
                                'line-height'   => '1.2em',
                                'font-family' => 'Roboto',
                                'font-weight' => '300',
                            ),
                        ), 
                        array (
                            'id'       => 'all-post-title',
                            'type'     => 'typography',
                            'title'    => __( 'All Main Post &amp; Page Title', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the all post and page title font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('.post .entry-title, .page .entry-title'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '1.563em',
                                'line-height'   => '1.3em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ), 
                        array (
                            'id'       => 'heading-one',
                            'type'     => 'typography',
                            'title'    => __( 'Heading h1 Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the heading h1 font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('h1'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '2.441em',
                                'line-height'   => '1.2em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ), 
                        array (
                            'id'       => 'heading-two',
                            'type'     => 'typography',
                            'title'    => __( 'Heading h2 Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the heading h2 font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('h2'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '1.953em',
                                'line-height'   => '1.2em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ), 
                        array (
                            'id'       => 'heading-three',
                            'type'     => 'typography',
                            'title'    => __( 'Heading h3 Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the heading h3 font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('h3'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '1.563em',
                                'line-height'   => '1.2em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ), 
                        array (
                            'id'       => 'heading-four',
                            'type'     => 'typography',
                            'title'    => __( 'Heading h4 Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the heading h4 font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('h4'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '1.25em',
                                'line-height'   => '1.2em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ), 
                        array (
                            'id'       => 'heading-five',
                            'type'     => 'typography',
                            'title'    => __( 'Heading h5 Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the heading h5 font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('h5'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '1em',
                                'line-height'   => '1.1em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ), 

                        array (
                            'id'       => 'heading-six',
                            'type'     => 'typography',
                            'title'    => __( 'Heading h6 Font', 'glimmer' ),
                            'subtitle' => __( 'You can Specify the heading h6 font properties.', 'glimmer' ),
                            'google'   => true,
                            'color'    => false,
                            'text-align'=> false,
                            'update-weekly'=>false,
                            'line-height'=> true,
                            'subsets'  =>false,
                            'font-style'=> true,
                            'font-backup' => false,
                            'font-size' => true,
                            'font-weight'=>true,
                            'units'     => 'em',
                            'output'      => array('h6'),
                            'units'   => 'em',
                            'default'  => array(
                                'font-size'   => '0.8em',
                                'line-height'   => '1.1em',
                                'font-family' => 'Lora',
                                'font-weight' => '400',
                            ),
                        ),   
                    )
                ) 
            ); //Typography

            Redux::setSection( $opt_name, array(
                    'title' => __('Color Options', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-life-ring',
                    'fields' => array ( 
                        array(
                            'id' => 'glimmer_color_scheme',
                            'type' => 'radio',
                            'title' => __('Color Scheme', 'glimmer'),
                            'subtitle' => __('Select Predefined Color Schemes or your Own', 'glimmer'),
                            'options'  => array(
                                '1' => '<div class="redux-custom-color" style="background: #C69F73;"> </div>',
                                '2' => '<div class="redux-custom-color" style="background: #1ABC9C;"> </div>',
                                '3' => '<div class="redux-custom-color" style="background: #D2527F;"> </div>',
                                '4' => '<div class="redux-custom-color" style="background: #F26D7E;"> </div>',
                                '5' => '<div class="redux-custom-color" style="background: #CC6054;"> </div>',
                                '6' => '<div class="redux-custom-color" style="background: #667A61;"> </div>',
                                '7' => '<div class="redux-custom-color" style="background: #A74C5B;"> </div>',
                                '8' => '<div class="redux-custom-color" style="background: #95A5A6;"> </div>',
                                '9' => '<img src="'.THEME_URL . '/images/color-scheme.png'.'">',
                            ),
                            'default'  => '1'
                        ),
                        array(
                            'id' => 'glimmer_custom_color',
                            'type' => 'color',
                            'title' => __('Your Own Theme Color', 'glimmer'),
                            'subtitle' => __('Pick a custom color', 'glimmer'),
                            'default' => '#FFFFFF',
                            'validate' => 'color',
                            'required' => array("glimmer_color_scheme", "=", "9")
                        ),
                        array(
                            'id'       => 'chat_bg_color',
                            'type'     => 'softhopper_chat_bg',
                            'title'    => __( 'Chat Post Speaker Background', 'glimmer' ),
                            'subtitle' => __( 'You can specify chat post speaker background color', 'glimmer' ),
                            //'speaker_one'   => false, // Disable speaker_one Color
                            'default'  => array (
                                'speaker_one' => '#C69F73',
                                'speaker_two'   => '#667A61',
                                'speaker_three'  => '#95A5A6',
                                'speaker_four'  => '#A74C5B',                   
                                'speaker_five'  => '#27ae60',                   
                                'speaker_six'  => '#2980b9',                   
                            )
                        ),
                    )
                ) 
            ); //Style

            Redux::setSection( $opt_name, array(
                    'title' => __('Post Settings', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-file-text',
                    'fields' => array (
                        array(
                            'id'            => 'post_excerpt',
                            'type'          => 'slider',
                            'title' => __('Default Excerpt Length', 'glimmer'),
                            'subtitle' => __('How many words you want to show in post excerpt', 'glimmer'),
                            'default'       => 55,
                            'min'           => 1,
                            'step'          => 1,
                            'max'           => 300,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'       => 'post_meta',
                            'type'     => 'sortable',
                            'mode'     => 'checkbox', // checkbox or text
                            'title'    => __( 'Post Header Meta', 'glimmer' ),
                            'subtitle' => __( 'Whose meta you want to show in post header?', 'glimmer' ),
                            'options'  => array(
                                'cat_meta' => 'Category Meta',
                                'author_meta' => 'Author Meta',
                                'date_meta' => 'Date Meta',
                                'total_post_view_meta' => 'Total Post View Meta',
                            ),
                            'default'  => array(
                                'cat_meta' => true,
                                'author_meta' => true,
                                'date_meta' => true,
                                'total_post_view_meta' => false,
                            )
                        ), 
                        array(
                            'id'       => 'author_info_box',
                            'type'     => 'switch',
                            'title'    => __( 'Author Info Box', 'glimmer' ),
                            'subtitle' => __( 'You can show or hide author info box', 'glimmer' ),
                            'default' => 1
                        ),                        
                        array(
                            'id'       => 'section-start',
                            'type'     => 'section',
                            'title'    => __( 'Related Posts Settings', 'glimmer' ),
                            'indent'   => true, // Indent all options below until the next 'section' option is set.
                        ),
                        array(
                            'id'       => 'related_post',
                            'type'     => 'switch',
                            'title'    => __( 'Related Posts', 'glimmer' ),
                            'subtitle' => __( 'You can show or hide related posts', 'glimmer' ),
                            'default' => 1
                        ),
                        array(
                            'id'            => 'related_post_number',
                            'type'          => 'slider',
                            'title'         => __('Number of posts to show', 'glimmer'),
                            'default'       => 5,
                            'min'           => 1,
                            'step'          => 1,
                            'max'           => 20,
                            'display_value' => 'text'
                        ),
                        array(
                            'id'       => 'related_query',
                            'type'     => 'radio',
                            'title'    => __( 'Query Type', 'glimmer' ),
                            'subtitle' => __( 'Show related by this query type', 'glimmer' ),
                            //Must provide key => value pairs for radio options
                            'options'  => array(
                                'tag' => 'Tag',
                                'category' => 'Category',
                                'author' => 'Author'
                            ),
                            'default'  => 'tag'
                        ),
                        array(
                            'id'     => 'section-end',
                            'type'   => 'section',
                            'indent' => false, // Indent all options below until the next 'section' option is set.
                        ),  
                    )
                ) 
            ); //Post Settings

            Redux::setSection( $opt_name, array(
                    'title' => __('Header Section', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-laptop',
                    'fields' => array(                   
                        array(
                            'id' => 'header_logo',
                            'type' => 'media',
                            'title' => __('Header Logo', 'glimmer'),
                            'subtitle' => __('This image will show as a logo in header banner area', 'glimmer'),
                            'default' => array("url" => THEME_URL . "/images/logo.png"),
                            'preview' => true,
                            'readonly' => false,
                            "url" => true,
                        ),
                        array(
                            'id'=>'header_logo_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Header Logo for retina', 'glimmer'),
                            'subtitle' => __('2x logo size, for screens with high DPI.
                            Use the exact same filename and add @2x after the name. example: logo@2x.png', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/logo@2x.png'
                            )
                        ),
                        array(
                            'id' => 'header_bg_img',
                            'type' => 'media',
                            'title' => __('Header background image', 'glimmer'),
                            'subtitle' => __('This background image will show in header banner area', 'glimmer'), 
                            'default' => array("url" => THEME_URL . "/images/header.jpg"),
                            'preview' => true,
                            'readonly' => false,
                            "url" => true,
                        ),
                        array(
                            'id'          => 'header_menu_social_link',
                            'type'        => 'softhopper_slides',
                            'title'       => __( 'Header Menu Social Link', 'glimmer' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),                            
                            'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'glimmer' ),
                            'placeholder' => array(
                                'title'       => __( 'Add font awesome Icon Class', 'glimmer' ),
                                'url'       => __( 'Social Icon Url', 'glimmer' ),
                            ), 
                            'default' => array(
                                0 => array(
                                    'title' => 'fa-facebook',
                                    'url' => '#'
                                ),
                                1 => array(
                                    'title' => 'fa-twitter',
                                    'url' => '#'
                                ),
                                2 => array(
                                    'title' => 'fa-google-plus',
                                    'url' => '#'
                                ),
                                3 => array(
                                    'title' => 'fa-behance',
                                    'url' => '#'
                                ),
                                4 => array(
                                    'title' => 'fa-pinterest-p',
                                    'url' => '#'
                                ),
                            ),                                                                    
                        ),   
                    )
                )
            ); //header

            Redux::setSection( $opt_name, array(
                    'title' => __('Featured Post', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-star',
                    'fields' => array(                       
                        array(
                            'id' => 'featured_display',
                            'type' => 'switch',
                            'title' => __('Display Featured Post', 'glimmer'),
                            'default' => 1,
                        ),
                        array(
                            'id' => 'featured_title',
                            'type' => 'text',
                            'title' => __('Fearured Title', 'glimmer'),
                            'default'=> "Enjoy the our Monthly <span>Featured</span> And <span>Top Review </span>Blog post"
                        ),
                        array(
                            'id' => 'featured_description',
                            'type' => 'editor',
                            'title' => __('Featured Description', 'glimmer'),
                            'default'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis ue nec rhoncus. Nam ut orci augue. Phasellus ac venenatis orci. Nullam iaculis lao reet massa. Suspendisse iaculis laoreet eros, a vee natis metus....',
                            'args'    => array(
                                //'wpautop'       => false,
                                'media_buttons' => false,
                                'textarea_rows' => 7,
                                'teeny'         => false,
                            )
                        ), 
                        array(
                            'id'       => 'featured_per_page',
                            'type'     => 'select',
                            'title'    => __( 'Number of featured post', 'glimmer' ),
                            'subtitle' => __( 'How many featured post you want to show', 'glimmer' ),
                            //Must provide key => value pairs for select options
                            'options'  => array(
                                '1' => '1',
                                '2' => '2',
                                '3' => '3',
                                '4' => '4',
                                '5' => '5',
                                '6' => '6',
                                '7' => '7',
                                '8' => '8',
                                '9' => '9',
                                '10' => '10',
                                '11' => '11',
                                '12' => '12',
                                '13' => '13',
                                '14' => '14',
                                '15' => '15',
                            ),
                            'default'  => '7'
                        ),
                        array(
                            'id'       => 'featured_slide_delay',
                            'type'     => 'text',
                            'title'    => __( 'Featured slide interval time', 'glimmer' ),
                            'desc'    => __( 'Value 1000 = 1 Second', 'glimmer' ),
                            'subtitle' => __( 'How much delay featured post will slide', 'glimmer' ),
                            'default'  => '3500'
                        ),
                    )
                )
            ); //featured post


            Redux::setSection( $opt_name, array(
                    'title' => __('About Me Page', 'glimmer'),
                    'icon' => 'fa fa-user',
                    'fields' => array( 
                        array(
                            'id'       => 'author_img',
                            'type'     => 'media',
                            'title'    => __('Author Image', 'glimmer'),
                            'subtitle' => __('This image will show in the navigation', 'glimmer'),
                            'default'  => array("url" => THEME_URL . "/images/author/author-image.jpg"),
                            'preview'  => true,
                            'readonly' => false,
                            "url"      => true,
                        ),                      
                        array(
                            'id'       => 'author_name',
                            'type'     => 'text',
                            'title'    => __('Author Name', 'glimmer'),
                            'subtitle' => __( 'This author will show in about page below author image', 'glimmer' ),
                            'default'  => "Johan Smith",
                        ), 
                        array(
                            'id'      => 'author_description',
                            'type'    => 'editor',
                            'title'   => __( 'Author description', 'glimmer' ),
                            'default' => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis pellentesque aug ue nec rhoncus. Nam ut orci augue. Phase llus ac venenatis orci. Nullam iaculis lao reet massa, vitae tempus ante tincidunte et. Suspendisse iaculis laoreet eros, a vee natis metus.Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>',
                            'args'    => array(
                                'wpautop'       => false,
                                'media_buttons' => false,
                                'textarea_rows' => 5,
                                'teeny'         => false,
                                'quicktags'     => false,
                            )
                        ), 
                        array (
                            'id' => 'author_sign',
                            'type' => 'media',
                            'title' => __('Author Singnature', 'glimmer'),
                            'subtitle' => __('This image will show in the navigation', 'glimmer'),
                            'default' => array("url" => THEME_URL . "/images/author/sign.png"),
                            'preview' => true,
                            'readonly' => false,
                            "url" => true,
                        ), 
                        array(
                            'id'          => 'author_social_link',
                            'type'        => 'softhopper_slides',
                            'title'       => __( 'Add Follow Link', 'glimmer' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),
                            'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'glimmer' ),
                            'placeholder' => array(
                                'title'       => __( 'Add font awesome Icon Class', 'glimmer' ),
                                'url'       => __( 'Social Icon Url', 'glimmer' ),
                            ),
                            'default' => array(
                                0 => array(
                                    'title' => 'fa-facebook',
                                    'url' => '#'
                                ),
                                1 => array(
                                    'title' => 'fa-twitter',
                                    'url' => '#'
                                ),
                                2 => array(
                                    'title' => 'fa-google-plus',
                                    'url' => '#'
                                ),
                                3 => array(
                                    'title' => 'fa-behance',
                                    'url' => '#'
                                ),
                                4 => array(
                                    'title' => 'fa-linkedin',
                                    'url' => '#'
                                ),
                            ),
                        ), 
                        array(
                            'id'          => 'author_skills',
                            'type'        => 'softhopper_slides_skill',
                            'title'       => __( 'Add author skills', 'glimmer' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),
                            'subtitle'    => __( "Add your skills for about page skill section", 'glimmer' ),
                            'placeholder' => array(
                                'title'       => __( 'Add skill name', 'glimmer' ),
                                'url'       => __( 'Add skill percent', 'glimmer' ),
                            ),
                            'default' => array(
                                0 => array(
                                    'title' => 'Article Writing',
                                    'url' => '97%'
                                ),
                                1 => array(
                                    'title' => 'Wordpress',
                                    'url' => '90%'
                                ),
                                2 => array(
                                    'title' => 'PSD Design',
                                    'url' => '95%'
                                ),
                                3 => array(
                                    'title' => 'HTML/CSS',
                                    'url' => '90%'
                                ),
                                4 => array(
                                    'title' => 'Programming',
                                    'url' => '92%'
                                ),
                                5 => array(
                                    'title' => 'SEO',
                                    'url' => '82%'
                                ),
                            ),
                        ),                   
                    )
                )
            ); //about me

            Redux::setSection( $opt_name, array(
                    'title' => __('Contact Page', 'glimmer'),
                    'icon' => 'fa fa-phone',
                    'fields' => array(
                        array(
                            'id' => 'contact_lat',
                            'type' => 'text',
                            'title' => __('Latitude', 'glimmer'),
                            'subtitle' => __("You can get Latitude and Longitude from <a href='http://www.latlong.net/'>latlong.net</a>", "glimmer"),
                            'default' => "-37.817314",
                        ),
                        array(
                            'id' => 'contact_lon',
                            'type' => 'text',
                            'title' => __('Longitude', 'glimmer'),
                            'default' => "144.955431",
                        ),
                        array(
                            'id' => 'map_mouse_wheel',
                            'type' => 'switch',
                            'title' => __('Map Mouse Wheel', 'glimmer'),
                            'default' => true
                        ),
                        array(
                            'id' => 'map_zoom_control',
                            'type' => 'switch',
                            'title' => __('Map zoomControl', 'glimmer'),
                            'default' => true
                        ),
                        array(
                            'id' => 'contact_map_point_img',
                            'type' => 'media',
                            'title' => __('Contact Map Point Image', 'glimmer'),
                            'subtitle' => __('This image will show in the google map of your location point', 'glimmer'),
                            'default' => array("url" => THEME_URL . "/images/map-icon.png"),
                            'preview' => true,
                            'readonly' => false,
                            "url" => true,
                        ),
                        array(
                            'id' => 'contact_subtitle',
                            'type' => 'editor',
                            'title' => __('Contact Subtitle', 'glimmer'),
                            'default'=> 'We provides a wide array of specialised advisory and<br>Strategic services for our clients.',
                            'args'    => array(                                
                                'textarea_rows' => 5,
                            )
                        ),
                        array(
                            'id' => 'contact_description',
                            'type' => 'editor',
                            'title' => __('Contact Description', 'glimmer'),
                            'default'=> 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis pellentesque aug ue nec rhoncus. Nam ut orci augue. Phase llus ac venenatis orci.<br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae nibh nisl. Cras et mauris eget lorem ultricies fermentum a in diam. Morbi mollis pellentesque aug ue nec rhoncus. Nam ut orci augue. Phase llus ac venenatis orci. 
                            ',
                            'args'    => array(                                
                                'textarea_rows' => 6,
                            )
                        ),
                        array(
                            'id' => 'contact_address',
                            'type' => 'editor',
                            'title' => __('Contact Address', 'glimmer'),
                            'default'=>'<h4>Creative Agency, Melbourne, Australia</h4>
                                        <dl class="dl-horizontal">
                                            <dt>Phone :</dt>
                                            <dd>+088(017) 1123 - 987654321</dd>
                                            <dt>Fax :</dt>
                                            <dd>+088(142) 564-2225632</dd>
                                            <dt>E-mail :</dt>
                                            <dd><a href="mailto:#">glimmer@domain.net</a></dd>  
                                            <dt>Web :</dt>
                                            <dd><a href="#">http://glimmercompany.net</a></dd>       
                                        </dl>',
                        ),
                        array(
                            'id' => 'contact_form7_shortcode',
                            'type' => 'text',
                            'title' => __('Contact Form 7 Shortcode', 'glimmer'),
                            'default'=> ""
                        ),
                        
                    )
                )
            ); //contact

            Redux::setSection( $opt_name, array(
                    'title' => __('Search Page', 'glimmer'),
                    'icon' => 'fa fa-search',
                    'fields' => array(
                        array (
                            'id' => 'sidebar_layout_search',
                            'type' => 'image_select',
                            'title' => __('Search Layout', 'glimmer'),
                            'subtitle' => __('Default layout of search page ', 'glimmer'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'empty.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/empty.png'
                                ),
                                '2' => array(
                                    'alt' => 'single-left.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-left.png'
                                ),
                                '3' => array(
                                    'alt' => 'single-right.png',
                                    'img' => THEME_URL . '/images/backend/sidebars/single-right.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),
                        array(
                            'id'       => 'search_only_form_post',
                            'type'     => 'switch',
                            'title'    => __( 'Search only post', 'glimmer' ),
                            'subtitle' => __( 'If you on this option query search form post. Otherwise query search form post and pages.', 'glimmer' ),
                            'default' => 0
                        ),
                        array(
                            'id'       => 'search_form_in_search_page',
                            'type'     => 'switch',
                            'title'    => __( 'Search form', 'glimmer' ),
                            'subtitle' => __( 'Show search form in search page', 'glimmer' ),
                            'default' => 0
                        ),                        
                    )
                )
            ); //search

            Redux::setSection( $opt_name, array(
                    'title' => __('Footer Section', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-edit',
                    'fields' => array(  
                        array(
                            'id'       => 'footer_widgets',
                            'type'     => 'switch',
                            'title'    => __( 'Enable Footer Widget', 'glimmer' ),
                            'subtitle' => __( 'You can off or on footer widgets', 'glimmer' ),
                            'default'  => true,
                        ),
                        array (
                            'id' => 'footer_widget_columns',
                            'type' => 'image_select',
                            'required' => array( 'footer_widgets', '=', '1' ),
                            'title' => __('Default Layout', 'glimmer'),
                            'subtitle' => __('How many sidebar you want to show on footer', 'glimmer'),
                            'options' => array (
                                '1' => array (
                                    'alt' => 'one-column.png',
                                    'img' => THEME_URL . '/images/backend/footer/one-column.png'
                                ),
                                '2' => array(
                                    'alt' => 'two-columns.png',
                                    'img' => THEME_URL . '/images/backend/footer/two-columns.png'
                                ),
                                '3' => array(
                                    'alt' => 'three-columns.png',
                                    'img' => THEME_URL . '/images/backend/footer/three-columns.png'
                                ), 
                                '4' => array(
                                    'alt' => 'four-columns.png',
                                    'img' => THEME_URL . '/images/backend/footer/four-columns.png'
                                ),                               
                            ),
                            'default' => '3'
                        ),  
                        array(
                            'id' => 'footer_logo',
                            'type' => 'media',
                            'title' => __('Footer Banner Logo', 'glimmer'),
                            'subtitle' => __('This image will show as a logo in footer banner area', 'glimmer'),
                            'default' => array("url" => THEME_URL . "/images/logo.png"),
                            'preview' => true,
                            'readonly' => false,
                            "url" => true,
                        ),
                        array(
                            'id'=>'footer_logo_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('Footer Logo for retina', 'glimmer'),
                            'subtitle' => __('2x logo size, for screens with high DPI.
                            Use the exact same filename and add @2x after the name. example: logo@2x.png', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/logo@2x.png'
                            )
                        ),
                        array(
                            'id' => 'footer_bg_img',
                            'type' => 'media',
                            'title' => __('Footer Banner Image', 'glimmer'),
                            'subtitle' => __('This image will show in footer banner area', 'glimmer'), 
                            'default' => array("url" => THEME_URL . "/images/header.jpg"),
                            'preview' => true,
                            'readonly' => false,
                            "url" => true,
                        ),                 
                        array(
                            'id'      => 'footer_copyright_info',
                            'type'    => 'editor',
                            'title'   => __( 'Footer Copyright Info', 'glimmer' ),
                            'default' => 'Copyright &copy; 2015 All right reserved Glimmer. By <a href="#">SoftHopper</a> ',
                            'args'    => array(
                                'wpautop'       => false,
                                'media_buttons' => false,
                                'textarea_rows' => 5,
                                //'tabindex' => 1,
                                //'editor_css' => '',
                                'teeny'         => false,
                                //'tinymce' => array(),
                                'quicktags'     => false,
                            )
                        ),
                        array(
                            'id'          => 'footer_social_link',
                            'type'        => 'softhopper_slides',
                            'title'       => __( 'Footer Social Link', 'glimmer' ),
                            'show' => array(
                                'title' => true,
                                'description' => false,
                                'url' => true,
                                ),                            
                            'subtitle'    => __( "Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>", 'glimmer' ),
                            'placeholder' => array(
                                'title'       => __( 'Add font awesome Icon Class', 'glimmer' ),
                                'url'       => __( 'Social Icon Url', 'glimmer' ),
                            ), 
                            'default' => array(
                                0 => array(
                                    'title' => 'fa-facebook',
                                    'url' => '#'
                                ),
                                1 => array(
                                    'title' => 'fa-twitter',
                                    'url' => '#'
                                ),
                                2 => array(
                                    'title' => 'fa-google-plus',
                                    'url' => '#'
                                ),
                                3 => array(
                                    'title' => 'fa-behance',
                                    'url' => '#'
                                ),
                                4 => array(
                                    'title' => 'fa-pinterest-p',
                                    'url' => '#'
                                ),
                            ),                                                                    
                        ),
                    )
                )
            ); //footer

            Redux::setSection( $opt_name, array(
                    'title' => __('404 Settings', 'glimmer'),
                    //'icon_class' => 'fa-lg',
                    'icon' => 'fa fa-question-circle',                
                    'fields' => array(
                        array(
                            'id' => '404_img',
                            'type' => 'media',
                            'title' => __('404  Image', 'glimmer'),
                            'subtitle' => __('This image will show in 404 page', 'glimmer'),
                            'default' => array("url" => THEME_URL . "/images/404.png"),
                            'readonly' => false,
                            'preview' => true,
                            "url" => true,
                        ),
                        array(
                            'id'=>'404_img_retina',
                            'type' => 'media',
                            'url'=> true,
                            'readonly' => false,
                            'title' => __('404  image for retina', 'glimmer'),
                            'subtitle' => __('2x logo size, for screens with high DPI.
                            Use the exact same filename and add @2x after the name. example: 404@2x.png', 'glimmer'),
                            'default' => array(
                                'url' => THEME_URL . '/images/404@2x.png'
                            )
                        ),
                        array(
                            'id'=>'404_heading',
                            'type' => 'text',
                            'title' => __('404  Title', 'glimmer'),
                            'default'=>'Whoops! Page Not Found'
                        ),
                        array(
                            'id'=>'404_subheading',
                            'type' => 'text',
                            'title' => __('404 Sub Title', 'glimmer'),
                            'default'=>'The page you are looking for doesn\'t exit.'
                        ),
                    )
                )
            ); //404   
    /*
     * <--- END SECTIONS
     */

    /**
     * This is a test function that will let you see when the compiler hook occurs.
     * It only runs if a field    set with compiler=>true is changed.
     * */
    function compiler_action( $options, $css, $changed_values ) {
        echo '<h1>The compiler hook has run!</h1>';
        echo "<pre>";
        print_r( $changed_values ); // Values that have changed since the last save
        echo "</pre>";
        //print_r($options); //Option values
        //print_r($css); // Compiler selector CSS values  compiler => array( CSS SELECTORS )
    }

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ) {
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error   = false;
            $warning = false;

            //do your validation
            if ( $value == 1 ) {
                $error = true;
                $value = $existing_value;
            } elseif ( $value == 2 ) {
                $warning = true;
                $value   = $existing_value;
            }

            $return['value'] = $value;

            if ( $error == true ) {
                $return['error'] = $field;
                $field['msg']    = 'your custom error message';
            }

            if ( $warning == true ) {
                $return['warning'] = $field;
                $field['msg']      = 'your custom warning message';
            }

            return $return;
        }
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ) {
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    }

    /**
     * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
     * Simply include this function in the child themes functions.php file.
     * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
     * so you must use THEME_URL if you want to use any of the built in icons
     * */
    function dynamic_section( $sections ) {
        //$sections = array();
        $sections[] = array(
            'title'  => __( 'Section via hook', 'glimmer' ),
            'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'glimmer' ),
            'icon'   => 'el el-paper-clip',
            // Leave this as a blank section, no options just some intro text set above.
            'fields' => array()
        );

        return $sections;
    }

    /**
     * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
     * */
    function change_arguments( $args ) {
        //$args['dev_mode'] = true;

        return $args;
    }

    /**
     * Filter hook for filtering the default value of any given field. Very useful in development mode.
     * */
    function change_defaults( $defaults ) {
        $defaults['str_replace'] = 'Testing filter hook!';

        return $defaults;
    }

    // Remove the demo link and the notice of integrated demo from the redux-framework plugin
    function remove_demo() {

        // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
        if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
            remove_filter( 'plugin_row_meta', array(
                ReduxFrameworkPlugin::instance(),
                'plugin_metalinks'
            ), null, 2 );

            // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
            remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
        }
    }