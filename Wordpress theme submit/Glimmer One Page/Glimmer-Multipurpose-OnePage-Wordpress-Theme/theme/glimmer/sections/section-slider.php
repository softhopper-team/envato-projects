<?php
    global $glimmer;
?>	
<!-- Header
================================================== --> 		   
<header id="header" class="home">    
	<div class="header-holder">
		<div class="container hidden-phone">
			<div class="row">
				<div class="col-lg-12">
					<div class="heder-menu">

						<div class="brow">
							<div class="brick-one transparent"></div>
						</div> <!-- /.brow --> 

						<div class="brow">
							<div class="brick-one thumb head-left wow headerInUp" data-wow-delay="2s" data-wow-duration="1.5s">
								<div class="nav-item">
									<img src="<?php if (isset($glimmer['section_header_nav_slider_img_one']['url'])) echo $glimmer['section_header_nav_slider_img_one']['url'];
                            ?>" alt="<?php if (isset($glimmer['section_header_nav_slider_img_one']['url'])) echo $glimmer['section_header_nav_slider_img_one']['url'];
                            ?>">
								</div> <!-- /.nav-item -->
							</div> <!-- /.brick-one -->

							<div class="brick-one odd wow headerInUp" data-wow-delay="1s" data-wow-duration="1.2s">
								<a href="<?php if (isset($glimmer['section_header_nav_top_icon_url'])) echo $glimmer['section_header_nav_top_icon_url'];
                            ?>" class="nav-item go-service" data-toggle="tooltip" data-placement="top" title="<?php if (isset($glimmer['section_header_nav_top_icon_tooltip'])) echo $glimmer['section_header_nav_top_icon_tooltip'];
                            ?>">
									<div class="nav-hover"></div>
									<i class="<?php if (isset($glimmer['section_header_nav_top_icon'])) echo $glimmer['section_header_nav_top_icon'];
                            ?>"></i>
								</a>
							</div> <!-- /.brick-one -->
						</div> <!-- /.brow --> 
						
						<div class="brow">
							<div class="brick-one odd wow headerInRight" data-wow-delay="2s" data-wow-duration="1.5s">
								<a href="<?php if (isset($glimmer['section_header_nav_left_icon_url'])) echo $glimmer['section_header_nav_left_icon_url'];
                            ?>" class="nav-item go-about" data-toggle="tooltip" data-placement="top" title="<?php if (isset($glimmer['section_header_nav_left_icon_tooltip'])) echo $glimmer['section_header_nav_left_icon_tooltip'];
                            ?>">
									<div class="nav-hover"></div>
									<i class="<?php if (isset($glimmer['section_header_nav_left_icon'])) echo $glimmer['section_header_nav_left_icon'];
                            ?>"></i>
								</a>
							</div> <!-- /.brick-one -->

							<div class="brick-one blank wow headerInRight" data-wow-delay="1s" data-wow-duration="1.2s">
								<div class="nav-item"></div>
							</div> <!-- /.brick-one blank -->


							<div class="brick-one thumb wow headerIn" data-wow-delay="0.5s" data-wow-duration="0.5s">
								<div class="nav-item">
									<img src="<?php if (isset($glimmer['section_header_nav_slider_img_two']['url'])) echo $glimmer['section_header_nav_slider_img_two']['url'];
                            ?>" alt="<?php if (isset($glimmer['section_header_nav_slider_img_two']['url'])) echo $glimmer['section_header_nav_slider_img_two']['url'];
                            ?>">
								</div>
							</div><!-- /.brick-one thumb -->

							<div class="brick-one odd wow headerInLeft" data-wow-delay="1s" data-wow-duration="1.2s">
								<a href="<?php if (isset($glimmer['section_header_nav_right_icon_url'])) echo $glimmer['section_header_nav_right_icon_url'];
                                ?>" class="nav-item go-portfolio" data-toggle="tooltip" data-placement="top" title="<?php if (isset($glimmer['section_header_nav_right_icon_tooltip'])) echo $glimmer['section_header_nav_right_icon_tooltip'];
                                ?>">
										<div class="nav-hover"></div>
										<i class="<?php if (isset($glimmer['section_header_nav_right_icon'])) echo $glimmer['section_header_nav_right_icon'];
                                ?>"></i>
								</a>
							</div> <!-- /.brick-one odd -->
							
							<div class="brick-one blank wow headerInLeft" data-wow-delay="2s" data-wow-duration="1.5s">
								<div class="nav-item"></div>
							</div> <!-- /.brick-one blank -->

						</div> <!-- /.brow --> 

						<div class="brow">
							<div class="brick-one head-left thumb wow headerInDown" data-wow-delay="2s" data-wow-duration="1.5s">
								<div class="nav-item">
									<img src="<?php if (isset($glimmer['section_header_nav_slider_img_three']['url'])) echo $glimmer['section_header_nav_slider_img_three']['url'];
                            ?>" alt="<?php if (isset($glimmer['section_header_nav_slider_img_three']['url'])) echo $glimmer['section_header_nav_slider_img_three']['url'];
                            ?>">
								</div> <!-- /.nav-item -->
							</div> <!-- /.brick-one thumb -->

							<div class="brick-one odd wow headerInDown" data-wow-delay="1s" data-wow-duration="1.2s">
								<a href="<?php if (isset($glimmer['section_header_nav_bottom_icon_url'])) echo $glimmer['section_header_nav_bottom_icon_url'];
                                    ?>" class="nav-item go-contact" data-toggle="tooltip" data-placement="bottom" title="<?php if (isset($glimmer['section_header_nav_bottom_icon_tooltip'])) echo $glimmer['section_header_nav_bottom_icon_tooltip'];
                                    ?>">
											<div class="nav-hover"></div>
											<i class="<?php if (isset($glimmer['section_header_nav_bottom_icon'])) echo $glimmer['section_header_nav_bottom_icon'];
                                    ?>"></i>
								</a>
							</div> <!-- /.brick-one odd -->

							<div class="brick-one blank wow headerInDown" data-wow-delay="2s" data-wow-duration="1.5s">
								<div class="nav-item"></div>
							</div> <!-- /.brick-one blank -->

						</div><!-- /.brow -->

						<div class="brow">
							<div class="brick-one transparent"></div>
						</div> <!-- /.brow -->

					</div> <!-- /.header-menu -->

				</div> <!-- /.col-lg-12 -->
			</div> <!-- /.row -->

		</div> <!-- /.container  hidden-phone -->
		
		<div class="container visible-phone">
			<div class="row">
				<div class="col-lg-12">
					<div class="header-menu">
						<div class="brow">
							<div class="brick-one transparent"></div>
						</div> <!-- /.brow --> 
						<div class="brow">
							<div class="brick-one thumb head-left wow headerInUp" data-wow-delay="2s">
								<div class="nav-item">
									<img src="<?php if (isset($glimmer['section_header_nav_slider_img_one']['url'])) echo $glimmer['section_header_nav_slider_img_one']['url'];
                                    ?>" alt="<?php if (isset($glimmer['section_header_nav_slider_img_one']['url'])) echo $glimmer['section_header_nav_slider_img_one']['url'];
                                    ?>">
								</div> <!-- /.nav-item -->
							</div> <!-- /.brick-one thumb -->

							<div class="brick-one odd wow headerInUp" data-wow-delay="1s">
								<a href="<?php if (isset($glimmer['section_header_nav_left_icon_url'])) echo $glimmer['section_header_nav_left_icon_url'];
                                    ?>" class="nav-item go-about" data-toggle="tooltip" data-placement="top" title="<?php if (isset($glimmer['section_header_nav_left_icon_tooltip'])) echo $glimmer['section_header_nav_left_icon_tooltip'];
                                    ?>">
											<div class="nav-hover"></div>
											<i class="<?php if (isset($glimmer['section_header_nav_left_icon'])) echo $glimmer['section_header_nav_left_icon'];
                                    ?>"></i>
								</a>
							</div> <!-- /.brick-one odd -->

						</div> <!-- /.brow --> 

						<div class="brow">
							<div class="brick-one odd head-left wow headerInLeft" data-wow-delay="1s">
								<a href="<?php if (isset($glimmer['section_header_nav_right_icon_url'])) echo $glimmer['section_header_nav_right_icon_url'];
                                    ?>" class="nav-item go-portfolio" data-toggle="tooltip" data-placement="top" title="<?php if (isset($glimmer['section_header_nav_right_icon_tooltip'])) echo $glimmer['section_header_nav_right_icon_tooltip'];
                                    ?>">
											<div class="nav-hover"></div>
											<i class="<?php if (isset($glimmer['section_header_nav_right_icon'])) echo $glimmer['section_header_nav_right_icon'];
                                    ?>"></i>
								</a>
							</div> <!-- /.brick-one odd -->

							<div class="brick-one thumb wow headerIn" data-wow-delay="0.2s">
								<div class="nav-item">
									<img src="<?php if (isset($glimmer['section_header_nav_slider_img_two']['url'])) echo $glimmer['section_header_nav_slider_img_two']['url'];
                                    ?>" alt="<?php if (isset($glimmer['section_header_nav_slider_img_two']['url'])) echo $glimmer['section_header_nav_slider_img_two']['url'];
                                    ?>">
								</div> <!-- /.nav-item -->
							</div> <!-- /.brick-one thumb -->

						</div> <!-- /.brow --> 

						<div class="brow">
							<div class="brick-one head-left thumb wow headerInDown" data-wow-delay="2s">
								<div class="nav-item">
									<img src="<?php if (isset($glimmer['section_header_nav_slider_img_three']['url'])) echo $glimmer['section_header_nav_slider_img_three']['url'];
                                    ?>" alt="<?php if (isset($glimmer['section_header_nav_slider_img_three']['url'])) echo $glimmer['section_header_nav_slider_img_three']['url'];
                                    ?>">
								</div> <!-- /.nav-item -->
							</div> <!-- /.brick-one thumb -->

							<div class="brick-one odd wow headerInDown" data-wow-delay="1s">
								<a href="<?php if (isset($glimmer['section_header_nav_bottom_icon_url'])) echo $glimmer['section_header_nav_bottom_icon_url'];
                                ?>" class="nav-item go-contact" data-toggle="tooltip" data-placement="bottom" title="<?php if (isset($glimmer['section_header_nav_bottom_icon_tooltip'])) echo $glimmer['section_header_nav_bottom_icon_tooltip'];
                                ?>">
										<div class="nav-hover"></div>
										<i class="<?php if (isset($glimmer['section_header_nav_bottom_icon'])) echo $glimmer['section_header_nav_bottom_icon'];
                                ?>"></i>
									</a>
							</div> <!-- /.brick-one odd -->

						</div><!-- /.brow -->

						<div class="brow">
							<div class="brick-one transparent"></div>
						</div> <!-- /.brow -->

					</div> <!-- /.header-menu -->

				</div> <!-- /.col-lg-12 -->
			</div> <!-- /.row -->
		</div> <!-- /.container visible-phone -->


	</div><!-- /.header-holder -->
</header> <!--/.home -->
		