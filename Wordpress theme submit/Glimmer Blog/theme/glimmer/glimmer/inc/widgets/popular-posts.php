<?php
class Glimmer_Popular_Posts extends WP_Widget {

	function __construct() {
		$params = array (
			'description' => 'Glimmer : Popular Posts',
			'name' => 'Glimmer : Popular Posts'
		);
		parent::__construct('Glimmer_Popular_Posts','',$params);
	}

	public function form( $instance) {
		extract($instance);
		?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','glimmer'); ?></label>
			<input
				class="widefat"
				type="text"
				id="<?php echo $this->get_field_id('title'); ?>"
				name="<?php echo $this->get_field_name('title'); ?>"
				value="<?php if( isset($title) ) echo esc_attr($title); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('glimmer_popular_post_limit'); ?>"><?php _e('Number of posts to show:','glimmer'); ?></label>
			<input 
				id="<?php echo $this->get_field_id('glimmer_popular_post_limit'); ?>" 
				type="text" 
				name="<?php echo $this->get_field_name('glimmer_popular_post_limit'); ?>"
				value="<?php if( isset($glimmer_popular_post_limit) ) echo esc_attr($glimmer_popular_post_limit); ?>"
				size="3" />
		</p>
		<?php
	} // end form function

	function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        //Strip tags from title and name to remove HTML
        $instance['title'] = strip_tags( $new_instance['title'] );
        $instance['glimmer_popular_post_limit'] = intval( $new_instance['glimmer_popular_post_limit'] );
     
        return $instance;
    }

	public function widget($args, $instance) {
		extract($args);
		extract($instance);
		$title = apply_filters('widget_title',$title);
		$glimmer_popular_post_limit = apply_filters('widget_glimmer_popular_post_limit',$glimmer_popular_post_limit);
		if ( empty($glimmer_popular_post_limit) ) $glimmer_popular_post_limit = 5;

		echo $before_widget;
			if ( !empty( $title ) ) {
				echo $before_title . $title . $after_title;
			}
			?>			
				<?php 
					$glimmer_popular_post = new WP_Query( array( 'posts_per_page' => $glimmer_popular_post_limit, 'meta_key' => 'softhopper_post_views_count', 'orderby' => 'meta_value', 'order' => 'DESC'  ) );
				?>
				<div class="popular-widget">
                    <ul class="popular-newsfeed">
						<?php while ( $glimmer_popular_post->have_posts() ) : $glimmer_popular_post->the_post(); ?>
						<li class="popular-wrap">
	                        <div class="popular-item">          
	                            <div class="popular-image">
		                            <?php
								    	$meta = get_post_meta( get_the_ID() );
		                            	$post_format = get_post_format();
										if ( false === $post_format ) {
											$post_format = "standard";
										}
										
										if ( $post_format == "gallery" ) {	
											// show first image of gallery post
				                            if ( isset ( $meta["_glimmer_format_gallery"][0] ) ) {
				                                $imgs_urls = $meta["_glimmer_format_gallery"][0];  
				                                $imgs_url = explode( '"', $imgs_urls );
			                                	?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php if(isset($imgs_url[1])) echo $imgs_url[1]; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php                           
				                            }  else {
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/gallery.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                            }
								        } elseif ( $post_format == "audio" ){
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/audio.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                        } elseif ( $post_format == "video" ){
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/video.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                        } elseif ( $post_format == "quote" ){
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/quote.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                        } elseif ( $post_format == "aside" ){
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/aside.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                        } elseif ( $post_format == "chat" ){
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/chat.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                        } elseif ( $post_format == "link" ){
			                        		if( $meta["_glimmer_format_link_bg_img"][0] ) {
			                        			?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo $meta["_glimmer_format_link_bg_img"][0]; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
			                        		} else {
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/link.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
			                                }
				                        } elseif ( $post_format == "status" ){
				                                ?>
			                                	<a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <img class="popular-item-thumb" src="<?php echo THEME_URL.'/images/post/no-media/status.jpg'; ?>" alt="Thumb"> 
						                            </figure>
						                        </a>                          	
			                                	<?php 
				                        } else {
					                        if ( has_post_thumbnail() ) {
					                            ?>
						                        <a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                                <?php
						                                	the_post_thumbnail('popular-post', array( 'class' => "popular-item-thumb", 'alt' => get_the_title()));
						                                ?>
						                            </figure>
						                        </a>
					                            <?php
					                        } else {
					                        ?>
						                        <a href="<?php the_permalink(); ?>">
						                            <figure class="fit-img">
						                               <img class="popular-item-thumb" src="<?php echo THEME_URL; ?>/images/post/no-media/image.jpg" alt="No Thumb" />
						                            </figure>
						                        </a>
					                        <?php
					                        } //end else
					                    } //end else
				                    ?>  
	                            </div> <!-- /.popular-image -->   

	                            <div class="popular-item-text">
	                                <h4><a href="<?php the_permalink(); ?>"><?php echo custom_post_excerpt( get_the_title(), 70, '&hellip;'); ?></a></h4>
	                                <span class="popular-item-meta"><?php the_time( 'j M, Y' ); ?></span>,
	                                <span class="popular-item-meta"><?php echo softhopper_get_post_views(get_the_ID()); ?></span>
	                            </div> <!-- /.popular-item-text -->
	                        </div>  <!-- /.popular-item --> 
                    	</li> <!-- /.popular-wrap -->
		                <?php
							endwhile;
						?>
					</ul> <!-- /.popular-newsfeed -->
                </div> <!-- /.popular-widget -->  
			<?php
		echo $after_widget;
	}
}