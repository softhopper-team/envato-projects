<?php
/**
 * The template for displaying quote post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Candid
 */
?>
<?php              
	$meta = get_post_meta( $post->ID );
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('format-quote'); ?>>
    <div class="entry-content">
        <p>
        <?php              
			if( isset ( $meta["_candid_format_quote"][0] ) ) echo $meta["_candid_format_quote"][0];
		?>
		</p>
        <?php
        if ( isset ( $meta["_candid_format_quote_author"][0] ) ) :
            ?>
            <span>
            	<?php if( isset ( $meta["_candid_format_quote_url"][0] ) ) {
            		?>
            			<a href="<?php if( isset ( $meta["_candid_format_quote_url"][0] ) ) echo $meta["_candid_format_quote_url"][0]; ?>"><?php if( isset ( $meta["_candid_format_quote_author"][0] ) ) echo $meta["_candid_format_quote_author"][0]; ?></a> 
            		<?php
        		} else {
            		?>
            		<?php if( isset ( $meta["_candid_format_quote_author"][0] ) ) echo $meta["_candid_format_quote_author"][0]; ?>
            		<?php
        		} ?>        	
            </span>
            <?php
            endif;
        ?>
    </div><!-- .entry-content --> 
</article> <!-- /.post format-quote -->