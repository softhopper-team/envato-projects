<?php
/**
 * The template for displaying chat post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="ribbon"><span>'.__('Sticky', 'glimmer').'</span></div>';
    	}
    ?>
    <script type="text/javascript">
		jQuery( window ).load(function() {
			jQuery('.format-chat .chat-text p:contains("more-link")').parent().parent().css('display', 'none');  
		});	
    </script>
	<header class="entry-header">
		
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php glimmer_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

		<?php 		
			if ( is_single() ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
			} else {
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
			}
		?>
	</header>

	<?php
		if ( has_post_thumbnail() ) {
	    ?>
	    <figure class="post-thumb">
			<?php
	          	the_post_thumbnail('single-full', array( 'class' => " img-responsive", 'alt' => get_the_title()));
	        ?>
		</figure> <!-- /.post-thumb -->
	    <?php
	    } else {
	    	echo "<hr>";
	    }
	?>
	
	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
			} else {
				the_content(); 
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
	<?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->