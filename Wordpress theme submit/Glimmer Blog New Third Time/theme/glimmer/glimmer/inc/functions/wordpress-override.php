<?php
/*
* This template for override wordpress some function to match the theme
*/

// change read more link of Wordpress excerpt
/*add_action( 'the_content_more_link', 'glimmer_add_morelink_class', 10, 2 );
function glimmer_add_morelink_class( $link, $text )
{
    return str_replace(
        'more-link',
        'more-link text-center',
        $link
    );
}*/

// remove parentheses from category list and add span class to count
add_filter('wp_list_categories','glimmer_categories_postcount_filter');
function glimmer_categories_postcount_filter ($args) {
    $args = str_replace('(', '<span class="count"> ', $args);
    $args = str_replace(')', ' </span>', $args);
   return $args;
}

// remove parentheses from archive list and add span class to count
add_filter('get_archives_link', 'glimmer_archive_count_no_brackets');
function glimmer_archive_count_no_brackets($links) {
    $links = str_replace('</a>&nbsp;(', '</a> <span class="count">', $links);
    $links = str_replace(')', ' </span>', $links);
    return $links;
}

/* remove redux framework menu under the tools */
add_action( 'admin_menu', 'glimmer_remove_redux_menu', 12 );
function glimmer_remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}

// glimmer rewrite excerpt function
function glimmer_wp_new_excerpt($text)
{
    global $glimmer;
    if ($text == '')
    {
        $text = get_the_content('');
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]>', $text);
        //$text = strip_tags($text);
        $text = nl2br($text);
        $excerpt_length = apply_filters('excerpt_length', $glimmer['post_excerpt']);
        $words = explode(' ', $text, $excerpt_length + 1);
        if (count($words) > $excerpt_length) {
            array_pop($words);
            array_push($words, __( '&hellip;<a href="'.get_the_permalink().'" class="more-link">'.__( '<span>Continue Reading</span>',  'glimmer').'</a>', 'glimmer'));
            $text = implode(' ', $words);
        }
    }
    return $text;
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'glimmer_wp_new_excerpt');

// change glimmer content read  more link
function glimmer_content_more($more) {
    global $post;
    if ( is_tax( 'post_format', 'post-format-chat' ) ) {
    return __( '<a href="'.get_the_permalink().'" class="more-link">'.__( '<span>Continue Reading</span>',  'glimmer').'</a>', 'glimmer');
    } else {
       return __( '&hellip;<a href="'.get_the_permalink().'" class="more-link">'.__( '<span>Continue Reading</span>',  'glimmer').'</a>', 'glimmer'); 
    }
}   
add_filter( 'the_content_more_link', 'glimmer_content_more' );

// Support shortcodes in text widgets
add_filter( 'widget_text', 'do_shortcode' );

// custom post excerpt
function custom_post_excerpt($string, $length, $dots = "&hellip;") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

// to get current template name
add_filter( 'template_include', 'glimmer_var_template_include', 1000 );
function glimmer_var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function glimmer_get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}

// this function for fix html5 error for wordpress mediaelement link, Add property attribute in mediaelement link tag
function glimmer_html5_add_property_attribute( $tag, $handle ) {
    if ( strpos( $tag, 'mediaelement' ) !== FALSE ) {
        $tag = str_replace( "/>", "property='stylesheet' />", $tag );
    }
    return $tag;
}
add_filter( 'style_loader_tag', 'glimmer_html5_add_property_attribute', 10, 2 );

// remove unnecessary p and br tag from shortcode
if( !function_exists('glimmer_fix_shortcodes') ) {
    function glimmer_fix_shortcodes($content){
        $array = array (
            '<p>[' => '[',
            ']</p>' => ']',
            ']<br />' => ']'
        );
        $content = strtr($content, $array);
        return $content;   
    }
    add_filter('the_content', 'glimmer_fix_shortcodes');
}
