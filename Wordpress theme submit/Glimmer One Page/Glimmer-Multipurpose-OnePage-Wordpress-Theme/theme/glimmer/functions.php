<?php
/**
 * glimmer functions and definitions
 *
 * @package glimmer
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}
//Inlcude the Redux theme options framework extension
if ( !class_exists( 'glimmer_redux_register_custom_extension_loader' ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/redux-extensions-loader/loader.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/redux-extensions-loader/loader.php' );
}
//Inlcude the Redux theme options framework
if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/ReduxFramework/admin-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/ReduxFramework/admin-config.php' );
}
/**
 * Include the TGM_Plugin_Activation class.
 */
require_once dirname( __FILE__ ) . '/libs/tgm-plugin-activation/tgm-admin-config.php';



if ( ! function_exists( 'glimmer_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function glimmer_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on glimmer, use a find and replace
	 * to change 'glimmer' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'glimmer', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/* Featured image support*/
	add_theme_support('post-thumbnails');

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __('Primary Menu', 'glimmer'),
		'blog-menu' => __( 'Blog Menu', 'glimmer' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
	//add_image_size("single-full", "100", "100", true);
	add_image_size("popular-post-img", "100", "100", true);

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'audio','video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'glimmer_custom_background_args', array(
		'default-color' => '#fff',
		'default-image' => '',
	) ) );

	// Add section to the wordpress option panel
    update_option("glimmer_sections",glimmer_get_all_sections());
}
endif; // glimmer_setup
add_action( 'after_setup_theme', 'glimmer_setup' );


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
require get_template_directory() . '/inc/register-widget.php';
/**
 * Enqueue scripts and styles.
 */
require get_template_directory() . '/inc/enqueue-function.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Register Custom Navigation Walker
 */
require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

/**
 * Include CMB Meta boxes
 */
require get_template_directory() . '/libs/cmb2/cmb-admin-config.php';

/**
 * Query function to get post
 */
require get_template_directory() . '/inc/query-function-for-post.php';

/**
 * Glimmer Sections
 */
require get_template_directory() . '/inc/glimmer-sections.php';

/**
 * Custom Post
 */
require get_template_directory() . '/inc/custom-post.php';

/**
 * Active Js plugin function
 */
require get_template_directory() . '/inc/active-js-plugin-functions.php';

/**
 * Wordpress comment seciton override 
 */
require get_template_directory() . '/inc/wp-comment-section-override.php';

/**
 * Wordpress comment seciton override 
 */
require get_template_directory() . '/inc/wordpress-popular-post.php';

/**
 * Glimmer custom widget 
 */
require get_template_directory() . '/inc/glimmer-custom-widget.php';

/**
 * Active Js plugin function
 */
require get_template_directory() . '/inc/glimmer-override-css.php';

require get_template_directory() . '/inc/glimmer-color-scheme.php';




/*
* Add Favicon and some condition script in html head section
*/
function glimmer_add_custom_code_for_head_section(){ ?>
    <!-- Custom Favicons -->
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.png">
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/images/apple-touch-icon-114x114.png"> 
	<!--[if lt IE 9]>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/respond.min.js"></script>
	<![endif]--> 
    <?php }
add_action('wp_head','glimmer_add_custom_code_for_head_section',5);

// change read more link of wordpress excerpt
function glimmer_excerpt_readmore($more) {
        return '...<a href="'. get_permalink() . '" class="readmore">' .__('Read More','glimmer-admin'). '</a>';
}
add_filter('excerpt_more', 'glimmer_excerpt_readmore');

// Add extra social link field in profile page
function glimmer_new_contactmethods( $methods ) {
	$methods['facebook'] = __('Facebook','glimmer-admin');
	$methods['twitter'] = __('Twitter','glimmer-admin');
	return $methods;
}
add_filter('user_contactmethods','glimmer_new_contactmethods',10,1);

// to get current template name
add_filter( 'template_include', 'var_template_include', 1000 );
function var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}