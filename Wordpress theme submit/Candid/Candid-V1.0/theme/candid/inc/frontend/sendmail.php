<?php
    require_once('../../../../../wp-load.php'); 
    global $candid;
    if (isset($candid['contact_receiver'])) {
        $contact_receiver_email = $candid['contact_receiver'];
    }
    if (isset($candid['contact_success_message'])) {
        $contact_success_message = $candid['contact_success_message'];
    }
                                
    header('Content-type: application/json');
    $status = array(
        'type'=>'success',
        'message'=> $contact_success_message
    );

    $name = trim(stripslashes($_POST['name'])); 
    $email = trim(stripslashes($_POST['email'])); 
    $subject = trim(stripslashes($_POST['subject'])); 
    $message = trim(stripslashes($_POST['message'])); 

    $email_from = $email;
    $email_to = $contact_receiver_email;//replace with your email

    $body = 'Name: ' . $name . "\n\n" . 'Email: ' . $email . "\n\n" . 'Subject: ' . $subject . "\n\n" . 'Message: ' . $message;

    $success = mail($email_to, $subject, $body, 'From: <'.$email_from.'>');

    echo json_encode($status);
    die;