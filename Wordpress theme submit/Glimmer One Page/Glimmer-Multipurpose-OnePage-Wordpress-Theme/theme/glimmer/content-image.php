<?php
/**
 * The template for displaying image post formats
 *
 *
 * @package WordPress
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-content" >  	
    	<?php
            if ( has_post_thumbnail() ) {
                ?>
                	<figure class="blog-image"> 
			            <?php
		                  	the_post_thumbnail('single-full', array( 'class' => "img-responsive", 'alt' => get_the_title()));
			            ?>
			        </figure> <!-- /.blog-image -->
                <?php
            }
        ?>
        
        <div class="blog-details clearfix">
                <?php the_title( sprintf( '<h2 class="blog-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?> 
        </div> <!-- /.blog-details-full -->
    </div><!-- /.blog-content -->
</article> <!-- /.blog-content -->

<?php 
	if(is_single()) {
		get_template_part( 'partials/content', 'authorinfo' ); 
	}
?>