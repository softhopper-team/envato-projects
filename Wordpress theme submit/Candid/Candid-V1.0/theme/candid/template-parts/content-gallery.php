<?php
/**
 * The template for displaying gallery post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Candid
 */
?>
<?php 
	global $candid;
	$category = get_the_category();  
	$img_rul = wp_get_attachment_url( get_post_thumbnail_id( $post->ID ) ); 
?> 
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php
        //if ( has_post_thumbnail() ) {
            ?>
            <div class="post-thumbnail">
            	<?php              
            		$meta = get_post_meta( $post->ID );
                	if( isset ( $meta["_candid_format_gallery"][0] ) ) {
                	?>
	                    <div class="owl-carousel full-view">
	                        <?php              
	                            if ( isset ( $meta["_candid_format_gallery"][0] ) ) {
	                                $imgs_urls = $meta["_candid_format_gallery"][0];                            
	                            }  else {
	                                $imgs_urls = '';
	                            }        
	                            $imgs_url = explode( '"', $imgs_urls );
	                            for ( $x = 0; $x < count ( $imgs_url ); $x++ ) {                            
	                                if($x % 2 != 0) {		                                
		                                	?>
		                                	<div class="item">
	                                       		<img src="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>" alt="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>">    
	                                   		</div>
		                                	<?php		                                
	                                } // end if
	                            } // end for 
	                        ?>  
	                     </div> <!-- /#full-view -->   
                	<?php                      
                	}  
                ?>

                <?php         
                	( isset( $meta["_candid_gallery_style"][0] ) ) ? $gallery_style = $meta["_candid_gallery_style"][0] : $gallery_style = "" ; 
                	if ( $gallery_style == "gallery-one"  ) {
                	?>
	                    <div class="owl-carousel list-view">
	                        <?php              
	                            if ( isset ( $meta["_candid_format_gallery"][0] ) ) {
	                                $imgs_urls = $meta["_candid_format_gallery"][0];                            
	                            }  else {
	                                $imgs_urls = '';
	                            }        
	                            $imgs_url = explode( '"', $imgs_urls );
	                            for ( $x = 0; $x < count ( $imgs_url ); $x++ ) {
	                            	if($x % 2 != 0) {
	                                	?>
	                                	<div class="item">
                                       		<img src="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>" alt="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>"> 
                                   		</div>
	                                	<?php                  		
	                                } // end if
	                            } // end for
	                        ?>  
	                     </div> <!-- /#full-view -->   
                	<?php                      
                	}  // end if;
                ?>

        	</div> <!-- /.post-thumbnail -->
            <?php
        //}
    ?>

    <header class="entry-header">
		<?php 		
			if ( is_single() ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
			} else {
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
			}
		?>		
		<div class="entry-meta">
			<?php candid_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

    <div class="entry-content">
		
		<?php 
			the_content( __('<span class="read-button">Continue reading</span>', 'candid') ); 
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'candid' ),
				'after'  => '</div>',
			) );
		?>
		<?php 
			candid_entry_footer();
		?>
	</div><!-- .entry-content -->
</article> <!-- /.post -->