<div class="author-info">
    <div class="media">
        <div class="pull-left">
            <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>">
                <?php echo get_avatar( get_the_author_meta('email') , 86 ); ?>
            </a>
        </div>

        <div class="media-body">
        	<?php
        		$author_name = sprintf(
				_x( 'Written By %s', 'post author', 'glimmer' ),
				'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
				);
			?>
            <h4 class="media-heading"><?php echo $author_name; ?></h4>

            <p>
            	<?php the_author_meta('description'); ?>
            </p>
            <div class="author-social">
            	<?php
            		$fb_url = get_the_author_meta( 'facebook' );
					if ( $fb_url && $fb_url != '' ) {
						echo '<a class="fa fa-facebook" href="' . esc_url($fb_url) . '"></a>';
					}
					$twitter_url = get_the_author_meta( 'twitter' );
					if ( $twitter_url && $twitter_url != '' ) {
						echo '<a class="fa fa-twitter" href="' . esc_url($twitter_url) . '"></a>';
					}
            	?>
            </div> <!-- /.author-social -->
        </div> <!-- /.media-body -->
    </div> <!-- /.media -->

    <hr> <!-- /.hr -->

    <nav>
        <ul class="pager">
            <?php
                if( is_page() ) {
            ?>
                <li class="previous"> 
                <?php previous_post_link('%link', __('<span aria-hidden="true">&larr;</span>Previous Page','glimmer')); ?>
                </li>
                <li class="next">
                <?php next_post_link('%link', __('Next Page<span aria-hidden="true">&rarr;</span>','glimmer')); ?>
                </li>
            <?php
                } else {
            ?>
                <li class="previous"> 
                <?php previous_post_link('%link', __('<span aria-hidden="true">&larr;</span>Previous Post','glimmer')); ?>
                </li>
                <li class="next">
                <?php next_post_link('%link', __('Next Post<span aria-hidden="true">&rarr;</span>','glimmer')); ?>
                </li>
            <?php
                }
            ?>
            
        </ul> <!-- /.pager -->
    </nav>     
</div> <!-- /.author-info -->