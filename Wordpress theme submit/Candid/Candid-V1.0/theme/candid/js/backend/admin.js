
var image_field;
jQuery( document ).ready( function($) {
	// redux framework custom js code 
	jQuery('#candid-candid_hidden_slides').parent().parent('tr').addClass( "candid-candid_hidden_slides" );
	 
	// Show/hide settings for post format when choose post format
	var $format = $( '#post-formats-select' ).find( 'input.post-format' ),
		$formatBox = $( '#_candid_post_format_details' );
		// add a link class default because in cmb2 link class not add
		$('.cmb2-id--candid-format-link, .cmb2-id--candid-format-link-text').addClass('link');

	$format.on( 'change', function() {
		var	type = $format.filter( ':checked' ).val();

		$formatBox.hide();
		if( $formatBox.find( '.cmb-row' ).hasClass( type ) ) {
			$formatBox.show();
		}

		$formatBox.find( '.cmb-row' ).slideUp();
		$formatBox.find( '.' + type ).slideDown();
	} );
	$format.filter( ':checked' ).trigger( 'change' );

	// Show/hide settings for custom layout settings
	$( '#_candid_custom_layout' ).on( 'change', function() {
		if( $( this ).is( ':checked' ) ) {
			$( '.cmb2-id--candid-layout' ).slideDown();
		}
		else {
			$( '.cmb2-id--candid-layout' ).slideUp();
		}
	} ).trigger( 'change' );
} );
