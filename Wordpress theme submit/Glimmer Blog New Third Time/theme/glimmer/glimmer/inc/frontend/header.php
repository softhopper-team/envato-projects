<?php
/**
 * Hooks for template header
 *
 * @package Glimmer
 */

/**
 * Get favicon and home screen icons
 *
 * @since  1.0
 */
function glimmer_header_icons() {
	global $glimmer;
	$favicon = $glimmer['favicon']['url'];
	$header_icons =  ( $favicon ) ? '<link rel="shortcut icon" type="image/x-ico" href="' . esc_url( $favicon ) . '" />' : '';

	$icon_iphone = $glimmer['icon_iphone']['url'];
	$header_icons .= ( $icon_iphone ) ? '<link rel="apple-touch-icon" sizes="57x57"  href="' . esc_url( $icon_iphone ) . '" />' : '';

	$icon_iphone_retina = $glimmer['icon_iphone_retina']['url'];
	$header_icons .= ( $icon_iphone_retina ) ? '<link rel="apple-touch-icon" sizes="114x114" href="' . esc_url( $icon_iphone_retina ). '" />' : '';

	$icon_ipad = $glimmer['icon_ipad']['url'];
	$header_icons .= ( $icon_ipad ) ? '<link rel="apple-touch-icon" sizes="72x72" href="' . esc_url( $icon_ipad ) . '" />' : '';

	$icon_ipad_retina = $glimmer['icon_ipad_retina']['url'];
	$header_icons .= ( $icon_ipad_retina ) ? '<link rel="apple-touch-icon" sizes="144x144" href="' . esc_url( $icon_ipad_retina ) . '" />' : '';

	echo $header_icons;
}
add_action( 'wp_head', 'glimmer_header_icons' );

/**
 * Custom scripts and styles on header
 *
 * @since  1.0
 */
function glimmer_header_scripts() {
	global $glimmer, $post;
	// Include color schemer code
	require THEME_DIR . '/inc/frontend/color-schemer.php';
	
	$custom_background = '';                          
	$custom_background = get_theme_mod( 'background_color' ); 
	if ( $custom_background == "" ) {
	    $custom_background = "#f2f2f2";
	}
	?>
	<style type="text/css"> 
		body {
			background: <?php echo $custom_background; ?>;
		}		
	</style> 						
	<?php
	// Custom CSS
	if ( isset( $post->ID ) ) $meta = get_post_meta( $post->ID );
	$inline_css='';
	isset( $meta["_glimmer_custom_css"][0] ) ? $glimmer_custom_css = $meta["_glimmer_custom_css"][0] : $glimmer_custom_css = '';
	isset( $glimmer['custom_css'] ) ? $custom_css = $glimmer['custom_css'] : $custom_css = '';
	$inline_css = $glimmer_custom_css . $custom_css;
	
	$inline_css = '<style type="text/css">' . $inline_css . '</style>';
	
	if( $inline_css ) {
		echo $inline_css;
	}	

	//header tracking code
	if( isset ( $glimmer['tracking_code_for_head'] )  && ! empty ( $glimmer['tracking_code_for_head']) ) {
        echo $glimmer['tracking_code_for_head'];
    } 
}
add_action( 'wp_head', 'glimmer_header_scripts', 300 );
