<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package Candid
 */
?>
<?php 
    get_header(); 
    global $candid;
?>
<!-- Content
================================================== -->
<main id="content" class="error-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="main">

                    <?php get_template_part( 'template-parts/content', 'searchform' ); ?>
                    
                    <div class="error-image clearfix">
                        <img src="<?php if ( isset ( $candid['404_img']['url'] ) ) echo $candid['404_img']['url']; ?>" alt="404" class="img-responsive">
                    </div> <!-- /.error-image -->
                    <div class="error-description">
                        <h2><?php if ( isset ( $candid['404_heading'] ) ) echo $candid['404_heading']; ?></h2> 
                        <h4><?php if ( isset ( $candid['404_subheading'] ) ) echo $candid['404_subheading']; ?></h4>   
                        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="go-to">
                            <span class="go-button"><?php _e( 'Go to Homepage', 'candid' ) ?></span>
                        </a>
                    </div> <!-- /.error-description -->
                </div> <!-- /#main -->
            </div> <!-- /.col-md-12 --> 
        </div> <!-- /.row --> 
    </div> <!-- /.container -->  
</main> <!-- /#content -->
<?php get_footer(); ?>