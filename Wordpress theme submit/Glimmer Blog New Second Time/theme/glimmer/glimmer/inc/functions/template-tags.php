<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Glimmer
 */

if ( ! function_exists( 'the_posts_navigation' ) ) :
/**
 * Display navigation to next/previous set of posts when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_posts_navigation() {
	// Don't print empty markup if there's only one page.
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	?>
	<nav class="navigation posts-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Posts navigation', 'glimmer' ); ?></h2>
		<div class="nav-links">

			<?php if ( get_next_posts_link() ) : ?>
			<div class="nav-previous"><?php next_posts_link( esc_html__( 'Older posts', 'glimmer' ) ); ?></div>
			<?php endif; ?>

			<?php if ( get_previous_posts_link() ) : ?>
			<div class="nav-next"><?php previous_posts_link( esc_html__( 'Newer posts', 'glimmer' ) ); ?></div>
			<?php endif; ?>

		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'the_post_navigation' ) ) :
/**
 * Display navigation to next/previous post when applicable.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 */
function the_post_navigation() {
	// Don't print empty markup if there's nowhere to navigate.
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation" role="navigation">
		<h2 class="screen-reader-text"><?php esc_html_e( 'Post navigation', 'glimmer' ); ?></h2>
		<div class="nav-links">
			<?php
				previous_post_link( '<div class="nav-previous">%link</div>', '%title' );
				next_post_link( '<div class="nav-next">%link</div>', '%title' );
			?>
		</div><!-- .nav-links -->
	</nav><!-- .navigation -->
	<?php
}
endif;

if ( ! function_exists( 'glimmer_posts_pagination_nav' ) ) :
/**
 * This is for post pagination
 */
function glimmer_posts_pagination_nav() {
	 if( is_singular() )
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<div class="navigation paging-navigation"><ul class="nav-links">' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() ) {
        printf( '<li class="nav-previous pull-left">%s</li>' . "\n", get_previous_posts_link('<span class="ico-left-angle"></span><span>'.__('Previous','glimmer').'</span>') );
    } else {
    	?>
    	<li class="nav-previous pull-left disabled">
            <a href="#"><span class="ico-left-angle"></span><span><?php _e(' No Previous','glimmer'); ?></span></a>
        </li>
    	<?php
    }


    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';

        printf( '<li%s><a class="page-numbers" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li><span class="page-numbers dots">&#46;&#46;&#46;</span></li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a class="page-numbers" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li><span class="page-numbers dots">&#46;&#46;&#46;</span></li>' . "\n";

        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a class="page-numbers curent" href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() ) {
        printf( '<li class="nav-next pull-right">%s</li>' . "\n", get_next_posts_link('<span>'.__('Next','glimmer').'</span><span class="ico-right-angle"></span>') );
    } else {
    	?>
    	<li class="nav-next pull-right disabled">
            <a href="#"><span><?php _e('No Next','glimmer'); ?></span><span class="ico-right-angle"></span>
            </a>
        </li>
    	<?php
    }

    echo '</ul></div>' . "\n";
}
endif;

if ( ! function_exists( 'glimmer_entry_header' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_entry_header() {
	?>
    <div class="entry-date">
        <?php the_time( get_option( 'date_format' ) ); ?>
    </div>
    <?php       
        if ( is_single() ) {
            the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
        } else {
            the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
        }
    ?>
    <div class="hr-line"></div>
    <div class="entry-meta">
        <span class="cat-links">
            <?php _e('In ','glimmer').the_category( ', ' ); ?>
        </span>
        <span class="devider">/</span>
        <span class="byline">
            <span class="author vcard">
                <?php _e('By ','glimmer').the_author_posts_link(); ?>
            </span>
        </span>
    </div> <!-- .entry-meta -->
    <?php
}
endif;

if ( ! function_exists( 'glimmer_page_entry_header' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function glimmer_page_entry_header() {
    ?>
    
    <?php       
        the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
    ?>
    <div class="hr-line"></div>
    <?php
}
endif;

if ( ! function_exists( 'glimmer_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function glimmer_entry_footer() {

	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		?>
        <div class="footer-meta clearfix">  
            <div class="post-comment">
                <a href="<?php comments_link(); ?>" class="comments-link">
                    <span><?php comments_number( 'No Comments', 'One Comment', '% Comments' ); ?></span>
                </a>
            </div>
            <div class="share-area">    
                <script type="text/javascript">
                      function glimmerPopupWindow(url, title, w, h) {
                          var left = (screen.width/2)-(w/2);
                          var top = (screen.height/2)-(h/2);
                          return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                        }   
                </script>                        
                <span><?php _e('Share&nbsp;:&nbsp;&nbsp;', 'glimmer'); ?></span>
                <!-- facebook share -->
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><span class="fa fa-facebook"></span></a>
                <!-- twitter share -->
                <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><span class="fa fa-twitter"></span></a>
                <!-- google plus share -->
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'google-plus-share', 550,530); return false;"><span class="fa fa-google-plus"></span></a>
                <!-- pinterest share -->
                <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="fa fa-pinterest-p"></span></a>
                <!-- linkedin share -->
                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'linkedin-share', 550,530); return false;"><span class="fa fa-linkedin"></span></a>
            </div><!-- /.share-area -->
        </div> <!-- /.footer-meta -->			
		<?php
	} // end if post type
}
endif;


if ( ! function_exists( 'glimmer_entry_footer_single' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function glimmer_entry_footer_single() {

	// Hide category and tag text for pages.
	if ( 'post' == get_post_type() ) {
		?>
		<div class="footer-meta clearfix">  
            <div class="post-comment">
                <a href="<?php comments_link(); ?>" class="comments-link">
                    <span><?php comments_number( 'No Comments', 'One Comment', '% Comments' ); ?></span>
                </a>
            </div>
            <div class="share-area">    
                <script type="text/javascript">
                      function glimmerPopupWindow(url, title, w, h) {
                          var left = (screen.width/2)-(w/2);
                          var top = (screen.height/2)-(h/2);
                          return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
                        }   
                </script>                        
                <span><?php _e('Share&nbsp;:&nbsp;&nbsp;', 'glimmer'); ?></span>
                <!-- facebook share -->
                <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><span class="fa fa-facebook"></span></a>
                <!-- twitter share -->
                <a href="https://twitter.com/home?status=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><span class="fa fa-twitter"></span></a>
                <!-- google plus share -->
                <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'google-plus-share', 550,530); return false;"><span class="fa fa-google-plus"></span></a>
                <!-- pinterest share -->
                <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><span class="fa fa-pinterest-p"></span></a>
                <!-- linkedin share -->
                <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'linkedin-share', 550,530); return false;"><span class="fa fa-linkedin"></span></a>
            </div><!-- /.share-area -->
        </div> <!-- /.footer-meta -->   
		<?php
	} // end if post type
}
endif;


if ( ! function_exists( 'glimmer_page_footer' ) ) :
/**
 * Prints HTML with meta information for page template
 */
function glimmer_page_footer() {
	?>	
	
    <div class="page-share">                            
        <script type="text/javascript">
        	  function glimmerPopupWindow(url, title, w, h) {
				  var left = (screen.width/2)-(w/2);
				  var top = (screen.height/2)-(h/2);
				  return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
				}   
        </script>                        
        <span><?php _e('Share ', 'glimmer'); ?></span>
		<!-- facebook share -->
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><i class="fa fa-facebook"></i></a>
        <!-- twitter share -->
		<a href="https://twitter.com/home?status=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'facebook-share', 580, 400); return false;"><i class="fa fa-twitter"></i></a>
		<!-- google plus share -->
		<a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'google-plus-share', 550,530); return false;"><i class="fa fa-google-plus"></i></a>
		<!-- pinterest share -->
	    <a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());"><i class="fa fa-pinterest-p"></i></a>
	    <!-- linkedin share -->
	    <a href="https://www.linkedin.com/shareArticle?mini=true&amp;url=<?php the_permalink(); ?>" onclick="glimmerPopupWindow(this.href, 'linkedin-share', 550,530); return false;"><i class="fa fa-linkedin"></i></a>
    </div><!-- /.page-share -->

	<?php
}
endif;


if ( ! function_exists( 'glimmer_archive_title' ) ) :
/**
 * Shim for `glimmer_archive_title()`.
 *
 * Display the archive title based on the queried object.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the title. Default empty.
 * @param string $after  Optional. Content to append to the title. Default empty.
 */
function glimmer_archive_title( $before = '', $after = '' ) {
	if ( is_category() ) {
		$title = sprintf( __( '<span>Browsing Category</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), single_cat_title( '', false ) );
	} elseif ( is_tag() ) {
		$title = sprintf( __( '<span>Browsing Tag</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), single_tag_title( '', false ) );
	} elseif ( is_author() ) {
		$title = sprintf( __( '<span>Browsing Author</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), '<span class="vcard">' . get_the_author() . '</span>' );
	} elseif ( is_year() ) {
		$title = sprintf( __( '<span>Browsing Year</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), get_the_date( esc_html_x( 'Y', 'yearly archives date format', 'glimmer' ) ) );
	} elseif ( is_month() ) {
		$title = sprintf( __( '<span>Browsing Month</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), get_the_date( esc_html_x( 'F Y', 'monthly archives date format', 'glimmer' ) ) );
	} elseif ( is_day() ) {
		$title = sprintf( __( '<span>Browsing Day</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), get_the_date( esc_html_x( 'F j, Y', 'daily archives date format', 'glimmer' ) ) );
	} elseif ( is_tax( 'post_format' ) ) {
		if ( is_tax( 'post_format', 'post-format-aside' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Asides', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Galleries', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Images', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Videos', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Quotes', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Links', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Statuses', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Audio', 'post format archive title', 'glimmer' );
		} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
			$title = __( '<span>Browsing Post Format</span>', 'glimmer' ).esc_html_x( 'Chats', 'post format archive title', 'glimmer' );
		}
	} elseif ( is_post_type_archive() ) {
		$title = sprintf( __( '<span>Browsing Archives</span>', 'glimmer' ).esc_html__( '%s', 'glimmer' ), post_type_archive_title( '', false ) );
	} elseif ( is_tax() ) {
		$tax = get_taxonomy( get_queried_object()->taxonomy );
		/* translators: 1: Taxonomy singular name, 2: Current taxonomy term */
		$title = sprintf( esc_html__( '%1$s: %2$s', 'glimmer' ), $tax->labels->singular_name, single_term_title( '', false ) );
	} else {
		$title = esc_html__( 'Browsing Archives', 'glimmer' );
	}

	/**
	 * Filter the archive title.
	 *
	 * @param string $title Archive title to be displayed.
	 */
	$title = apply_filters( 'get_the_archive_title', $title );

	if ( ! empty( $title ) ) {
		echo $before . $title . $after;  // WPCS: XSS OK
	}
}
endif;

if ( ! function_exists( 'the_archive_description' ) ) :
/**
 * Shim for `the_archive_description()`.
 *
 * Display category, tag, or term description.
 *
 * @todo Remove this function when WordPress 4.3 is released.
 *
 * @param string $before Optional. Content to prepend to the description. Default empty.
 * @param string $after  Optional. Content to append to the description. Default empty.
 */
function the_archive_description( $before = '', $after = '' ) {
	$description = apply_filters( 'get_the_archive_description', term_description() );

	if ( ! empty( $description ) ) {
		/**
		 * Filter the archive description.
		 *
		 * @see term_description()
		 *
		 * @param string $description Archive description to be displayed.
		 */
		echo $before . $description . $after;  // WPCS: XSS OK
	}
}
endif;

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function glimmer_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'glimmer_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'glimmer_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so glimmer_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so glimmer_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in glimmer_categorized_blog.
 */
function glimmer_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'glimmer_categories' );
}
add_action( 'edit_category', 'glimmer_category_transient_flusher' );
add_action( 'save_post',     'glimmer_category_transient_flusher' );
