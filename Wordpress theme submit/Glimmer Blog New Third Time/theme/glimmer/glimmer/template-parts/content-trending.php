<?php
/**
 * The template for displaying trending posts
 *
 * Used for index.
 *
 * @package glimmer
 */
?>
<?php 
    global $glimmer;
   
    $display_trending_post = "";
    ( isset($_GET["display_trending"]) ) ? $display_trending = $_GET["display_trending"]  : $display_trending = "" ;
    ( $display_trending == "no" ) ? $display_trending_post = false : $display_trending_post = $glimmer['trending_display'];
    if ( $display_trending_post ) : 
    if ( !is_paged() ) : 
?>
<!-- Trending Post
================================================== -->        
<div class="trending-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    if ( isset($glimmer['trending_section_title']) ) :
                ?>                    
                <h2 class="area-title">
                    <span><?php echo $glimmer['trending_section_title']; ?></span>    
                </h2>
                <?php 
                    endif;
                ?>
                <div id="trending-post" class="owl-carousel">
                    <?php
                        $glimmer_trending_post_limit = $glimmer['trending_post_per_page'];
                        $glimmer_trending_post = new WP_Query( array(
                        'posts_per_page' => $glimmer_trending_post_limit,
                        'meta_key' => 'softhopper_post_views_count', 
                        'ignore_sticky_posts' => true, 
                        'orderby' => 'meta_value_num', 
                        'order' => 'DESC'  ) );
                    ?>
                    <?php while ( $glimmer_trending_post->have_posts() ) : $glimmer_trending_post->the_post(); ?>

                    <div class="item">
                        <div class="col-sm-6">
                            <?php
                                get_template_part( 'template-parts/content', 'noimage' );
                            ?>  
                            <div class="td-overlay">
                                <div class="td-wrapper">
                                    <div class="cat-links">
                                        <?php the_category( ', ' ); ?>
                                    </div>        
                                    <?php
                                        the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
                                    ?>
                                    <div class="entry-date">
                                        <?php the_time( get_option( 'date_format' ) ); ?>
                                    </div>
                                </div>
                            </div> <!-- /.td-overlay -->  
                        </div>
                        <div class="col-sm-6 mb-none">
                            <?php
                                get_template_part( 'template-parts/content', 'noimage' );
                            ?> 
                        </div>
                    </div> <!-- /.item -->
                    <?php
                        endwhile;
                    ?>
                </div> <!-- #trending-post -->
            </div>          
        </div>
    </div>
</div> <!-- /.trending-area -->
<?php endif; endif; ?>

