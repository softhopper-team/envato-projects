<?php
/**
 * The template for displaying featured posts
 *
 * Used for index.
 *
 * @package Glimmer
 */
?>
<?php 
    global $glimmer;
    if ( $glimmer['featured_display'] == 1 ) : 
?>
<!-- Featured Area
================================================== -->
<div id="featured" class="feature-area">
    <div id="featured-header">
        <div class="container">
            <div class="featured-head-content">
                <h2 class="entry-title"><?php if (isset($glimmer['featured_title'])) echo $glimmer['featured_title']; ?></h2> <!-- /.entry-title -->
                
                <p class="featured-sub-title"><?php if (isset($glimmer['featured_description'])) echo $glimmer['featured_description']; ?></p>
            </div> <!-- /.featured-head-content -->
        </div> <!-- /.container -->
        
    </div> <!-- #featured-header -->

    <div id="featured-content">
        <div class="container-fluid">
            <div class="row">
                <div id="featured-item" class="owl-carousel">
                    <?php
                        $query = new WP_Query ( array ( 
                                    'category_name' => 'featured',
                                    'posts_per_page' => $glimmer['featured_per_page']
                                )
                            );
                        while ( $query->have_posts() ) : $query->the_post();
                    ?>            
                    <div class="item">
                        <?php                                        
                            $img_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); 
                            $alt_text = get_post_meta( get_post_thumbnail_id($post->ID), '_wp_attachment_image_alt', true);
                        ?>
                        <img class="lazyOwl" src="<?php echo get_template_directory_uri(); ?>/images/transparent.png" data-src="<?php echo $img_url; ?>" alt="<?php echo $alt_text; ?>">

                        <a href="<?php the_permalink(); ?>" class="image-extra">
                            <div class="extra-content">
                                <div class="inner-extra">
                                    <div class="entry-meta">
                                        <span class="cat-links">
                                            <?php _e( 'In ', 'glimmer' ); ?><span rel="category tag">
                                            <?php
                                                $categories = get_the_category($post->ID);
                                                foreach ($categories as $category ) {
                                                    echo $category->name.', ' ;
                                                }
                                            ?>
                                            </span>
                                        </span>
                                        <span class="entry-date">
                                            <?php the_time( get_option( 'date_format' ) ); ?>
                                        </span>
                                    </div> <!-- .entry-meta -->
                                    <h2 class="entry-title">
                                        <?php echo custom_post_excerpt( get_the_title(), 50, '&hellip;'); ?> 
                                    </h2>
                                </div> <!-- /.inner-extra -->
                            </div> <!-- /.extra-content -->
                        </a> <!-- /.image-extra -->
                    </div> <!-- /.item -->
                    <?php 
                        endwhile;
                        wp_reset_query(); 
                    ?> 
                </div> <!-- #featured-item -->                    
            </div> <!-- /.row -->
        </div> <!-- /.container-fluid -->
    </div> <!-- #featured-content -->
</div> <!-- /#featured -->
<?php endif; ?>