<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php 
    	if ( is_sticky() ) {
    		echo '<div class="ribbon"><span>'.__('Sticky', 'glimmer').'</span></div>';
    	}
    ?>
	<header class="entry-header">
		
		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php glimmer_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

		<?php 		
			if ( is_single() ) {
				the_title( sprintf( '<h2 class="entry-title">', esc_url( get_permalink() ) ), '</h2>' ); 
			} else {
				the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); 
			}
		?>
	</header>

    <?php
	 	$meta = get_post_custom($post->ID);
		$soundcloud = isset( $meta["_glimmer_format_audio_soundcloud"][0] ) ? $meta["_glimmer_format_audio_soundcloud"][0] : '';
		if( !empty( $soundcloud ) ) {
			?>
			<div class="post-media">						
			<?php
				echo glimmer_soundcloud( $soundcloud );
			?>
			</div>
			<?php
		} else { ?>
			<div class="post-media">						
			<?php
				$mp3 = isset( $meta["_glimmer_format_audio_mp3"][0] ) ? $meta["_glimmer_format_audio_mp3"][0] : '';
				$oga = isset( $meta["_glimmer_format_audio_oga"][0] ) ? $meta["_glimmer_format_audio_oga"][0] : '';
				$m4a = isset( $meta["_glimmer_format_audio_m4a"][0] ) ? $meta["_glimmer_format_audio_m4a"][0] : '';
				echo '<div class="post-audio-player">'.do_shortcode('[audio mp3="'.$mp3.'" ogg="'.$oga.'" m4a="'.$m4a.'"]').'</div>';
			?>
			</div>
		<?php
		}
	?>

	<div class="entry-content">
		<?php 
			if ( is_single() ) {
				the_content(); 
				edit_post_link( esc_html__( '(Edit Post)', 'glimmer' ), '<span class="edit-link">', '</span>' );
			} else {
				the_content();
			}
		?>  		

		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'glimmer' ),
				'after'  => '</div>',
			) );
		?>
	</div> <!-- .entry-content -->

    <footer class="entry-footer">
	<?php
		if ( is_single() ) {
			glimmer_entry_footer_single();
		} else { 
			glimmer_entry_footer(); 
		}
	?>
    </footer> <!-- .entry-footer -->
</article> <!-- /.post-->