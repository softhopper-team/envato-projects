<?php
/**
 * Load and register widgets
 *
 * @package Candid
 */

require_once THEME_DIR . '/inc/widgets/popular-posts.php';
require_once THEME_DIR . '/inc/widgets/latest-posts.php';
require_once THEME_DIR . '/inc/widgets/about-me.php';
require_once THEME_DIR . '/inc/widgets/follow-me.php';

/**
 * Register widgets
 *
 */

add_action('widgets_init','candid_register_widgets');
function candid_register_widgets() {
	register_widget('Candid_Popular_Posts');
	register_widget('Candid_Latest_Posts');
	register_widget('Candid_About_Me');
	register_widget('Candid_Follow_Me');
}