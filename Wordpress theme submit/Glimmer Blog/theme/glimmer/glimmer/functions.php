<?php
/**
 * Glimmer functions and definitions
 *
 * @package Glimmer
 */

/**
 * Define theme's constant
 */
if ( ! defined( 'THEME_VERSION' ) ) {
	define( 'THEME_VERSION', '1.0' );
}
if ( ! defined( 'THEME_DIR' ) ) {
	define( 'THEME_DIR', get_template_directory() );
}
if ( ! defined( 'THEME_URL' ) ) {
	define( 'THEME_URL', get_template_directory_uri() );
}

/**
 * Include the Redux theme options framework
 */

//Include the Redux theme options framework extension

if ( !class_exists( 'glimmer_redux_register_custom_extension_loader' ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-extensions-loader/loader.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-extensions-loader/loader.php' );
}

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/ReduxCore/framework.php' );
}

if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-framework-config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/inc/libs/redux-framework/redux-framework-config.php' );
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
if ( ! function_exists( 'glimmer_setup' ) ) :

function glimmer_setup() {
	global $glimmer;
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Glimmer, use a find and replace
	 * to change 'glimmer' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'glimmer', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main-menu' => esc_html__( 'Top Menu', 'glimmer' ),
		'footer-menu' => esc_html__( 'Footer Menu', 'glimmer' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) 
	);
	/* Define image size */
	add_image_size( 'featured-img', 555, 350, true );
	add_image_size( 'single-full', 1055, 594, true );
	add_image_size( 'popular-post', 90, 80, true );
	add_image_size( 'latest-post', 90, 80, true );
	add_image_size( 'related-posts', 370, 250, true );
	
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'image', 'gallery', 'audio', 'video', 'quote', 'link', 'aside', 'status', 'chat'
		) 
	);
        
	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'glimmer_custom_background_args', array (
		'default-color' => 'f2f2f2',
		'default-image' => '',
	) ) );

	// Create custom category when theme setup
	if (file_exists (ABSPATH.'/wp-admin/includes/taxonomy.php')) {
        require_once (ABSPATH.'/wp-admin/includes/taxonomy.php'); 
        if ( ! get_cat_ID( 'Featured' ) ) {
            wp_create_category( 'Featured' );
        }
    }
}
endif; // glimmer_setup
add_action( 'after_setup_theme', 'glimmer_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
if ( ! isset( $content_width ) ) {
	$content_width = 960; /* pixels */
}


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function glimmer_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'glimmer' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h2 class="widget-title">',
		'after_title'   => '</h2></div>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Instagram Widget Area', 'glimmer' ),
		'id'            => 'instagram',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h2 class="widget-title">',
		'after_title'   => '</h2></div>',
	) );
	// Register footer sidebars
	register_sidebars( 4, array(
		'name'          => __( 'Footer %d', 'glimmer' ),
		'id'            => 'footer-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<div class="widget-title-area"><h2 class="widget-title">',
		'after_title'   => '</h2></div>',
	) );
}
add_action( 'widgets_init', 'glimmer_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function glimmer_scripts() {
	global $wp_scripts, $glimmer;
	$protocol = is_ssl() ? 'https' : 'http';
	// enqueue style
	wp_enqueue_style('font-awesome', THEME_URL . "/fonts/font-awsome/css/font-awesome.min.css");
	wp_enqueue_style('bootstrap', THEME_URL . "/lib/bootstrap/css/bootstrap.min.css");
	wp_enqueue_style('glimmer-icon', THEME_URL . "/fonts/glimmer-icon/glimmer-icon.css");
	wp_enqueue_style('owl-carousel', THEME_URL . "/lib/owl-carousel/owl.carousel.css");
	wp_enqueue_style('owl-theme', THEME_URL . "/lib/owl-carousel/owl.theme.css");
	wp_enqueue_style( 'glimmer-style', get_stylesheet_uri() );

	// enqueue scripts
	wp_enqueue_script('modernizr', THEME_URL . '/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', false);
	wp_enqueue_script('bootstrap-js', THEME_URL . "/lib/bootstrap/js/bootstrap.min.js", array("jquery"), false, true);
	wp_enqueue_script('classie-js', THEME_URL . '/js/classie.js', array("jquery"), false, true);
	wp_enqueue_script('fitvids-js', THEME_URL . '/lib/fitvits/jquery.fitvids.js', array("jquery"), false, true);
	wp_enqueue_script('owl-carousel-js', THEME_URL . '/lib/owl-carousel/owl.carousel.min.js', array("jquery"), false, true);
	wp_enqueue_script('menu-js', THEME_URL . '/js/menu.js', array("jquery"), false, true);
	wp_enqueue_script('search-effect', THEME_URL . '/js/search-effect.js', array("jquery"), false, true);
	wp_enqueue_script('smoothscroll-js', THEME_URL . '/js/SmoothScroll.js', array("jquery"), false, true);
	wp_enqueue_script('retina-js', THEME_URL . '/js/retina.min.js', array("jquery"), false, true);

	//jquery.appear.js load only about page
	if ( glimmer_get_current_template() == "aboutme-page.php" ) {
		wp_enqueue_script('appear-js', THEME_URL . '/js/jquery.appear.js', array("jquery"), false, true);
	}
	//google map load only contact page
	if ( glimmer_get_current_template() == "contact-page.php" ) {
		wp_enqueue_script('googleapis-js', "$protocol://maps.googleapis.com/maps/api/js?sensor=true", array("jquery"), false, true);
		wp_enqueue_script('gmaps-js', THEME_URL . '/js/gmaps.js', array("jquery"), false, true);
	}
	wp_enqueue_script('glimmer-js', THEME_URL . '/js/glimmer.js', array("jquery"), false, true);
	wp_enqueue_script( 'glimmer-navigation', THEME_URL . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'glimmer-skip-link-focus-fix', THEME_URL . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( isset( $glimmer['contact_lat'] ) && isset( $glimmer['contact_lon'] ) ) {
		( $glimmer['sidebar_layout'] == 1 ) ? $owl_item = 7 : $owl_item = 5;
		( $glimmer['sidebar_layout_single'] == 1 ) ? $owl_item_related = 3 : $owl_item_related = 2;
        wp_localize_script("glimmer-js", "glimmer", array (
	        	"lat" => $glimmer['contact_lat'],
	        	"lon" => $glimmer['contact_lon'],
	        	"map_mouse_wheel" => $glimmer['map_mouse_wheel'],
	        	"map_zoom_control" => $glimmer['map_zoom_control'],
	        	"map_point_img" => $glimmer['contact_map_point_img'],
	        	"owl_item" => $owl_item,
	        	"owl_item_related" => $owl_item_related,
	        	"theme_uri" => THEME_URL 
        	)
        );
    }

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'glimmer_scripts' );

/**
 * Enqueue scripts and styles for WordPress admin panel.
 */
function glimmer_admin_panel_scripts($hook) {
	// enqueue style
	$protocol = is_ssl() ? 'https' : 'http';
	wp_enqueue_style('admin-font-awesome', THEME_URL . "/fonts/font-awsome/css/font-awesome.min.css");
	wp_enqueue_style('admin-custom', THEME_URL . "/css/backend/custom.css");
	wp_enqueue_style('redux-custom', THEME_URL . "/css/backend/redux-custom.css");	
	if( $hook == 'widgets.php' ) {
		wp_enqueue_style('thickbox');
	}

	// enqueue scripts
	wp_enqueue_script('glimmer-backend-js', THEME_URL . '/js/backend/admin.js', array("jquery"), false, true);
	if( $hook == 'widgets.php' ) {		 
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('widget-js', THEME_URL . '/js/backend/widget.js', null, null, true);
	}
}
add_action( 'admin_enqueue_scripts', 'glimmer_admin_panel_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require THEME_DIR . '/inc/functions/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require THEME_DIR . '/inc/functions/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require THEME_DIR . '/inc/functions/extras.php';

/**
 * Customizer additions.
 */
require THEME_DIR . '/inc/functions/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require THEME_DIR . '/inc/functions/jetpack.php';

/**
 * Include the TGM_Plugin_Activation class.
 */
require THEME_DIR . '/inc/libs/tgm-plugin-activation/tgm-admin-config.php';

/**
 * Configure CMB2 Meta Box
 */
require THEME_DIR . '/inc/libs/cmb2/cmb2-config.php';

/**
 * Load custom widget
 */
require THEME_DIR . '/inc/widgets/widgets.php';

/**
 * Register Custom Navigation Walker
 */
require THEME_DIR . '/inc/functions/glimmer-nav-walker.php';

/**
 * Wordpress comment seciton override 
 */
require THEME_DIR . '/inc/functions/wp-comment-section-override.php';

/**
 * Query function to get post
 */
require THEME_DIR . '/inc/functions/function-for-post.php';

/**
 * Glimmer Shortcode Function 
 */
require THEME_DIR . '/inc/functions/glimmer-shortcode.php';


/**
 * Popular Post functions
 */
require THEME_DIR . '/inc/functions/popular-post.php';

/**
 * Include breadcrumbs 
 */
require THEME_DIR . '/inc/frontend/breadcrumbs.php';

/**
 * Include header, Hooks for template header
 */
require THEME_DIR . '/inc/frontend/header.php';

/**
 * Include header, Hooks for template header
 */
require THEME_DIR . '/inc/frontend/footer.php';

/**
 * Include chat-post-modify
 */
require THEME_DIR . '/inc/functions/chat-post-modify.php';

// change read more link of Wordpress excerpt
add_action( 'the_content_more_link', 'glimmer_add_morelink_class', 10, 2 );
function glimmer_add_morelink_class( $link, $text )
{
    return str_replace(
        'more-link',
        'more-link button',
        $link
    );
}

// remove parentheses from category list and add span class to count
add_filter('wp_list_categories','glimmer_categories_postcount_filter');
function glimmer_categories_postcount_filter ($args) {
	$args = str_replace('(', '<span class="count"> ', $args);
	$args = str_replace(')', ' </span>', $args);
   return $args;
}

// remove parentheses from archive list and add span class to count
add_filter('get_archives_link', 'glimmer_archive_count_no_brackets');
function glimmer_archive_count_no_brackets($links) {
	$links = str_replace('</a>&nbsp;(', '</a> <span class="count">', $links);
	$links = str_replace(')', ' </span>', $links);
	return $links;
}

/* remove redux framework menu under the tools */
add_action( 'admin_menu', 'glimmer_remove_redux_menu', 12 );
function glimmer_remove_redux_menu() {
    remove_submenu_page('tools.php','redux-about');
}

// glimmer rewrite excerpt function
function glimmer_wp_new_excerpt($text)
{
	global $glimmer;
	if ($text == '')
	{
		$text = get_the_content('');
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]>', $text);
		$text = strip_tags($text);
		$text = nl2br($text);
		$excerpt_length = apply_filters('excerpt_length', $glimmer['post_excerpt']);
		$words = explode(' ', $text, $excerpt_length + 1);
		if (count($words) > $excerpt_length) {
			array_pop($words);
			array_push($words, __( '&hellip;', 'glimmer'));
			$text = implode(' ', $words);
		}
	}
	return $text;
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'glimmer_wp_new_excerpt');

// change glimmer content read  more link
function glimmer_content_more($more) {
   global $post;
   return __( '&hellip;', 'glimmer');
}   
add_filter( 'the_content_more_link', 'glimmer_content_more' );

// Support shortcodes in widgets
add_filter( 'widget_text', 'do_shortcode' );

// custom post excerpt
function custom_post_excerpt($string, $length, $dots = "&hellip;") {
    return (strlen($string) > $length) ? substr($string, 0, $length - strlen($dots)) . $dots : $string;
}

// to get current template name
add_filter( 'template_include', 'glimmer_var_template_include', 1000 );
function glimmer_var_template_include( $t ){
    $GLOBALS['current_theme_template'] = basename($t);
    return $t;
}

function glimmer_get_current_template( $echo = false ) {
    if( !isset( $GLOBALS['current_theme_template'] ) )
        return false;
    if( $echo )
        echo $GLOBALS['current_theme_template'];
    else
        return $GLOBALS['current_theme_template'];
}

// this function for fix html5 error for wordpress mediaelement link, Add property attribute in mediaelement link tag
function glimmer_html5_add_property_attribute( $tag, $handle ) {
	if ( strpos( $tag, 'mediaelement' ) !== FALSE ) {
		$tag = str_replace( "/>", "property='stylesheet' />", $tag );
	}
	return $tag;
}
add_filter( 'style_loader_tag', 'glimmer_html5_add_property_attribute', 10, 2 );

