
<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Candid
 */
    get_header();
    global $glimmer;
?>
    <?php get_template_part( 'template-parts/content', 'featured' ); ?> 
    <!-- Content
    ================================================== -->
	<div id="content" class="site-content">
		<div class="container">
			<div class="row">
			<?php
                /* Show sidebar with user condition */
                $columns_grid = 8;
                $columns_offset  = '';
                if ( $glimmer['sidebar_layout'] == 2 ) {
                    get_sidebar();
                } elseif ( $glimmer['sidebar_layout'] == 1 ) {
                    $columns_grid = 10;
                    $columns_offset = 'col-md-offset-1';
                }
            ?>
            <div class="<?php echo $columns_offset; ?> col-md-<?php echo $columns_grid; ?>">
					<!-- Content Area -->
					<div id="primary" class="content-area">
						<main id="main" class="site-main" role="main">								
                            <?php 
                            // don't show featured post in index page
                            global $wp_query;
                            $cat_slug = get_category_by_slug('featured'); 
                            $cat_id = $cat_slug->term_id;
                            $post_per_page = get_option('posts_per_page');
                            $wp_query = new WP_Query();
                            $wp_query->query('category__not_in='.$cat_id.'&showposts='.$post_per_page.'&paged='.$paged);

                            if ( $wp_query->have_posts() ) : ?>

                            <?php /* Start the Loop */ ?>
                            <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

                                <?php
                                    /* Include the Post-Format-specific template for the content.
                                     * If you want to override this in a child theme, then include a file
                                     * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                     */
                                    get_template_part( 'template-parts/content', get_post_format() );
                                ?>

                            <?php endwhile; ?>

                            <?php glimmer_posts_pagination_nav(); ?>

                            <?php else : ?>

                                <?php get_template_part( 'template-parts/content', 'none' ); ?>

                            <?php endif; ?>								

						</main> <!-- #main -->
					</div> <!-- #primary -->
				</div> <!-- /.col-md-8 -->
                <?php
                    /* Show sidebar with user condition */
                    if ( $glimmer['sidebar_layout'] == 3 ) {
                        get_sidebar();
                    }
                ?>						
			</div> <!-- /.row -->
		</div> <!-- /.container -->		
	</div><!-- #content -->
<?php get_footer(); ?>