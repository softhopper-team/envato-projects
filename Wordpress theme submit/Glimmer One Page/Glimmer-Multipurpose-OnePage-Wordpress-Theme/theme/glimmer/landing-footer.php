<?php
/**
 * The template for displaying the landing footer.
 *
 * @package glimmer
 */
?>
<?php
	global $glimmer;
?>	

<?php get_template_part( 'partials/content', 'footer' ); ?>
 
<?php get_footer(); ?>
		