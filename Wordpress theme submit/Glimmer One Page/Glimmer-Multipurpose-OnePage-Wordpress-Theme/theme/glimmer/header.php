<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="header">
 *
 * @package Glimmer
 */
?>
<?php
	global $glimmer;
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
	<head>
	  
		<!-- Basic Page Needs
		================================================== -->
	    <meta charset="<?php bloginfo( 'charset' ); ?>">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

		<!-- Specific Meta
		================================================== -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"> 
		<?php wp_head(); ?>		
    	<?php
		    if (isset($glimmer['custom_css'])) echo $glimmer['custom_css'];
		    if (isset($glimmer['custom_ga'])) echo $glimmer['custom_ga'];
	    ?>
    </head>

    <body <?php body_class(); ?>>

	<!-- Loader
	================================================== --> 
	<div class="pageloader">
		<div class="drawing" id="loading">
			<div class="loading-dot"></div>
		</div>
	</div>	 