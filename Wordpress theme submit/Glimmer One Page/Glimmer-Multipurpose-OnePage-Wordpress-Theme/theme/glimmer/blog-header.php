<?php
/**
 * The template for displaying blog header.
 *
 * @package Glimmer
 */
global $glimmer;
?>
<!-- Menu
================================================== --> 
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            
            <a class="navbar-brand" href="<?php echo site_url(); ?>">            
            </a>
        </div>
        
        <div class="navbar-collapse collapse" id="navbar-collapse-1">
            <!-- Right nav -->
            <?php
                wp_nav_menu( array(
                    'theme_location'    => 'blog-menu',
                    'depth'             =>  0,
                    'menu_class'        => 'nav navbar-nav navbar-right',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
                );
            ?>
        </div><!--/.nav-collapse -->
    </div> <!-- /.container -->
</div> <!-- /.nabvar -->
            
<!-- blog header
================================================== --> 
<?php
if ( !is_404() ) {
?>
<div class="blog-header" id="parallax-header">
    <div class="patern-overlay"></div>
    <div class="parallax-heading">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="heading">
                        <h2 class="title">
                            <?php echo get_bloginfo("title"); ?>
                        </h2>
                            <span class="small-border-two"></span>
                        <h5 class="subtitle"><?php echo get_bloginfo("description"); ?></h5>      
                    </div> <!-- /.heading -->

                </div><!--/.service-heading-->  
            </div> <!-- /.row -->
        </div> <!-- /.container -->                
    </div> <!-- /.parallax-heading -->
</div> <!-- /.blog-header -->
<?php  
    }
?>