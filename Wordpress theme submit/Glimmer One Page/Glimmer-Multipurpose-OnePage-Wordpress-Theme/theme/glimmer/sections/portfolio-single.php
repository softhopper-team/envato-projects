<?php 
// Include WordPress
    require_once('../../../../wp-load.php'); 
    global $glimmer;
    global $post;
    $portfolio_single = glimmer_get_custom_posts("portfolio", 1);
    $meta = get_post_meta($_GET["id"]);  
?>
<section id="single-portfolio"> 
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="portfolio-head pull-left">
                    <h2><?php
                            $post_id = get_post($_GET["id"]); 
                            echo $post_id->post_title;
                        ?>               
                    </h2>
                </div>
                <div class="cross-icon pull-right">
                    <a class="close-folio-item" href="javascript:void(0)"><span class="icon-cross"></span></a>
                </div>
            </div> <!-- /.col-lg-12 -->
        </div> <!-- /.row -->

        <div class="row">
            <div class="col-lg-7">
                <?php              
                if(isset($meta["_glimmer_portfolio_images"][0])) {
                ?>
                    <div id="project-image" class="owl-carousel">
                        <?php              
                            if(isset($meta["_glimmer_portfolio_images"][0])) {
                                $imgs_urls = $meta["_glimmer_portfolio_images"][0];                            
                            }  else {
                                $imgs_urls = '';
                            }        
                            $imgs_url = explode('"',$imgs_urls);
                            for ($x = 0; $x < count($imgs_url); $x++) {                            
                                if($x % 2 != 0) {                                
                        ?>
                                   <div class="item">
                                       <img class="img-responsive lazyOwl" data-src="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>" alt="<?php if(isset($imgs_url[$x])) echo $imgs_url[$x]; ?>">   
                                   </div>
                        <?php
                                }
                            } 
                        ?>  
                    </div> <!-- /.project-image -->  
                <?php                      
                }  else {
                ?>
                    <div id="project-video">
                        <?php 
                            if(isset($meta["_glimmer_portfolio_video"][0])) echo $meta["_glimmer_portfolio_video"][0];
                        ?>    
                    </div> <!-- /.project-image --> 
                <?php   
                }                   
                ?>

            </div> <!-- /.col-lg-7 -->

            <div class="col-lg-5">
                <div class="portfolio-details">
                    <p><?php if(isset($meta["_glimmer_portfolio_description"][0])) echo $meta["_glimmer_portfolio_description"][0]; ?></p>
                </div> <!-- /.portfolio-details --> 

                 <div class="clients-details">
                    <div class="portfolio-clients">
                        <h5><?php _e('Client Details','glimmer'); ?></h5>
                        <dl class="dl-horizontal">
                        <?php              
                            if(isset($meta["_glimmer_client_name"][0])) {
                                $client_name = $meta["_glimmer_client_name"][0];  
                                echo "<dt>".__('Client : ','glimmer')."</dt>"." <dd>$client_name</dd>";
                            } 

                            if(isset($meta["_glimmer_delivery_date"][0])) {
                                $delivery_date = $meta["_glimmer_delivery_date"][0];  
                                echo "<dt>".__('Delivery Date : ','glimmer')." </dt>"." <dd>$delivery_date</dd>";
                            } 

                            if(isset($meta["_glimmer_project_skills"][0])) {
                                $project_skills = $meta["_glimmer_project_skills"][0];  
                                echo "<dt>".__('Skills : ','glimmer')."</dt>"." <dd>$project_skills</dd>";
                            } 

                            if(isset($meta["_glimmer_project_url"][0])) {
                                $project_url = $meta["_glimmer_project_url"][0];  
                                echo "<dt>".__('URL : ','glimmer')."</dt>"."<dd><a href='$project_url'>$project_url</a></dd>";
                            }                               
                        ?>  
                        </dl>   
                    </div> <!-- /.clients-details --> 
                    
                </div> <!-- /.clients-details -->  
            </div> <!-- /.col-lg-5 -->
        </div> <!-- /.row -->

    </div> <!-- /.container -->
</section>   
