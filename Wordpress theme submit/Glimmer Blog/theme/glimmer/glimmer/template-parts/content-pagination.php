<?php
/**
 * The template for displaying previous next pagination link
 *
 * Used for both single and page.
 *
 * @package Glimmer
 */
?>
<?php if ( get_previous_post() || get_next_post() ) : ?>
<!-- pagination -->
<nav class="navigation paging-navigation-post">
    <ul class="nav-links">
    	<?php
            if( is_page() ) {
        ?>           
            <?php if ( get_previous_post() ) { ?>
            <li class="nav-previous">
                <?php previous_post_link('%link','<span class="glyphicon glyphicon-menu-left"></span><span>'.__('Previous Page', 'glimmer').'</span>'); ?>
                
            </li>  
            <?php } ?> 

            <?php if ( get_next_post() ) { ?>
            <li class="nav-next">
                <?php next_post_link('%link', '<span>'.__('Next Page', 'glimmer').'</span><span class="glyphicon glyphicon-menu-right"></span>'); ?>
            </li>
            <?php } ?> 

        <?php
            } else {
        ?>
            <?php if ( get_previous_post() ) { ?>
            <li class="nav-previous">
                <?php previous_post_link('%link','<span class="glyphicon glyphicon-menu-left"></span><span>'.__('Previous Post', 'glimmer').'</span>'); ?>
                
            </li>  
            <?php } ?> 

            <?php if ( get_next_post() ) { ?>
            <li class="nav-next">
                <?php next_post_link('%link', '<span>'.__('Next Post', 'glimmer').'</span><span class="glyphicon glyphicon-menu-right"></span>'); ?>
            </li>
            <?php } ?> 
        <?php
            }
        ?>
    </ul>
</nav> <!-- /.navigation -->
<?php endif; ?>