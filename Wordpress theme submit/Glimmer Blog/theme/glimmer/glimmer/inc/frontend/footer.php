<?php
/**
 * Hooks for template footer
 *
 * @package Glimmer
 */

/**
 * Custom scripts  on footer
 *
 * @since  1.0
 */
function glimmer_footer_scripts() {
	global $glimmer, $post;
	// Custom javascript
	if ( isset( $post->ID ) ) $meta = get_post_meta( $post->ID );
	$inline_js = '';
	isset( $meta["_glimmer_custom_js"][0] ) ? $glimmer_custom_js = $meta["_glimmer_custom_js"][0] : $glimmer_custom_js = '';
	isset( $glimmer['custom_js'] ) ? $custom_js = $glimmer['custom_js'] : $custom_js = '';
	
	$js_custom = $glimmer_custom_js. $custom_js;
	$inline_js =  '<script type="text/javascript">' . $js_custom . '</script>' ;

	if( $inline_js ) {
		echo $inline_js;
	}
	// footer tracking code
	if( isset ( $glimmer['tracking_code_for_footer'] )  && ! empty ( $glimmer['tracking_code_for_footer']) ) {
        echo $glimmer['tracking_code_for_footer'];
    } 
}
add_action( 'wp_footer', 'glimmer_footer_scripts', 200 );
