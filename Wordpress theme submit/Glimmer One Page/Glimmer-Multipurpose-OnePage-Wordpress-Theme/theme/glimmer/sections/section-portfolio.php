<?php
    global $glimmer, $post;
    $portfolios = glimmer_get_custom_posts("portfolio", 100);
?>  
<!-- portfolio
================================================== --> 
<section class="portfolio" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <header class="heading wow <?php if (isset($glimmer['section_portfolio_animation_style'])) echo $glimmer['section_portfolio_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_portfolio_animation_duration'])) echo $glimmer['section_portfolio_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_portfolio_animation_delay'])) echo $glimmer['section_portfolio_animation_delay']; ?>">
                    <h2 class="title">
                       <?php if (isset($glimmer['section_portfolio_title'])) echo $glimmer['section_portfolio_title'];
                            ?>
                    </h2>
                        <span class="small-border"></span>

                    <h5 class="subtitle"><?php if (isset($glimmer['section_portfolio_subtitle'])) echo $glimmer['section_portfolio_subtitle'];
                            ?></h5>
                </header> <!-- /.heading -->
            </div> <!-- /.col-md-12 -->
        </div> <!-- /.row -->
    </div> <!-- /.container -->

    <div class="container">

        <div class="portfolio-filter">
            <ul>
                <li><a href="#" data-filter="*" class="current">Show All</a></li>
                <?php 
                    $args = array(
                        'type'        => 'portfolio',                                
                        'taxonomy'    => 'portfolio'
                    ); 
                ?>
                <?php 
                    $categories = get_categories( $args ); 
                    foreach($categories as $category) {
                ?>                          
                    <li><a href="#" data-filter=".<?php echo $category->slug; ?>"><?php echo $category->name; ?></a></li> 
                <?php
                    }
                ?>

            </ul>
        </div> <!-- /.portfolio-filter -->

        <div class="portfolio-items base-padding-four">
            <div class="row">
                <?php
                    foreach ($portfolios as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?>                        
                <div class="<?php                                   
                                    $category_names=get_the_terms($post->ID, 'portfolio');
                                    foreach($category_names as $cat_name) {                                           
                                    echo $cat_name->slug.' ';
                                    }                         
                                ?> col-xs-12 col-sm-6 col-md-3 col-lg-3 portfolio-pad">
                    <div class="view view-thumb wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                        <div class="portfolio-image">
                            <img src="<?php if(isset($meta["_glimmer_portfolio_thumbnail"][0])) echo $meta["_glimmer_portfolio_thumbnail"][0]; ?>" alt="<?php if(isset($meta["_glimmer_portfolio_thumbnail"][0])) echo $meta["_glimmer_portfolio_thumbnail"][0]; ?>">   
                        </div>
                        <div class="mask">
                            <a class="read-more" href="#" data-single-url="<?php echo get_stylesheet_directory_uri(); ?>/sections/portfolio-single.php?id=<?php echo $post->ID; ?>" >
                                <i class="icon-details"></i>
                            </a>
                            <p>
                                <?php if(isset($meta["_glimmer_project_name"][0])) echo $meta["_glimmer_project_name"][0]; ?>
                            </p>
                        </div>
                    </div> <!-- /.view -->       
                </div> <!-- /.col-lg-3 -->
                <?php } ?>

            </div>  <!-- /.row -->   
        </div> <!-- /.portfolio-items -->

    </div> <!-- /.container -->

    <div id="portfolio-single-wrap">
        <div id="portfolio-single">

        </div> <!-- /#portfolio-single -->
    </div><!-- /#portfolio-single-wrap -->
</section> <!-- /.portfolio -->