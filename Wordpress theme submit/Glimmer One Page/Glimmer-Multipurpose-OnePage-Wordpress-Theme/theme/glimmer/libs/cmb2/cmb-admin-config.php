<?php
/**
 * Include and setup custom metaboxes and fields. (make sure you copy this file to outside the CMB directory)
 *
 * Be sure to replace all instances of 'yourprefix_' with your project's prefix.
 * http://nacin.com/2010/05/11/in-wordpress-prefix-everything/
 *
 * @category YourThemeOrPlugin
 * @package  Metaboxes
 * @license  http://www.opensource.org/licenses/gpl-license.php GPL v2.0 (or later)
 * @link     https://github.com/WebDevStudios/CMB2
 */

/**
 * Get the bootstrap! If using the plugin from wordpress.org, REMOVE THIS!
 */

if ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
} elseif ( file_exists( dirname( __FILE__ ) . '/init.php' ) ) {
	require_once dirname( __FILE__ ) . '/init.php';
}

add_action( 'cmb2_init', 'yourprefix_register_demo_metabox' );
/**
 * Hook in and add a demo metabox. Can only happen on the 'cmb2_init' hook.
 */

function yourprefix_register_demo_metabox() {

	// Start with an underscore to hide fields from custom fields list
	$prefix = '_glimmer_';

	// this array contains all animation style list
	$animation_style_list_item = array("No Animation","bounce","flash","pulse","rubberBand","shake","swing","tada","wobble","bounceIn","bounceInDown","bounceInLeft","bounceInRight","bounceInUp","bounceOut","bounceOutDown","bounceOutLeft","bounceOutRight","bounceOutUp","fadeIn","fadeInDown","fadeInDownBig","fadeInLeft","fadeInLeftBig","fadeInRight","fadeInRightBig","fadeInUp","fadeInUpBig","fadeOut","fadeOutDown","fadeOutDownBig","fadeOutLeft","fadeOutLeftBig","fadeOutRight","fadeOutRightBig","fadeOutUp","fadeOutUpBig","flip","flipInX","flipInY","flipOutX","flipOutY","lightSpeedIn","lightSpeedOut","rotateIn","rotateInDownLeft","rotateInDownRight","rotateInUpLeft","rotateInUpRight","rotateOut","rotateOutDownLeft","rotateOutDownRight","rotateOutUpLeft","rotateOutUpRight","slideInUp","slideInDown","slideInLeft","slideInRight","slideOutUp","slideOutDown","slideOutLeft","slideOutRight","zoomIn","zoomInDown","zoomInLeft","zoomInRight","zoomInUp","zoomOut","zoomOutDown","zoomOutLeft","zoomOutRight","zoomOutUp","hinge","rollIn","rollOut");
	foreach ($animation_style_list_item as  $value) {
		$animation_style_list_array[$value]=$value;
	}
	
	/**
	 * Sample metabox to demonstrate each field type included
	 */
    //blog_post metabox start
    $blog_post = new_cmb2_box( array(
        'id'            => $prefix . 'post',
        'title'         => __( 'Blog Post Metabox', 'glimmer-admin' ),
        'object_types'  => array( 'post', ), // Post type
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
    ) );

    $blog_post->add_field( array(
        'name' => __('Embed your audio or video code',"glimmer-admin"),
        'id' => $prefix . 'post_embed_code',
        'type' => 'textarea_code',
        'desc' => __("Paste your embed code, Like: youtube, Vimeo, Soundcloud etc. Note: if your upload featured image it will not show. ","glimmer-admin"), 
    ) );    

    //blog_post metabox end

	//aboutus metabox start
	$aboutus = new_cmb2_box( array(
		'id'            => $prefix . 'aboutus',
		'title'         => __( 'About Us Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'aboutus', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$aboutus->add_field( array(
        'name' => __('Icon Code',"glimmer-admin"),
        'id' => $prefix . 'icon_code',
        'type' => 'text_medium',
        'desc' => __("Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>","glimmer-admin"), 
    ) );	

	$aboutus->add_field( array(
	    'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
	    'show_option_none' => false,
	    'default'          => '',
	    'options'          => $animation_style_list_array,
	) );

	$aboutus->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

	$aboutus->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );
    //aboutus metabox end

    //teammember metabox start
	$teammember = new_cmb2_box( array(
		'id'            => $prefix . 'teammember',
		'title'         => __( 'Team Member Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'teammember', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$teammember->add_field( array(
        'name' => 'Order',
        'id' => $prefix . 'team_order',
        'type' => 'text_medium',
    ) );

	$teammember->add_field( array(
        'name' => __('Photo of the Person ',"glimmer-admin"),
        'id' => $prefix . 'teammember_photo',
        'type' => 'file'
    ) );	

	$teammember->add_field( array(
        'name' => 'Position Description',
        'id' => $prefix . 'position',
        'type' => 'text_medium',
    ) );

    $teammember->add_field( array(
        'name' => __('Social Icon 1',"glimmer-admin"),
        'id' => $prefix . 'social_icon_1',
        'type' => 'text_medium',
        'default'=>'fa-facebook',
        'desc' => __("Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>","glimmer-admin"),
    ) );

    $teammember->add_field( array(
        'name' => __('Social URL 1',"glimmer-admin"),
        'id' => $prefix . 'social_url_1',
        'type' => 'text_medium',
        'default'=>'#'
    ) );

    $teammember->add_field( array(
        'name' => __('Social Icon 2',"glimmer-admin"),
        'id' => $prefix . 'social_icon_2',
        'type' => 'text_medium',
        'default'=>'fa-twitter',
        'desc' => __("Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>","glimmer-admin"),
    ) );

    $teammember->add_field( array(
        'name' => __('Social URL 2',"glimmer-admin"),
        'id' => $prefix . 'social_url_2',
        'type' => 'text_medium',
        'default'=>'#'
    ) );

    $teammember->add_field( array(
        'name' => __('Social Icon 3',"glimmer-admin"),
        'id' => $prefix . 'social_icon_3',
        'type' => 'text_medium',
        'default'=>'fa-google-plus',
        'desc' => __("Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>","glimmer-admin"),
    ) );

    $teammember->add_field( array(
        'name' => __('Social URL 3',"glimmer-admin"),
        'id' => $prefix . 'social_url_3',
        'type' => 'text_medium',
        'default'=>'#'
    ) );

    $teammember->add_field( array(
	    'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
	    'show_option_none' => false,
	    'default'          => '',
	    'options'          => $animation_style_list_array,
	) );

	$teammember->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

	$teammember->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );
    //teammember metabox end

	//funfact metabox start
	$funfact = new_cmb2_box( array(
		'id'            => $prefix . 'funfact',
		'title'         => __( 'Fun fact Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'funfact', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$funfact->add_field( array(
        'name' => 'Order',
        'id' => $prefix . 'funfact_order',
        'type' => 'text_medium',
    ) );

    $funfact->add_field( array(
        'name' => __('Fun fact Icon',"glimmer-admin"),
        'id' => $prefix . 'funfact_icon',
        'type' => 'text_medium',
        'default'=>'fa-file-code-o',
        'desc' => __("Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>","glimmer-admin"), 
    ) );	

    $funfact->add_field( array(
        'name' => __('Fun fact Count Number',"glimmer-admin"),
        'id' => $prefix . 'funfact_count_number',
        'type' => 'text_medium',
        'default'=>'0'
    ) );	

	$funfact->add_field( array(
	    'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
	    'show_option_none' => false,
	    'default'          => '',
	    'options'          => $animation_style_list_array,
	) );

	$funfact->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

	$funfact->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );
    //funfact metabox end

    //skill metabox start
	$skill = new_cmb2_box( array(
		'id'            => $prefix . 'skill',
		'title'         => __( 'Skill Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'skill', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$skill->add_field( array(
        'name' => __('Skill Percent',"glimmer-admin"),
        'desc' => __('Use Like 70%',"glimmer-admin"),
        'id' => $prefix . 'skill_percent',
        'type' => 'text_medium',
    ) );

	$skill->add_field( array(
	    'name' => __('Animation Style',"glimmer-admin"),
	    'desc' => __('This animation for triangle',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
	    'show_option_none' => false,
	    'default'          => '',
	    'options'          => $animation_style_list_array,
	) );

	$skill->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

	$skill->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );
    //skill metabox end

	//portfolio metabox start
	$portfolio = new_cmb2_box( array(
		'id'            => $prefix . 'portfolio',
		'title'         => __( 'Portfolio Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'portfolio', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$portfolio->add_field( array(
	    'name' => __('Project Name',"glimmer-admin"),
	    'desc' => __('This project name will show above thumbnail image',"glimmer-admin"),
	    'id' => $prefix . 'project_name',
	    'type' => 'text_medium',
	) );

	$portfolio->add_field( array(
        'name' => __('Portfolio Thumbnail',"glimmer-admin"),
        'desc' => __('Image size should 279x200',"glimmer-admin"),
        'id' => $prefix . 'portfolio_thumbnail',
        'type' => 'file',
    ) );

    $portfolio->add_field( array(
        'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
        'show_option_none' => false,
        'default'          => '',
        'options'          => $animation_style_list_array,
    ) );

    $portfolio->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

    $portfolio->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );

	$portfolio->add_field( array(
        'name' => __('Add single page images',"glimmer-admin"),
        'desc' => __('Image size should 650x450',"glimmer-admin"),
        'id' => $prefix . 'portfolio_images',
        'type' => 'file_list',
        'preview_size' => array( 100, 100 ),
    ) );

    $portfolio->add_field( array(
        'name' => __('Embed your video code',"glimmer-admin"),
        'id' => $prefix . 'portfolio_video',
        'type' => 'textarea_code',
        'desc' => __("Paste your embed code, Like: youtube, Vimeo etc. Note: if your single page images it will not show. ","glimmer-admin"), 
    ) );   

    $portfolio->add_field( array(
	    'name' => __('Portfolio Description',"glimmer-admin"),
	    'id' => $prefix . 'portfolio_description',
	    'type' => 'textarea',
	) );

    $portfolio->add_field( array(
        'name' => __('Client Name',"glimmer-admin"),
        'id' => $prefix . 'client_name',
        'type' => 'text_medium',
    ) );

    $portfolio->add_field( array(
        'name' => __('Delivery Date',"glimmer-admin"),
        'id' => $prefix . 'delivery_date',
        'type' => 'text_medium',
    ) ); 

    $portfolio->add_field( array(
        'name' => __('Project Skills',"glimmer-admin"),
        'id' => $prefix . 'project_skills',
        'type' => 'text_medium',
    ) );  

    $portfolio->add_field( array(
        'name' => __('Project URL',"glimmer-admin"),
        'id' => $prefix . 'project_url',
        'type' => 'text_medium',
    ) );    
  
    //portfolio metabox end

	//testimonial metabox start
	$testimonial = new_cmb2_box( array(
		'id'            => $prefix . 'testimonial',
		'title'         => __( 'Testimonial Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'testimonial', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$testimonial->add_field( array(
        'name' => __('Photo of the Person ',"glimmer-admin"),
        'id' => $prefix . 'buyer_image',
        'type' => 'file'
    ) );

	$testimonial->add_field( array(
        'name' => __('Name of the Person ',"glimmer-admin"),
        'id' => $prefix . 'buyer_name',
        'type' => 'text_medium'
    ) );

	$testimonial->add_field( array(
        'name' => __('Testimonial ',"glimmer-admin"),
        'id' => $prefix . 'buyer_description',
        'type' => 'textarea'
    ) );

    //testimonial metabox end

    //service metabox start
	$service = new_cmb2_box( array(
		'id'            => $prefix . 'service',
		'title'         => __( 'Service Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'service', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$service->add_field( array(
        'name' => __('Icon Code',"glimmer-admin"),
        'id' => $prefix . 'icon_code',
        'type' => 'text_medium',
        'desc' => __("Font Awesome Icon Class, ex: fa-search. Get the full list <a href='http://fortawesome.github.io/Font-Awesome/cheatsheet/'>Here</a>","glimmer-admin"), 
    ) );	

	$service->add_field( array(
	    'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
	    'show_option_none' => false,
	    'default'          => '',
	    'options'          => $animation_style_list_array,
	) );

	$service->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

	$service->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );
    //service metabox end

    //pricingtable metabox start
	$pricingtable = new_cmb2_box( array(
		'id'            => $prefix . 'pricingtable',
		'title'         => __( 'Pricing Table Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'pricingtable', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$pricingtable->add_field( array(
        'name' => __('Featured',"glimmer-admin"),
        'id' => $prefix . 'pricing_featured',
        'type' => 'checkbox',
    ) );	

    $pricingtable->add_field( array(
        'name' => __('Order',"glimmer-admin"),
        'id' => $prefix . 'pricing_order',
        'type' => 'text_medium',
    ) );

    $pricingtable->add_field( array(
        'name' => __('Sub Title',"glimmer-admin"),
        'id' => $prefix . 'pricing_subtitle',
        'type' => 'text_medium',
    ) );

    $pricingtable->add_field( array(
        'name' => __('Currency Symbols',"glimmer-admin"),
        'id' => $prefix . 'pricing_currency_symbols',
        'type' => 'text_medium',
        'default' => '$',
    ) );	

	$pricingtable->add_field( array(
        'name' => __('Price',"glimmer-admin"),
        'id' => $prefix . 'pricing_price',
        'type' => 'text_medium',
    ) );	

    $pricingtable->add_field( array(
        'name' => __('Duration',"glimmer-admin"),
        'id' => $prefix . 'pricing_duration',
        'type' => 'text_medium',
        'default' => 'Per Month',
    ) );

    $pricingtable->add_field( array(
        'name' => __('Table Elements (One in each line)',"glimmer-admin"),
        'id' => $prefix . 'pricing_elements',
        'type' => 'textarea',
    ) );	

    $pricingtable->add_field( array(
        'name' => __('Button Text',"glimmer-admin"),
        'id' => $prefix . 'pricing_button',
        'type' => 'text_medium',
        'default'=>"Get Now"
    ) );	

    $pricingtable->add_field( array(
        'name' => __('Button Link',"glimmer-admin"),
        'id' => $prefix . 'pricing_button_link',
        'type' => 'text_medium',
        'default'=>"#"
    ) );	

	$pricingtable->add_field( array(
	    'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
	    'show_option_none' => false,
	    'default'          => '',
	    'options'          => $animation_style_list_array,
	) );

	$pricingtable->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

	$pricingtable->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );
    //pricingtable metabox end

    //happyclient metabox start
	$happyclient = new_cmb2_box( array(
		'id'            => $prefix . 'happyclient',
		'title'         => __( 'Happy Client Metabox', 'glimmer-admin' ),
		'object_types'  => array( 'happyclient', ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
	) );

	$happyclient->add_field( array(
        'name' => __('Happy Client Logo',"glimmer-admin"),
        'id' => $prefix . 'happy_client_logo',
        'type' => 'file'
    ) );	

	$happyclient->add_field( array(
        'name' => __('Happy Client URL',"glimmer-admin"),
        'id' => $prefix . 'happy_client_url',
        'type' => 'text_medium',
        'default' => '#'
    ) );

    $happyclient->add_field( array(
        'name' => __('Animation Style',"glimmer-admin"),
        'id' => $prefix . 'object_animation',
        'type' => 'select',
        'show_option_none' => false,
        'default'          => '',
        'options'          => $animation_style_list_array,
    ) );

    $happyclient->add_field( array(
        'name' => __('Animation Duration',"glimmer-admin"),
        'desc' => __('To give animation duration use like: 2s (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_duration',
        'type' => 'text_medium',
    ) );

    $happyclient->add_field( array(
        'name' => __('Animation Delay',"glimmer-admin"),
        'desc' => __('How much delay animation will start (s for second)',"glimmer-admin"),                
        'id' => $prefix . 'animation_delay',
        'type' => 'text_medium',
        'default'=>'0.5s'
    ) );

    //happyclient metabox end

}

/*override cbm2 rakib*/
function cmb_opt_groups( $args, $defaults, $field_object, $field_types_object ) {
	
	// Only do this for the field we want (vs all select fields)
	if ( '_cmb_option_field' != $field_types_object->_id() ) {
		return $args;
	}

	$option_array = array(
		'Group 1' => array(
			'group1-value1' => 'Value 1',
			'group1-value2' => 'Value 2',
			'group1-value3' => 'Value 3',
		),
		'Group 2' => array(
			'group2-value1' => 'Value 1',
			'group2-value2' => 'Value 2',
			'group2-value3' => 'Value 3',
		),
		'Group 3' => array(
			'group3-value1' => 'Value 1',
			'group3-value2' => 'Value 2',
			'group3-value3' => 'Value 3',
		),
	);

	$saved_value = $field_object->escaped_value();
	$value       = $saved_value ? $saved_value : $field_object->args( 'default' );

	$options_string = '';
	$options_string .= $field_types_object->option( __( 'Select an Option', 'glimmer' ), '', ! $value );

	foreach ( $option_array as $group_label => $group ) {

		$options_string .= '<optgroup label="'. $group_label .'">';

		foreach ( $group as $key => $label ) {
			$options_string .= $field_types_object->option( $label, $key, $value == $key );
		}
		$options_string .= '</optgroup>';
	}

	// Ok, replace the options value
	$defaults['options'] = $options_string;

	return $defaults;
}
add_filter( 'cmb2_select_attributes', 'cmb_opt_groups', 10, 4 );