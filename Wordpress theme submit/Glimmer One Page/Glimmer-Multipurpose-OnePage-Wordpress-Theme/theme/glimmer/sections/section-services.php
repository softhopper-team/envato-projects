<?php
    global $glimmer, $post;
    $services = glimmer_get_custom_posts("service", 4);
    $classes = array(0 => "", "1" => "col-md-12 col-sm-6", "2" => "col-md-6 col-sm-6", "3" => "col-md-4 col-sm-6", "4" => "col-md-3 col-sm-6");
    $class = $classes[count($services)];

    $services_second_row = glimmer_get_custom_posts_with_offset("service", 4, 4);
    $second_row_classes = array(0 => "", "1" => "col-md-12 col-sm-6", "2" => "col-md-6 col-sm-6", "3" => "col-md-4 col-sm-6", "4" => "col-md-3 col-sm-6");
    $second_row_class = $second_row_classes[count($services_second_row)];
?>       

<!-- service
================================================== --> 
<section id="our-service" class="our-service">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <header class="heading wow <?php if (isset($glimmer['section_services_animation_style'])) echo $glimmer['section_services_animation_style']; ?>" data-wow-duration="<?php if (isset($glimmer['section_services_animation_duration'])) echo $glimmer['section_services_animation_duration']; ?>" data-wow-delay="<?php if (isset($glimmer['section_services_animation_delay'])) echo $glimmer['section_services_animation_delay']; ?>">                       
                   <h2 class="title">
                       <?php if (isset($glimmer['section_services_title'])) echo $glimmer['section_services_title'];
                            ?>
                    </h2>
                        <span class="small-border"></span>

                    <h5 class="subtitle"><?php if (isset($glimmer['section_services_subtitle'])) echo $glimmer['section_services_subtitle'];
                            ?></h5>
                </header> <!-- /.heading -->
            </div><!--/. col-lg-12--> 
        </div> <!-- /.row -->
    
    

        <div class="base-padding-sm">
            <div class="row">
                <?php
                    foreach ($services as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?>
                <div class="<?php echo $class; ?>">
                    <div class="service-content text-center wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                        <div class="service-icon">
                            <i class="fa <?php if(isset($meta["_glimmer_icon_code"][0])) echo $meta["_glimmer_icon_code"][0]; ?>"></i>                                    
                        </div><!-- /.service-icon -->
                        <h4><?php the_title(); ?></h4>
                            <?php the_content(); ?>
                    </div> 
                </div> <!-- /.col-md-3 -->   
                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </div> <!--/. row-->
            
            <div class="row">
                <?php
                    foreach ($services_second_row as $post) {
                    setup_postdata($post);
                    $meta = get_post_meta($post->ID);
                ?>
                <div class="<?php echo $second_row_class; ?>">
                    <div class="service-content margin-top text-center wow <?php if(isset($meta["_glimmer_object_animation"][0])) echo $meta["_glimmer_object_animation"][0]; ?>" data-wow-duration="<?php if(isset($meta["_glimmer_animation_duration"][0])) echo $meta["_glimmer_animation_duration"][0]; ?>" data-wow-delay="<?php if(isset($meta["_glimmer_animation_delay"][0])) echo $meta["_glimmer_animation_delay"][0]; ?>">
                        <div class="service-icon">
                            <i class="fa <?php if(isset($meta["_glimmer_icon_code"][0])) echo $meta["_glimmer_icon_code"][0]; ?>"></i>                                    
                        </div><!-- /.service-icon -->
                        <h4><?php the_title(); ?></h4>
                            <?php the_content(); ?>
                    </div> 
                </div> <!-- /.col-md-3 -->   
                <?php } ?>
                <?php wp_reset_postdata(); ?>
            </div> <!--/. row-->
        </div><!-- /.base-padding -->

    </div> <!--/.container-->
   
</section> <!--/.our-service-->
