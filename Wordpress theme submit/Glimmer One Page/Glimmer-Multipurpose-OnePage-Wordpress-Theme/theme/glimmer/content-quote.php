<?php
/**
 * The template for displaying quote post formats
 *
 * Used for both single and index/archive/search.
 *
 * @package Glimmer
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="blog-content" > 	
        <div class="blog-details clearfix">
			<?php the_content(); ?>	
        </div> <!-- /.blog-details-->
    </div><!-- /.blog-content -->
</article> 