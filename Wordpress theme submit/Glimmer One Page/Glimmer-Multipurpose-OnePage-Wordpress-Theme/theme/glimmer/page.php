<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package Glimmer
 */
get_header(); ?>
<?php
    global $glimmer, $post;
?>
<?php get_template_part("blog","header");?>

        <!-- blog list
        ================================================== -->
        <div class="blog-list">
            <div class="container">

                <div class="row base-padding-blog">
                    <div class="col-md-8">
                            <?php
                                if ( have_posts() ) :
                                    while ( have_posts() ) : the_post();                                
                            ?>
                            <?php
                                /* Include the Post-Format-specific template for the content.
                                 * If you want to override this in a child theme, then include a file
                                 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                                 */
                                get_template_part( 'content', 'page' );
                            ?>
                            <?php endwhile; ?>
                            
                            <?php else : ?>

                                <?php get_template_part( 'content', 'none' ); ?>

                            <?php endif; ?>
                           

                            <?php
                                // If comments are open or we have at least one comment, load up the comment template
                                if ( comments_open() || get_comments_number()) :
                                 comments_template();
                                endif;
                            ?>
         
                    </div> <!-- /.col-md-8 -->

                    <?php get_sidebar(); ?>                    

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div> <!-- /.blog-list -->
    
<?php get_template_part("blog", "footer"); ?>
<?php get_footer(); ?>