<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package Candid
 */
?>
<div class="col-md-4">
    <!-- sidebar -->
    <div id="sidebar">
    	<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>

            <?php the_widget('WP_Widget_Search', '', 'before_title=<div class="widget-title-area"><h2 class="widget-title">&after_title=</h2></div>'); ?>

            <?php the_widget('WP_Widget_Categories', 'title=Categories&count=1', 'before_title=<div class="widget-title-area"><h2 class="widget-title">&after_title=</h2></div>'); ?>

            <?php the_widget('WP_Widget_Archives', 'title=Archives&count=1&type=monthly', 'before_title=<div class="widget-title-area"><h2 class="widget-title">&after_title=</h2></div>'); ?>

            <?php the_widget('WP_Widget_Tag_Cloud', 'title=Tags', 'before_title=<div class="widget-title-area"><h2 class="widget-title">&after_title=</h2></div>'); ?>  

    	<?php endif; // end sidebar widget area ?>
    </div> <!-- /#sidebar -->   
</div> <!-- /.col-md-4 -->  